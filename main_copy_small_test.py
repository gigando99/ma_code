import a2c_rework as a2c
import common.data_downloading as dd
import common.data_preprocessing as dp
import common.env as envi

import argparse
import tensorflow as tf
import numpy as np


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--a', type = int, dest = "nAgents", default = 5)
    parser.add_argument('--aa', type = int, dest = "all_actions", default = 0)
    parser.add_argument('--b', type = int, dest = "nBoroughs", default = 63)
    #parser.add_argument('--l', type = int, dest = "rq_limit", default = 20)
    parser.add_argument('--lr', type = float, dest = "lr_a", default = 0.0001)
    parser.add_argument('--r', type = float, dest = "reward_param", default = 0.5)
    parser.add_argument('--s', type = str, dest = "setting", default = "exemplary")
    args = parser.parse_args()
    print(args)

    seed = 1
    verbosity = 0

    tf.random.set_seed(seed)
    np.random.seed(seed)

    downloader = dd.dataDownloader()

    #downloader.download_taxi_data()

    data = dp.inputData(downloader, verbose = verbosity)

    data.check_or_create_pickles()

    env = envi.AmodEnv(data = data, nAgents = args.nAgents, nBoroughs = args.nBoroughs,
                       reward_param = args.reward_param,
                       seed = seed, verbose = verbosity)

    trainer = a2c.trainer = a2c.unifiedTrainer(env = env, lr_a = args.lr_a, setting = args.setting, all_actions = args.all_actions)

    print("Created all necessary objects: \n - {} \n - {} \n - {} \n - {}".format(downloader,data,env,trainer))

    #nEpisodes_per_mode = {"train": 3, "validate": 1, "test": 1}
    test_len = len(env.dict_of_list_of_data_days["test"])
    nEpisodes_per_mode = {"train": 1, "validate": 1, "test": 0}

    trainer.run_final(nEpochs = 1, nEpisodes_per_mode = nEpisodes_per_mode,  nSteps = 100)

main()
