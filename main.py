import a2c_rework as a2c
import common.data_downloading as dd
import common.data_preprocessing as dp
import common.env as envi

import argparse
import tensorflow as tf
import numpy as np
import cProfile, pstats


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--na', type = int, dest = "nAgents", default = 2)
    parser.add_argument('--aa', type = int, dest = "all_actions", default = 0)
    parser.add_argument('--nb', type = int, dest = "nBoroughs", default = 5)
    parser.add_argument('--lr', type = float, dest = "lr_a", default = 0.00001)
    parser.add_argument('--r', type = float, dest = "reward_param", default = 0.5)
    parser.add_argument('--nr', type = int, dest = "random_episodes", default = 0)
    parser.add_argument('--s', type = str, dest = "setting", default = "exemplary")
    parser.add_argument('--t', type = str, dest = "run_type", default = "deploy")
    parser.add_argument('--e', type = float, dest = "entropy", default = 0.05)
    parser.add_argument('--se', type = int, dest = "seed", default = 1)

    args = parser.parse_args()
    print(args)

    seed = args.seed
    verbosity = 0

    tf.random.set_seed(seed)
    np.random.seed(seed)

    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))

    downloader = dd.dataDownloader()

    #downloader.download_taxi_data()

    data = dp.inputData(downloader, verbose = verbosity)

    data.check_or_create_pickles()

    env = envi.AmodEnv(data = data, nAgents = args.nAgents, nBoroughs = args.nBoroughs,
                       reward_param = args.reward_param,
                       seed = seed, verbose = verbosity)

    trainer = a2c.trainer = a2c.unifiedTrainer(env = env, lr_a = args.lr_a, setting = args.setting, all_actions = args.all_actions,
                                                n_random_start_episodes = args.random_episodes, entropy = args.entropy)

    print("Created all necessary objects: \n - {} \n - {} \n - {} \n - {}".format(downloader,data,env,trainer))

    if args.run_type == "deploy":
        test_len = len(env.dict_of_list_of_data_days["test"])
        nEpisodes_per_mode = {"train": 15, "validate": 5, "test": test_len}
        nSteps = 86400
        nEpochs = 30
    elif args.run_type == "mini_deploy":
        nEpisodes_per_mode = {"train": 15, "validate": 2, "test": 1}
        nSteps = 86400
        nEpochs = 2
    elif args.run_type == "test":
        nEpisodes_per_mode = {"train": 1, "validate": 1, "test": 1}
        nSteps = 40000
        nEpochs = 1
    elif args.run_type == "mini_test":
        nEpisodes_per_mode = {"train": 1, "validate": 0, "test": 0}
        nSteps = 1000
        nEpochs = 1
    elif args.run_type == "ep_test":
        nEpisodes_per_mode = {"train": 5, "validate": 1, "test": 0}
        nSteps = 10000
        nEpochs = 1
    else:
        print("Non-valid run_type given")


    #profiler = cProfile.Profile()
    #profiler.enable()
    #tf.profiler.experimental.start('results/tb')
    trainer.run_final(nEpochs = nEpochs, nEpisodes_per_mode = nEpisodes_per_mode,  nSteps = nSteps, save_models = True)
    #tf.profiler.experimental.stop()
    #profiler.disable()
    #stats = pstats.Stats(profiler).sort_stats('cumtime')
    #print_cprofile_stats(stats)


def print_cprofile_stats(stats):
    stats.print_stats("a2c_rework.py")
    stats.print_stats("env.py")
    stats.print_stats("learn")
    stats.print_stats("action")
    stats.print_stats("norm")
    stats.print_stats("logger")
    stats.print_stats("tf__")

main()
