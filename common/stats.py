# %% imports

import common.data_preprocessing as dp
import common.data_downloading as dd
import common.utils as utils

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import os
import re
import pickle
from calendar import monthrange
import pandas as pd
import datetime
import time
import geopandas as gpd
import bokeh as bk
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, LinearColorMapper, HoverTool, ColorBar
from bokeh.palettes import Cividis256 as palette
from bokeh.io import output_notebook, show
from shapely.geometry import Polygon, MultiPolygon


# %% Class with all functions (actual analyses below)

class DataAnalyzer():
    def __init__(self, downloader, preprocessor):
        self.downloader = downloader
        self.preprocessor = preprocessor
        self.cutoffs = [5,8,13,19,26,38,63]
        self.cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]

    ############################## RQ_LIMIT ##############################
    # analyze how many requests come within certain window
    # idea:
    #   - have one large container which counts the number of requests that came in in this second
    #   - then bin this container in larger chunks

    def estimate_rq_limit(self, window_size, filters = []):

        ### TODO ####
        ### include array description in subplot next to histogram ###
        ### mean, median, min, max, std, variance ###

        c, flattened = self.create_container(filters)
        self.describe_arr(flattened)
        # Length: 63072000
        # Sum: 230086174.0
        # Max: 39.0
        # Min: 0.0
        # Mean: 3.647992357940132
        # Median: 3.0

        binned = self.rolling_window_over_flat_c(flattened, window_size)
        self.describe_arr(binned)
        # Length: 6307200
        # Sum: 230086174.0
        # Max: 157.0
        # Min: 0.0
        # Mean: 36.479923579401316
        # Median: 31.0

        std = np.std(binned)
        print("std: {}, var: {}".format(std, std ** 2))

        upper_limit = round((binned.max()+10) / 10) * 10
        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.hist(binned, range(0, upper_limit))
        plt.savefig("figures/" + now + "_binned_" + str(window_size) + "_rq_limit.jpg")
        plt.show()

        labels = ["Lenght", "Sum", "Max", "Min", "Mean", "Median", "Std", "Variance"]
        temp = [len(binned), sum(binned), binned.max(), binned.min(), binned.mean(), np.median(binned), std, std ** 2]
        data_to_graph = pd.DataFrame(labels, temp)
        data_to_graph.to_csv("figures/" + now + "_binned_" + str(window_size) + "_rq_limit.csv")


    def create_container(self, filters):
        counter = 0
        container = np.zeros([730, 86400])

        for year in self.downloader.years:
            for month in self.downloader.months:
                for day in range(monthrange(year,month)[1]):
                    str_month = str(month).zfill(2)
                    str_day = str(day+1).zfill(2)
                    path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)
                    df = pd.read_pickle(path)
                    print("ESTIMATE_RQ_LIMIT: ", year, month, day+1, counter)
                    if not filters:
                        for elem in filters:
                             df.query(elem, inplace = True)
                    container = self.container_helper(df,container,counter)
                    counter += 1
        return container, container.flatten()


    def container_helper(self, df, container, counter):
        for row in df.itertuples():
            try:
                container[counter, row[4]] += 1
            except:
                print(row)
        return container

    def rolling_window_over_flat_c(self, flat_c, window_size):
        new_len = int(len(flat_c) / window_size)
        binned = np.zeros(new_len)
        for i in range(new_len):
            binned[i] += sum(flat_c[i*window_size:i*window_size+window_size])

        return binned


    def describe_arr(self, arr):
        print("# Length: {} \n# Sum: {} \n# Max: {} \n# Min: {} \n# Mean: {} \n# Median: {}".format(len(arr), sum(arr), arr.max(), arr.min(), arr.mean(), np.median(arr)))


    ############################## Data of Days ##############################
    # analyze how many trips there are per day

    def get_trips_per_day(self):

        container = list()
        for year in self.downloader.years:
            for month in self.downloader.months:
                for day in range(monthrange(year,month)[1]):
                    str_month = str(month).zfill(2)
                    str_day = str(day+1).zfill(2)
                    path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)
                    df = pd.read_pickle(path)
                    print("GET_TRIPS_PER_DAY: ", year, month, day+1)
                    container.append(len(df))

        c_arr = np.array(container)
        self.describe_arr(c_arr)
        #print(c_arr[75:125])
        #plt.figure(figsize=(40,20))
        plt.bar(range(730), c_arr, linewidth = 0.0, width = 1.0)
        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_trips_per_day.jpg")
        plt.show()

        data_to_graph = pd.DataFrame(range(730), container)
        data_to_graph.to_csv("figures/" + now + "_trips_per_day.csv")


    ############################## Data Reduction ##############################
    # analyze how much the data was reduced
    # this could be displayed in a waterfall graph in the thesis

    def get_number_of_trips_on_working_days():
        full = utils.create_full_list_of_data_days()
        reduced = utils.filter_out_working_days(full)
        results = 0
        for elem in reduced:
            month = elem.month
            year = elem.year
            day = elem.day
            str_month = str(month).zfill(2)
            str_day = str(day).zfill(2)
            path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)
            df = pd.read_pickle(path)
            results += len(df)

        return results # 215342171

    def plot_data_reduction(self, dict):

        keys = list()
        values = [[],[],[],[]]
        names = ["Yellow","Green","FHV","HVFHV"]
        colors = ["yellow", "green", "blue", "red"]
        # keys = ['total_length', 'only_manhattan', 'wo_in-zone', 'cleaned']
        # values = list of values for these four keys for all names

        for key, value in dict.items():
            keys.append(key)
            for count, elem in enumerate(value):
                values[count].append(elem)

        labels = keys
        #yellow = values[0]
        #green = values[1]
        #fhv = values[2]
        #hvfhv = values[3]

        x = np.arange(len(labels))  # the label locations
        width = 0.15  # the width of the bars

        fig, ax = plt.subplots()

        rects = list()
        for count, name in enumerate(names):
            rects.append(ax.bar(x - (3/2 - count) * width, values[count], width, label=name,color = colors[count]))
            #plt.bar(x - (3/2 + count) * width, values[count], width, label=name,color = colors[count])

        #rects1 = ax.bar(x - 3/2*width, yellow, width, label='Yellow',color = "yellow")
        #rects2 = ax.bar(x - width/2, green, width, label='Green', color = "green")
        #rects3 = ax.bar(x + width/2, fhv, width, label='FHV', color = "blue")
        #rects4 = ax.bar(x + 3/2*width, hvfhv, width, label='HVFHV', color = "red")

        #plt.bar(x - 3/2*width, yellow, width, label='Yellow',color = "yellow")
        #plt.bar(x - width/2, green, width, label='Green', color = "green")
        #plt.bar(x + width/2, fhv, width, label='FHV', color = "blue")
        #plt.bar(x + 3/2*width, hvfhv, width, label='HVFHV', color = "red")

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Recorded Trips')
        ax.set_title('Reducing Data over various Stages')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()


        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom')


        #autolabel(rects1)
        #autolabel(rects2)
        #autolabel(rects3)
        #autolabel(rects4)

        fig.tight_layout()

        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_data_reduction.jpg")
        plt.show()

        print("PLOT_DATA_REDUCTION: ", values, keys)
        data_to_graph = pd.DataFrame(values, keys)
        data_to_graph.to_csv("figures/" + now + "_data_reduction_data.csv")


    def plot_data_reduction_cumulated(dict):
        sums = []
        for elem in dict.keys():
            sums.append(sum(dict[elem]))

        working_days = self.get_number_of_trips_on_working_days()
        sums.append(working_days)

    def plot_data_reduction2(self):
        sums = [739534696, 317417841, 302548331, 301867899, 215342171]
        labels = ["Total dataset", "Manhattan Island\n only", "Without intra-\nzone trips", "Cleaned", "Working days\n only"]
        font = {'fontname':'Arial'}
        fig, ax = plt.subplots(figsize = (20,10))
        ax.bar(range(len(sums)), sums)
        print(sums, labels)
        ax.set_xticks(range(len(sums)))
        ax.set_xticklabels(labels,**font)
        ax.set_ylabel("Total number of trips",**font)
        matplotlib.rcParams.update({"font.family": "Arial", 'font.size': 20})
        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_data_reduction.jpg")



    ############################## Hourly Cutoff ##############################
    # analyze when trips are started

    def get_hourly_cutoff(self):
        hours = range(0,24)
        pu = np.empty(24)
        #pu = np.array([])#[self.nTrips])

        #counter = 0
        for year in self.downloader.years:
            #year = np.array([])
            for month in self.downloader.months:
                #month = np.array([])
                for day in range(monthrange(year,month)[1]):
                    str_month = str(month).zfill(2)
                    str_day = str(day+1).zfill(2)
                    path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)

                    df = pd.read_pickle(path)
                    for hour in hours:
                        temp = df.query('PU_dt.dt.hour == @hour')
                        pu[hour] += len(temp)
                    #for elem in df.PU_dt.dt.hour:
                        #pu[elem] += 1

                    #temp = np.array(df.PU_dt.dt.hour)
                    #month = np.concatenate((month, temp))

                    #counter += 1
                    #break
                #year = np.concatenate((year, month))
                print("GET_HOURLY_CUTOFF: added %s/%s" %(year, str_month))
                #break
            #break
            #pu = np.concatenate((pu, year))

        return pu

    def plot_hourly_cutoff(self, pu):
        hours = range(24)
        font = {'fontname':'Arial'}
        fig, ax = plt.subplots(figsize = (20,10))
        ax.bar(hours, pu, align = "edge")
        ax.set_ylabel('Number of recorded trips', **font)
        ax.set_xlabel('Hour of the day',**font)

        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_hourly_cutoff.jpg")
        plt.show()



        ####################
    # runs approx. 5 min
    def get_nominal_trips_per_zone(self):

        trips_per_zone_PU = np.zeros((len(self.cutoffs),63))
        trips_per_zone_DO = np.zeros((len(self.cutoffs),63))
        for year in self.downloader.years:
                for month in self.downloader.months:
                    for day in range(monthrange(year,month)[1]):
                        str_month = str(month).zfill(2)
                        str_day = str(day+1).zfill(2)
                        path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)
                        df = pd.read_pickle(path)
                        print("trips per zone: ", year, month, day+1)

                        for i, c in enumerate(self.cutoffs):
                            if c == 63:
                                dict1 = dict(list(df.groupby(by = "PU_id")))
                                for count, elem in enumerate(self.preprocessor.boroughs):
                                    trips_per_zone_PU[len(self.cutoffs)-1][count] += len(dict1[elem])
                                dict1 = dict(list(df.groupby(by = "DO_id")))
                                for count, elem in enumerate(self.preprocessor.boroughs):
                                    trips_per_zone_DO[len(self.cutoffs)-1][count] += len(dict1[elem])
                            else:
                                zones = self.cluster_south[:c]
                                #print(zones)
                                temp = df.query('DO_id in @zones and PU_id in @zones')
                                dict1 = dict(list(temp.groupby(by = "PU_id")))
                                for count, elem in enumerate(zones):
                                    trips_per_zone_PU[i][count] += len(dict1[elem])
                                dict1 = dict(list(df.groupby(by = "DO_id")))
                                for count, elem in enumerate(zones):
                                    trips_per_zone_DO[i][count] += len(dict1[elem])

                        '''#print(preprocessor.boroughs)
                        dict1 = dict(list(df.groupby(by = "PU_id")))
                        for count, elem in enumerate(self.preprocessor.boroughs):
                            trips_per_zone_PU[count] += len(dict1[elem])
                        dict1 = dict(list(df.groupby(by = "DO_id")))
                        for count, elem in enumerate(self.preprocessor.boroughs):
                            trips_per_zone_DO[count] += len(dict1[elem])
                        #print(trips_per_zone_PU, trips_per_zone_DO)'''
        '''
        pu = np.copy(trips_per_zone_PU)
        do = np.copy(trips_per_zone_DO)
        mean = np.mean(pu)

        cluster_south = [12,88,87, 261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4]
        cutoff = 19 # good values: 5, 8, 13, 19
        cs = [idx(x) for i, x in enumerate(cluster_south) if i < cutoff]


        cs_pu = [pu[idx] for idx in cs]
        cs_do = [do[idx] for idx in cs]
        print(cluster_south)
        print(cs)
        print(sum(cs_pu)/np.sum(pu), sum(cs_do)/np.sum(pu))
        '''

        return trips_per_zone_PU, trips_per_zone_DO

    def get_trips_per_cluster(self):
        tic = time.time()
        cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]
        cutoffs = [5,8,13,19,26,38]
        trips_per_cluster = np.zeros((len(cutoffs)+1,730))
        counter = 0
        for year in self.downloader.years:
            for month in self.downloader.months:
                for day in range(monthrange(year,month)[1]):
                    str_month = str(month).zfill(2)
                    str_day = str(day+1).zfill(2)
                    path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)
                    df = pd.read_pickle(path)
                    #print("trips per cluster: ", year, month, day+1)
                    trips_per_cluster[len(cutoffs)][counter] += len(df)
                    for i, c in enumerate(cutoffs):
                        zones = cluster_south[:c]
                        #print(zones)
                        temp = df.query('DO_id in @zones and PU_id in @zones')
                        trips_per_cluster[i][counter] += len(temp)
                    counter += 1
                print("running trips per cluster: ", year, month)
        print("returning trips per cluster of sizes 5, 8, 13, 19, 26, 38 and 63 after {}s".format(time.time()-tic))

        return trips_per_cluster

    def get_trips_per_cluster_and_hour(self):
        '''
        runs approx. 9 minutes and returns an array of:
            - x: clusters (5, 8, 13, 19, 26, 38, 63)
            - y: hours: 0-23
            - z: days: 0-729
        '''
        tic = time.time()
        cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]
        cutoffs = [5,8,13,19,26,38]
        hours = range(24)
        trips_per_cluster = np.zeros((len(cutoffs)+1,len(hours),730))
        counter = 0
        for year in self.downloader.years:
            for month in self.downloader.months:
                for day in range(monthrange(year,month)[1]):
                    str_month = str(month).zfill(2)
                    str_day = str(day+1).zfill(2)
                    path = "data/taxi_data/%s/%s/%s.pkl" %(year, str_month, str_day)
                    df = pd.read_pickle(path)
                    print("trips per cluster: ", year, month, day+1, time.time()-tic)
                    for hour in hours:
                        temp = df.query('delta > @hour*3600 and delta < 3600*(@hour+1)')
                        trips_per_cluster[len(cutoffs)][(hour+4)%24][counter] += len(temp)
                        for i, c in enumerate(cutoffs):
                            zones = cluster_south[:c]
                            #print(zones)
                            temp1 = temp.query('DO_id in @zones and PU_id in @zones')
                            trips_per_cluster[i][(hour+4)%24][counter] += len(temp1)
                    counter += 1
                #print("running trips per cluster: ", year, month)
        print("returning trips per cluster of sizes 5, 8, 13, 19, 26, 38 and 63 after {}s".format(time.time()-tic))

        return trips_per_cluster

    def postprocess_trips_per_cluster_and_hour(self, results):
        """
        - takes raw input from get_trips_per_cluster_and_hour()
        - sums over days, filters out maximum hour per cluster
        - returns values similar to get_trips_per_cluster(), but extrapolated from maximum hour
        - extra: how to interpret these values

        """
        sum_over_days = np.sum(results, axis = 2)
        #print(sum_over_days.shape) #(7, 24)
        cutoffs = [5,8,13,19,26,38,63]
        max_values = list()
        mean_values = list()
        factors = list()
        for i in range(7):
            #print("max per hour: ", cutoffs[i], np.max(sum_over_days[i]/730), np.argmax(sum_over_days[i]))
            #print("avg per hour: ", cutoffs[i], np.mean(sum_over_days[i])/730)
            #print("max per day:  ", cutoffs[i], np.max(sum_over_days[i]/730)*24)
            #print("avg per day:  ", cutoffs[i], np.mean(sum_over_days[i])/730*24)
            #print("factor: ", cutoffs[i], np.max(sum_over_days[i]/730) / np.mean(sum_over_days[i]/730))
            #print(np.sum(sum_over_days, axis = 1)[0]/730/24)
            max_values.append(round(np.max(sum_over_days[i]/730)*24, 2))
            mean_values.append(round(np.mean(sum_over_days[i]/730)*24, 2))
            factors.append(round(np.max(sum_over_days[i]/730) / np.mean(sum_over_days[i]/730),2))
        print(max_values)
        print(mean_values)
        print(factors)

    def analyze_trips_per_cluster(self, trips_per_cluster):
        clusters = [5,8,13,19,26,38,63]
        # percentage reduction
        for id,val in enumerate(clusters):
            a = trips_per_cluster[id]
            print("cutoff value: ", val)
            print("Description of data: \nMin: {} ||| Max: {} ||| Mean: {} ||| Median: {}".format(np.amin(a),np.amax(a),np.mean(a),np.median(a)))
            print("reduced data to {}% of original data\n".format(round(np.sum(a)/np.sum(trips_per_cluster[len(clusters)-1])*100,3)))

    def get_scale_ratio_regarding_nBoroughs(self):
        '''
        - runs approx. 2.5 min
        - calls get_trips_per_cluster
        '''

        results = self.get_trips_per_cluster()
        per_day = np.sum(results, axis = -1) #contains the number of total trips given the cluster size

        all_zone_stats = [] #contains the number of total trips at each stage of preprocessing
        for key,val in preprocessor.stats_dict.items():
            all_zone_stats.append(sum(val))

        scale_ratio = list()
        for elem in per_day:
            temp = round(elem/all_zone_stats[3]*100,2)
            scale_ratio.append(temp)
            print(temp)

        return scale_ratio

    def get_patience(self):
        # returns: {5: 391.0, 8: 516.5, 13: 496.0, 19: 520.5, 26: 630.0, 38: 620.0, 63: 708.5}
        # will be rounded to next 50
        # used in env: {5: 400, 8: 550, 13: 550, 19: 550, 26: 700, 38: 700, 63: 800}
        results = dict()
        for elem in self.cutoffs:
            zones = self.cluster_south[:elem]
            if elem == 63:
                zones = self.preprocessor.boroughs
            filtered = self.preprocessor.list_of_dfs[1].query('origin_ID in @zones and destination_ID in @zones')
            print("mean {}, max {}, 95 quantile {}".format(round(filtered["travel_time"].mean()*10,1), filtered["travel_time"].max()*10, round(filtered["travel_time"].quantile(0.95)*10,1)))
            results[elem] = round(filtered["travel_time"].quantile(0.95)*10,1)
        return results


    ############### how many cars do we need in smaller setting? ############

    def calc_trips_per_car_in_original_data(self):
        init = self.preprocessor.stats_dict["total_length"]
        cars = [13587,2895,78620,23043]

        trips_total = list()

        for idx in range(4):
            trips_total.append(init[idx] / cars[idx] / 730)

        print("trips per car and type at total_length: ", trips_total)

        return trips_total

    def calc_adapted_fleet_size(self, trips_total):

        end = self.preprocessor.stats_dict["cleaned"]
        cars_cleaned = list()
        trips_cleaned = list()

        for idx in range(4):
            #cars_cleaned.append(end[idx] / trips_total[idx] / 730)
            cars_cleaned.append(end[idx] / 20 / 730)
            trips_cleaned.append(end[idx] / cars_cleaned[idx] / 730)

        #assert round(trips_cleaned[0],1) == round(trips_total[0],1), "trips per car and type of input and output dont seem to match"

        print("cars per type at cleaned: ", cars_cleaned)
        print("total number of cars needed: ", int(sum(cars_cleaned)))
        #print("trips per car and type at cleaned: ", trips_cleaned)

    def calc_avg_trip_duration(self, env, sample_size):
        '''
        stores the number of trips and the average trip duration for each of the possible cluster sizes for sample_size samples of the working days in the data set
        returns the average trip duration
        '''

        tic = time.time()
        prev = tic
        cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]
        cutoffs = [5,8,13,19,26,38]
        # create array of dimensions: (nSamples, nAreas, nValuesToBeStored)
        average_per_day = np.zeros((sample_size,len(cutoffs)+1))
        trips_per_day = np.zeros((sample_size, len(cutoffs)+1))
        flod = utils.filter_out_working_days(utils.create_full_list_of_data_days(), env.first_day, env.last_day)
        if sample_size == 502:
            samples = flod
        else:
            samples = np.random.choice(flod,sample_size)

        for s_i, s in enumerate(samples):
            str_month = str(s.month).zfill(2)
            str_day = str(s.day).zfill(2)
            path = "data/taxi_data/%s/%s/%s.pkl" %(s.year, str_month, str_day)
            df = pd.read_pickle(path)
            trips_per_day[s_i][len(cutoffs)] = len(df)
            average_per_day[s_i][len(cutoffs)] = self.calc_avg_trip_duration_helper(df)

            for c_i, c in enumerate(cutoffs):
                        zones = cluster_south[:c]
                        temp = df.query('DO_id in @zones and PU_id in @zones')
                        trips_per_day[s_i][c_i] = len(temp)
                        average_per_day[s_i][c_i] = self.calc_avg_trip_duration_helper(temp)

            toc = time.time()
            print("running avg trip duration for {}-{}-{}, current iteration: {} ".format(str_month,str_day,s.year,round(toc-prev,4)))
            prev = toc

        result = np.zeros(len(cutoffs)+1)
        total_trips = np.sum(trips_per_day,axis=0) #contains total trips per cluster size
        for c_i in range(len(cutoffs)+1):
            for s_i in range(len(samples)):
                result[c_i] += trips_per_day[s_i][c_i] / total_trips[c_i] * average_per_day[s_i][c_i] # weigh each individual result with its weight in total_trips

        return result


    def calc_avg_trip_duration_helper(self, df):
        """
        returns the average trip duration of all trips included in df
        """

        temp_avg = np.zeros(len(df))
        counter = 0
        for row in df.itertuples():
            pu = self.preprocessor.boroughs.index(row[2])
            do = self.preprocessor.boroughs.index(row[3])
            tt = self.preprocessor.travel_stats[pu][do][0]
            temp_avg[counter] = tt
            counter += 1
        #print(np.mean(temp_avg))
        return np.mean(temp_avg)

    def plot_avg_trip_duration(self, results):

        labels = [5,8,13,19,26,38,63]

        x = np.arange(len(labels))

        fig, ax = plt.subplots()
        rects1 = ax.bar(x, results)

        ax.set_ylabel('Average trip duration in [s]')
        ax.set_xlabel('Cluster size')
        ax.set_title('Average trip duration depending on cluster size')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)


        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_avg_trip_duration.jpg")

        plt.show()

        data_to_graph = pd.DataFrame(labels, results)
        data_to_graph.to_csv("figures/" + now + "_avg_trip_duration.csv")


    def show_heatmap(self, pu, nZones):

        # code adapted from https://github.com/angelrps/MasterDataScience_FinalProject/blob/master/notebooks/Data_Analysis_01.ipynb
        # via https://medium.com/swlh/machine-learning-and-cab-industry-64e8add3b8cd

        # get df_shape
        df_shape = gpd.read_file('data/taxi_data/shape_files/tz.shp').to_crs(epsg=3785)
        df_shape = df_shape.drop(['shape_area', 'shape_leng', 'objectid'], axis=1)
        df_shape = df_shape[df_shape['borough'] == 'Manhattan']

        # get df_pu
        size = self.cutoffs.index(nZones)
        df_pu = pd.DataFrame(list(pu[size]/730/24)[0:nZones], columns = ["pickups"])
        if nZones == 63:
            df_pu = df_pu.assign(location_i = self.preprocessor.boroughs)
        else:
            df_pu = df_pu.assign(location_i = self.cluster_south[0:nZones])

        # merge them
        df_shape_merged = df_pu.merge(df_shape, left_on='location_i', right_on='location_i')

        # make nice plot
        def XY_from_shape(shape_data):
            data = []
            for zonename, LocationID, shape, pickups in shape_data[["zone", "location_i", "geometry", "pickups"]].values:
                #If shape is polygon, extract X and Y coordinates of boundary line:
                if isinstance(shape, Polygon):
                    X, Y = shape.boundary.xy
                    X = [int(x) for x in X]
                    Y = [int(y) for y in Y]
                    data.append([LocationID, zonename, X, Y, pickups])

                #If shape is Multipolygon, extract X and Y coordinates of each sub-Polygon:
                if isinstance(shape, MultiPolygon):
                    for poly in shape:
                        X, Y = poly.boundary.xy
                        X = [int(x) for x in X]
                        Y = [int(y) for y in Y]
                        data.append([LocationID, zonename, X, Y, pickups])

            #Create new DataFrame with X an Y coordinates separated:
            shape_data = pd.DataFrame(data, columns=["LocationID", "ZoneName", "X", "Y", 'pickups'])
            return shape_data

        # Get Manhattan geometry and top10 zones geometry
        shape_data = XY_from_shape(df_shape_merged)

        # Create 'ColumnDataSource' object and 'LinearColorMapper'
        source = ColumnDataSource(shape_data)

        max_passengers_per_hour = df_shape_merged['pickups'].max()
        color_mapper = LinearColorMapper(palette=palette[::-1], high=max_passengers_per_hour, low=0)

        # Create 'figure'
        p = figure(title="Taxi Zones by number of pickups",
                   plot_width=450, plot_height=750,
                   toolbar_location=None,
                   tools='pan,wheel_zoom,box_zoom,reset,save')

        p.xgrid.grid_line_color = None
        p.ygrid.grid_line_color = None
        p.axis.visible = False
        p.background_fill_color = None
        p.border_fill_color = None

        # Create 'patches'
        patches = p.patches(xs="X", ys="Y", source=source,
                          fill_color={'field': 'pickups', 'transform': color_mapper},
                          line_color="black")

        color_bar = ColorBar(color_mapper=color_mapper, label_standoff=12)
        p.add_layout(color_bar, 'right')

        # Create hover efect
        hovertool = HoverTool(tooltips=[('ZoneName:', "@ZoneName"),
                                        ("pickups:", "@pickups")])
        p.add_tools(hovertool)
        show(p)
        #now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        #export_png(p, filename="figures/" + now + "_heatmap_{}.png".format(nZones))

    # reward tables ####

    def get_test_avg_int(self, metric, p, ptf, nAgents = 1):

        path_to_folder = p+ptf

        path_to_file = os.listdir(path_to_folder)[0]

        pkl_file = open(path_to_folder + "/" + path_to_file, 'rb')
        checkup = pickle.load(pkl_file)

        nEpisodes = 26
        results = np.zeros(nEpisodes)
        for agent in range(nAgents):
            for episode in range(nEpisodes):
                if nAgents == 1:
                    results[episode] += checkup[metric][episode]
                else:
                    results[episode] += checkup[metric + '-agent' +str(agent)][episode]

        print("{} for {}: {}".format(metric, ptf, np.mean(results)))

        return np.mean(results)


    def get_reward_csv(self):
        path = "../../LRZ Sync+Share/Mobile Uploads/Uni/WiSe2021/Masterarbeit/Abgabe/Results/single_agent/results/"
        lof = os.listdir(path)

        rewards = ["r0.0","r0.25","r0.5","r0.75","r1.0"]
        #settings = ["NH","exe", "cen", "iac", "sha"]
        settings = ["NH", "iac", "sha", "cen", "exe", "sin"]
        #seeds = ["se10", "se200"]
        seeds = ["se10", "se30"]
        columns = list()
        for i in settings:
            for j in seeds:
                columns.append(i + "-" + j)

        result_df = pd.DataFrame(index = rewards, columns = columns)

        #print(result_df, columns)

        for elem in lof:
            #print(elem)
            for setting in settings:
                p1 = re.compile(setting)
                if p1.search(elem):
                    #print(setting)
                    break
            for seed in seeds:
                p2 = re.compile(seed)
                if p2.search(elem):
                    #print(seed)
                    break
            for reward in rewards:
                p3 = re.compile("-"+reward)
                if p3.search(elem):
                    #print(reward)
                    break

            col = setting+"-"+ seed
            result_df.at[reward,col] = self.get_test_avg_int("test-reward", path,elem,nAgents = 3)
            #get_test_avg_arr("test-action_distribution", path,elem)

        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        path = "../../LRZ Sync+Share/Mobile Uploads/Uni/WiSe2021/Masterarbeit/FinalResults/auswertungen/"+now +"-rewards.csv"
        result_df.to_csv(path)

        return result_df

    #### action distribution ####

    def get_test_avg_arr(self, metric, p, ptf):

        path_to_folder = p+ptf

        path_to_file = os.listdir(path_to_folder)[0]
        pkl_file = open(path_to_folder + "/" + path_to_file, 'rb')
        checkup = pickle.load(pkl_file)

        nEpisodes = 26
        nAgents = 3 #20
        results = np.empty((nEpisodes, checkup[metric][0].shape[0]))
        print(results.shape)
        for agent in range(nAgents):
            for episode in range(nEpisodes):
                results[episode] = checkup[metric][episode]

        #print("{} for {}: {}".format(metric, ptf, np.mean(results, axis = 0)))
        print(path_to_file)
        print(checkup[metric][episode])
        print(results[-1])
        print(np.mean(results, axis = 0).shape)
        return np.mean(results, axis = 0)

    def get_ad_data(self, metric):
        path = "../../LRZ Sync+Share/Mobile Uploads/Uni/WiSe2021/Masterarbeit/Abgabe/Results/single_agent/results/"
        lof = os.listdir(path)

        rewards = ["r0.0"]#,"r0.25","r0.5","r0.75","r1.0"]
        #settings = ["NH", "iac", "sha", "cen", "exe"]
        settings = ["NH", "iac", "sha", "cen", "exe", "sin"]
        seeds = ["se10", "se30"]#"se200"] #
        columns = list()
        for i in settings:
            for j in seeds:
                columns.append(i + "-" + j)

        result_df = pd.DataFrame(index = rewards, columns = columns)

        #print(result_df, columns)

        for elem in lof:
            #print(elem)
            if re.compile("NH").search(elem) or re.compile("R20").search(elem):
                print(elem)
                continue
            for setting in settings:
                p1 = re.compile(setting)
                if p1.search(elem):
                    #print(setting)
                    break
            for seed in seeds:
                p2 = re.compile(seed)
                if p2.search(elem):
                    #print(seed)
                    break
            for reward in rewards:
                p3 = re.compile("-"+reward)
                if p3.search(elem):
                    #print(reward)
                    break

            col = setting+"-"+ seed
            result_df.at[reward,col] = self.get_test_avg_arr(metric, path,elem)
            #get_test_avg_arr("test-action_distribution", path,elem)

        return result_df

    def merge_results(self, results):
        settings = ["iac", "sha", "cen", "exe", "sin"]#]
        #rewards = ["r0.0","r0.25","r0.5","r0.75","r1.0"]
        rewards = ["r0.0"]
        result_merged = pd.DataFrame(index = rewards, columns = settings)
        for row in results.itertuples():
            temp_list = list()
            for i in range(1,len(settings)+1):
                print(row[2*i+1])
                print(row[2*i+2])
                temp_list.append((0.5 * row[2*i+1] + 0.5 * row[2*i+2])/3/8640)
            result_merged.loc[row[0]] = temp_list

        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        path = "../../LRZ Sync+Share/Mobile Uploads/Uni/WiSe2021/Masterarbeit/FinalResults/auswertungen/"+now +"-action_distribution.csv"
        result_merged.to_csv(path)
        return result_merged

    def get_color_list(self):
        n1 = range(19)
        n2 = range(3)
        n3 = range(1)
        c = list()
        for i in n1:
            c.append("red")
        for i in n2:
            c.append("green")
        c.append("blue")

        return c

    def plot_ad(self, result_merged):
        fig,axs = plt.subplots(1,5, figsize = (20,3))
        columns = ["IAC", "IASAC", "IACC", "SPAC", "SAAC"]
        rows = ["r0.0","r0.25","r0.5","r0.75","r1.0"]
        font = {'fontname':'Arial'}
        for i in range(1):
            for j in range(5):
                if i == 0:
                    #axs[i,j].set_title(columns[j], **font,pad=20)
                    axs[j].set_title(columns[j], **font,pad=20)
                if j == 0:
                    #axs[i,j].set_ylabel(rows[i], rotation=0, size='large', **font, labelpad=60)
                    axs[j].set_ylabel(rows[i], rotation=0, size='large', **font, labelpad=60)
                #print(i,j, result_merged.iloc[i,j])
                c = self.get_color_list()
                #axs[i,j].bar(range(len(result_merged.iloc[i,j])),result_merged.iloc[i,j], color = c)
                axs[j].bar(range(len(result_merged.iloc[i,j])),result_merged.iloc[i,j], color = c)


        fig.tight_layout()
        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_ad_pretty.jpg")
        plt.show()

#### losses ####

    def get_losses_and_titles(self):
        path = "../../LRZ Sync+Share/Mobile Uploads/Uni/WiSe2021/Masterarbeit/Abgabe/Results/general/results/"
        lof = os.listdir(path)

        rewards = ["r0.0","r0.25","r0.5","r0.75","r1.0"]
        settings = ["exe", "cen", "iac", "sha"]
        seeds = ["se10", "se200"]
        alosses = dict()
        closses = dict()

        for setting in settings:
            for reward in rewards:
                alosses[setting + "-" + reward] = list()
                closses[setting + "-" + reward] = list()

        for elem in lof:
            #print(elem)
            if re.compile("NH").search(elem) or re.compile("R20").search(elem):
                print(elem)
                continue
            for setting in settings:
                p1 = re.compile(setting)
                if p1.search(elem):
                    #print(setting)
                    break
            for seed in seeds:
                p2 = re.compile(seed)
                if p2.search(elem):
                    #print(seed)
                    break
            for reward in rewards:
                p3 = re.compile("-"+reward)
                if p3.search(elem):
                    #print(reward)
                    break
            aloss, closs = self.get_loss_avg_int(path, elem)
            alosses[setting + "-" + reward].append(aloss)
            closses[setting + "-" + reward].append(closs)
            #print(len(alosses[setting + "-" + reward]))

        losses = [[],[]]
        titles = [[],[]]
        count = 0
        rewards = ["r0.0","r0.25","r0.5","r0.75","r1.0"]
        for elem in closses.keys():
            if re.compile("exe").search(elem):
                x = np.stack([alosses[elem][0], alosses[elem][1]])
                y = np.stack([closses[elem][0], closses[elem][1]])
                losses[0].append(np.mean(x, axis = 0))
                losses[1].append(np.mean(y, axis = 0))
                titles[0].append("Actor loss " + rewards[count])
                titles[1].append("Critic loss " + rewards[count])
                count += 1
                print(elem)
        print(titles)
        return losses, titles

    def get_loss_avg_int(self, p, ptf):

        path_to_folder = p+ptf

        path_to_file = os.listdir(path_to_folder)[0]
        pkl_file = open(path_to_folder + "/" + path_to_file, 'rb')
        checkup = pickle.load(pkl_file)

        actors = list()
        critics = list()

        for key in checkup.keys():
            if re.compile("aloss").search(key):
                #print(key)
                actors.append(key)
            if re.compile("closs").search(key):
                #print(key)
                critics.append(key)


        results_actor = self.loss_helper(checkup, actors, type_agent = "actor")
        results_critic = self.loss_helper(checkup, critics, type_agent = "critic")

        print(ptf)
        print(results_actor.shape, results_critic.shape)
        #print(results_actor, results_critic)
        return np.squeeze(results_actor), np.squeeze(results_critic)

    def loss_helper(self, checkup, agents, type_agent = "actor"):
        nEpisodes = 450
        results_agent = np.zeros((nEpisodes,len(agents)))
        for agent, metric in enumerate(agents):
            for episode in range(nEpisodes):
                results_agent[episode][agent] += checkup[metric][episode]
        return results_agent

    def plot_loss(self, losses, titles):
        fig, axs = plt.subplots(nrows=len(losses[0]), ncols=len(losses),figsize=(10,20))
        font = {'fontname':'Arial'}

        for col, loss in enumerate(losses):
            for row, indloss in enumerate(loss):
                try:
                    nAgents = loss.shape[1]
                    for agent in nAgents:
                        axs[row,col].plot(indloss[:,agent])
                except: # just one
                    axs[row,col].plot(indloss)

                axs[row,col].set_xlabel('Training episodes',**font)
                axs[row,col].set_title(titles[col][row], **font)
        fig.tight_layout()

        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_exe_losses.jpg")

        plt.show()
