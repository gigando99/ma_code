import numpy as np
import tensorflow as tf

class Logger():
    def __init__(self, env):
        self.env = env
        self.locations = self.init_locations()
        self.env_stats = self.init_env_stats()
        self.actions = self.init_actions()
        self.tf_stats = None
        self.dict_of_temp_metrics = dict()
        self.dict_of_final_metrics = dict()
        self.stats_dict_lookup = {0: "rebalancing_fulfilled", 1: "job_preprocessed", 2: "job_fulfilled", 3: "rebalancing_interrupted"}

    def __repr__(self):
        return "Logger object, which hopefully facilitates tracking and stats"

    def reset(self):
        self.locations = self.init_locations()
        self.env_stats = self.init_env_stats()
        self.actions = self.init_actions()


    def activate_tf_stats(self, nActors, nCritics):
        self.tf_stats = {"nActors": nActors, "nCritics": nCritics}
        self._init_dicts_of_metrics()

    def init_locations(self):
        return np.zeros((self.env.nBoroughs), dtype = np.int32)

    def init_actions(self):
        return np.zeros((self.env.no_op + 1), dtype = np.int32)

    def init_env_stats(self):
        final_dict = dict()

        # aq_len: aq length when request is missed # waiting_time: waiting time before rq is served
        request_analysis = {"aq_len": list(), "waiting_time": list()} #, "rq": np.zeros((3,8641))

        # overall stats on requests
        request_stats = {"missed": 0, "served": 0, "deleted": 0}

        # stats on an agent basis
        agent_stats = dict()
        for agent in range(self.env.nAgents):
            agent_stats[agent] = dict()
            job_tracking = {"rebalancing_fulfilled": 0, "rebalancing_interrupted": 0, "job_preprocessed": 0, "job_fulfilled": 0}
            # idle_eng: no actions in aq, idle_weit: no requests in aq
            time_tracking = {"t_fulf_reb": 0, "t_prep_job": 0, "t_job": 0, "idle_eng": 0, "idle_weit": 0}
            agent_stats[agent].update(job_tracking)
            agent_stats[agent].update(time_tracking)

        # merge dicts
        final_dict.update(request_analysis)
        final_dict.update(request_stats)
        final_dict.update(agent_stats)

        return  final_dict

    def _init_dicts_of_metrics(self):

        modes = ["train", "test", "validate"]

        #time
        for mode in modes:
            metric_name = mode + "-episode_time"
            self.dict_of_temp_metrics[metric_name] = tf.keras.metrics.Mean(metric_name, dtype=tf.float32)
        self.dict_of_temp_metrics["epoch_time"] = tf.keras.metrics.Mean("epoch_time", dtype=tf.float32)

        # agent specific
        for agent in range(self.env.nAgents):
            #metric_name = "reward-agent"+str(agent)
            #self.dict_of_temp_metrics[metric_name] = tf.keras.metrics.Sum(metric_name, dtype=tf.float32)
            metric_name = "reward_per_agent"
            self.dict_of_temp_metrics[metric_name] = np.zeros((86400,self.env.nAgents))
            for mode in modes:
                metric_name = mode + "-rebalancing_jobs-agent"+str(agent)
                self.dict_of_final_metrics[metric_name] = list()
                metric_name = mode + "-served_jobs-agent"+str(agent)
                self.dict_of_final_metrics[metric_name] = list()
                metric_name = mode + "-reward-agent"+str(agent)
                self.dict_of_final_metrics[metric_name] = list()

        #actor specific
        for actor in range(self.tf_stats["nActors"]):
            metric_name = "aloss-actor" + str(actor)
            self.dict_of_final_metrics[metric_name] = list()
            metric_name = "aloss-actor" + str(actor)
            self.dict_of_temp_metrics[metric_name] = tf.keras.metrics.Mean(metric_name, dtype=tf.float32)

        # critic specific
        for critic in range(self.tf_stats["nCritics"]):
            metric_name = "closs-critic" + str(critic)
            self.dict_of_final_metrics[metric_name] = list()
            metric_name = "closs-critic" + str(critic)
            self.dict_of_temp_metrics[metric_name] = tf.keras.metrics.Mean(metric_name, dtype=tf.float32)

        # general
        for mode in modes:
            metric_name = mode + "-missed_jobs"
            self.dict_of_final_metrics[metric_name] = list()
            metric_name = mode + "-action_distribution"
            self.dict_of_final_metrics[metric_name] = list()
        metric_name = "global_reward"
        self.dict_of_temp_metrics[metric_name] = tf.keras.metrics.Mean(metric_name, dtype=tf.float32)

        # env
        for mode in modes:
            self.dict_of_final_metrics[mode+"aq_len"] = list()
            self.dict_of_final_metrics[mode+"waiting_time"] = list()
            self.dict_of_final_metrics[mode+"job_time"] = list()
            self.dict_of_final_metrics[mode+"idle_eng"] = list()
            self.dict_of_final_metrics[mode+"idle_weit"] = list()
            for agent in range(self.env.nAgents):
                self.dict_of_final_metrics[mode+"job_time"+str(agent)] = list()
                self.dict_of_final_metrics[mode+"idle_eng"+str(agent)] = list()
                self.dict_of_final_metrics[mode+"idle_weit"+str(agent)] = list()

    def track_locations(self, locations):
        # gets full location list
        for elem in locations:
            self.locations[elem[0]] += 1

    #def track_actions(self, counter, list_of_actions):
    #    self.actions[counter] = list_of_actions
    def track_actions(self, list_of_actions):
        for elem in list_of_actions:
            self.actions[elem] += 1

    def save_episode_stats(self, episode_time, mode, nSteps):
        sum_of_rewards_pa = np.sum(self.dict_of_temp_metrics["reward_per_agent"], axis = 0)
        sum_of_rewards = np.sum(sum_of_rewards_pa)
        for agent in range(self.env.nAgents):
            self.dict_of_final_metrics[mode+"-rebalancing_jobs-agent"+str(agent)].append(self.env_stats[agent]["rebalancing_fulfilled"])
            self.dict_of_final_metrics[mode+"-served_jobs-agent"+str(agent)].append(self.env_stats[agent]["job_fulfilled"])
            self.dict_of_final_metrics[mode+"-reward-agent"+str(agent)].append(sum_of_rewards_pa[agent])
        self.dict_of_temp_metrics["reward_per_agent"] = np.zeros((86400,self.env.nAgents))

        if mode == "validate":
            self.dict_of_temp_metrics["global_reward"](sum_of_rewards)

        if mode == "train":
            for actor in range(self.tf_stats["nActors"]):
                self.dict_of_final_metrics["aloss-actor" + str(actor)].append(np.round(self.dict_of_temp_metrics["aloss-actor" + str(actor)].result().numpy(),4))
                self.dict_of_temp_metrics["aloss-actor" + str(actor)].reset_states()

            for critic in range(self.tf_stats["nCritics"]):
                self.dict_of_final_metrics["closs-critic" + str(critic)].append(np.round(self.dict_of_temp_metrics["closs-critic" + str(critic)].result().numpy(),4))
                self.dict_of_temp_metrics["closs-critic" + str(critic)].reset_states()

        self.dict_of_final_metrics[mode+"-missed_jobs"].append(self.env_stats["missed"])
        self.dict_of_final_metrics[mode+"-action_distribution"].append(self.actions)
        self.dict_of_temp_metrics[mode+"-episode_time"](episode_time)

        #print(self.dict_of_final_metrics)

        #print("Finished {} episode after {}s".format(mode, episode_time))
        #print("Episode reward: ", sum_of_rewards)
        #print("Episode reward per agent: ", sum_of_rewards_pa)
        #print("Environment Stats: ")#, self.env.stats_dict)
        return sum_of_rewards

    def postprocess_epoch(self, epoch_time):
        self.dict_of_temp_metrics["epoch_time"](epoch_time)
        print("\nFinished epoch after {}s".format(epoch_time))

        print("Average training episode time in current epoch: ", np.round(self.dict_of_temp_metrics["train-episode_time"].result().numpy(),4))
        print("Average evaluation episode time in current epoch: ", np.round(self.dict_of_temp_metrics["validate-episode_time"].result().numpy(),4))
        avg_reward = np.round(self.dict_of_temp_metrics["global_reward"].result().numpy(),4)
        print("Average global reward in validation runs: ", avg_reward)
        self.dict_of_temp_metrics["train-episode_time"].reset_states()
        self.dict_of_temp_metrics["validate-episode_time"].reset_states()
        self.dict_of_temp_metrics["global_reward"].reset_states()

        return avg_reward

    def print_episode_stats(self, nSteps, episode_time, episode_rewards, mode):
        ### Overall ###
        print("\nEpisode Stats")
        print("Finished {} episode after {}s".format(mode, episode_time))
        print("Episode reward: ", round(episode_rewards,3))
        '''print("served: ", self.env_stats["served"])
        print("missed: ", self.env_stats["missed"])
        print("deleted: ", self.env_stats["deleted"])'''
        print("- Jobs: [Served: {}, Missed: {}, Deleted: {}]".format(self.env_stats["served"],self.env_stats["missed"],self.env_stats["deleted"]))
        ### actions ###
        #without_zeros = self.actions[:int(nSteps/10)]
        #prepped_count = np.concatenate([np.concatenate(without_zeros), range(self.env.nActions)]) # add each action once to make them appear in distribution
        #action_distribution = np.unique(prepped_count, return_counts=True)[1] - 1 # subtract the added action
        #print("Actions: ", action_distribution)
        print("- Actions: ", self.actions)

        ### locations ###
        print("- Locations: ", self.locations)

        ### random ###
        self.env_stats["aq_len"].extend([0,1,2,3])
        aq_len_unique = np.unique(self.env_stats["aq_len"], return_counts=True)[1] - 1
        print("aq length when job is missed:", aq_len_unique)#, np.mean(self.env.more_stats["avg"]))
        avg_waiting_time = np.mean(self.env_stats["waiting_time"])
        print("average wating time before serving: ", avg_waiting_time)
        avg_time_spent_with_jobs = 0
        for agent in range(self.env.nAgents):
            avg_time_spent_with_jobs += self.env_stats[agent]["t_job"] / self.env.nAgents
        print("avg % of time spent with jobs: {}%".format(round(avg_time_spent_with_jobs/nSteps * 100,3)))
        avg_time_spent_idle_eng = 0
        for agent in range(self.env.nAgents):
            avg_time_spent_idle_eng += self.env_stats[agent]["idle_eng"] / self.env.nAgents
        print("avg % of time spent idle (empty aq): {}%".format(round(avg_time_spent_idle_eng/nSteps * 100,3)))
        avg_time_spent_idle_weit = 0
        for agent in range(self.env.nAgents):
            avg_time_spent_idle_weit += self.env_stats[agent]["idle_weit"] / self.env.nAgents
        print("avg % of time spent idle (no job in aq): {}%".format(round(avg_time_spent_idle_weit/nSteps * 100,3)))
        #self.dict_of_final_metrics["rq"].append(self.env.more_stats["rq"])
        #print(np.histogram(self.env.more_stats["timing"], 100))
        #print(self.env.more_stats["timing"])

        # save env_stats
        self.dict_of_final_metrics[mode+"aq_len"].append(aq_len_unique)
        self.dict_of_final_metrics[mode+"waiting_time"].append(avg_waiting_time)
        self.dict_of_final_metrics[mode+"job_time"].append(avg_time_spent_with_jobs)
        self.dict_of_final_metrics[mode+"idle_eng"].append(avg_time_spent_idle_eng)
        self.dict_of_final_metrics[mode+"idle_weit"].append(avg_time_spent_idle_weit)
        for agent in range(self.env.nAgents):
            self.dict_of_final_metrics[mode+"job_time"+str(agent)].append(self.env_stats[agent]["t_job"])
            self.dict_of_final_metrics[mode+"idle_eng"+str(agent)].append(self.env_stats[agent]["idle_eng"])
            self.dict_of_final_metrics[mode+"idle_weit"+str(agent)].append(self.env_stats[agent]["idle_weit"])

    def print_step_stats(self, actions):
        current_locations = [x[0] for x in self.env.location_of_all_agents]
        print("Stats in time step {}:".format(self.env.current_time))
        print("- Actions: {}".format(actions))
        #print("- Locations: {}".format(current_locations))
        print("- Jobs: [Served: {}, Missed: {}, Deleted: {}]".format(self.env_stats["served"],self.env_stats["missed"],self.env_stats["deleted"]))

        # print in last 10000 steps
