import os
import pandas as pd
import re
import datetime
from calendar import monthrange
import numpy as np

def test_function():
    print("Loading successfully. We can really access the functions.")

class inputData():

    def __init__(self, download_Object, verbose):

        # G E N E R A L
        self.verbose = verbose
        self.downloader = download_Object


        # D A T A   P R E P R O C E S S I N G

        self.pattern_remaining_cols = [
                                r"([p,P]ick[u,U]p_[d,D]ate[t,T]ime)",
                                r"(PU[L,l]ocationID)",
                                r"(DO[L,l]ocationID)"
                                ]

        self._create_taxi_metadata_file()

        self.list_of_dfs = self._init_list_of_dfs()
        self.data_dfs = list()
        self.preprocessed_dfs = list() #will be filled manually (so far). maybe save as pickle? to only do this once?
        self.pickle_temp = None #

        self.boroughs = self.list_of_dfs[0]["LocationID"].tolist() #which boroughs are relevant for us?

        # pricing data obtained from https://taxis-fare.com/uber-fare-state-new-york
        # (tobias used this www.alvia.com/uber-city/uber-new-york/, having a lower min_fare)
        self.min_fare = 8
        self.base_fare = 2.55
        self.time_cost = 0.35/60
        self.mileage_cost = 1.75/1609

        self.travel_stats = self._calculate_travel_stats()

        self.hourly_cutoff = 4
        self.stats_dict = {"total_length": [0,0,0,0], "only_manhattan": [0,0,0,0], "wo_in-zone": [0,0,0,0], "cleaned": [0,0,0,0]}


    def __repr__(self):

        return "Object for data Preprocessing: inputData(nBoroughs = %s)" %(self.nBoroughs)

    @property
    def nBoroughs(self):
        '''
        use property method instead of init value to ensure correct value even after only certain boroughs have been selected
        '''
        return len(self.boroughs)


    def _init_list_of_dfs(self):
        '''
        fills self.list_of_dfs
        '''
        list_of_dfs = list()
        for elem in ['data/manhattan_metadata.csv', 'data/preprocessing_tobias/neighbor_routes.csv']:
            list_of_dfs.append(self._get_CSV(elem))
        list_of_dfs.append(self._get_CSV('data/preprocessing_tobias/all_routes.csv', converters_input = {"route": eval}))

        return list_of_dfs


    def change_problem_size(self, new_nBoroughs):
        '''
        Parameters:
        - new_nBoroughs (int): number of boroughs to be considered

        Does:
        - selects new_nBoroughs neighboring boroughs
        - reduces relevant objects to the requested size
        '''

        old = self.nBoroughs

        self.list_of_dfs = self._init_list_of_dfs()

        if new_nBoroughs == 63:
            self.boroughs = self.list_of_dfs[0]["LocationID"].tolist()

        else:
            cluster_south = self._find_neighboring_boroughs()
            self.boroughs = cluster_south[0:new_nBoroughs]
            #print(len(self.list_of_dfs[2]))
            self.list_of_dfs[2] = self.list_of_dfs[2].query('origin_ID in @self.boroughs and destination_ID in @self.boroughs')
            #print(len(self.list_of_dfs[2]))
        #print("reduced boroughs: ", self.boroughs)
        self.travel_stats = self._calculate_travel_stats()

        if self.verbose:
            print("Changed problem size from %s to %s boroughs" %(old, min(63,new_nBoroughs)))

        """for elem in self.preprocessed_dfs:
            elem = elem.query('DO_id in @self.boroughs and PU_id in @self.boroughs')"""

        return 0



    def _get_CSV(self, path, converters_input = None, parse_dates_input = None, date_parser_input = None, nrows_input = None):
        '''
        Parameters:
        - path (str): path to csv
        - converters_input (dict): converters to be applied while reading the csv
        - parse_dates_input (list): columns that contain dates
        - date_parser_input (function): function to be applied for parsing dates
        '''
        df = pd.read_csv(path, converters = converters_input, parse_dates = parse_dates_input, nrows = nrows_input)
        #print(type(df.iloc[0][1]))

        return df


    def _create_taxi_metadata_file(self):

        '''
        creates file: "manhattan_metadata.csv", which contains the zone IDs, the zone centers and their verbal descriptions for all 63 selected zones.
        selected zones (by Tobias): all in borough of Manhattan, minus: 103, 104, 105, 153, 194, 202
        '''

        path1 = os.getcwd()
        path2 = path1 + "/data"

        if "manhattan_metadata.csv" in os.listdir(path2):
            if self.verbose:
                print("metadata file already there")
        else:
            list_of_dfs = []
            for elem in ['data/zone_to_name_wo_quotations.csv','data/preprocessing_tobias/osm_matched_nodes.csv']:
                list_of_dfs.append(self._get_CSV(elem))
            zone_names = list_of_dfs[0]
            matched_nodes = list_of_dfs[1]
            metadata = matched_nodes.merge(zone_names, left_on="LocationID", right_on="LocationID", how="inner")
            metadata.to_csv("data/manhattan_metadata.csv", index=False)
            if self.verbose:
                print("Succesfully merged zone names and zone centers into new metadata file")

        return 0


    def read_data_dfs(self, path, nrows_input = None):

        # get columns of df
        list_of_cols = pd.read_csv(path, index_col=0, nrows=0).columns.tolist()

        # get remaining columns
        parse_dates_input = list()
        for col in list_of_cols:
            if re.search(self.pattern_remaining_cols[0], col) != None:
                parse_dates_input.append(col)

        # get df from csv
        df = self._get_CSV(path, parse_dates_input = parse_dates_input, nrows_input = nrows_input)
        if self.verbose:
            print("read file at path ", path)

        return df


    def preprocess_taxi_data(self, list_of_dfs, month, year):

        '''
        Parameters:
        list_of_dfs:  a list containing dfs with taxi data
        --------------------------------------------------
        Returns:
        new_dfs: a list containing the preprocessed taxi data.
        '''

        #elem entspricht einzelnen datensätzen
        for count, elem in enumerate(list_of_dfs):

            #save required columns
            remaining_cols = list()
            for col in elem:
                for i in self.pattern_remaining_cols:
                    if re.search(i, col) != None:
                        remaining_cols.append(col)

            #filter out relevant colums
            elem = elem[remaining_cols]

            #elem = elem.rename(columns = {"tpep_pickup_datetime": "PU_dt", "tpep_dropoff_datetime": "DO_dt", "PULocationID": "PU_id", "DOLocationID": "DO_id"})
            elem = elem.rename(columns = {remaining_cols[0]: "PU_dt", remaining_cols[1]: "PU_id", remaining_cols[2]: "DO_id"})

            #filter for relevant boroughs and reset index
            print(count, self.stats_dict)
            self.stats_dict["total_length"][count] += len(elem)
            elem.query('DO_id in @self.boroughs and PU_id in @self.boroughs', inplace = True)
            self.stats_dict["only_manhattan"][count] += len(elem)
            elem.query('DO_id != PU_id', inplace = True)
            self.stats_dict["wo_in-zone"][count] += len(elem)
            #info_dict = {len_before, len_after, len_aa]
            elem.reset_index(drop=True, inplace=True)
            elem.PU_dt = pd.to_datetime(elem.PU_dt)
            #print(elem)
            elem = self.clean_taxi_data(elem, month, year)
            self.stats_dict["cleaned"][count] += len(elem)

            self.preprocessed_dfs.append(elem)


        return 0

    def clean_taxi_data(self, df, month, year):
        '''
        Parameters:
        - df: which dataframe to be cleaned
        - month: current month involved
        - year: current year
        Does:
        - drops duplicates
        - should select only data in relevant month + year
        Returns:
        - def
        '''

        df.drop_duplicates(keep = "first", inplace = True, ignore_index = True)
        #lista = [month - 1, month, month + 1]
        #listb = [year - 1, year]
        #listc = [year + 1, year]
        df.query("PU_dt.dt.month in [@month - 1, @month, @month + 1]", inplace = True)
        if month == 1:
            df.query("PU_dt.dt.year in [@year - 1, @year]", inplace = True)
        elif month == 12:
            df.query("PU_dt.dt.year in [@year + 1, @year]", inplace = True)
        else:
            df.query("PU_dt.dt.year == @year", inplace = True)

        #self.stats_dict["cleaned"] += len(df)

        return df

    def split_into_different_days_and_dump(self, month, year):

        num_days = monthrange(year, month)[1]
        if  str(1).zfill(2) + ".pkl" and str(num_days).zfill(2) + ".pkl" in os.listdir("./data/taxi_data/"+str(year)+"/"+str(month).zfill(2)+"/"):
            print("pickles already exists, done")
        else:

            for i in range(1,num_days+1):
                df1 = self.preprocessed_dfs.query("PU_dt.dt.day == @i and PU_dt.dt.hour >= @self.hourly_cutoff")
                # last day of the month is separate:
                if i == num_days:
                    #print("using temp of last month", self.pickle_temp)
                    df2 = self.pickle_temp
                else:
                    df2 = self.preprocessed_dfs.query("PU_dt.dt.day == @i+1 and PU_dt.dt.hour < @self.hourly_cutoff")

                # this will also work if df2 == None
                df = pd.concat([df1,df2], ignore_index=True)
                day = datetime.date(year, month, i)
                df["delta"] = (df["PU_dt"] - datetime.datetime.combine(day, datetime.time(self.hourly_cutoff,0,0))).dt.seconds
                df.sort_values(by = "PU_dt", inplace = True)
                df.to_pickle("./data/taxi_data/"+str(year)+"/"+str(month).zfill(2) + "/" + str(i).zfill(2) + ".pkl")
            print("created pickle files")

            # save new pickle_temps
            self.pickle_temp = self.preprocessed_dfs.query("PU_dt.dt.day == 1 and PU_dt.dt.hour < @self.hourly_cutoff")

        return 0

    def pickle_data_days(self, month, year): # check if files already exist?

        types = ["yellow", "green", "fhv"]
        if year == 2019 and month > 1:
            types.append("fhvhv")
        for taxi_type in types:
            monthstr = str(month).zfill(2)
            #paths.append('data/taxi_data/raw/%s_tripdata_%s-%s.csv' %(type, year, monthstr))
            path = 'data/taxi_data/raw/%s_tripdata_%s-%s.csv' %(taxi_type, year, monthstr)
            self.data_dfs.append(self.read_data_dfs(path))
        self.preprocess_taxi_data(self.data_dfs, month, year)
        self.preprocessed_dfs = pd.concat(self.preprocessed_dfs, ignore_index=True)
        #self.preprocessed_dfs = self.clean_taxi_data(self.preprocessed_dfs, month, year)
        self.split_into_different_days_and_dump(month, year)

    def check_or_create_pickles(self):

        for year in reversed(self.downloader.years):
            for month in reversed(self.downloader.months):
                if  "01.pkl" and "28.pkl" in os.listdir("./data/taxi_data/"+str(year)+"/"+str(month).zfill(2)+"/"):
                    print("pickles for %s-%s already exist" %(year, month))
                    # fill stats dict with values
                    # got these values from an actual run
                    #self.stats_dict['total_length'] = 504905577
                    #self.stats_dict['only_manhattan'] = 243392950
                    #self.stats_dict['wo_in-zone'] = 230960022
                    #self.stats_dict['cleaned'] = 230193129
                    self.stats_dict = {'total_length': [187203269, 14851353, 302850955, 234629119],
                                       'only_manhattan': [157382220, 4358698, 81652032, 74024891],
                                       'wo_in-zone': [147915831, 3758167, 79286024, 71588309],
                                       'cleaned': [147504131, 3750119, 79138401, 71475248]}
                else:
                    self.data_dfs = list()
                    self.preprocessed_dfs = list()
                    self.pickle_data_days(month, year)

        return 0


    def _retrieve_travel_time(self, o, d):
        '''
        retrieves travel_time from all_routes.csv
        '''

        df = self.list_of_dfs[2] #all_routes
        #temp = df[(df["origin_ID"] == o) & (df["destination_ID"] == d)]

        return 10 * df[(df["origin_ID"] == o) & (df["destination_ID"] == d)].travel_time.values[0]

    def _retrieve_travel_distance(self, o, d):
        '''
        retrieves travel distance from all_routes.csv
        '''

        df = self.list_of_dfs[2] #all_routes

        return df[(df["origin_ID"] == o) & (df["destination_ID"] == d)].distance.values[0]


    def _calculate_reward(self, o, d, time, distance):
        '''
        calculates rewards using the initialized pricing data
        '''

        return round(max(self.min_fare, self.base_fare + time * self.time_cost + distance * self.mileage_cost),3)


    def _calculate_travel_stats(self):
        '''
        inputs travel_time and reward of all o-d-pairs into np.array
        '''

        od_lookup = np.zeros([self.nBoroughs, self.nBoroughs,3])

        for row in range(self.nBoroughs):
            for col in range(self.nBoroughs):
                if row == col:
                    od_lookup[row][col] = [0,0,0]
                else:
                    o = self.boroughs[row]
                    d = self.boroughs[col]
                    #print(row,col,o,d)
                    tt = self._retrieve_travel_time(o,d)
                    dist = self._retrieve_travel_distance(o,d)
                    rew = self._calculate_reward(o,d,tt,dist)

                    od_lookup[row][col] = [tt,dist,rew]
        #print(od_lookup)
        return od_lookup

    def _find_neighboring_boroughs(self):
        '''
        finding neighboring nBoroughs
        '''
        '''
        neighbor_boroughs = list()
        orig = self.list_of_dfs[1].origin_ID
        dest = self.list_of_dfs[1].destination_ID
        for i in range(len(self.list_of_dfs[1])):
            neighbor_boroughs.append(orig[i])
            neighbor_boroughs.append(dest[i])
        wo_dupl = list()
        for elem in neighbor_boroughs:
            if elem not in wo_dupl:
                wo_dupl.append(elem)

        return wo_dupl
        '''

        cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]

        return cluster_south
