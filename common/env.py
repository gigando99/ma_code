import numpy as np
import pandas as pd
import datetime
import common.utils as utils
import common.logger as logger
from sklearn.model_selection import train_test_split
from calendar import monthrange
from pandas.tseries import holiday
import time


#### Action numbers ####
# there are 63 boroughs
# request queue limit is 50
# x = 000-062 means relocate to borough x
# x = 063-112 means accept job at position x-63 in the request_queue_limit
# x = 113 means noop

#### Requests in request queue ####
# consist of a tuple: (a,b,c,d,e)
# a: origin_ID
# b: destination_ID
# c: time_to_completion
# d: reward
# e: patience_timer (is deleted as soon as request gets into action queue - under the condition that >> time_to_pickup < patience_timer) <<

#### Action queue ####
# consist of a tuple: (a,b,c,d,e)
# a: origin_ID
# b: destination_ID
# c: remaining time_to_completion
# d: reward
# e: type (0: rebalancing, 1: mandatory rebalancing, 2: fulfilling)

#### Location of agents ####
# consist of a tuple: (a,b,c)
# a: current zone
# b: on the way to this neighboring zone
# c: 11.03: time to finish, previously: seconds already on the way there
# if noop: a = b, c = 0

#### Verbose ####
# 0: print nothing
# 1: print only most relevant stuff
# 2: print all relevant information


class AmodEnv():
    '''
    This class comprises the main functionality, the environment, of the Stochastic Problem.

    Arguments:
        - data (InputData object)
        - nAgents (int): number of agents in the system
        - nBoroughs (int): number of taxi zones (only works safely for {2, 63})
        - step_interval (int): number of seconds between each environment step
        - reward_param (float in [0,1]): ranges from 0 (cooperative) to 1 (competitive)
        - seed (int): seed to allow reproducibility
        - verbose (int): level of verbosity

    Main attributes:
        - nAgents: number of agents
        - nBoroughs: number of taxi zones
        - nActions: number of possible actions
        - current_data_day: current day data is gathered from

    Main functions:
        - step(): takes actions, performs a step in the environment and returns the new state
        - transform_state_to_input(): takes shaped state, returns flat state
        - reset(): reset state to initial state

    '''

    def __init__(self, data, nAgents, nBoroughs, reward_param, seed, verbose):

        #set random seeds
        self.seed = seed
        np.random.seed(seed)
        self.verbose = verbose

        # obtained from stats:
        self.cutoffs = [5,8,13,19,26,38,63]
        self.cluster_south = cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]
        self.avg_trip_duration = [338.04, 485.34, 606.62, 719.62, 847.54, 905.63, 1070.22] # obtained values below via stats: calc_avg_trip_duration
        self.possible_trips_per_agent = [255.59, 178.02, 142.43, 120.06, 101.94, 95.4, 80.73] #[125.77, 87.45, 68.89, 58.45, 49.81, 46.47, 39.52] # obtained values below via stats: calc_avg_trip_duration
        self.avg_trips_per_cluster_size = [1084.22, 4406.58, 13846.6, 40879.34, 92785.76, 224276.89, 413370.98]#[1881.24, 7468.93, 24087.95, 75457.08, 164987.97, 378751.69, 698738.37] # obtained values below via stats: get_trips_per_cluster_and_hour

        self.data = data # data object containing all relevant files
        self.nAgents = nAgents

        self.scale_down_factor = 1
        self.scale_down_rq_counter = 0
        self.request_queue_limit = self.nAgents # theoretical limit to have a fixed size request queue, will be changed with activate_scaling_down()
        self.nBoroughs = nBoroughs
        assert self.nBoroughs in self.cutoffs, "Please choose a different number of zones. Supported: {}".format(self.cutoffs)
        self.data.change_problem_size(nBoroughs)
        assert self.nBoroughs == self.data.nBoroughs, "Something went wrong when reducing the problem size. env: {}, data: {}".format(self.nBoroughs, self.data.nBoroughs)
        self.patience_timeout = self._initialize_patience_timeout() #1 * 60 #5 * 60 # 5 minutes
        self.activate_scaling_down()

        self.first_day = datetime.datetime(2018,1,1)
        self.last_day = datetime.datetime(2019,12,31)
        self.train_test_ratio = (0.9, 0.05, 0.05)
        self.dict_of_list_of_data_days = self.split_into_train_test()

        self.data_df = None
        self.route_df = self.data.list_of_dfs[2]
        self.current_data_day = None
        self.gas_cost = - 0.00056 / 1.609 # 56ct per mile

        self.reward_param = reward_param
        self.reward_dict = {"selection": 1, "pickup": 0, "dropoff": 0}
        self.action_queue = self._initialize_action_queue() #one list per agent in a list
        self.request_queue = self._initialize_request_queue() #simple list
        self.step_interval = 10 #seconds
        self.location_of_all_agents = self._initialize_agent_locations() # len = nAgents
        self.current_routes = self._initialize_current_routes()
        self.action_queue_limit = 5 # theoretical limit to have a fixed size action queue
        self.initiation_time =  0 # seconds into the day. we will start our day at: datetime.time(3,0,0)
        self.terminal_time = 86400 #seconds of a day. we end our day at: datetime.time(2,59,50)
        self.current_time = 0
        self.state = [self.initiation_time, self.request_queue, self.location_of_all_agents, self.action_queue]
        self.done = False

        # size of array: 1 (time) + rq_limit * 3 + nAgents * 3 + nAgents * aq_limit * 5
        self.len_rq_entry = 3
        self.loc_entry = 3
        self.len_aq_entry = 5
        self.state_len = 1 + self.request_queue_limit * self.len_rq_entry + self.nAgents * self.loc_entry + self.nAgents * self.action_queue_limit * self.len_aq_entry

        self.logger = logger.Logger(self)

    def __repr__(self):
        '''
        pretty string representation of the object containing most important infos"
        '''

        if self.reward_param == 1:
            reward_setting = "Competitive"
        elif self.reward_param == 0:
            reward_setting = "Cooperative"
        else:
            reward_setting = "Mixed ({}% Competitive)".format(100 * self.reward_param)

        return "Environment Object: AmodEnv(nAgents = %s, nBoroughs = %s, step_interval = %s, reward_setting: %s)" %(self.nAgents, self.nBoroughs, self.step_interval, reward_setting)

    @property
    def no_op(self):
        '''
        returns the no_op operation, the last action that can be chosen
        '''

        return self.nBoroughs + self.request_queue_limit

    @property
    def nActions(self):
        '''
        returns the number of eligible actions
        '''

        return self.no_op + 1

    def step(self, actions, transformed = False):
        '''
        Performs one full environment step

        Arguments:
            - actions (list of length nAgents)
            - transformed

        Returns:
            - (transformed) new state
            - array of rewards
            - info if episode is finished
        '''

        # update time
        self.current_time = self.current_time + self.step_interval
        if self.current_time == self.terminal_time:
            self.done = True

        if self.current_time % 10000 == 0:
            self.logger.print_step_stats(actions)

        env_actions = actions.copy()
        # check if jobs are requested that could not be reached within patience_timer
        # check if multiple agents want to fulfill the same job
        a = self._clean_actions(env_actions)

        # update action queues: remove jobs that got done, reduce remaining time on others, add new actions
        raw_rewards = self._update_action_queue(a)

        # move agents
        raw_rewards = self._update_agents_location(raw_rewards)

        # update request queue: delete jobs that were selected, reduce patience, add new requests
        self._update_request_queue()

        # calculate rewards
        r = self._reward_function(raw_rewards)

        if self.verbose > 0:
            print("env.step(): Current State:")
            print("    - Current time: {}, {}% finished".format(self.current_time, round(self.current_time / self.terminal_time * 100, 2) ))
            print("    - Chosen Actions: ", actions)
            print("    - New locations: ", self.location_of_all_agents)
            print("    - New action queue: ", self.action_queue)
            print("    - Raw rewards: ", raw_rewards)
            print("    - Rewards: ", r)

        # update state
        self.state = self._build_state()

        for agent in range(self.nAgents):
            if len(self.action_queue[agent]) == 0:
                self.logger.env_stats[agent]["idle_eng"] += self.step_interval
            counter = 0
            for action in self.action_queue[agent]:
                if action[4] == 2:
                    counter += 1
            if counter == 0:
                self.logger.env_stats[agent]["idle_weit"] += self.step_interval

        self.logger.track_locations(self.location_of_all_agents)
        self.logger.track_actions(actions)

        if not transformed:
            return self.state, r, self.done
        else:
            return self.transform_state_to_input(self.state), r, self.done


    def reset(self, transformed = False):
        '''
        Prepares environment for the start of the next episode

        Returns:
            - initial state
        '''

        self.current_time = self.initiation_time
        self.action_queue = self._initialize_action_queue()
        self.request_queue = self._initialize_request_queue()
        self.location_of_all_agents = self._initialize_agent_locations()
        self.state = self._build_state()
        self.done = False
        self.logger.reset()
        self.scale_down_rq_counter = 0
        self.current_routes = self._initialize_current_routes()

        if not transformed:
            return self.state
        else:
            return self.transform_state_to_input(self.state)

    def _initialize_action_queue(self):

        # lists are filled up to limit in self.transform_state_to_input()
        return [[] for i in range(self.nAgents)]

    def _initialize_request_queue(self):

        # list is filled up to limit in self.transform_state_to_input()
        return list()

    def _initialize_current_routes(self):

        return [[] for i in range(self.nAgents)]

    def _initialize_patience_timeout(self):
        # obtained via stats:
        timeout_per_cluster = {5: 400, 8: 550, 13: 550, 19: 550, 26: 700, 38: 700, 63: 800}

        return timeout_per_cluster[self.nBoroughs]

    def _update_agents_location(self, raw_rewards):
        '''
        Updates all agents' location with the help of __update_location_helper()

        Returns:
            - list containing the raw rewards per agent
        '''

        for i in range(self.nAgents):

            pers_reward = 0

            # action queue has at least one item
            if len(self.action_queue[i]) > 0:
                location, pers_reward = self.__update_location_helper(i, self.step_interval, self.location_of_all_agents[i], pers_reward)
                self.location_of_all_agents[i] = location

            else:
                if self.verbose > 1:
                    print("U_A_L: empty action queue, no movement")
            pass

            raw_rewards[i] += pers_reward

        return raw_rewards

    def __update_location_helper(self, i, remaining_step_time, location, pers_reward):
        '''
        Recursive function, in which agents are moved

        Arguments:
            - i (int): index of agent
            - remaining_step_time (int)
            - pers_reward (float)

        Returns:
            - new location
            - personal reward
        '''
        if remaining_step_time > 0:

            # time to finish: travel time between two nodes - already passed time
            ttf = location[2]

            # new zone is not reached, update third part only
            if ttf > remaining_step_time: #
                location[2] -= remaining_step_time
                pers_reward += self.___calculate_gas_cost(location[0], location[1], remaining_step_time)
                self.action_queue[i][0][2] -= remaining_step_time

                # assign reward for pickung up customer
                # cond1: it's a job, cond2: agent starts where job starts, cond3: agent starts route
                if self.action_queue[i][0][4] == 2 and self.action_queue[i][0][0] == location[0] and ttf == self.data.travel_stats[location[0]][location[1]][0] :
                    pers_reward += self.reward_dict["pickup"] * self.action_queue[i][0][3]

                if self.verbose > 1:
                    print("U_L_H: added time on route, this cost us ", pers_reward)

            # new zone is reached, update all parts and try again
            else:
                # retrieving current route is costly, reduce these calls
                if not self.current_routes[i] or self.action_queue[i][0][1] != self.current_routes[i][-1]:
                    current_route = self.____retrieve_travel_route(self.action_queue[i][0][0],self.action_queue[i][0][1])
                    self.current_routes[i] = current_route
                else:
                    current_route = self.current_routes[i]

                pers_reward += self.___calculate_gas_cost(location[0], location[1], ttf)
                self.action_queue[i][0][2] -= ttf
                location[0] = location[1]
                try:
                    location[1] = current_route[current_route.index(location[0]) + 1]
                    location[2] = self.data.travel_stats[location[0], location[1], 0]
                    if self.verbose > 1:
                        print("U_L_H: starting a new route after finishing old route, which cost ", pers_reward)
                    location, pers_reward = self.__update_location_helper(i, remaining_step_time - ttf, location, pers_reward)

                # occurs if this was the last travel of this action
                except IndexError:
                    try:
                        fulfilled = self.action_queue[i].pop(0)
                        self.logger.env_stats[i][self.logger.stats_dict_lookup[fulfilled[4]]] += 1
                        if fulfilled[4] == 2:
                            self.logger.env_stats["served"] += 1
                            self.logger.env_stats[i]["t_job"] += self.data.travel_stats[fulfilled[0], fulfilled[1], 0]
                        if fulfilled[4] == 1:
                            self.logger.env_stats[i]["t_prep_job"] += self.data.travel_stats[fulfilled[0], fulfilled[1], 0]
                        if fulfilled[4] == 0:
                            self.logger.env_stats[i]["t_fulf_reb"] += self.data.travel_stats[fulfilled[0], fulfilled[1], 0]

                        pers_reward += fulfilled[3] * self.reward_dict["dropoff"]

                        if self.verbose > 1:
                            print("U_L_H: fulfilled job and gained rewards: ", fulfilled[3])
                        current_route = self.____retrieve_travel_route(self.action_queue[i][0][0],self.action_queue[i][0][1])
                        self.current_routes[i] = current_route
                        location[1] = current_route[current_route.index(location[0]) + 1]
                        location[2] = self.data.travel_stats[location[0], location[1],0]
                        location, pers_reward = self.__update_location_helper(i, remaining_step_time - ttf, location, pers_reward)

                    # occurs if no other action in queue
                    except IndexError:
                        location[1] = location[0]
                        location[2] = 0
                        if self.verbose > 1:
                            print("U_L_H: finished all actions")

        return location, pers_reward

    def _remove_rebalancing_tasks(self, agent):
        # simply remove all rebalancing tasks
        to_be_removed = list()
        for elem in reversed(self.action_queue[agent]):
            if elem[4] == 0:
                to_be_removed.append(elem)
            else:
                break

        for elem in to_be_removed:
            self.action_queue[agent].remove(elem)

        return 0

    def _update_action_queue(self, actions):
        '''
        Adds the incoming actions to each agent's action queue with the help of self._new_on_empty_aq() and self._new_on_full_aq():
            - deletes rebalancing if obsolet
            - adds preprocessing if necessary

        Arguments:
            - list containing all the actions
        '''

        chosen_requests = list()
        raw_rewards = np.zeros(self.nAgents)

        for i in range(self.nAgents):

            # add new action_space
            # check if it is the no-op action
            if actions[i] != self.no_op:

                ### remove old rebalancing tasks
                self._remove_rebalancing_tasks(i)

                # action is a rebalancing operation
                if actions[i] < self.nBoroughs:
                    ### case 1: on full action queue, start from last end point
                    if self.action_queue[i]:
                        if self.verbose > 1:
                            print("U_AQ: adding rebalancing on full queue, agent: ", i)
                        self._new_a_on_full_aq(i, actions[i], 0)

                    ### case 2: on empty action queue, consider existing loc
                    else:
                        if self.verbose > 1:
                            print("U_AQ: adding rebalancing on empty queue, agent: ", i)
                        self._new_a_on_empty_aq(i, actions[i], 0)

                # action is a job
                else:
                    req = self.request_queue[actions[i] - self.nBoroughs].copy()
                    raw_rewards[i] = req[3] * self.reward_dict["selection"]
                    chosen_requests.append(req.copy())

                    ### full aq: add job including preprocessing
                    if self.action_queue[i]:
                        if self.verbose > 1:
                            print("U_AQ: adding job on full queue, agent: ", i)
                        # preprocessing
                        req[4] = 2
                        self._new_a_on_full_aq(i, req[0], 1)

                        # append actual job
                        self.action_queue[i].append(req)

                    ### empty aq
                    else:
                        if self.verbose > 1:
                            print("U_AQ: adding job on empty queue, agent: ", i)

                        #preprocessing
                        req[4] = 2
                        self._new_a_on_empty_aq(i, req[0], 1)

                        # append actual job
                        self.action_queue[i].append(req)

        if self.verbose > 1:
            print("U_AQ: chosen requests: ", chosen_requests)
        # now remove chosen requests from request_queue

        for elem in chosen_requests:
            self.logger.env_stats["waiting_time"].append(self.patience_timeout - elem[4])
            self.request_queue.remove(elem)

        return raw_rewards

    def _new_a_on_full_aq(self, i, d, atype):

        o = self.action_queue[i][-1][1] # get end point of last job
        if o != d:
            self.action_queue[i].append([o, d, self.data.travel_stats[o, d, 0], 0, atype])

    def _new_a_on_empty_aq(self, i, dest_index, atype):
        ### subcase 1: agent not in center of zone (because a rebalancing operation was forced to stop)
        if self.location_of_all_agents[i][2] != 0:
            # continue current route to next zone, add this as a job
            o = self.location_of_all_agents[i][0]
            d = self.location_of_all_agents[i][1]

            self.action_queue[i].append([o, d, self.location_of_all_agents[i][2], 0, 3])

        ### subcase 2: agent in center of zone
        # actual rebalancing
        o = self.location_of_all_agents[i][1]
        d = dest_index
        if o != d:
            self.action_queue[i].append([o, d ,self.data.travel_stats[o, dest_index, 0], 0, atype])

    def _update_request_queue(self):
        '''
        Retrieves request data from current data file
        '''
        to_be_removed = list()
        for elem in self.request_queue:
            # job patience runs out, record stats
            if elem[4] <= self.step_interval:
                to_be_removed.append(elem)
                self.logger.env_stats["missed"] += 1
                for aq in self.action_queue:
                    self.logger.env_stats["aq_len"].append(len(aq))

            else:
                elem[4] -= self.step_interval

        for elem in to_be_removed:
            self.request_queue.remove(elem)


        # receive incoming Requests
        self.request_queue.extend(self._incoming_requests())

        # make sure rq does not excede limit
        diff = len(self.request_queue) - self.request_queue_limit
        if  diff > 0:
            for i in range(diff):
                del self.request_queue[-1]
                self.logger.env_stats["deleted"] += 1

        return 0

    def _clean_actions(self, actions):
        '''
        - Deletes job requests that could not be reached within patience_timeout
        - Distribute job randomly if more than 1 agent wants to fulfill

        Arguments:
            - list of actions

        Returns:
            - Cleaned actions
        '''
        index = 0
        dict_actions = dict()

        for elem in actions:
            # quick check for rebalancing ops
            if elem in range(0, self.nBoroughs):
                # aq already full?
                if len(self.action_queue[index]) >= self.action_queue_limit:
                    actions[index] = self.no_op
                # same rebalancing op as currently active?
                if self.action_queue[index]:
                    if self.action_queue[index][-1][4] == 0 and self.action_queue[index][-1][1] == elem:
                        actions[index] = self.no_op

            if elem > self.no_op:
                print("env._clean_actions: Something is weird, non eligible action chosen")
                actions[index] = self.no_op

            # only check thoroughly for actual jobs
            if elem in range(self.nBoroughs, self.no_op):

                # infeasible case 1: higher job then nRequest selected
                if elem - self.nBoroughs >= len(self.request_queue):
                    actions[index] = self.no_op
                    index += 1
                    continue

                # infeasible case 2: check if already too many jobs in aq
                job_counter = 0
                action_counter = 0
                for temp in self.action_queue[index]:
                    action_counter += 1
                    if temp[4] == 2:
                        job_counter += 1

                # job limit of 2 to prevent reaching queue limit of 5 actions in total
                if job_counter > 1 or action_counter >= self.action_queue_limit:
                    actions[index] = self.no_op
                    index += 1
                    continue

                #  infeasible case 3: job cannot be reached without patience timeout
                time_to_job = 0
                # subcase 1: no job in aq
                # ttj  = remaining time on current route + travel time to job
                if job_counter == 0:
                    time_to_job += self.location_of_all_agents[index][2]
                    o = self.location_of_all_agents[index][1]
                    d = self.request_queue[elem - self.nBoroughs][0]
                    time_to_job += self.data.travel_stats[o,d,0]

                # subcase 2: job in aq
                # ttj  = time until job is finished + travel time to job
                else:
                    for temp in self.action_queue[index]:
                        time_to_job += temp[2]
                        if temp[4] ==2:
                            break
                    o = temp[1]
                    d = self.request_queue[elem - self.nBoroughs][0]
                    time_to_job += self.data.travel_stats[o,d,0]

                if time_to_job > self.patience_timeout:
                    actions[index] = self.no_op
                    index += 1
                    continue

                # create dict containing chosen jobs with frequency and indices
                #  check if it is already in the dict
                if elem in dict_actions:
                    dict_actions[elem][0] += 1
                    dict_actions[elem][1].append(index)
                    dict_actions[elem][2].append(time_to_job)

                # only add actions to list which relate to fulfilling a job (instead of rebalancing)
                #elif elem in range(self.data.nBoroughs,self.no_op):
                else:
                    dict_actions[elem] = [1,[index],[time_to_job]]

                # save ttj per agent
                #dict_ttj[elem] = time_to_job

            index += 1

        # dict_actions:
        # key: number of request
        # val[0]: number of agents, who want to take on this request
        # val[1]: index of agents, who want to take on this request
        # val[2]: ttj of agents corresponding to val[1]

        # choose agent who will get the job, if more than one attempted
        change_to_noop = list()
        for key, val in dict_actions.items():
            if val[0] < 2:
                continue

            # choose randomly
            # keeper = np.random.choice(val[1])

            # choose closest agent
            # keeper =  min(val[1], key=dict_ttj.__getitem__)
            # always pick first
            #keeper_id = np.argmin(np.array(val[2]))
            # choose randomly if more than one

            keeper_id = np.random.choice(np.flatnonzero(val[2] == np.min(val[2])))
            keeper = val[1][keeper_id]
            for j in val[1]:
                if j != keeper:
                    change_to_noop.append(j)

        # change action to no-op for all others
        for i in change_to_noop:
            actions[i] = self.no_op

        return actions

    def _reward_function(self, raw_rewards):
        '''
        Reward function shaped according to the given setting:
            - self.reward_param = 0: fully cooperative
            - self.reward_param = 1: fully competitive
            - self.reward_param in (0,1): mixed setting

        Arguments:
            - list of all agents' raw rewards

        Returns:
            - Appropriate reward array
       '''

        # the shared part
        sum_of_rewards = sum(raw_rewards)

        share_per_agent = (1 - self.reward_param) * sum_of_rewards / self.nAgents
        r = np.array(self.nAgents * [share_per_agent])

        # the individual part
        r += np.array([self.reward_param * x for x in raw_rewards])

        return r

    def reduce_request_queue(self):

        reduced_rq = list()
        for elem in self.request_queue:
            reduced_rq.append(elem[0:2]+[elem[-1]])
        return reduced_rq

    def _build_state(self):
        '''
        Builds state from existing components
        '''
        red_rq = self.reduce_request_queue()
        return [self.current_time / self.terminal_time, red_rq, self.location_of_all_agents, self.action_queue]

    def _initialize_agent_locations(self, manually = []):
        '''
        Initializes setup of taxi fleet
        '''
        # assume random placement at first
        # TODO: might get more sophisticated later (where jobs end before cutoff)
        if not manually:
            list_of_locs = np.random.choice(range(self.nBoroughs), self.nAgents).tolist()
            for i in range(self.nAgents):
                list_of_locs[i] = [list_of_locs[i], list_of_locs[i],0]

            return list_of_locs

        else:
            return manually

    def _incoming_requests(self):
        '''
        Filters data for relevant boroughs and currently relevant time window

        Returns:
        - list containing all relevant new requests
        '''

        rq = list()
        # filter out requests in relevant time span
        df = self.data_df[self.current_time]

        for row in df.itertuples():

            if self.scale_down_rq_counter % self.scale_down_factor == 0:

                pu = self.data.boroughs.index(row[2])
                do = self.data.boroughs.index(row[3])
                #pu = 0
                #do = 18
                tt = self.data.travel_stats[pu][do][0]
                r = self.data.travel_stats[pu][do][2]
                pt = self.patience_timeout - (row[4]-self.current_time)

                rq.append([pu,do,tt,r,pt])
            self.scale_down_rq_counter += 1

        return rq

    def split_into_train_test(self):

        ratio = self.train_test_ratio

        full_list_of_data_days = utils.create_full_list_of_data_days()
        list_of_data_days = utils.filter_out_working_days(full_list_of_data_days, self.first_day, self.last_day)
        # fix seed so that test data is always the same
        # but shuffle train data
        train_data, temp_data = train_test_split(list_of_data_days, test_size = ratio[1]+ratio[2], random_state = 1, shuffle = True)
        val_data, test_data = train_test_split(temp_data, test_size = ratio[2]/(ratio[1]+ratio[2]), random_state = 1, shuffle = True)
        np.random.shuffle(train_data)
        as_a_dict = {"train": train_data, "validate": val_data, "test": test_data}

        return as_a_dict

    def pick_data_randomly(self, mode = "train"):
        '''
        Chooses a new data day randomly
        '''

        day = np.random.choice(self.dict_of_list_of_data_days[mode])

        path = "data/taxi_data/" + str(day.year) + "/" + str(day.month).zfill(2) + "/" + str(day.day).zfill(2) + ".pkl"
        df = pd.read_pickle(path)

        # filter out relevant boroughs
        df = df.query('DO_id in @self.data.boroughs and PU_id in @self.data.boroughs')

        bins = [self.step_interval*x for x in range(int(self.terminal_time/self.step_interval)+2)]
        labels = bins[:-1]
        binned_data_df = df.groupby(pd.cut(df.delta, bins, right=False, labels = labels))

        self.data_df = dict(list(binned_data_df))

        self.current_data_day = day

        if self.verbose:
            print("P_D: picked %s-%s-%s" %(str(day.year), str(day.month).zfill(2), str(day.day).zfill(2)))


        return 0

    def pick_data_by_day(self, data_day):

        path = "data/taxi_data/" + str(data_day.year) + "/" + str(data_day.month).zfill(2) + "/" + str(data_day.day).zfill(2) + ".pkl"
        df = pd.read_pickle(path)


        # filter out relevant boroughs
        df = df.query('DO_id in @self.data.boroughs and PU_id in @self.data.boroughs')

        bins = [self.step_interval*x for x in range(int(self.terminal_time/self.step_interval)+2)]
        labels = bins[:-1]
        binned_data_df = df.groupby(pd.cut(df.delta, bins, right=False, labels = labels))

        self.data_df = dict(list(binned_data_df))

        self.current_data_day = data_day

        if self.verbose > 0:
            print("P_D: picked %s-%s-%s" %(str(data_day.year), str(data_day.month).zfill(2), str(data_day.day).zfill(2)))
            print("Total requests: {}, after scaling down: {}".format(len(df), int(len(df) / (self.scale_down_factor))))

        return 0

    def activate_scaling_down(self):
        '''
        self.real_agents: 39,000. this is the number of cars that should be in manhattan
        cutoff_ratio: value to adapt self.real_agents to a smaller problem size
        '''
        #cutoffs = [5,8,13,19,26,38,63]
        # obtained values below via stats: get_scale_ratio_regarding_nBoroughs()
        #list_of_cutoff_ratios = [0.26, 1.07, 3.35, 9.89, 22.44, 54.24, 100]

        # obtained values below via stats: calc_avg_trip_duration
        #possible_trips_per_agent = [125.77, 87.45, 68.89, 58.45, 49.81, 46.47, 39.52]
        #new values without factor 2 (to allow for repositioning) [255.59, 178.02, 142.43, 120.06, 101.94, 95.4, 80.73]

        # obtained values below via stats: get_trips_per_cluster
        #avg_trips_per_cluster_size = [1084.22, 4406.58, 13846.6, 40879.34, 92785.76, 224276.89, 413370.98]

        # obtained values below via stats: get_trips_per_cluster_and_hour and postprocess_trips_per_cluster_and_hour
        # postprocess to match numbers currently chosen
        # max_avg_trips_per_cluster_size = [1881.24, 7468.93, 24087.95, 75457.08, 164987.97, 378751.69, 698738.37]

        idx = self.cutoffs.index(self.nBoroughs)
        temp_factor = self.avg_trips_per_cluster_size[idx] / (self.nAgents * self.possible_trips_per_agent[idx])
        self.scale_down_factor = max(1, int(temp_factor))
        if temp_factor < 1:
            print("Too many agents for the given nBoroughs: {} > {}".format(self.nAgents, round(self.avg_trips_per_cluster_size[idx] / self.possible_trips_per_agent[idx],1)))
            print("With the given nAgents, they would receive fewer requests than in larger settings")

        #  [400, 550, 550, 550, 700, 700, 800] / [338.04, 485.34, 606.62, 719.62, 847.54, 905.63, 1070.22]
        # will result in: [2, 2, 1, 1, 1, 1, 1]
        self.request_queue_limit = (int(self.patience_timeout / self.avg_trip_duration[idx]) + 1 ) * self.nAgents

        if self.verbose > -1:
            print("env.SCALING_DOWN: From now on, only every {}th request is selected from the data. (temp_factor: {})".format(self.scale_down_factor, temp_factor))
            print("env.SCALING_DOWN: request_queue_limit set to {}".format(self.request_queue_limit))

        return 0


    def ____retrieve_travel_route(self, o, d):
        '''
        Retrieves travel route from all_routes.csv
        '''

        df = self.route_df #all_routes
        o = self.data.boroughs[o]
        d = self.data.boroughs[d]

        full_number = df[(df["origin_ID"] == o) & (df["destination_ID"] == d)].route.values[0]

        reduced = list()
        for elem in full_number:
            reduced.append(self.data.boroughs.index(elem))

        return reduced

    def ___calculate_gas_cost(self, o, d, time):
        '''
        Calculates gas cost for the given route
        '''

        if o == d:
            return 0
        else:
            return round(time / self.data.travel_stats[o,d,0] * self.data.travel_stats[o,d,1] * self.gas_cost,3)

    def transform_state_to_input(self, state):
        '''
        Transforms the weirdly shaped state into a flat np.array
        '''

        t = state[0]
        rq = state[1]
        loc = state[2]
        aq = state[3]

        new_repr = list()

        # time
        new_repr.append(t)

        # rq
        counter = 0
        for i in rq:
            for j in i:
                new_repr.append(j)
            counter += 1
        for i in range(self.request_queue_limit - counter):
            for j in range(self.len_rq_entry):
                new_repr.append(0.0)

        # locations
        for i in loc:
            for j in i:
                new_repr.append(j)

        # aq
        for i in aq:
            counter = 0
            for j in i:
                for k in j:
                    new_repr.append(k)
                counter += 1
            for j in range(self.action_queue_limit - counter):
                for k in range(self.len_aq_entry):
                    new_repr.append(0)

        assert len(new_repr) == self.state_len, "returning array of length %s, should be length %s, state: %s" %(len(new_repr), self.state_len, state)

        return np.array(new_repr)
