# %% imports
import common.data_downloading as dd
import common.data_preprocessing as dp
import common.env as envi
import a2c_rework as a2c

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import sys
from pandas.tseries import holiday
from calendar import monthrange
import datetime
import pickle

def prep_pipeline(nAgents = 2, nBoroughs = 63, reward_param = 0.5, # for env
                  lr_a = 0.0001, setting = "iac", all_actions = False, #for trainer
                  seed = 1, verbosity = 1, **kwargs):
    '''
    takes:
        - seed (int): seed to ensure reproducibility
    returns:
        - env (AmodEnv): environment model
    '''

    tf.random.set_seed(seed)
    np.random.seed(seed)

    downloader = dd.dataDownloader()
    data = dp.inputData(downloader, verbose = False)
    #data.check_or_create_pickles()
    env = envi.AmodEnv(data = data, nAgents = nAgents, nBoroughs = nBoroughs,
                       reward_param = reward_param,
                       seed = seed, verbose = verbosity)
    #env = envi.AmodEnv(data, nAgents = nAgents, nBoroughs = nBoroughs, rq_limit = rq_limit, step_interval = 10, reward_param = 0, seed = seed, verbose = verbosity)

    print("Created all necessary objects: \n - {} \n - {} \n - {}".format(downloader,data,env))

    return env

def prep_pipeline_final(nAgents = 2, nBoroughs = 63, step_interval = 10, rq_limit = 50, reward_param = 0.5, patience = 5 * 60, gas_cost = -0.00056 / 1.609, train_test_ratio = (0.8, 0.1, 0.1),
                  lr_a = 0.0001, lr_c_factor = 5, nNodes = 64, nLayers = 2, activation = "relu", setting = "iac", all_actions = False,
                  seed = 1, verbosity = 1, **kwargs):
    '''
    takes:
        - numerous parameters, usually given in kwarg-dicts
    returns:
        - trainer object
    '''

    tf.random.set_seed(seed)
    np.random.seed(seed)

    downloader = dd.dataDownloader()

    data = dp.inputData(downloader, verbose = verbosity)

    env = envi.AmodEnv(data = data, nAgents = nAgents, nBoroughs = nBoroughs,
                       step_interval = step_interval, rq_limit = rq_limit, reward_param = reward_param,
                       patience_timeout = patience, gas_cost = gas_cost, train_test_ratio = train_test_ratio,
                       seed = seed, verbose = verbosity)

    trainer = a2c.trainer = a2c.unifiedTrainer(env, setting, all_actions = all_actions)

    print("Created all necessary objects: \n - {} \n - {} \n - {} \n - {}".format(downloader,data,env,trainer))

    return trainer

def log_wrapper_before(algo, nSteps, env):
    old_stdout = sys.stdout
    sys.stdout = open('logs/log_'+ algo + '.txt','a')
    print("Running the %s with %s agents in %s zones for %s steps in day %s" %(algo, env.nAgents, env.data.nBoroughs, nSteps, env.current_data_day))
    return old_stdout

def log_wrapper_after(old_stdout):
    sys.stdout.close()
    sys.stdout = old_stdout
    print('hi again')

def save_to_pickle(path, object_to_pickle):
    '''
    will save object to path, automatically adding a timestamp
    '''

    timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    object_to_pickle = object_to_pickle
    with open(path + "-" + timestamp + ".pkl", 'wb') as f:
        pickle.dump(object_to_pickle, f)

def load_pickle(path, include_ending = False):
    if include_ending:
        ending = ".pkl"
    else:
        ending = ""
    pkl_file = open(path + ending, 'rb')
    restored_from_pickle = pickle.load(pkl_file)
    pkl_file.close()

    return restored_from_pickle

#### needed this for problem size reduction ####
def check_for_zones_helper(rel_routes, sel_zones, cutoff):
    for row in rel_routes.itertuples():
        for elem in row[5]:
            if elem not in sel_zones:
                print(row)

def check_for_zones(dataPreprocessor):
    cluster_south = [12,88,87,261,13,231,209,45,232,148,144,211,125,158,249,114,113,79,4,224,107,234,90,68,246,186,137,164,170,100,233,50,48,230,161,162,229,163]
    cutoffs = [5,8,13,19,26,38]

    print("Inputs given: ")
    print("    - ordered list of zones: ", cluster_south)
    print("    - cutoff values, i.e. eligible values for nBoroughs: ", cutoffs)

    all_routes = dataPreprocessor.list_of_dfs[2]

    for cutoff in cutoffs:
        sel_zones = cluster_south[:cutoff]
        print(sel_zones)
        rel_routes = all_routes.query("origin_ID in @sel_zones and destination_ID in @sel_zones")
        check_for_zones_helper(rel_routes, sel_zones, cutoff)


def create_full_list_of_data_days():
    years = [2018,2019]
    months = range(1,13)

    full_list_of_data_days = list()

    for year in years:
        for month in months:
            for day in range(monthrange(year, month)[1]):
                full_list_of_data_days.append(datetime.datetime(year,month,day+1))

    return full_list_of_data_days

def filter_out_working_days(full_list_of_data_days, first_day = datetime.datetime(2018,1,1), last_day = datetime.datetime(2019,12,31)):

    cal = holiday.USFederalHolidayCalendar()
    holidays = cal.holidays(start = first_day, end = last_day).to_pydatetime()

    print("length before reduction: ", len(full_list_of_data_days))

    for day in full_list_of_data_days[:]:

        if day.weekday() > 4:

            full_list_of_data_days.remove(day)
            #print("removed: ", day, day.weekday())

        if day in holidays:

            full_list_of_data_days.remove(day)
            #print("removed: ", day, day.weekday())

    print("length after reduction: ", len(full_list_of_data_days))

    return full_list_of_data_days
# define statistics over multiple runs etc....
