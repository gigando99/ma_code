import os
#import csv
#import pandas as pd
#import re
#import datetime
import wget




class dataDownloader():

    def __init__(self, verbose = 2):
        self.years = [2018, 2019]
        self.months = range(1,13)
        self.types = ["yellow", "green", "fhv"]
        self.verbose = verbose
        
    def __repr__(self):
        return "Object for downloading of data: dataDownloader(years = %s)" %(self.years)

    def download_taxi_data(self):
        '''
        checks if files are already downloaded
        if not, downloads data files
        '''
        for i in self.years:
            for j in self.months:
                for k in self.types:
                    paths = self.generate_file_path("target", k, i, j)
                    #print(os.listdir(), paths[0])
                    if paths[1] in os.listdir(paths[0]):
                        #print("already there: ", paths[1], "at location ", paths[0]+paths[1])
                        #print("already there: ", paths[1])
                        pass
                    else:
                        url = self.generate_file_path("raw", k, i, j)
                        url = url[0]+url[1]
                        #print("not there: ", paths[1])
                        #url = self.generate_file_path("raw", k, i, j)
                        #print("download via: ", url, "access via ", paths[0]+paths[1])
                        #wget.download(url[0]+url[1], out=paths[0]+paths[1])
                        wget.download(url, out=paths[0]+paths[1])
                        #df = pd.read_csv(url[0]+url[1])
                        #df.to_csv(paths[0]+paths[1])
                        
                # new type after january 2019
                if i == 2019 and j > 1:
                    k = "fhvhv"
                    paths = self.generate_file_path("target", k, i, j)
                    if paths[1] in os.listdir(paths[0]):
                        pass
                    else:
                        url = self.generate_file_path("raw", k, i, j)
                        url = url[0]+url[1]
                        wget.download(url, out=paths[0]+paths[1])
                    
        if self.verbose > 1:
            print(self, ": \nAll files have been downloaded and saved to the specified location. \n")
        

        return 0


    def generate_file_path(self, raw_or_target, type, year, month):
        '''
        generates file paths to either NYCTC's raw data ("raw") or own folder (target)
        '''

        file = type + "_tripdata_" + str(year) + "-" + str(month).zfill(2) + ".csv"

        if raw_or_target == "raw":
             return "https://s3.amazonaws.com/nyc-tlc/trip+data/", file
        elif raw_or_target == "target":
            return "data/taxi_data/raw/", file
