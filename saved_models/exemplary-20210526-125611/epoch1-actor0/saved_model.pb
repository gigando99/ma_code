��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
,
Exp
x"T
y"T"
Ttype:

2
�
GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"

batch_dimsint "
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype�
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
;
Sub
x"T
y"T
z"T"
Ttype:
2	
�
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.4.12v2.4.0-49-g85c8b2a817f8��
�
actor/actor_i/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�d*%
shared_nameactor/actor_i/kernel
~
(actor/actor_i/kernel/Read/ReadVariableOpReadVariableOpactor/actor_i/kernel*
_output_shapes
:	�d*
dtype0
|
actor/actor_i/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*#
shared_nameactor/actor_i/bias
u
&actor/actor_i/bias/Read/ReadVariableOpReadVariableOpactor/actor_i/bias*
_output_shapes
:d*
dtype0
�
actor/actor_o/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*%
shared_nameactor/actor_o/kernel
}
(actor/actor_o/kernel/Read/ReadVariableOpReadVariableOpactor/actor_o/kernel*
_output_shapes

:@*
dtype0
|
actor/actor_o/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameactor/actor_o/bias
u
&actor/actor_o/bias/Read/ReadVariableOpReadVariableOpactor/actor_o/bias*
_output_shapes
:*
dtype0
�
actor/actor_hl0/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d@*'
shared_nameactor/actor_hl0/kernel
�
*actor/actor_hl0/kernel/Read/ReadVariableOpReadVariableOpactor/actor_hl0/kernel*
_output_shapes

:d@*
dtype0
�
actor/actor_hl0/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameactor/actor_hl0/bias
y
(actor/actor_hl0/bias/Read/ReadVariableOpReadVariableOpactor/actor_hl0/bias*
_output_shapes
:@*
dtype0
�
actor/actor_hl1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*'
shared_nameactor/actor_hl1/kernel
�
*actor/actor_hl1/kernel/Read/ReadVariableOpReadVariableOpactor/actor_hl1/kernel*
_output_shapes

:@@*
dtype0
�
actor/actor_hl1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameactor/actor_hl1/bias
y
(actor/actor_hl1/bias/Read/ReadVariableOpReadVariableOpactor/actor_hl1/bias*
_output_shapes
:@*
dtype0

NoOpNoOp
�
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�
value�B� B�
�
i
hidden_layers
o
regularization_losses
trainable_variables
	variables
	keras_api

signatures
h

	kernel

bias
	variables
regularization_losses
trainable_variables
	keras_api

0
1
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
 
8
	0

1
2
3
4
5
6
7
8
	0

1
2
3
4
5
6
7
�

layers
layer_regularization_losses
non_trainable_variables
regularization_losses
layer_metrics
metrics
trainable_variables
	variables
 
MK
VARIABLE_VALUEactor/actor_i/kernel#i/kernel/.ATTRIBUTES/VARIABLE_VALUE
IG
VARIABLE_VALUEactor/actor_i/bias!i/bias/.ATTRIBUTES/VARIABLE_VALUE

	0

1
 

	0

1
�
	variables

 layers
!layer_regularization_losses
"non_trainable_variables
regularization_losses
#metrics
trainable_variables
$layer_metrics
h

kernel
bias
%	variables
&regularization_losses
'trainable_variables
(	keras_api
h

kernel
bias
)	variables
*regularization_losses
+trainable_variables
,	keras_api
MK
VARIABLE_VALUEactor/actor_o/kernel#o/kernel/.ATTRIBUTES/VARIABLE_VALUE
IG
VARIABLE_VALUEactor/actor_o/bias!o/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
	variables

-layers
.layer_regularization_losses
/non_trainable_variables
regularization_losses
0metrics
trainable_variables
1layer_metrics
\Z
VARIABLE_VALUEactor/actor_hl0/kernel0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEactor/actor_hl0/bias0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEactor/actor_hl1/kernel0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEactor/actor_hl1/bias0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUE

0
1
2
3
 
 
 
 
 
 
 
 
 

0
1
 

0
1
�
%	variables

2layers
3layer_regularization_losses
4non_trainable_variables
&regularization_losses
5metrics
'trainable_variables
6layer_metrics

0
1
 

0
1
�
)	variables

7layers
8layer_regularization_losses
9non_trainable_variables
*regularization_losses
:metrics
+trainable_variables
;layer_metrics
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
�
serving_default_input_1Placeholder*,
_output_shapes
:����������*
dtype0*!
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1actor/actor_i/kernelactor/actor_i/biasactor/actor_hl0/kernelactor/actor_hl0/biasactor/actor_hl1/kernelactor/actor_hl1/biasactor/actor_o/kernelactor/actor_o/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference_signature_wrapper_38552
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename(actor/actor_i/kernel/Read/ReadVariableOp&actor/actor_i/bias/Read/ReadVariableOp(actor/actor_o/kernel/Read/ReadVariableOp&actor/actor_o/bias/Read/ReadVariableOp*actor/actor_hl0/kernel/Read/ReadVariableOp(actor/actor_hl0/bias/Read/ReadVariableOp*actor/actor_hl1/kernel/Read/ReadVariableOp(actor/actor_hl1/bias/Read/ReadVariableOpConst*
Tin
2
*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *'
f"R 
__inference__traced_save_38764
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameactor/actor_i/kernelactor/actor_i/biasactor/actor_o/kernelactor/actor_o/biasactor/actor_hl0/kernelactor/actor_hl0/biasactor/actor_hl1/kernelactor/actor_hl1/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__traced_restore_38798��
�
|
'__inference_actor_i_layer_call_fn_38591

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������d*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_actor_i_layer_call_and_return_conditional_losses_383432
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������d2

Identity"
identityIdentity:output:0*3
_input_shapes"
 :����������::22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:����������
 
_user_specified_nameinputs
��
�
 __inference__wrapped_model_38309
input_13
/actor_actor_i_tensordot_readvariableop_resource1
-actor_actor_i_biasadd_readvariableop_resource5
1actor_actor_hl0_tensordot_readvariableop_resource3
/actor_actor_hl0_biasadd_readvariableop_resource5
1actor_actor_hl1_tensordot_readvariableop_resource3
/actor_actor_hl1_biasadd_readvariableop_resource3
/actor_actor_o_tensordot_readvariableop_resource1
-actor_actor_o_biasadd_readvariableop_resource
identity��&actor/actor_hl0/BiasAdd/ReadVariableOp�(actor/actor_hl0/Tensordot/ReadVariableOp�&actor/actor_hl1/BiasAdd/ReadVariableOp�(actor/actor_hl1/Tensordot/ReadVariableOp�$actor/actor_i/BiasAdd/ReadVariableOp�&actor/actor_i/Tensordot/ReadVariableOp�$actor/actor_o/BiasAdd/ReadVariableOp�&actor/actor_o/Tensordot/ReadVariableOp�
&actor/actor_i/Tensordot/ReadVariableOpReadVariableOp/actor_actor_i_tensordot_readvariableop_resource*
_output_shapes
:	�d*
dtype02(
&actor/actor_i/Tensordot/ReadVariableOp�
actor/actor_i/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
actor/actor_i/Tensordot/axes�
actor/actor_i/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
actor/actor_i/Tensordot/freeu
actor/actor_i/Tensordot/ShapeShapeinput_1*
T0*
_output_shapes
:2
actor/actor_i/Tensordot/Shape�
%actor/actor_i/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_i/Tensordot/GatherV2/axis�
 actor/actor_i/Tensordot/GatherV2GatherV2&actor/actor_i/Tensordot/Shape:output:0%actor/actor_i/Tensordot/free:output:0.actor/actor_i/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2"
 actor/actor_i/Tensordot/GatherV2�
'actor/actor_i/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_i/Tensordot/GatherV2_1/axis�
"actor/actor_i/Tensordot/GatherV2_1GatherV2&actor/actor_i/Tensordot/Shape:output:0%actor/actor_i/Tensordot/axes:output:00actor/actor_i/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_i/Tensordot/GatherV2_1�
actor/actor_i/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
actor/actor_i/Tensordot/Const�
actor/actor_i/Tensordot/ProdProd)actor/actor_i/Tensordot/GatherV2:output:0&actor/actor_i/Tensordot/Const:output:0*
T0*
_output_shapes
: 2
actor/actor_i/Tensordot/Prod�
actor/actor_i/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_i/Tensordot/Const_1�
actor/actor_i/Tensordot/Prod_1Prod+actor/actor_i/Tensordot/GatherV2_1:output:0(actor/actor_i/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2 
actor/actor_i/Tensordot/Prod_1�
#actor/actor_i/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2%
#actor/actor_i/Tensordot/concat/axis�
actor/actor_i/Tensordot/concatConcatV2%actor/actor_i/Tensordot/free:output:0%actor/actor_i/Tensordot/axes:output:0,actor/actor_i/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2 
actor/actor_i/Tensordot/concat�
actor/actor_i/Tensordot/stackPack%actor/actor_i/Tensordot/Prod:output:0'actor/actor_i/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
actor/actor_i/Tensordot/stack�
!actor/actor_i/Tensordot/transpose	Transposeinput_1'actor/actor_i/Tensordot/concat:output:0*
T0*,
_output_shapes
:����������2#
!actor/actor_i/Tensordot/transpose�
actor/actor_i/Tensordot/ReshapeReshape%actor/actor_i/Tensordot/transpose:y:0&actor/actor_i/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2!
actor/actor_i/Tensordot/Reshape�
actor/actor_i/Tensordot/MatMulMatMul(actor/actor_i/Tensordot/Reshape:output:0.actor/actor_i/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������d2 
actor/actor_i/Tensordot/MatMul�
actor/actor_i/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2!
actor/actor_i/Tensordot/Const_2�
%actor/actor_i/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_i/Tensordot/concat_1/axis�
 actor/actor_i/Tensordot/concat_1ConcatV2)actor/actor_i/Tensordot/GatherV2:output:0(actor/actor_i/Tensordot/Const_2:output:0.actor/actor_i/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_i/Tensordot/concat_1�
actor/actor_i/TensordotReshape(actor/actor_i/Tensordot/MatMul:product:0)actor/actor_i/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������d2
actor/actor_i/Tensordot�
$actor/actor_i/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_i_biasadd_readvariableop_resource*
_output_shapes
:d*
dtype02&
$actor/actor_i/BiasAdd/ReadVariableOp�
actor/actor_i/BiasAddBiasAdd actor/actor_i/Tensordot:output:0,actor/actor_i/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������d2
actor/actor_i/BiasAdd�
(actor/actor_hl0/Tensordot/ReadVariableOpReadVariableOp1actor_actor_hl0_tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02*
(actor/actor_hl0/Tensordot/ReadVariableOp�
actor/actor_hl0/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
actor/actor_hl0/Tensordot/axes�
actor/actor_hl0/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
actor/actor_hl0/Tensordot/free�
actor/actor_hl0/Tensordot/ShapeShapeactor/actor_i/BiasAdd:output:0*
T0*
_output_shapes
:2!
actor/actor_hl0/Tensordot/Shape�
'actor/actor_hl0/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl0/Tensordot/GatherV2/axis�
"actor/actor_hl0/Tensordot/GatherV2GatherV2(actor/actor_hl0/Tensordot/Shape:output:0'actor/actor_hl0/Tensordot/free:output:00actor/actor_hl0/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_hl0/Tensordot/GatherV2�
)actor/actor_hl0/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)actor/actor_hl0/Tensordot/GatherV2_1/axis�
$actor/actor_hl0/Tensordot/GatherV2_1GatherV2(actor/actor_hl0/Tensordot/Shape:output:0'actor/actor_hl0/Tensordot/axes:output:02actor/actor_hl0/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$actor/actor_hl0/Tensordot/GatherV2_1�
actor/actor_hl0/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_hl0/Tensordot/Const�
actor/actor_hl0/Tensordot/ProdProd+actor/actor_hl0/Tensordot/GatherV2:output:0(actor/actor_hl0/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
actor/actor_hl0/Tensordot/Prod�
!actor/actor_hl0/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!actor/actor_hl0/Tensordot/Const_1�
 actor/actor_hl0/Tensordot/Prod_1Prod-actor/actor_hl0/Tensordot/GatherV2_1:output:0*actor/actor_hl0/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 actor/actor_hl0/Tensordot/Prod_1�
%actor/actor_hl0/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_hl0/Tensordot/concat/axis�
 actor/actor_hl0/Tensordot/concatConcatV2'actor/actor_hl0/Tensordot/free:output:0'actor/actor_hl0/Tensordot/axes:output:0.actor/actor_hl0/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_hl0/Tensordot/concat�
actor/actor_hl0/Tensordot/stackPack'actor/actor_hl0/Tensordot/Prod:output:0)actor/actor_hl0/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
actor/actor_hl0/Tensordot/stack�
#actor/actor_hl0/Tensordot/transpose	Transposeactor/actor_i/BiasAdd:output:0)actor/actor_hl0/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������d2%
#actor/actor_hl0/Tensordot/transpose�
!actor/actor_hl0/Tensordot/ReshapeReshape'actor/actor_hl0/Tensordot/transpose:y:0(actor/actor_hl0/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2#
!actor/actor_hl0/Tensordot/Reshape�
 actor/actor_hl0/Tensordot/MatMulMatMul*actor/actor_hl0/Tensordot/Reshape:output:00actor/actor_hl0/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2"
 actor/actor_hl0/Tensordot/MatMul�
!actor/actor_hl0/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!actor/actor_hl0/Tensordot/Const_2�
'actor/actor_hl0/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl0/Tensordot/concat_1/axis�
"actor/actor_hl0/Tensordot/concat_1ConcatV2+actor/actor_hl0/Tensordot/GatherV2:output:0*actor/actor_hl0/Tensordot/Const_2:output:00actor/actor_hl0/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"actor/actor_hl0/Tensordot/concat_1�
actor/actor_hl0/TensordotReshape*actor/actor_hl0/Tensordot/MatMul:product:0+actor/actor_hl0/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
actor/actor_hl0/Tensordot�
&actor/actor_hl0/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl0_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl0/BiasAdd/ReadVariableOp�
actor/actor_hl0/BiasAddBiasAdd"actor/actor_hl0/Tensordot:output:0.actor/actor_hl0/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2
actor/actor_hl0/BiasAdd�
actor/actor_hl0/ReluRelu actor/actor_hl0/BiasAdd:output:0*
T0*+
_output_shapes
:���������@2
actor/actor_hl0/Relu�
(actor/actor_hl1/Tensordot/ReadVariableOpReadVariableOp1actor_actor_hl1_tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02*
(actor/actor_hl1/Tensordot/ReadVariableOp�
actor/actor_hl1/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
actor/actor_hl1/Tensordot/axes�
actor/actor_hl1/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
actor/actor_hl1/Tensordot/free�
actor/actor_hl1/Tensordot/ShapeShape"actor/actor_hl0/Relu:activations:0*
T0*
_output_shapes
:2!
actor/actor_hl1/Tensordot/Shape�
'actor/actor_hl1/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl1/Tensordot/GatherV2/axis�
"actor/actor_hl1/Tensordot/GatherV2GatherV2(actor/actor_hl1/Tensordot/Shape:output:0'actor/actor_hl1/Tensordot/free:output:00actor/actor_hl1/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_hl1/Tensordot/GatherV2�
)actor/actor_hl1/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)actor/actor_hl1/Tensordot/GatherV2_1/axis�
$actor/actor_hl1/Tensordot/GatherV2_1GatherV2(actor/actor_hl1/Tensordot/Shape:output:0'actor/actor_hl1/Tensordot/axes:output:02actor/actor_hl1/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$actor/actor_hl1/Tensordot/GatherV2_1�
actor/actor_hl1/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_hl1/Tensordot/Const�
actor/actor_hl1/Tensordot/ProdProd+actor/actor_hl1/Tensordot/GatherV2:output:0(actor/actor_hl1/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
actor/actor_hl1/Tensordot/Prod�
!actor/actor_hl1/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!actor/actor_hl1/Tensordot/Const_1�
 actor/actor_hl1/Tensordot/Prod_1Prod-actor/actor_hl1/Tensordot/GatherV2_1:output:0*actor/actor_hl1/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 actor/actor_hl1/Tensordot/Prod_1�
%actor/actor_hl1/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_hl1/Tensordot/concat/axis�
 actor/actor_hl1/Tensordot/concatConcatV2'actor/actor_hl1/Tensordot/free:output:0'actor/actor_hl1/Tensordot/axes:output:0.actor/actor_hl1/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_hl1/Tensordot/concat�
actor/actor_hl1/Tensordot/stackPack'actor/actor_hl1/Tensordot/Prod:output:0)actor/actor_hl1/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
actor/actor_hl1/Tensordot/stack�
#actor/actor_hl1/Tensordot/transpose	Transpose"actor/actor_hl0/Relu:activations:0)actor/actor_hl1/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������@2%
#actor/actor_hl1/Tensordot/transpose�
!actor/actor_hl1/Tensordot/ReshapeReshape'actor/actor_hl1/Tensordot/transpose:y:0(actor/actor_hl1/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2#
!actor/actor_hl1/Tensordot/Reshape�
 actor/actor_hl1/Tensordot/MatMulMatMul*actor/actor_hl1/Tensordot/Reshape:output:00actor/actor_hl1/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2"
 actor/actor_hl1/Tensordot/MatMul�
!actor/actor_hl1/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!actor/actor_hl1/Tensordot/Const_2�
'actor/actor_hl1/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl1/Tensordot/concat_1/axis�
"actor/actor_hl1/Tensordot/concat_1ConcatV2+actor/actor_hl1/Tensordot/GatherV2:output:0*actor/actor_hl1/Tensordot/Const_2:output:00actor/actor_hl1/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"actor/actor_hl1/Tensordot/concat_1�
actor/actor_hl1/TensordotReshape*actor/actor_hl1/Tensordot/MatMul:product:0+actor/actor_hl1/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
actor/actor_hl1/Tensordot�
&actor/actor_hl1/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl1/BiasAdd/ReadVariableOp�
actor/actor_hl1/BiasAddBiasAdd"actor/actor_hl1/Tensordot:output:0.actor/actor_hl1/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2
actor/actor_hl1/BiasAdd�
actor/actor_hl1/ReluRelu actor/actor_hl1/BiasAdd:output:0*
T0*+
_output_shapes
:���������@2
actor/actor_hl1/Relu�
&actor/actor_o/Tensordot/ReadVariableOpReadVariableOp/actor_actor_o_tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02(
&actor/actor_o/Tensordot/ReadVariableOp�
actor/actor_o/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
actor/actor_o/Tensordot/axes�
actor/actor_o/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
actor/actor_o/Tensordot/free�
actor/actor_o/Tensordot/ShapeShape"actor/actor_hl1/Relu:activations:0*
T0*
_output_shapes
:2
actor/actor_o/Tensordot/Shape�
%actor/actor_o/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_o/Tensordot/GatherV2/axis�
 actor/actor_o/Tensordot/GatherV2GatherV2&actor/actor_o/Tensordot/Shape:output:0%actor/actor_o/Tensordot/free:output:0.actor/actor_o/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2"
 actor/actor_o/Tensordot/GatherV2�
'actor/actor_o/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_o/Tensordot/GatherV2_1/axis�
"actor/actor_o/Tensordot/GatherV2_1GatherV2&actor/actor_o/Tensordot/Shape:output:0%actor/actor_o/Tensordot/axes:output:00actor/actor_o/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_o/Tensordot/GatherV2_1�
actor/actor_o/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
actor/actor_o/Tensordot/Const�
actor/actor_o/Tensordot/ProdProd)actor/actor_o/Tensordot/GatherV2:output:0&actor/actor_o/Tensordot/Const:output:0*
T0*
_output_shapes
: 2
actor/actor_o/Tensordot/Prod�
actor/actor_o/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_o/Tensordot/Const_1�
actor/actor_o/Tensordot/Prod_1Prod+actor/actor_o/Tensordot/GatherV2_1:output:0(actor/actor_o/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2 
actor/actor_o/Tensordot/Prod_1�
#actor/actor_o/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2%
#actor/actor_o/Tensordot/concat/axis�
actor/actor_o/Tensordot/concatConcatV2%actor/actor_o/Tensordot/free:output:0%actor/actor_o/Tensordot/axes:output:0,actor/actor_o/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2 
actor/actor_o/Tensordot/concat�
actor/actor_o/Tensordot/stackPack%actor/actor_o/Tensordot/Prod:output:0'actor/actor_o/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
actor/actor_o/Tensordot/stack�
!actor/actor_o/Tensordot/transpose	Transpose"actor/actor_hl1/Relu:activations:0'actor/actor_o/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������@2#
!actor/actor_o/Tensordot/transpose�
actor/actor_o/Tensordot/ReshapeReshape%actor/actor_o/Tensordot/transpose:y:0&actor/actor_o/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2!
actor/actor_o/Tensordot/Reshape�
actor/actor_o/Tensordot/MatMulMatMul(actor/actor_o/Tensordot/Reshape:output:0.actor/actor_o/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2 
actor/actor_o/Tensordot/MatMul�
actor/actor_o/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2!
actor/actor_o/Tensordot/Const_2�
%actor/actor_o/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_o/Tensordot/concat_1/axis�
 actor/actor_o/Tensordot/concat_1ConcatV2)actor/actor_o/Tensordot/GatherV2:output:0(actor/actor_o/Tensordot/Const_2:output:0.actor/actor_o/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_o/Tensordot/concat_1�
actor/actor_o/TensordotReshape(actor/actor_o/Tensordot/MatMul:product:0)actor/actor_o/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������2
actor/actor_o/Tensordot�
$actor/actor_o/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_o_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02&
$actor/actor_o/BiasAdd/ReadVariableOp�
actor/actor_o/BiasAddBiasAdd actor/actor_o/Tensordot:output:0,actor/actor_o/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������2
actor/actor_o/BiasAdd�
#actor/actor_o/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
���������2%
#actor/actor_o/Max/reduction_indices�
actor/actor_o/MaxMaxactor/actor_o/BiasAdd:output:0,actor/actor_o/Max/reduction_indices:output:0*
T0*+
_output_shapes
:���������*
	keep_dims(2
actor/actor_o/Max�
actor/actor_o/subSubactor/actor_o/BiasAdd:output:0actor/actor_o/Max:output:0*
T0*+
_output_shapes
:���������2
actor/actor_o/subz
actor/actor_o/ExpExpactor/actor_o/sub:z:0*
T0*+
_output_shapes
:���������2
actor/actor_o/Exp�
#actor/actor_o/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
���������2%
#actor/actor_o/Sum/reduction_indices�
actor/actor_o/SumSumactor/actor_o/Exp:y:0,actor/actor_o/Sum/reduction_indices:output:0*
T0*+
_output_shapes
:���������*
	keep_dims(2
actor/actor_o/Sum�
actor/actor_o/truedivRealDivactor/actor_o/Exp:y:0actor/actor_o/Sum:output:0*
T0*+
_output_shapes
:���������2
actor/actor_o/truediv�
IdentityIdentityactor/actor_o/truediv:z:0'^actor/actor_hl0/BiasAdd/ReadVariableOp)^actor/actor_hl0/Tensordot/ReadVariableOp'^actor/actor_hl1/BiasAdd/ReadVariableOp)^actor/actor_hl1/Tensordot/ReadVariableOp%^actor/actor_i/BiasAdd/ReadVariableOp'^actor/actor_i/Tensordot/ReadVariableOp%^actor/actor_o/BiasAdd/ReadVariableOp'^actor/actor_o/Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*K
_input_shapes:
8:����������::::::::2P
&actor/actor_hl0/BiasAdd/ReadVariableOp&actor/actor_hl0/BiasAdd/ReadVariableOp2T
(actor/actor_hl0/Tensordot/ReadVariableOp(actor/actor_hl0/Tensordot/ReadVariableOp2P
&actor/actor_hl1/BiasAdd/ReadVariableOp&actor/actor_hl1/BiasAdd/ReadVariableOp2T
(actor/actor_hl1/Tensordot/ReadVariableOp(actor/actor_hl1/Tensordot/ReadVariableOp2L
$actor/actor_i/BiasAdd/ReadVariableOp$actor/actor_i/BiasAdd/ReadVariableOp2P
&actor/actor_i/Tensordot/ReadVariableOp&actor/actor_i/Tensordot/ReadVariableOp2L
$actor/actor_o/BiasAdd/ReadVariableOp$actor/actor_o/BiasAdd/ReadVariableOp2P
&actor/actor_o/Tensordot/ReadVariableOp&actor/actor_o/Tensordot/ReadVariableOp:U Q
,
_output_shapes
:����������
!
_user_specified_name	input_1
� 
�
D__inference_actor_hl1_layer_call_and_return_conditional_losses_38708

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
� 
�
D__inference_actor_hl0_layer_call_and_return_conditional_losses_38390

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������d2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������d::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������d
 
_user_specified_nameinputs
�%
�
B__inference_actor_o_layer_call_and_return_conditional_losses_38628

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������2	
BiasAddy
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
���������2
Max/reduction_indices�
MaxMaxBiasAdd:output:0Max/reduction_indices:output:0*
T0*+
_output_shapes
:���������*
	keep_dims(2
Maxg
subSubBiasAdd:output:0Max:output:0*
T0*+
_output_shapes
:���������2
subP
ExpExpsub:z:0*
T0*+
_output_shapes
:���������2
Expy
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
���������2
Sum/reduction_indices�
SumSumExp:y:0Sum/reduction_indices:output:0*
T0*+
_output_shapes
:���������*
	keep_dims(2
Sumj
truedivRealDivExp:y:0Sum:output:0*
T0*+
_output_shapes
:���������2	
truediv�
IdentityIdentitytruediv:z:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
� 
�
D__inference_actor_hl1_layer_call_and_return_conditional_losses_38437

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
~
)__inference_actor_hl0_layer_call_fn_38677

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_actor_hl0_layer_call_and_return_conditional_losses_383902
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������d::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������d
 
_user_specified_nameinputs
�
�
B__inference_actor_i_layer_call_and_return_conditional_losses_38343

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes
:	�d*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*,
_output_shapes
:����������2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������d2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������d2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������d2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������d2

Identity"
identityIdentity:output:0*3
_input_shapes"
 :����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:T P
,
_output_shapes
:����������
 
_user_specified_nameinputs
�
~
)__inference_actor_hl1_layer_call_fn_38717

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_actor_hl1_layer_call_and_return_conditional_losses_384372
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
|
'__inference_actor_o_layer_call_fn_38637

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_actor_o_layer_call_and_return_conditional_losses_384902
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�%
�
B__inference_actor_o_layer_call_and_return_conditional_losses_38490

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������2	
BiasAddy
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
���������2
Max/reduction_indices�
MaxMaxBiasAdd:output:0Max/reduction_indices:output:0*
T0*+
_output_shapes
:���������*
	keep_dims(2
Maxg
subSubBiasAdd:output:0Max:output:0*
T0*+
_output_shapes
:���������2
subP
ExpExpsub:z:0*
T0*+
_output_shapes
:���������2
Expy
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
���������2
Sum/reduction_indices�
SumSumExp:y:0Sum/reduction_indices:output:0*
T0*+
_output_shapes
:���������*
	keep_dims(2
Sumj
truedivRealDivExp:y:0Sum:output:0*
T0*+
_output_shapes
:���������2	
truediv�
IdentityIdentitytruediv:z:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
__inference__traced_save_38764
file_prefix3
/savev2_actor_actor_i_kernel_read_readvariableop1
-savev2_actor_actor_i_bias_read_readvariableop3
/savev2_actor_actor_o_kernel_read_readvariableop1
-savev2_actor_actor_o_bias_read_readvariableop5
1savev2_actor_actor_hl0_kernel_read_readvariableop3
/savev2_actor_actor_hl0_bias_read_readvariableop5
1savev2_actor_actor_hl1_kernel_read_readvariableop3
/savev2_actor_actor_hl1_bias_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*�
value�B�	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0/savev2_actor_actor_i_kernel_read_readvariableop-savev2_actor_actor_i_bias_read_readvariableop/savev2_actor_actor_o_kernel_read_readvariableop-savev2_actor_actor_o_bias_read_readvariableop1savev2_actor_actor_hl0_kernel_read_readvariableop/savev2_actor_actor_hl0_bias_read_readvariableop1savev2_actor_actor_hl1_kernel_read_readvariableop/savev2_actor_actor_hl1_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
2	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*X
_input_shapesG
E: :	�d:d:@::d@:@:@@:@: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:%!

_output_shapes
:	�d: 

_output_shapes
:d:$ 

_output_shapes

:@: 

_output_shapes
::$ 

_output_shapes

:d@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:	

_output_shapes
: 
�
�
#__inference_signature_wrapper_38552
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *)
f$R"
 __inference__wrapped_model_383092
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*K
_input_shapes:
8:����������::::::::22
StatefulPartitionedCallStatefulPartitionedCall:U Q
,
_output_shapes
:����������
!
_user_specified_name	input_1
�
�
%__inference_actor_layer_call_fn_38529
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_actor_layer_call_and_return_conditional_losses_385072
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*K
_input_shapes:
8:����������::::::::22
StatefulPartitionedCallStatefulPartitionedCall:U Q
,
_output_shapes
:����������
!
_user_specified_name	input_1
� 
�
D__inference_actor_hl0_layer_call_and_return_conditional_losses_38668

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������d2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������d::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������d
 
_user_specified_nameinputs
�
�
@__inference_actor_layer_call_and_return_conditional_losses_38507
input_1
actor_i_38354
actor_i_38356
actor_hl0_38401
actor_hl0_38403
actor_hl1_38448
actor_hl1_38450
actor_o_38501
actor_o_38503
identity��!actor_hl0/StatefulPartitionedCall�!actor_hl1/StatefulPartitionedCall�actor_i/StatefulPartitionedCall�actor_o/StatefulPartitionedCall�
actor_i/StatefulPartitionedCallStatefulPartitionedCallinput_1actor_i_38354actor_i_38356*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������d*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_actor_i_layer_call_and_return_conditional_losses_383432!
actor_i/StatefulPartitionedCall�
!actor_hl0/StatefulPartitionedCallStatefulPartitionedCall(actor_i/StatefulPartitionedCall:output:0actor_hl0_38401actor_hl0_38403*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_actor_hl0_layer_call_and_return_conditional_losses_383902#
!actor_hl0/StatefulPartitionedCall�
!actor_hl1/StatefulPartitionedCallStatefulPartitionedCall*actor_hl0/StatefulPartitionedCall:output:0actor_hl1_38448actor_hl1_38450*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_actor_hl1_layer_call_and_return_conditional_losses_384372#
!actor_hl1/StatefulPartitionedCall�
actor_o/StatefulPartitionedCallStatefulPartitionedCall*actor_hl1/StatefulPartitionedCall:output:0actor_o_38501actor_o_38503*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_actor_o_layer_call_and_return_conditional_losses_384902!
actor_o/StatefulPartitionedCall�
IdentityIdentity(actor_o/StatefulPartitionedCall:output:0"^actor_hl0/StatefulPartitionedCall"^actor_hl1/StatefulPartitionedCall ^actor_i/StatefulPartitionedCall ^actor_o/StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*K
_input_shapes:
8:����������::::::::2F
!actor_hl0/StatefulPartitionedCall!actor_hl0/StatefulPartitionedCall2F
!actor_hl1/StatefulPartitionedCall!actor_hl1/StatefulPartitionedCall2B
actor_i/StatefulPartitionedCallactor_i/StatefulPartitionedCall2B
actor_o/StatefulPartitionedCallactor_o/StatefulPartitionedCall:U Q
,
_output_shapes
:����������
!
_user_specified_name	input_1
�
�
B__inference_actor_i_layer_call_and_return_conditional_losses_38582

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes
:	�d*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*,
_output_shapes
:����������2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������d2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������d2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������d2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������d2

Identity"
identityIdentity:output:0*3
_input_shapes"
 :����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:T P
,
_output_shapes
:����������
 
_user_specified_nameinputs
�%
�
!__inference__traced_restore_38798
file_prefix)
%assignvariableop_actor_actor_i_kernel)
%assignvariableop_1_actor_actor_i_bias+
'assignvariableop_2_actor_actor_o_kernel)
%assignvariableop_3_actor_actor_o_bias-
)assignvariableop_4_actor_actor_hl0_kernel+
'assignvariableop_5_actor_actor_hl0_bias-
)assignvariableop_6_actor_actor_hl1_kernel+
'assignvariableop_7_actor_actor_hl1_bias

identity_9��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_2�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*�
value�B�	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*8
_output_shapes&
$:::::::::*
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp%assignvariableop_actor_actor_i_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp%assignvariableop_1_actor_actor_i_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp'assignvariableop_2_actor_actor_o_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp%assignvariableop_3_actor_actor_o_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp)assignvariableop_4_actor_actor_hl0_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp'assignvariableop_5_actor_actor_hl0_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp)assignvariableop_6_actor_actor_hl1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp'assignvariableop_7_actor_actor_hl1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_79
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�

Identity_8Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_8�

Identity_9IdentityIdentity_8:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7*
T0*
_output_shapes
: 2

Identity_9"!

identity_9Identity_9:output:0*5
_input_shapes$
": ::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_7:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
@
input_15
serving_default_input_1:0����������@
output_14
StatefulPartitionedCall:0���������tensorflow/serving/predict:�d
�
i
hidden_layers
o
regularization_losses
trainable_variables
	variables
	keras_api

signatures
<_default_save_signature
*=&call_and_return_all_conditional_losses
>__call__"�
_tf_keras_model�{"class_name": "Actor", "name": "actor", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"layer was saved without config": true}, "is_graph_network": false, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Actor"}}
�

	kernel

bias
	variables
regularization_losses
trainable_variables
	keras_api
*?&call_and_return_all_conditional_losses
@__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "actor_i", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_i", "trainable": true, "dtype": "float32", "units": 100, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 232}}}, "build_input_shape": {"class_name": "TensorShape", "items": [10, 1, 232]}}
.
0
1"
trackable_list_wrapper
�

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
*A&call_and_return_all_conditional_losses
B__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "actor_o", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_o", "trainable": true, "dtype": "float32", "units": 24, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [10, 1, 64]}}
 "
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
�

layers
layer_regularization_losses
non_trainable_variables
regularization_losses
layer_metrics
metrics
trainable_variables
	variables
>__call__
<_default_save_signature
*=&call_and_return_all_conditional_losses
&="call_and_return_conditional_losses"
_generic_user_object
,
Cserving_default"
signature_map
':%	�d2actor/actor_i/kernel
 :d2actor/actor_i/bias
.
	0

1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
�
	variables

 layers
!layer_regularization_losses
"non_trainable_variables
regularization_losses
#metrics
trainable_variables
$layer_metrics
@__call__
*?&call_and_return_all_conditional_losses
&?"call_and_return_conditional_losses"
_generic_user_object
�

kernel
bias
%	variables
&regularization_losses
'trainable_variables
(	keras_api
*D&call_and_return_all_conditional_losses
E__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "actor_hl0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl0", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 100}}}, "build_input_shape": {"class_name": "TensorShape", "items": [10, 1, 100]}}
�

kernel
bias
)	variables
*regularization_losses
+trainable_variables
,	keras_api
*F&call_and_return_all_conditional_losses
G__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "actor_hl1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl1", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [10, 1, 64]}}
&:$@2actor/actor_o/kernel
 :2actor/actor_o/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
	variables

-layers
.layer_regularization_losses
/non_trainable_variables
regularization_losses
0metrics
trainable_variables
1layer_metrics
B__call__
*A&call_and_return_all_conditional_losses
&A"call_and_return_conditional_losses"
_generic_user_object
(:&d@2actor/actor_hl0/kernel
": @2actor/actor_hl0/bias
(:&@@2actor/actor_hl1/kernel
": @2actor/actor_hl1/bias
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
%	variables

2layers
3layer_regularization_losses
4non_trainable_variables
&regularization_losses
5metrics
'trainable_variables
6layer_metrics
E__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
)	variables

7layers
8layer_regularization_losses
9non_trainable_variables
*regularization_losses
:metrics
+trainable_variables
;layer_metrics
G__call__
*F&call_and_return_all_conditional_losses
&F"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�2�
 __inference__wrapped_model_38309�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *+�(
&�#
input_1����������
�2�
@__inference_actor_layer_call_and_return_conditional_losses_38507�
���
FullArgSpec!
args�
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *+�(
&�#
input_1����������
�2�
%__inference_actor_layer_call_fn_38529�
���
FullArgSpec!
args�
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *+�(
&�#
input_1����������
�2�
B__inference_actor_i_layer_call_and_return_conditional_losses_38582�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_actor_i_layer_call_fn_38591�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_actor_o_layer_call_and_return_conditional_losses_38628�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_actor_o_layer_call_fn_38637�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference_signature_wrapper_38552input_1"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_actor_hl0_layer_call_and_return_conditional_losses_38668�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_actor_hl0_layer_call_fn_38677�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_actor_hl1_layer_call_and_return_conditional_losses_38708�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_actor_hl1_layer_call_fn_38717�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
 __inference__wrapped_model_38309z	
5�2
+�(
&�#
input_1����������
� "7�4
2
output_1&�#
output_1����������
D__inference_actor_hl0_layer_call_and_return_conditional_losses_38668d3�0
)�&
$�!
inputs���������d
� ")�&
�
0���������@
� �
)__inference_actor_hl0_layer_call_fn_38677W3�0
)�&
$�!
inputs���������d
� "����������@�
D__inference_actor_hl1_layer_call_and_return_conditional_losses_38708d3�0
)�&
$�!
inputs���������@
� ")�&
�
0���������@
� �
)__inference_actor_hl1_layer_call_fn_38717W3�0
)�&
$�!
inputs���������@
� "����������@�
B__inference_actor_i_layer_call_and_return_conditional_losses_38582e	
4�1
*�'
%�"
inputs����������
� ")�&
�
0���������d
� �
'__inference_actor_i_layer_call_fn_38591X	
4�1
*�'
%�"
inputs����������
� "����������d�
@__inference_actor_layer_call_and_return_conditional_losses_38507l	
5�2
+�(
&�#
input_1����������
� ")�&
�
0���������
� �
%__inference_actor_layer_call_fn_38529_	
5�2
+�(
&�#
input_1����������
� "�����������
B__inference_actor_o_layer_call_and_return_conditional_losses_38628d3�0
)�&
$�!
inputs���������@
� ")�&
�
0���������
� �
'__inference_actor_o_layer_call_fn_38637W3�0
)�&
$�!
inputs���������@
� "�����������
#__inference_signature_wrapper_38552�	
@�=
� 
6�3
1
input_1&�#
input_1����������"7�4
2
output_1&�#
output_1���������