��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.4.12v2.4.0-49-g85c8b2a817f8Ǭ
{
critic_i/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�@* 
shared_namecritic_i/kernel
t
#critic_i/kernel/Read/ReadVariableOpReadVariableOpcritic_i/kernel*
_output_shapes
:	�@*
dtype0
r
critic_i/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namecritic_i/bias
k
!critic_i/bias/Read/ReadVariableOpReadVariableOpcritic_i/bias*
_output_shapes
:@*
dtype0
z
critic_o/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@* 
shared_namecritic_o/kernel
s
#critic_o/kernel/Read/ReadVariableOpReadVariableOpcritic_o/kernel*
_output_shapes

:@*
dtype0
r
critic_o/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namecritic_o/bias
k
!critic_o/bias/Read/ReadVariableOpReadVariableOpcritic_o/bias*
_output_shapes
:*
dtype0
~
critic_hl0/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*"
shared_namecritic_hl0/kernel
w
%critic_hl0/kernel/Read/ReadVariableOpReadVariableOpcritic_hl0/kernel*
_output_shapes

:@@*
dtype0
v
critic_hl0/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@* 
shared_namecritic_hl0/bias
o
#critic_hl0/bias/Read/ReadVariableOpReadVariableOpcritic_hl0/bias*
_output_shapes
:@*
dtype0
~
critic_hl1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*"
shared_namecritic_hl1/kernel
w
%critic_hl1/kernel/Read/ReadVariableOpReadVariableOpcritic_hl1/kernel*
_output_shapes

:@@*
dtype0
v
critic_hl1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@* 
shared_namecritic_hl1/bias
o
#critic_hl1/bias/Read/ReadVariableOpReadVariableOpcritic_hl1/bias*
_output_shapes
:@*
dtype0

NoOpNoOp
�
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�
value�B� B�
�
i
hidden_layers
o
trainable_variables
	variables
regularization_losses
	keras_api

signatures
h

	kernel

bias
trainable_variables
	variables
regularization_losses
	keras_api

0
1
h

kernel
bias
trainable_variables
	variables
regularization_losses
	keras_api
8
	0

1
2
3
4
5
6
7
8
	0

1
2
3
4
5
6
7
 
�
non_trainable_variables
layer_metrics
metrics
trainable_variables

layers
layer_regularization_losses
	variables
regularization_losses
 
HF
VARIABLE_VALUEcritic_i/kernel#i/kernel/.ATTRIBUTES/VARIABLE_VALUE
DB
VARIABLE_VALUEcritic_i/bias!i/bias/.ATTRIBUTES/VARIABLE_VALUE

	0

1

	0

1
 
�
 non_trainable_variables
!layer_metrics
"metrics
trainable_variables

#layers
$layer_regularization_losses
	variables
regularization_losses
h

kernel
bias
%trainable_variables
&	variables
'regularization_losses
(	keras_api
h

kernel
bias
)trainable_variables
*	variables
+regularization_losses
,	keras_api
HF
VARIABLE_VALUEcritic_o/kernel#o/kernel/.ATTRIBUTES/VARIABLE_VALUE
DB
VARIABLE_VALUEcritic_o/bias!o/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
-non_trainable_variables
.layer_metrics
/metrics
trainable_variables

0layers
1layer_regularization_losses
	variables
regularization_losses
WU
VARIABLE_VALUEcritic_hl0/kernel0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEcritic_hl0/bias0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEcritic_hl1/kernel0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEcritic_hl1/bias0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUE
 
 
 

0
1
2
3
 
 
 
 
 
 

0
1

0
1
 
�
2non_trainable_variables
3layer_metrics
4metrics
%trainable_variables

5layers
6layer_regularization_losses
&	variables
'regularization_losses

0
1

0
1
 
�
7non_trainable_variables
8layer_metrics
9metrics
)trainable_variables

:layers
;layer_regularization_losses
*	variables
+regularization_losses
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
|
serving_default_input_1Placeholder*(
_output_shapes
:����������*
dtype0*
shape:����������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1critic_i/kernelcritic_i/biascritic_hl0/kernelcritic_hl0/biascritic_hl1/kernelcritic_hl1/biascritic_o/kernelcritic_o/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference_signature_wrapper_35006
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename#critic_i/kernel/Read/ReadVariableOp!critic_i/bias/Read/ReadVariableOp#critic_o/kernel/Read/ReadVariableOp!critic_o/bias/Read/ReadVariableOp%critic_hl0/kernel/Read/ReadVariableOp#critic_hl0/bias/Read/ReadVariableOp%critic_hl1/kernel/Read/ReadVariableOp#critic_hl1/bias/Read/ReadVariableOpConst*
Tin
2
*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *'
f"R 
__inference__traced_save_35131
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamecritic_i/kernelcritic_i/biascritic_o/kernelcritic_o/biascritic_hl0/kernelcritic_hl0/biascritic_hl1/kernelcritic_hl1/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__traced_restore_35165��
�
�
&__inference_critic_layer_call_fn_34983
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *J
fERC
A__inference_critic_layer_call_and_return_conditional_losses_349612
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*G
_input_shapes6
4:����������::::::::22
StatefulPartitionedCallStatefulPartitionedCall:Q M
(
_output_shapes
:����������
!
_user_specified_name	input_1
�

*__inference_critic_hl1_layer_call_fn_35084

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_critic_hl1_layer_call_and_return_conditional_losses_349182
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�

*__inference_critic_hl0_layer_call_fn_35064

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_critic_hl0_layer_call_and_return_conditional_losses_348912
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
__inference__traced_save_35131
file_prefix.
*savev2_critic_i_kernel_read_readvariableop,
(savev2_critic_i_bias_read_readvariableop.
*savev2_critic_o_kernel_read_readvariableop,
(savev2_critic_o_bias_read_readvariableop0
,savev2_critic_hl0_kernel_read_readvariableop.
*savev2_critic_hl0_bias_read_readvariableop0
,savev2_critic_hl1_kernel_read_readvariableop.
*savev2_critic_hl1_bias_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*�
value�B�	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0*savev2_critic_i_kernel_read_readvariableop(savev2_critic_i_bias_read_readvariableop*savev2_critic_o_kernel_read_readvariableop(savev2_critic_o_bias_read_readvariableop,savev2_critic_hl0_kernel_read_readvariableop*savev2_critic_hl0_bias_read_readvariableop,savev2_critic_hl1_kernel_read_readvariableop*savev2_critic_hl1_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
2	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*X
_input_shapesG
E: :	�@:@:@::@@:@:@@:@: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:%!

_output_shapes
:	�@: 

_output_shapes
:@:$ 

_output_shapes

:@: 

_output_shapes
::$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:	

_output_shapes
: 
�	
�
E__inference_critic_hl1_layer_call_and_return_conditional_losses_34918

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
}
(__inference_critic_o_layer_call_fn_35044

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_critic_o_layer_call_and_return_conditional_losses_349442
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�	
�
C__inference_critic_o_layer_call_and_return_conditional_losses_35035

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�+
�
 __inference__wrapped_model_34850
input_12
.critic_critic_i_matmul_readvariableop_resource3
/critic_critic_i_biasadd_readvariableop_resource4
0critic_critic_hl0_matmul_readvariableop_resource5
1critic_critic_hl0_biasadd_readvariableop_resource4
0critic_critic_hl1_matmul_readvariableop_resource5
1critic_critic_hl1_biasadd_readvariableop_resource2
.critic_critic_o_matmul_readvariableop_resource3
/critic_critic_o_biasadd_readvariableop_resource
identity��(critic/critic_hl0/BiasAdd/ReadVariableOp�'critic/critic_hl0/MatMul/ReadVariableOp�(critic/critic_hl1/BiasAdd/ReadVariableOp�'critic/critic_hl1/MatMul/ReadVariableOp�&critic/critic_i/BiasAdd/ReadVariableOp�%critic/critic_i/MatMul/ReadVariableOp�&critic/critic_o/BiasAdd/ReadVariableOp�%critic/critic_o/MatMul/ReadVariableOp�
%critic/critic_i/MatMul/ReadVariableOpReadVariableOp.critic_critic_i_matmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02'
%critic/critic_i/MatMul/ReadVariableOp�
critic/critic_i/MatMulMatMulinput_1-critic/critic_i/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
critic/critic_i/MatMul�
&critic/critic_i/BiasAdd/ReadVariableOpReadVariableOp/critic_critic_i_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&critic/critic_i/BiasAdd/ReadVariableOp�
critic/critic_i/BiasAddBiasAdd critic/critic_i/MatMul:product:0.critic/critic_i/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
critic/critic_i/BiasAdd�
'critic/critic_hl0/MatMul/ReadVariableOpReadVariableOp0critic_critic_hl0_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02)
'critic/critic_hl0/MatMul/ReadVariableOp�
critic/critic_hl0/MatMulMatMul critic/critic_i/BiasAdd:output:0/critic/critic_hl0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
critic/critic_hl0/MatMul�
(critic/critic_hl0/BiasAdd/ReadVariableOpReadVariableOp1critic_critic_hl0_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02*
(critic/critic_hl0/BiasAdd/ReadVariableOp�
critic/critic_hl0/BiasAddBiasAdd"critic/critic_hl0/MatMul:product:00critic/critic_hl0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
critic/critic_hl0/BiasAdd�
critic/critic_hl0/ReluRelu"critic/critic_hl0/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
critic/critic_hl0/Relu�
'critic/critic_hl1/MatMul/ReadVariableOpReadVariableOp0critic_critic_hl1_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02)
'critic/critic_hl1/MatMul/ReadVariableOp�
critic/critic_hl1/MatMulMatMul$critic/critic_hl0/Relu:activations:0/critic/critic_hl1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
critic/critic_hl1/MatMul�
(critic/critic_hl1/BiasAdd/ReadVariableOpReadVariableOp1critic_critic_hl1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02*
(critic/critic_hl1/BiasAdd/ReadVariableOp�
critic/critic_hl1/BiasAddBiasAdd"critic/critic_hl1/MatMul:product:00critic/critic_hl1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
critic/critic_hl1/BiasAdd�
critic/critic_hl1/ReluRelu"critic/critic_hl1/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
critic/critic_hl1/Relu�
%critic/critic_o/MatMul/ReadVariableOpReadVariableOp.critic_critic_o_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02'
%critic/critic_o/MatMul/ReadVariableOp�
critic/critic_o/MatMulMatMul$critic/critic_hl1/Relu:activations:0-critic/critic_o/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
critic/critic_o/MatMul�
&critic/critic_o/BiasAdd/ReadVariableOpReadVariableOp/critic_critic_o_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02(
&critic/critic_o/BiasAdd/ReadVariableOp�
critic/critic_o/BiasAddBiasAdd critic/critic_o/MatMul:product:0.critic/critic_o/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
critic/critic_o/BiasAdd�
IdentityIdentity critic/critic_o/BiasAdd:output:0)^critic/critic_hl0/BiasAdd/ReadVariableOp(^critic/critic_hl0/MatMul/ReadVariableOp)^critic/critic_hl1/BiasAdd/ReadVariableOp(^critic/critic_hl1/MatMul/ReadVariableOp'^critic/critic_i/BiasAdd/ReadVariableOp&^critic/critic_i/MatMul/ReadVariableOp'^critic/critic_o/BiasAdd/ReadVariableOp&^critic/critic_o/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*G
_input_shapes6
4:����������::::::::2T
(critic/critic_hl0/BiasAdd/ReadVariableOp(critic/critic_hl0/BiasAdd/ReadVariableOp2R
'critic/critic_hl0/MatMul/ReadVariableOp'critic/critic_hl0/MatMul/ReadVariableOp2T
(critic/critic_hl1/BiasAdd/ReadVariableOp(critic/critic_hl1/BiasAdd/ReadVariableOp2R
'critic/critic_hl1/MatMul/ReadVariableOp'critic/critic_hl1/MatMul/ReadVariableOp2P
&critic/critic_i/BiasAdd/ReadVariableOp&critic/critic_i/BiasAdd/ReadVariableOp2N
%critic/critic_i/MatMul/ReadVariableOp%critic/critic_i/MatMul/ReadVariableOp2P
&critic/critic_o/BiasAdd/ReadVariableOp&critic/critic_o/BiasAdd/ReadVariableOp2N
%critic/critic_o/MatMul/ReadVariableOp%critic/critic_o/MatMul/ReadVariableOp:Q M
(
_output_shapes
:����������
!
_user_specified_name	input_1
�	
�
E__inference_critic_hl1_layer_call_and_return_conditional_losses_35075

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�	
�
C__inference_critic_o_layer_call_and_return_conditional_losses_34944

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
#__inference_signature_wrapper_35006
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *)
f$R"
 __inference__wrapped_model_348502
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*G
_input_shapes6
4:����������::::::::22
StatefulPartitionedCallStatefulPartitionedCall:Q M
(
_output_shapes
:����������
!
_user_specified_name	input_1
�
}
(__inference_critic_i_layer_call_fn_35025

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_critic_i_layer_call_and_return_conditional_losses_348642
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
�
C__inference_critic_i_layer_call_and_return_conditional_losses_34864

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
�
E__inference_critic_hl0_layer_call_and_return_conditional_losses_35055

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
A__inference_critic_layer_call_and_return_conditional_losses_34961
input_1
critic_i_34875
critic_i_34877
critic_hl0_34902
critic_hl0_34904
critic_hl1_34929
critic_hl1_34931
critic_o_34955
critic_o_34957
identity��"critic_hl0/StatefulPartitionedCall�"critic_hl1/StatefulPartitionedCall� critic_i/StatefulPartitionedCall� critic_o/StatefulPartitionedCall�
 critic_i/StatefulPartitionedCallStatefulPartitionedCallinput_1critic_i_34875critic_i_34877*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_critic_i_layer_call_and_return_conditional_losses_348642"
 critic_i/StatefulPartitionedCall�
"critic_hl0/StatefulPartitionedCallStatefulPartitionedCall)critic_i/StatefulPartitionedCall:output:0critic_hl0_34902critic_hl0_34904*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_critic_hl0_layer_call_and_return_conditional_losses_348912$
"critic_hl0/StatefulPartitionedCall�
"critic_hl1/StatefulPartitionedCallStatefulPartitionedCall+critic_hl0/StatefulPartitionedCall:output:0critic_hl1_34929critic_hl1_34931*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_critic_hl1_layer_call_and_return_conditional_losses_349182$
"critic_hl1/StatefulPartitionedCall�
 critic_o/StatefulPartitionedCallStatefulPartitionedCall+critic_hl1/StatefulPartitionedCall:output:0critic_o_34955critic_o_34957*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *L
fGRE
C__inference_critic_o_layer_call_and_return_conditional_losses_349442"
 critic_o/StatefulPartitionedCall�
IdentityIdentity)critic_o/StatefulPartitionedCall:output:0#^critic_hl0/StatefulPartitionedCall#^critic_hl1/StatefulPartitionedCall!^critic_i/StatefulPartitionedCall!^critic_o/StatefulPartitionedCall*
T0*'
_output_shapes
:���������2

Identity"
identityIdentity:output:0*G
_input_shapes6
4:����������::::::::2H
"critic_hl0/StatefulPartitionedCall"critic_hl0/StatefulPartitionedCall2H
"critic_hl1/StatefulPartitionedCall"critic_hl1/StatefulPartitionedCall2D
 critic_i/StatefulPartitionedCall critic_i/StatefulPartitionedCall2D
 critic_o/StatefulPartitionedCall critic_o/StatefulPartitionedCall:Q M
(
_output_shapes
:����������
!
_user_specified_name	input_1
�%
�
!__inference__traced_restore_35165
file_prefix$
 assignvariableop_critic_i_kernel$
 assignvariableop_1_critic_i_bias&
"assignvariableop_2_critic_o_kernel$
 assignvariableop_3_critic_o_bias(
$assignvariableop_4_critic_hl0_kernel&
"assignvariableop_5_critic_hl0_bias(
$assignvariableop_6_critic_hl1_kernel&
"assignvariableop_7_critic_hl1_bias

identity_9��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_2�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*�
value�B�	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*8
_output_shapes&
$:::::::::*
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp assignvariableop_critic_i_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp assignvariableop_1_critic_i_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp"assignvariableop_2_critic_o_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp assignvariableop_3_critic_o_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp$assignvariableop_4_critic_hl0_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp"assignvariableop_5_critic_hl0_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp$assignvariableop_6_critic_hl1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp"assignvariableop_7_critic_hl1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_79
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�

Identity_8Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_8�

Identity_9IdentityIdentity_8:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7*
T0*
_output_shapes
: 2

Identity_9"!

identity_9Identity_9:output:0*5
_input_shapes$
": ::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_7:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�	
�
E__inference_critic_hl0_layer_call_and_return_conditional_losses_34891

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�	
�
C__inference_critic_i_layer_call_and_return_conditional_losses_35016

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
<
input_11
serving_default_input_1:0����������<
output_10
StatefulPartitionedCall:0���������tensorflow/serving/predict:�d
�
i
hidden_layers
o
trainable_variables
	variables
regularization_losses
	keras_api

signatures
*<&call_and_return_all_conditional_losses
=__call__
>_default_save_signature"�
_tf_keras_model�{"class_name": "Critic", "name": "critic", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"layer was saved without config": true}, "is_graph_network": false, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Critic"}}
�

	kernel

bias
trainable_variables
	variables
regularization_losses
	keras_api
*?&call_and_return_all_conditional_losses
@__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_i", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, 1, 231]}, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_i", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 1, 231]}, "dtype": "float32", "units": 64, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 231}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 231]}}
.
0
1"
trackable_list_wrapper
�

kernel
bias
trainable_variables
	variables
regularization_losses
	keras_api
*A&call_and_return_all_conditional_losses
B__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_o", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_o", "trainable": true, "dtype": "float32", "units": 24, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 64]}}
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
 "
trackable_list_wrapper
�
non_trainable_variables
layer_metrics
metrics
trainable_variables

layers
layer_regularization_losses
	variables
regularization_losses
=__call__
>_default_save_signature
*<&call_and_return_all_conditional_losses
&<"call_and_return_conditional_losses"
_generic_user_object
,
Cserving_default"
signature_map
": 	�@2critic_i/kernel
:@2critic_i/bias
.
	0

1"
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
 non_trainable_variables
!layer_metrics
"metrics
trainable_variables

#layers
$layer_regularization_losses
	variables
regularization_losses
@__call__
*?&call_and_return_all_conditional_losses
&?"call_and_return_conditional_losses"
_generic_user_object
�

kernel
bias
%trainable_variables
&	variables
'regularization_losses
(	keras_api
*D&call_and_return_all_conditional_losses
E__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_hl0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_hl0", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 64]}}
�

kernel
bias
)trainable_variables
*	variables
+regularization_losses
,	keras_api
*F&call_and_return_all_conditional_losses
G__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_hl1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_hl1", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 64]}}
!:@2critic_o/kernel
:2critic_o/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
-non_trainable_variables
.layer_metrics
/metrics
trainable_variables

0layers
1layer_regularization_losses
	variables
regularization_losses
B__call__
*A&call_and_return_all_conditional_losses
&A"call_and_return_conditional_losses"
_generic_user_object
#:!@@2critic_hl0/kernel
:@2critic_hl0/bias
#:!@@2critic_hl1/kernel
:@2critic_hl1/bias
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
2non_trainable_variables
3layer_metrics
4metrics
%trainable_variables

5layers
6layer_regularization_losses
&	variables
'regularization_losses
E__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
7non_trainable_variables
8layer_metrics
9metrics
)trainable_variables

:layers
;layer_regularization_losses
*	variables
+regularization_losses
G__call__
*F&call_and_return_all_conditional_losses
&F"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�2�
A__inference_critic_layer_call_and_return_conditional_losses_34961�
���
FullArgSpec!
args�
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *'�$
"�
input_1����������
�2�
&__inference_critic_layer_call_fn_34983�
���
FullArgSpec!
args�
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *'�$
"�
input_1����������
�2�
 __inference__wrapped_model_34850�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *'�$
"�
input_1����������
�2�
C__inference_critic_i_layer_call_and_return_conditional_losses_35016�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_critic_i_layer_call_fn_35025�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_critic_o_layer_call_and_return_conditional_losses_35035�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_critic_o_layer_call_fn_35044�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference_signature_wrapper_35006input_1"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_critic_hl0_layer_call_and_return_conditional_losses_35055�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_critic_hl0_layer_call_fn_35064�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_critic_hl1_layer_call_and_return_conditional_losses_35075�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_critic_hl1_layer_call_fn_35084�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
 __inference__wrapped_model_34850r	
1�.
'�$
"�
input_1����������
� "3�0
.
output_1"�
output_1����������
E__inference_critic_hl0_layer_call_and_return_conditional_losses_35055\/�,
%�"
 �
inputs���������@
� "%�"
�
0���������@
� }
*__inference_critic_hl0_layer_call_fn_35064O/�,
%�"
 �
inputs���������@
� "����������@�
E__inference_critic_hl1_layer_call_and_return_conditional_losses_35075\/�,
%�"
 �
inputs���������@
� "%�"
�
0���������@
� }
*__inference_critic_hl1_layer_call_fn_35084O/�,
%�"
 �
inputs���������@
� "����������@�
C__inference_critic_i_layer_call_and_return_conditional_losses_35016]	
0�-
&�#
!�
inputs����������
� "%�"
�
0���������@
� |
(__inference_critic_i_layer_call_fn_35025P	
0�-
&�#
!�
inputs����������
� "����������@�
A__inference_critic_layer_call_and_return_conditional_losses_34961d	
1�.
'�$
"�
input_1����������
� "%�"
�
0���������
� �
&__inference_critic_layer_call_fn_34983W	
1�.
'�$
"�
input_1����������
� "�����������
C__inference_critic_o_layer_call_and_return_conditional_losses_35035\/�,
%�"
 �
inputs���������@
� "%�"
�
0���������
� {
(__inference_critic_o_layer_call_fn_35044O/�,
%�"
 �
inputs���������@
� "�����������
#__inference_signature_wrapper_35006}	
<�9
� 
2�/
-
input_1"�
input_1����������"3�0
.
output_1"�
output_1���������