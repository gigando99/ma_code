��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
�
GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"

batch_dimsint "
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.4.12v2.4.0-49-g85c8b2a817f8��
�
critic/critic_i/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:1@*'
shared_namecritic/critic_i/kernel
�
*critic/critic_i/kernel/Read/ReadVariableOpReadVariableOpcritic/critic_i/kernel*
_output_shapes

:1@*
dtype0
�
critic/critic_i/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_namecritic/critic_i/bias
y
(critic/critic_i/bias/Read/ReadVariableOpReadVariableOpcritic/critic_i/bias*
_output_shapes
:@*
dtype0
�
critic/critic_o/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*'
shared_namecritic/critic_o/kernel
�
*critic/critic_o/kernel/Read/ReadVariableOpReadVariableOpcritic/critic_o/kernel*
_output_shapes

:@*
dtype0
�
critic/critic_o/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_namecritic/critic_o/bias
y
(critic/critic_o/bias/Read/ReadVariableOpReadVariableOpcritic/critic_o/bias*
_output_shapes
:*
dtype0
�
critic/critic_hl0/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*)
shared_namecritic/critic_hl0/kernel
�
,critic/critic_hl0/kernel/Read/ReadVariableOpReadVariableOpcritic/critic_hl0/kernel*
_output_shapes

:@@*
dtype0
�
critic/critic_hl0/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*'
shared_namecritic/critic_hl0/bias
}
*critic/critic_hl0/bias/Read/ReadVariableOpReadVariableOpcritic/critic_hl0/bias*
_output_shapes
:@*
dtype0
�
critic/critic_hl1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*)
shared_namecritic/critic_hl1/kernel
�
,critic/critic_hl1/kernel/Read/ReadVariableOpReadVariableOpcritic/critic_hl1/kernel*
_output_shapes

:@@*
dtype0
�
critic/critic_hl1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*'
shared_namecritic/critic_hl1/bias
}
*critic/critic_hl1/bias/Read/ReadVariableOpReadVariableOpcritic/critic_hl1/bias*
_output_shapes
:@*
dtype0

NoOpNoOp
�
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�
value�B� B�
�
i
hidden_layers
o
trainable_variables
regularization_losses
	variables
	keras_api

signatures
h

	kernel

bias
trainable_variables
regularization_losses
	variables
	keras_api

0
1
h

kernel
bias
trainable_variables
regularization_losses
	variables
	keras_api
8
	0

1
2
3
4
5
6
7
 
8
	0

1
2
3
4
5
6
7
�

layers
trainable_variables
regularization_losses
non_trainable_variables
layer_regularization_losses
	variables
metrics
layer_metrics
 
OM
VARIABLE_VALUEcritic/critic_i/kernel#i/kernel/.ATTRIBUTES/VARIABLE_VALUE
KI
VARIABLE_VALUEcritic/critic_i/bias!i/bias/.ATTRIBUTES/VARIABLE_VALUE

	0

1
 

	0

1
�

 layers
trainable_variables
!non_trainable_variables
regularization_losses
"layer_regularization_losses
	variables
#metrics
$layer_metrics
h

kernel
bias
%trainable_variables
&regularization_losses
'	variables
(	keras_api
h

kernel
bias
)trainable_variables
*regularization_losses
+	variables
,	keras_api
OM
VARIABLE_VALUEcritic/critic_o/kernel#o/kernel/.ATTRIBUTES/VARIABLE_VALUE
KI
VARIABLE_VALUEcritic/critic_o/bias!o/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�

-layers
trainable_variables
.non_trainable_variables
regularization_losses
/layer_regularization_losses
	variables
0metrics
1layer_metrics
^\
VARIABLE_VALUEcritic/critic_hl0/kernel0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEcritic/critic_hl0/bias0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUE
^\
VARIABLE_VALUEcritic/critic_hl1/kernel0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEcritic/critic_hl1/bias0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUE

0
1
2
3
 
 
 
 
 
 
 
 
 

0
1
 

0
1
�

2layers
%trainable_variables
3non_trainable_variables
&regularization_losses
4layer_regularization_losses
'	variables
5metrics
6layer_metrics

0
1
 

0
1
�

7layers
)trainable_variables
8non_trainable_variables
*regularization_losses
9layer_regularization_losses
+	variables
:metrics
;layer_metrics
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
�
serving_default_input_1Placeholder*+
_output_shapes
:���������1*
dtype0* 
shape:���������1
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1critic/critic_i/kernelcritic/critic_i/biascritic/critic_hl0/kernelcritic/critic_hl0/biascritic/critic_hl1/kernelcritic/critic_hl1/biascritic/critic_o/kernelcritic/critic_o/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *-
f(R&
$__inference_signature_wrapper_738417
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename*critic/critic_i/kernel/Read/ReadVariableOp(critic/critic_i/bias/Read/ReadVariableOp*critic/critic_o/kernel/Read/ReadVariableOp(critic/critic_o/bias/Read/ReadVariableOp,critic/critic_hl0/kernel/Read/ReadVariableOp*critic/critic_hl0/bias/Read/ReadVariableOp,critic/critic_hl1/kernel/Read/ReadVariableOp*critic/critic_hl1/bias/Read/ReadVariableOpConst*
Tin
2
*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *(
f#R!
__inference__traced_save_738622
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamecritic/critic_i/kernelcritic/critic_i/biascritic/critic_o/kernelcritic/critic_o/biascritic/critic_hl0/kernelcritic/critic_hl0/biascritic/critic_hl1/kernelcritic/critic_hl1/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *+
f&R$
"__inference__traced_restore_738656��
�
~
)__inference_critic_i_layer_call_fn_738456

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_critic_i_layer_call_and_return_conditional_losses_7382152
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������1::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������1
 
_user_specified_nameinputs
�
�
$__inference_signature_wrapper_738417
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__wrapped_model_7381812
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:���������1::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:���������1
!
_user_specified_name	input_1
�
�
+__inference_critic_hl0_layer_call_fn_738535

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_critic_hl0_layer_call_and_return_conditional_losses_7382622
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
D__inference_critic_o_layer_call_and_return_conditional_losses_738355

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
B__inference_critic_layer_call_and_return_conditional_losses_738372
input_1
critic_i_738226
critic_i_738228
critic_hl0_738273
critic_hl0_738275
critic_hl1_738320
critic_hl1_738322
critic_o_738366
critic_o_738368
identity��"critic_hl0/StatefulPartitionedCall�"critic_hl1/StatefulPartitionedCall� critic_i/StatefulPartitionedCall� critic_o/StatefulPartitionedCall�
 critic_i/StatefulPartitionedCallStatefulPartitionedCallinput_1critic_i_738226critic_i_738228*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_critic_i_layer_call_and_return_conditional_losses_7382152"
 critic_i/StatefulPartitionedCall�
"critic_hl0/StatefulPartitionedCallStatefulPartitionedCall)critic_i/StatefulPartitionedCall:output:0critic_hl0_738273critic_hl0_738275*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_critic_hl0_layer_call_and_return_conditional_losses_7382622$
"critic_hl0/StatefulPartitionedCall�
"critic_hl1/StatefulPartitionedCallStatefulPartitionedCall+critic_hl0/StatefulPartitionedCall:output:0critic_hl1_738320critic_hl1_738322*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_critic_hl1_layer_call_and_return_conditional_losses_7383092$
"critic_hl1/StatefulPartitionedCall�
 critic_o/StatefulPartitionedCallStatefulPartitionedCall+critic_hl1/StatefulPartitionedCall:output:0critic_o_738366critic_o_738368*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_critic_o_layer_call_and_return_conditional_losses_7383552"
 critic_o/StatefulPartitionedCall�
IdentityIdentity)critic_o/StatefulPartitionedCall:output:0#^critic_hl0/StatefulPartitionedCall#^critic_hl1/StatefulPartitionedCall!^critic_i/StatefulPartitionedCall!^critic_o/StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:���������1::::::::2H
"critic_hl0/StatefulPartitionedCall"critic_hl0/StatefulPartitionedCall2H
"critic_hl1/StatefulPartitionedCall"critic_hl1/StatefulPartitionedCall2D
 critic_i/StatefulPartitionedCall critic_i/StatefulPartitionedCall2D
 critic_o/StatefulPartitionedCall critic_o/StatefulPartitionedCall:T P
+
_output_shapes
:���������1
!
_user_specified_name	input_1
�
�
D__inference_critic_i_layer_call_and_return_conditional_losses_738447

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:1@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������12
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������1::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������1
 
_user_specified_nameinputs
�
~
)__inference_critic_o_layer_call_fn_738495

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_critic_o_layer_call_and_return_conditional_losses_7383552
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
+__inference_critic_hl1_layer_call_fn_738575

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_critic_hl1_layer_call_and_return_conditional_losses_7383092
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
��
�
!__inference__wrapped_model_738181
input_15
1critic_critic_i_tensordot_readvariableop_resource3
/critic_critic_i_biasadd_readvariableop_resource7
3critic_critic_hl0_tensordot_readvariableop_resource5
1critic_critic_hl0_biasadd_readvariableop_resource7
3critic_critic_hl1_tensordot_readvariableop_resource5
1critic_critic_hl1_biasadd_readvariableop_resource5
1critic_critic_o_tensordot_readvariableop_resource3
/critic_critic_o_biasadd_readvariableop_resource
identity��(critic/critic_hl0/BiasAdd/ReadVariableOp�*critic/critic_hl0/Tensordot/ReadVariableOp�(critic/critic_hl1/BiasAdd/ReadVariableOp�*critic/critic_hl1/Tensordot/ReadVariableOp�&critic/critic_i/BiasAdd/ReadVariableOp�(critic/critic_i/Tensordot/ReadVariableOp�&critic/critic_o/BiasAdd/ReadVariableOp�(critic/critic_o/Tensordot/ReadVariableOp�
(critic/critic_i/Tensordot/ReadVariableOpReadVariableOp1critic_critic_i_tensordot_readvariableop_resource*
_output_shapes

:1@*
dtype02*
(critic/critic_i/Tensordot/ReadVariableOp�
critic/critic_i/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
critic/critic_i/Tensordot/axes�
critic/critic_i/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
critic/critic_i/Tensordot/freey
critic/critic_i/Tensordot/ShapeShapeinput_1*
T0*
_output_shapes
:2!
critic/critic_i/Tensordot/Shape�
'critic/critic_i/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'critic/critic_i/Tensordot/GatherV2/axis�
"critic/critic_i/Tensordot/GatherV2GatherV2(critic/critic_i/Tensordot/Shape:output:0'critic/critic_i/Tensordot/free:output:00critic/critic_i/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"critic/critic_i/Tensordot/GatherV2�
)critic/critic_i/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)critic/critic_i/Tensordot/GatherV2_1/axis�
$critic/critic_i/Tensordot/GatherV2_1GatherV2(critic/critic_i/Tensordot/Shape:output:0'critic/critic_i/Tensordot/axes:output:02critic/critic_i/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$critic/critic_i/Tensordot/GatherV2_1�
critic/critic_i/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
critic/critic_i/Tensordot/Const�
critic/critic_i/Tensordot/ProdProd+critic/critic_i/Tensordot/GatherV2:output:0(critic/critic_i/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
critic/critic_i/Tensordot/Prod�
!critic/critic_i/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!critic/critic_i/Tensordot/Const_1�
 critic/critic_i/Tensordot/Prod_1Prod-critic/critic_i/Tensordot/GatherV2_1:output:0*critic/critic_i/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 critic/critic_i/Tensordot/Prod_1�
%critic/critic_i/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%critic/critic_i/Tensordot/concat/axis�
 critic/critic_i/Tensordot/concatConcatV2'critic/critic_i/Tensordot/free:output:0'critic/critic_i/Tensordot/axes:output:0.critic/critic_i/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 critic/critic_i/Tensordot/concat�
critic/critic_i/Tensordot/stackPack'critic/critic_i/Tensordot/Prod:output:0)critic/critic_i/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
critic/critic_i/Tensordot/stack�
#critic/critic_i/Tensordot/transpose	Transposeinput_1)critic/critic_i/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������12%
#critic/critic_i/Tensordot/transpose�
!critic/critic_i/Tensordot/ReshapeReshape'critic/critic_i/Tensordot/transpose:y:0(critic/critic_i/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2#
!critic/critic_i/Tensordot/Reshape�
 critic/critic_i/Tensordot/MatMulMatMul*critic/critic_i/Tensordot/Reshape:output:00critic/critic_i/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2"
 critic/critic_i/Tensordot/MatMul�
!critic/critic_i/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!critic/critic_i/Tensordot/Const_2�
'critic/critic_i/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'critic/critic_i/Tensordot/concat_1/axis�
"critic/critic_i/Tensordot/concat_1ConcatV2+critic/critic_i/Tensordot/GatherV2:output:0*critic/critic_i/Tensordot/Const_2:output:00critic/critic_i/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"critic/critic_i/Tensordot/concat_1�
critic/critic_i/TensordotReshape*critic/critic_i/Tensordot/MatMul:product:0+critic/critic_i/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
critic/critic_i/Tensordot�
&critic/critic_i/BiasAdd/ReadVariableOpReadVariableOp/critic_critic_i_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&critic/critic_i/BiasAdd/ReadVariableOp�
critic/critic_i/BiasAddBiasAdd"critic/critic_i/Tensordot:output:0.critic/critic_i/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2
critic/critic_i/BiasAdd�
*critic/critic_hl0/Tensordot/ReadVariableOpReadVariableOp3critic_critic_hl0_tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*critic/critic_hl0/Tensordot/ReadVariableOp�
 critic/critic_hl0/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2"
 critic/critic_hl0/Tensordot/axes�
 critic/critic_hl0/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2"
 critic/critic_hl0/Tensordot/free�
!critic/critic_hl0/Tensordot/ShapeShape critic/critic_i/BiasAdd:output:0*
T0*
_output_shapes
:2#
!critic/critic_hl0/Tensordot/Shape�
)critic/critic_hl0/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)critic/critic_hl0/Tensordot/GatherV2/axis�
$critic/critic_hl0/Tensordot/GatherV2GatherV2*critic/critic_hl0/Tensordot/Shape:output:0)critic/critic_hl0/Tensordot/free:output:02critic/critic_hl0/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$critic/critic_hl0/Tensordot/GatherV2�
+critic/critic_hl0/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2-
+critic/critic_hl0/Tensordot/GatherV2_1/axis�
&critic/critic_hl0/Tensordot/GatherV2_1GatherV2*critic/critic_hl0/Tensordot/Shape:output:0)critic/critic_hl0/Tensordot/axes:output:04critic/critic_hl0/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2(
&critic/critic_hl0/Tensordot/GatherV2_1�
!critic/critic_hl0/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2#
!critic/critic_hl0/Tensordot/Const�
 critic/critic_hl0/Tensordot/ProdProd-critic/critic_hl0/Tensordot/GatherV2:output:0*critic/critic_hl0/Tensordot/Const:output:0*
T0*
_output_shapes
: 2"
 critic/critic_hl0/Tensordot/Prod�
#critic/critic_hl0/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2%
#critic/critic_hl0/Tensordot/Const_1�
"critic/critic_hl0/Tensordot/Prod_1Prod/critic/critic_hl0/Tensordot/GatherV2_1:output:0,critic/critic_hl0/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2$
"critic/critic_hl0/Tensordot/Prod_1�
'critic/critic_hl0/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'critic/critic_hl0/Tensordot/concat/axis�
"critic/critic_hl0/Tensordot/concatConcatV2)critic/critic_hl0/Tensordot/free:output:0)critic/critic_hl0/Tensordot/axes:output:00critic/critic_hl0/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2$
"critic/critic_hl0/Tensordot/concat�
!critic/critic_hl0/Tensordot/stackPack)critic/critic_hl0/Tensordot/Prod:output:0+critic/critic_hl0/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2#
!critic/critic_hl0/Tensordot/stack�
%critic/critic_hl0/Tensordot/transpose	Transpose critic/critic_i/BiasAdd:output:0+critic/critic_hl0/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������@2'
%critic/critic_hl0/Tensordot/transpose�
#critic/critic_hl0/Tensordot/ReshapeReshape)critic/critic_hl0/Tensordot/transpose:y:0*critic/critic_hl0/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2%
#critic/critic_hl0/Tensordot/Reshape�
"critic/critic_hl0/Tensordot/MatMulMatMul,critic/critic_hl0/Tensordot/Reshape:output:02critic/critic_hl0/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2$
"critic/critic_hl0/Tensordot/MatMul�
#critic/critic_hl0/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2%
#critic/critic_hl0/Tensordot/Const_2�
)critic/critic_hl0/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)critic/critic_hl0/Tensordot/concat_1/axis�
$critic/critic_hl0/Tensordot/concat_1ConcatV2-critic/critic_hl0/Tensordot/GatherV2:output:0,critic/critic_hl0/Tensordot/Const_2:output:02critic/critic_hl0/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2&
$critic/critic_hl0/Tensordot/concat_1�
critic/critic_hl0/TensordotReshape,critic/critic_hl0/Tensordot/MatMul:product:0-critic/critic_hl0/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
critic/critic_hl0/Tensordot�
(critic/critic_hl0/BiasAdd/ReadVariableOpReadVariableOp1critic_critic_hl0_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02*
(critic/critic_hl0/BiasAdd/ReadVariableOp�
critic/critic_hl0/BiasAddBiasAdd$critic/critic_hl0/Tensordot:output:00critic/critic_hl0/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2
critic/critic_hl0/BiasAdd�
critic/critic_hl0/ReluRelu"critic/critic_hl0/BiasAdd:output:0*
T0*+
_output_shapes
:���������@2
critic/critic_hl0/Relu�
*critic/critic_hl1/Tensordot/ReadVariableOpReadVariableOp3critic_critic_hl1_tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02,
*critic/critic_hl1/Tensordot/ReadVariableOp�
 critic/critic_hl1/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2"
 critic/critic_hl1/Tensordot/axes�
 critic/critic_hl1/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2"
 critic/critic_hl1/Tensordot/free�
!critic/critic_hl1/Tensordot/ShapeShape$critic/critic_hl0/Relu:activations:0*
T0*
_output_shapes
:2#
!critic/critic_hl1/Tensordot/Shape�
)critic/critic_hl1/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)critic/critic_hl1/Tensordot/GatherV2/axis�
$critic/critic_hl1/Tensordot/GatherV2GatherV2*critic/critic_hl1/Tensordot/Shape:output:0)critic/critic_hl1/Tensordot/free:output:02critic/critic_hl1/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$critic/critic_hl1/Tensordot/GatherV2�
+critic/critic_hl1/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2-
+critic/critic_hl1/Tensordot/GatherV2_1/axis�
&critic/critic_hl1/Tensordot/GatherV2_1GatherV2*critic/critic_hl1/Tensordot/Shape:output:0)critic/critic_hl1/Tensordot/axes:output:04critic/critic_hl1/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2(
&critic/critic_hl1/Tensordot/GatherV2_1�
!critic/critic_hl1/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2#
!critic/critic_hl1/Tensordot/Const�
 critic/critic_hl1/Tensordot/ProdProd-critic/critic_hl1/Tensordot/GatherV2:output:0*critic/critic_hl1/Tensordot/Const:output:0*
T0*
_output_shapes
: 2"
 critic/critic_hl1/Tensordot/Prod�
#critic/critic_hl1/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2%
#critic/critic_hl1/Tensordot/Const_1�
"critic/critic_hl1/Tensordot/Prod_1Prod/critic/critic_hl1/Tensordot/GatherV2_1:output:0,critic/critic_hl1/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2$
"critic/critic_hl1/Tensordot/Prod_1�
'critic/critic_hl1/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'critic/critic_hl1/Tensordot/concat/axis�
"critic/critic_hl1/Tensordot/concatConcatV2)critic/critic_hl1/Tensordot/free:output:0)critic/critic_hl1/Tensordot/axes:output:00critic/critic_hl1/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2$
"critic/critic_hl1/Tensordot/concat�
!critic/critic_hl1/Tensordot/stackPack)critic/critic_hl1/Tensordot/Prod:output:0+critic/critic_hl1/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2#
!critic/critic_hl1/Tensordot/stack�
%critic/critic_hl1/Tensordot/transpose	Transpose$critic/critic_hl0/Relu:activations:0+critic/critic_hl1/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������@2'
%critic/critic_hl1/Tensordot/transpose�
#critic/critic_hl1/Tensordot/ReshapeReshape)critic/critic_hl1/Tensordot/transpose:y:0*critic/critic_hl1/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2%
#critic/critic_hl1/Tensordot/Reshape�
"critic/critic_hl1/Tensordot/MatMulMatMul,critic/critic_hl1/Tensordot/Reshape:output:02critic/critic_hl1/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2$
"critic/critic_hl1/Tensordot/MatMul�
#critic/critic_hl1/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2%
#critic/critic_hl1/Tensordot/Const_2�
)critic/critic_hl1/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)critic/critic_hl1/Tensordot/concat_1/axis�
$critic/critic_hl1/Tensordot/concat_1ConcatV2-critic/critic_hl1/Tensordot/GatherV2:output:0,critic/critic_hl1/Tensordot/Const_2:output:02critic/critic_hl1/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2&
$critic/critic_hl1/Tensordot/concat_1�
critic/critic_hl1/TensordotReshape,critic/critic_hl1/Tensordot/MatMul:product:0-critic/critic_hl1/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
critic/critic_hl1/Tensordot�
(critic/critic_hl1/BiasAdd/ReadVariableOpReadVariableOp1critic_critic_hl1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02*
(critic/critic_hl1/BiasAdd/ReadVariableOp�
critic/critic_hl1/BiasAddBiasAdd$critic/critic_hl1/Tensordot:output:00critic/critic_hl1/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2
critic/critic_hl1/BiasAdd�
critic/critic_hl1/ReluRelu"critic/critic_hl1/BiasAdd:output:0*
T0*+
_output_shapes
:���������@2
critic/critic_hl1/Relu�
(critic/critic_o/Tensordot/ReadVariableOpReadVariableOp1critic_critic_o_tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02*
(critic/critic_o/Tensordot/ReadVariableOp�
critic/critic_o/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
critic/critic_o/Tensordot/axes�
critic/critic_o/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
critic/critic_o/Tensordot/free�
critic/critic_o/Tensordot/ShapeShape$critic/critic_hl1/Relu:activations:0*
T0*
_output_shapes
:2!
critic/critic_o/Tensordot/Shape�
'critic/critic_o/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'critic/critic_o/Tensordot/GatherV2/axis�
"critic/critic_o/Tensordot/GatherV2GatherV2(critic/critic_o/Tensordot/Shape:output:0'critic/critic_o/Tensordot/free:output:00critic/critic_o/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"critic/critic_o/Tensordot/GatherV2�
)critic/critic_o/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)critic/critic_o/Tensordot/GatherV2_1/axis�
$critic/critic_o/Tensordot/GatherV2_1GatherV2(critic/critic_o/Tensordot/Shape:output:0'critic/critic_o/Tensordot/axes:output:02critic/critic_o/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$critic/critic_o/Tensordot/GatherV2_1�
critic/critic_o/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
critic/critic_o/Tensordot/Const�
critic/critic_o/Tensordot/ProdProd+critic/critic_o/Tensordot/GatherV2:output:0(critic/critic_o/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
critic/critic_o/Tensordot/Prod�
!critic/critic_o/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!critic/critic_o/Tensordot/Const_1�
 critic/critic_o/Tensordot/Prod_1Prod-critic/critic_o/Tensordot/GatherV2_1:output:0*critic/critic_o/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 critic/critic_o/Tensordot/Prod_1�
%critic/critic_o/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%critic/critic_o/Tensordot/concat/axis�
 critic/critic_o/Tensordot/concatConcatV2'critic/critic_o/Tensordot/free:output:0'critic/critic_o/Tensordot/axes:output:0.critic/critic_o/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 critic/critic_o/Tensordot/concat�
critic/critic_o/Tensordot/stackPack'critic/critic_o/Tensordot/Prod:output:0)critic/critic_o/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
critic/critic_o/Tensordot/stack�
#critic/critic_o/Tensordot/transpose	Transpose$critic/critic_hl1/Relu:activations:0)critic/critic_o/Tensordot/concat:output:0*
T0*+
_output_shapes
:���������@2%
#critic/critic_o/Tensordot/transpose�
!critic/critic_o/Tensordot/ReshapeReshape'critic/critic_o/Tensordot/transpose:y:0(critic/critic_o/Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2#
!critic/critic_o/Tensordot/Reshape�
 critic/critic_o/Tensordot/MatMulMatMul*critic/critic_o/Tensordot/Reshape:output:00critic/critic_o/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 critic/critic_o/Tensordot/MatMul�
!critic/critic_o/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2#
!critic/critic_o/Tensordot/Const_2�
'critic/critic_o/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'critic/critic_o/Tensordot/concat_1/axis�
"critic/critic_o/Tensordot/concat_1ConcatV2+critic/critic_o/Tensordot/GatherV2:output:0*critic/critic_o/Tensordot/Const_2:output:00critic/critic_o/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"critic/critic_o/Tensordot/concat_1�
critic/critic_o/TensordotReshape*critic/critic_o/Tensordot/MatMul:product:0+critic/critic_o/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������2
critic/critic_o/Tensordot�
&critic/critic_o/BiasAdd/ReadVariableOpReadVariableOp/critic_critic_o_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02(
&critic/critic_o/BiasAdd/ReadVariableOp�
critic/critic_o/BiasAddBiasAdd"critic/critic_o/Tensordot:output:0.critic/critic_o/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������2
critic/critic_o/BiasAdd�
IdentityIdentity critic/critic_o/BiasAdd:output:0)^critic/critic_hl0/BiasAdd/ReadVariableOp+^critic/critic_hl0/Tensordot/ReadVariableOp)^critic/critic_hl1/BiasAdd/ReadVariableOp+^critic/critic_hl1/Tensordot/ReadVariableOp'^critic/critic_i/BiasAdd/ReadVariableOp)^critic/critic_i/Tensordot/ReadVariableOp'^critic/critic_o/BiasAdd/ReadVariableOp)^critic/critic_o/Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:���������1::::::::2T
(critic/critic_hl0/BiasAdd/ReadVariableOp(critic/critic_hl0/BiasAdd/ReadVariableOp2X
*critic/critic_hl0/Tensordot/ReadVariableOp*critic/critic_hl0/Tensordot/ReadVariableOp2T
(critic/critic_hl1/BiasAdd/ReadVariableOp(critic/critic_hl1/BiasAdd/ReadVariableOp2X
*critic/critic_hl1/Tensordot/ReadVariableOp*critic/critic_hl1/Tensordot/ReadVariableOp2P
&critic/critic_i/BiasAdd/ReadVariableOp&critic/critic_i/BiasAdd/ReadVariableOp2T
(critic/critic_i/Tensordot/ReadVariableOp(critic/critic_i/Tensordot/ReadVariableOp2P
&critic/critic_o/BiasAdd/ReadVariableOp&critic/critic_o/BiasAdd/ReadVariableOp2T
(critic/critic_o/Tensordot/ReadVariableOp(critic/critic_o/Tensordot/ReadVariableOp:T P
+
_output_shapes
:���������1
!
_user_specified_name	input_1
� 
�
F__inference_critic_hl0_layer_call_and_return_conditional_losses_738262

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�&
�
"__inference__traced_restore_738656
file_prefix+
'assignvariableop_critic_critic_i_kernel+
'assignvariableop_1_critic_critic_i_bias-
)assignvariableop_2_critic_critic_o_kernel+
'assignvariableop_3_critic_critic_o_bias/
+assignvariableop_4_critic_critic_hl0_kernel-
)assignvariableop_5_critic_critic_hl0_bias/
+assignvariableop_6_critic_critic_hl1_kernel-
)assignvariableop_7_critic_critic_hl1_bias

identity_9��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_2�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*�
value�B�	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*8
_output_shapes&
$:::::::::*
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp'assignvariableop_critic_critic_i_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp'assignvariableop_1_critic_critic_i_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp)assignvariableop_2_critic_critic_o_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp'assignvariableop_3_critic_critic_o_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp+assignvariableop_4_critic_critic_hl0_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp)assignvariableop_5_critic_critic_hl0_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp+assignvariableop_6_critic_critic_hl1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp)assignvariableop_7_critic_critic_hl1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_79
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�

Identity_8Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_8�

Identity_9IdentityIdentity_8:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7*
T0*
_output_shapes
: 2

Identity_9"!

identity_9Identity_9:output:0*5
_input_shapes$
": ::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_7:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
D__inference_critic_o_layer_call_and_return_conditional_losses_738486

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
� 
�
F__inference_critic_hl0_layer_call_and_return_conditional_losses_738526

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
� 
�
F__inference_critic_hl1_layer_call_and_return_conditional_losses_738566

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
D__inference_critic_i_layer_call_and_return_conditional_losses_738215

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:1@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������12
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������1::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������1
 
_user_specified_nameinputs
�
�
__inference__traced_save_738622
file_prefix5
1savev2_critic_critic_i_kernel_read_readvariableop3
/savev2_critic_critic_i_bias_read_readvariableop5
1savev2_critic_critic_o_kernel_read_readvariableop3
/savev2_critic_critic_o_bias_read_readvariableop7
3savev2_critic_critic_hl0_kernel_read_readvariableop5
1savev2_critic_critic_hl0_bias_read_readvariableop7
3savev2_critic_critic_hl1_kernel_read_readvariableop5
1savev2_critic_critic_hl1_bias_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*�
value�B�	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:01savev2_critic_critic_i_kernel_read_readvariableop/savev2_critic_critic_i_bias_read_readvariableop1savev2_critic_critic_o_kernel_read_readvariableop/savev2_critic_critic_o_bias_read_readvariableop3savev2_critic_critic_hl0_kernel_read_readvariableop1savev2_critic_critic_hl0_bias_read_readvariableop3savev2_critic_critic_hl1_kernel_read_readvariableop1savev2_critic_critic_hl1_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
2	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*W
_input_shapesF
D: :1@:@:@::@@:@:@@:@: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:1@: 

_output_shapes
:@:$ 

_output_shapes

:@: 

_output_shapes
::$ 

_output_shapes

:@@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:	

_output_shapes
: 
�
�
'__inference_critic_layer_call_fn_738394
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_critic_layer_call_and_return_conditional_losses_7383722
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:���������2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:���������1::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:���������1
!
_user_specified_name	input_1
� 
�
F__inference_critic_hl1_layer_call_and_return_conditional_losses_738309

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Tensordot/ReadVariableOp�
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis�
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis�
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const�
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1�
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis�
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat�
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack�
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:���������@2
Tensordot/transpose�
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:������������������2
Tensordot/Reshape�
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axis�
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1�
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:���������@2
	Tensordot�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:���������@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:���������@2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:���������@
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
?
input_14
serving_default_input_1:0���������1@
output_14
StatefulPartitionedCall:0���������tensorflow/serving/predict:�f
�
i
hidden_layers
o
trainable_variables
regularization_losses
	variables
	keras_api

signatures
<__call__
=_default_save_signature
*>&call_and_return_all_conditional_losses"�
_tf_keras_model�{"class_name": "Critic", "name": "critic", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"layer was saved without config": true}, "is_graph_network": false, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Critic"}}
�

	kernel

bias
trainable_variables
regularization_losses
	variables
	keras_api
?__call__
*@&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_i", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, 2, 49]}, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_i", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 2, 49]}, "dtype": "float32", "units": 64, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 49}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 49]}}
.
0
1"
trackable_list_wrapper
�

kernel
bias
trainable_variables
regularization_losses
	variables
	keras_api
A__call__
*B&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_o", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_o", "trainable": true, "dtype": "float32", "units": 8, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
 "
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
�

layers
trainable_variables
regularization_losses
non_trainable_variables
layer_regularization_losses
	variables
metrics
layer_metrics
<__call__
=_default_save_signature
*>&call_and_return_all_conditional_losses
&>"call_and_return_conditional_losses"
_generic_user_object
,
Cserving_default"
signature_map
(:&1@2critic/critic_i/kernel
": @2critic/critic_i/bias
.
	0

1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
�

 layers
trainable_variables
!non_trainable_variables
regularization_losses
"layer_regularization_losses
	variables
#metrics
$layer_metrics
?__call__
*@&call_and_return_all_conditional_losses
&@"call_and_return_conditional_losses"
_generic_user_object
�

kernel
bias
%trainable_variables
&regularization_losses
'	variables
(	keras_api
D__call__
*E&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_hl0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_hl0", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
�

kernel
bias
)trainable_variables
*regularization_losses
+	variables
,	keras_api
F__call__
*G&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "critic_hl1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "critic_hl1", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
(:&@2critic/critic_o/kernel
": 2critic/critic_o/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�

-layers
trainable_variables
.non_trainable_variables
regularization_losses
/layer_regularization_losses
	variables
0metrics
1layer_metrics
A__call__
*B&call_and_return_all_conditional_losses
&B"call_and_return_conditional_losses"
_generic_user_object
*:(@@2critic/critic_hl0/kernel
$:"@2critic/critic_hl0/bias
*:(@@2critic/critic_hl1/kernel
$:"@2critic/critic_hl1/bias
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�

2layers
%trainable_variables
3non_trainable_variables
&regularization_losses
4layer_regularization_losses
'	variables
5metrics
6layer_metrics
D__call__
*E&call_and_return_all_conditional_losses
&E"call_and_return_conditional_losses"
_generic_user_object
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�

7layers
)trainable_variables
8non_trainable_variables
*regularization_losses
9layer_regularization_losses
+	variables
:metrics
;layer_metrics
F__call__
*G&call_and_return_all_conditional_losses
&G"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�2�
'__inference_critic_layer_call_fn_738394�
���
FullArgSpec!
args�
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� **�'
%�"
input_1���������1
�2�
!__inference__wrapped_model_738181�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� **�'
%�"
input_1���������1
�2�
B__inference_critic_layer_call_and_return_conditional_losses_738372�
���
FullArgSpec!
args�
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� **�'
%�"
input_1���������1
�2�
)__inference_critic_i_layer_call_fn_738456�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_critic_i_layer_call_and_return_conditional_losses_738447�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_critic_o_layer_call_fn_738495�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_critic_o_layer_call_and_return_conditional_losses_738486�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
$__inference_signature_wrapper_738417input_1"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_critic_hl0_layer_call_fn_738535�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_critic_hl0_layer_call_and_return_conditional_losses_738526�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_critic_hl1_layer_call_fn_738575�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_critic_hl1_layer_call_and_return_conditional_losses_738566�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
!__inference__wrapped_model_738181y	
4�1
*�'
%�"
input_1���������1
� "7�4
2
output_1&�#
output_1����������
F__inference_critic_hl0_layer_call_and_return_conditional_losses_738526d3�0
)�&
$�!
inputs���������@
� ")�&
�
0���������@
� �
+__inference_critic_hl0_layer_call_fn_738535W3�0
)�&
$�!
inputs���������@
� "����������@�
F__inference_critic_hl1_layer_call_and_return_conditional_losses_738566d3�0
)�&
$�!
inputs���������@
� ")�&
�
0���������@
� �
+__inference_critic_hl1_layer_call_fn_738575W3�0
)�&
$�!
inputs���������@
� "����������@�
D__inference_critic_i_layer_call_and_return_conditional_losses_738447d	
3�0
)�&
$�!
inputs���������1
� ")�&
�
0���������@
� �
)__inference_critic_i_layer_call_fn_738456W	
3�0
)�&
$�!
inputs���������1
� "����������@�
B__inference_critic_layer_call_and_return_conditional_losses_738372k	
4�1
*�'
%�"
input_1���������1
� ")�&
�
0���������
� �
'__inference_critic_layer_call_fn_738394^	
4�1
*�'
%�"
input_1���������1
� "�����������
D__inference_critic_o_layer_call_and_return_conditional_losses_738486d3�0
)�&
$�!
inputs���������@
� ")�&
�
0���������
� �
)__inference_critic_o_layer_call_fn_738495W3�0
)�&
$�!
inputs���������@
� "�����������
$__inference_signature_wrapper_738417�	
?�<
� 
5�2
0
input_1%�"
input_1���������1"7�4
2
output_1&�#
output_1���������