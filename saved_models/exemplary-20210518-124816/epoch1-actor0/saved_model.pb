лю
Ђї
B
AssignVariableOp
resource
value"dtype"
dtypetype
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
,
Exp
x"T
y"T"
Ttype:

2
­
GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"

batch_dimsint "
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	

Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:

Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
О
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
;
Sub
x"T
y"T
z"T"
Ttype:
2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	

VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 "serve*2.4.12v2.4.0-49-g85c8b2a817f8ч№

actor/actor_i/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0d*%
shared_nameactor/actor_i/kernel
}
(actor/actor_i/kernel/Read/ReadVariableOpReadVariableOpactor/actor_i/kernel*
_output_shapes

:0d*
dtype0
|
actor/actor_i/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*#
shared_nameactor/actor_i/bias
u
&actor/actor_i/bias/Read/ReadVariableOpReadVariableOpactor/actor_i/bias*
_output_shapes
:d*
dtype0

actor/actor_o/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*%
shared_nameactor/actor_o/kernel
}
(actor/actor_o/kernel/Read/ReadVariableOpReadVariableOpactor/actor_o/kernel*
_output_shapes

:@*
dtype0
|
actor/actor_o/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameactor/actor_o/bias
u
&actor/actor_o/bias/Read/ReadVariableOpReadVariableOpactor/actor_o/bias*
_output_shapes
:*
dtype0

actor/actor_hl0/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d@*'
shared_nameactor/actor_hl0/kernel

*actor/actor_hl0/kernel/Read/ReadVariableOpReadVariableOpactor/actor_hl0/kernel*
_output_shapes

:d@*
dtype0

actor/actor_hl0/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameactor/actor_hl0/bias
y
(actor/actor_hl0/bias/Read/ReadVariableOpReadVariableOpactor/actor_hl0/bias*
_output_shapes
:@*
dtype0

actor/actor_hl1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*'
shared_nameactor/actor_hl1/kernel

*actor/actor_hl1/kernel/Read/ReadVariableOpReadVariableOpactor/actor_hl1/kernel*
_output_shapes

:@@*
dtype0

actor/actor_hl1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameactor/actor_hl1/bias
y
(actor/actor_hl1/bias/Read/ReadVariableOpReadVariableOpactor/actor_hl1/bias*
_output_shapes
:@*
dtype0

NoOpNoOp
е
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*
valueB Bќ

i
hidden_layers
o
	variables
regularization_losses
trainable_variables
	keras_api

signatures
h

	kernel

bias
	variables
regularization_losses
trainable_variables
	keras_api

0
1
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
8
	0

1
2
3
4
5
6
7
 
8
	0

1
2
3
4
5
6
7
­

layers
non_trainable_variables
	variables
layer_metrics
metrics
regularization_losses
trainable_variables
layer_regularization_losses
 
MK
VARIABLE_VALUEactor/actor_i/kernel#i/kernel/.ATTRIBUTES/VARIABLE_VALUE
IG
VARIABLE_VALUEactor/actor_i/bias!i/bias/.ATTRIBUTES/VARIABLE_VALUE

	0

1
 

	0

1
­

 layers
!non_trainable_variables
	variables
"layer_metrics
#metrics
regularization_losses
trainable_variables
$layer_regularization_losses
h

kernel
bias
%	variables
&regularization_losses
'trainable_variables
(	keras_api
h

kernel
bias
)	variables
*regularization_losses
+trainable_variables
,	keras_api
MK
VARIABLE_VALUEactor/actor_o/kernel#o/kernel/.ATTRIBUTES/VARIABLE_VALUE
IG
VARIABLE_VALUEactor/actor_o/bias!o/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
­

-layers
.non_trainable_variables
	variables
/layer_metrics
0metrics
regularization_losses
trainable_variables
1layer_regularization_losses
RP
VARIABLE_VALUEactor/actor_hl0/kernel&variables/2/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEactor/actor_hl0/bias&variables/3/.ATTRIBUTES/VARIABLE_VALUE
RP
VARIABLE_VALUEactor/actor_hl1/kernel&variables/4/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEactor/actor_hl1/bias&variables/5/.ATTRIBUTES/VARIABLE_VALUE

0
1
2
3
 
 
 
 
 
 
 
 
 

0
1
 

0
1
­

2layers
3non_trainable_variables
%	variables
4layer_metrics
5metrics
&regularization_losses
'trainable_variables
6layer_regularization_losses

0
1
 

0
1
­

7layers
8non_trainable_variables
)	variables
9layer_metrics
:metrics
*regularization_losses
+trainable_variables
;layer_regularization_losses
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

serving_default_input_1Placeholder*+
_output_shapes
:џџџџџџџџџ0*
dtype0* 
shape:џџџџџџџџџ0
і
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1actor/actor_i/kernelactor/actor_i/biasactor/actor_hl0/kernelactor/actor_hl0/biasactor/actor_hl1/kernelactor/actor_hl1/biasactor/actor_o/kernelactor/actor_o/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *-
f(R&
$__inference_signature_wrapper_547092
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
ѓ
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename(actor/actor_i/kernel/Read/ReadVariableOp&actor/actor_i/bias/Read/ReadVariableOp(actor/actor_o/kernel/Read/ReadVariableOp&actor/actor_o/bias/Read/ReadVariableOp*actor/actor_hl0/kernel/Read/ReadVariableOp(actor/actor_hl0/bias/Read/ReadVariableOp*actor/actor_hl1/kernel/Read/ReadVariableOp(actor/actor_hl1/bias/Read/ReadVariableOpConst*
Tin
2
*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *(
f#R!
__inference__traced_save_547304
Ю
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameactor/actor_i/kernelactor/actor_i/biasactor/actor_o/kernelactor/actor_o/biasactor/actor_hl0/kernelactor/actor_hl0/biasactor/actor_hl1/kernelactor/actor_hl1/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8 *+
f&R$
"__inference__traced_restore_547338аО
ю

*__inference_actor_hl0_layer_call_fn_547217

inputs
unknown
	unknown_0
identityЂStatefulPartitionedCallљ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_actor_hl0_layer_call_and_return_conditional_losses_5469302
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџ@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџd::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs

д
$__inference_signature_wrapper_547092
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identityЂStatefulPartitionedCallЄ
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 **
f%R#
!__inference__wrapped_model_5468492
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:џџџџџџџџџ0::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:џџџџџџџџџ0
!
_user_specified_name	input_1
Ћ
Б
!__inference__wrapped_model_546849
input_13
/actor_actor_i_tensordot_readvariableop_resource1
-actor_actor_i_biasadd_readvariableop_resource5
1actor_actor_hl0_tensordot_readvariableop_resource3
/actor_actor_hl0_biasadd_readvariableop_resource5
1actor_actor_hl1_tensordot_readvariableop_resource3
/actor_actor_hl1_biasadd_readvariableop_resource3
/actor_actor_o_tensordot_readvariableop_resource1
-actor_actor_o_biasadd_readvariableop_resource
identityЂ&actor/actor_hl0/BiasAdd/ReadVariableOpЂ(actor/actor_hl0/Tensordot/ReadVariableOpЂ&actor/actor_hl1/BiasAdd/ReadVariableOpЂ(actor/actor_hl1/Tensordot/ReadVariableOpЂ$actor/actor_i/BiasAdd/ReadVariableOpЂ&actor/actor_i/Tensordot/ReadVariableOpЂ$actor/actor_o/BiasAdd/ReadVariableOpЂ&actor/actor_o/Tensordot/ReadVariableOpР
&actor/actor_i/Tensordot/ReadVariableOpReadVariableOp/actor_actor_i_tensordot_readvariableop_resource*
_output_shapes

:0d*
dtype02(
&actor/actor_i/Tensordot/ReadVariableOp
actor/actor_i/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
actor/actor_i/Tensordot/axes
actor/actor_i/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
actor/actor_i/Tensordot/freeu
actor/actor_i/Tensordot/ShapeShapeinput_1*
T0*
_output_shapes
:2
actor/actor_i/Tensordot/Shape
%actor/actor_i/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_i/Tensordot/GatherV2/axis
 actor/actor_i/Tensordot/GatherV2GatherV2&actor/actor_i/Tensordot/Shape:output:0%actor/actor_i/Tensordot/free:output:0.actor/actor_i/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2"
 actor/actor_i/Tensordot/GatherV2
'actor/actor_i/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_i/Tensordot/GatherV2_1/axis
"actor/actor_i/Tensordot/GatherV2_1GatherV2&actor/actor_i/Tensordot/Shape:output:0%actor/actor_i/Tensordot/axes:output:00actor/actor_i/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_i/Tensordot/GatherV2_1
actor/actor_i/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
actor/actor_i/Tensordot/ConstИ
actor/actor_i/Tensordot/ProdProd)actor/actor_i/Tensordot/GatherV2:output:0&actor/actor_i/Tensordot/Const:output:0*
T0*
_output_shapes
: 2
actor/actor_i/Tensordot/Prod
actor/actor_i/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_i/Tensordot/Const_1Р
actor/actor_i/Tensordot/Prod_1Prod+actor/actor_i/Tensordot/GatherV2_1:output:0(actor/actor_i/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2 
actor/actor_i/Tensordot/Prod_1
#actor/actor_i/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2%
#actor/actor_i/Tensordot/concat/axisі
actor/actor_i/Tensordot/concatConcatV2%actor/actor_i/Tensordot/free:output:0%actor/actor_i/Tensordot/axes:output:0,actor/actor_i/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2 
actor/actor_i/Tensordot/concatФ
actor/actor_i/Tensordot/stackPack%actor/actor_i/Tensordot/Prod:output:0'actor/actor_i/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
actor/actor_i/Tensordot/stackЛ
!actor/actor_i/Tensordot/transpose	Transposeinput_1'actor/actor_i/Tensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ02#
!actor/actor_i/Tensordot/transposeз
actor/actor_i/Tensordot/ReshapeReshape%actor/actor_i/Tensordot/transpose:y:0&actor/actor_i/Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2!
actor/actor_i/Tensordot/Reshapeж
actor/actor_i/Tensordot/MatMulMatMul(actor/actor_i/Tensordot/Reshape:output:0.actor/actor_i/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџd2 
actor/actor_i/Tensordot/MatMul
actor/actor_i/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2!
actor/actor_i/Tensordot/Const_2
%actor/actor_i/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_i/Tensordot/concat_1/axis
 actor/actor_i/Tensordot/concat_1ConcatV2)actor/actor_i/Tensordot/GatherV2:output:0(actor/actor_i/Tensordot/Const_2:output:0.actor/actor_i/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_i/Tensordot/concat_1Ш
actor/actor_i/TensordotReshape(actor/actor_i/Tensordot/MatMul:product:0)actor/actor_i/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџd2
actor/actor_i/TensordotЖ
$actor/actor_i/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_i_biasadd_readvariableop_resource*
_output_shapes
:d*
dtype02&
$actor/actor_i/BiasAdd/ReadVariableOpП
actor/actor_i/BiasAddBiasAdd actor/actor_i/Tensordot:output:0,actor/actor_i/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџd2
actor/actor_i/BiasAddЦ
(actor/actor_hl0/Tensordot/ReadVariableOpReadVariableOp1actor_actor_hl0_tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02*
(actor/actor_hl0/Tensordot/ReadVariableOp
actor/actor_hl0/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
actor/actor_hl0/Tensordot/axes
actor/actor_hl0/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
actor/actor_hl0/Tensordot/free
actor/actor_hl0/Tensordot/ShapeShapeactor/actor_i/BiasAdd:output:0*
T0*
_output_shapes
:2!
actor/actor_hl0/Tensordot/Shape
'actor/actor_hl0/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl0/Tensordot/GatherV2/axisЁ
"actor/actor_hl0/Tensordot/GatherV2GatherV2(actor/actor_hl0/Tensordot/Shape:output:0'actor/actor_hl0/Tensordot/free:output:00actor/actor_hl0/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_hl0/Tensordot/GatherV2
)actor/actor_hl0/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)actor/actor_hl0/Tensordot/GatherV2_1/axisЇ
$actor/actor_hl0/Tensordot/GatherV2_1GatherV2(actor/actor_hl0/Tensordot/Shape:output:0'actor/actor_hl0/Tensordot/axes:output:02actor/actor_hl0/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$actor/actor_hl0/Tensordot/GatherV2_1
actor/actor_hl0/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_hl0/Tensordot/ConstР
actor/actor_hl0/Tensordot/ProdProd+actor/actor_hl0/Tensordot/GatherV2:output:0(actor/actor_hl0/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
actor/actor_hl0/Tensordot/Prod
!actor/actor_hl0/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!actor/actor_hl0/Tensordot/Const_1Ш
 actor/actor_hl0/Tensordot/Prod_1Prod-actor/actor_hl0/Tensordot/GatherV2_1:output:0*actor/actor_hl0/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 actor/actor_hl0/Tensordot/Prod_1
%actor/actor_hl0/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_hl0/Tensordot/concat/axis
 actor/actor_hl0/Tensordot/concatConcatV2'actor/actor_hl0/Tensordot/free:output:0'actor/actor_hl0/Tensordot/axes:output:0.actor/actor_hl0/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_hl0/Tensordot/concatЬ
actor/actor_hl0/Tensordot/stackPack'actor/actor_hl0/Tensordot/Prod:output:0)actor/actor_hl0/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
actor/actor_hl0/Tensordot/stackи
#actor/actor_hl0/Tensordot/transpose	Transposeactor/actor_i/BiasAdd:output:0)actor/actor_hl0/Tensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџd2%
#actor/actor_hl0/Tensordot/transposeп
!actor/actor_hl0/Tensordot/ReshapeReshape'actor/actor_hl0/Tensordot/transpose:y:0(actor/actor_hl0/Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2#
!actor/actor_hl0/Tensordot/Reshapeо
 actor/actor_hl0/Tensordot/MatMulMatMul*actor/actor_hl0/Tensordot/Reshape:output:00actor/actor_hl0/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ@2"
 actor/actor_hl0/Tensordot/MatMul
!actor/actor_hl0/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!actor/actor_hl0/Tensordot/Const_2
'actor/actor_hl0/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl0/Tensordot/concat_1/axis
"actor/actor_hl0/Tensordot/concat_1ConcatV2+actor/actor_hl0/Tensordot/GatherV2:output:0*actor/actor_hl0/Tensordot/Const_2:output:00actor/actor_hl0/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"actor/actor_hl0/Tensordot/concat_1а
actor/actor_hl0/TensordotReshape*actor/actor_hl0/Tensordot/MatMul:product:0+actor/actor_hl0/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
actor/actor_hl0/TensordotМ
&actor/actor_hl0/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl0_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl0/BiasAdd/ReadVariableOpЧ
actor/actor_hl0/BiasAddBiasAdd"actor/actor_hl0/Tensordot:output:0.actor/actor_hl0/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
actor/actor_hl0/BiasAdd
actor/actor_hl0/ReluRelu actor/actor_hl0/BiasAdd:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
actor/actor_hl0/ReluЦ
(actor/actor_hl1/Tensordot/ReadVariableOpReadVariableOp1actor_actor_hl1_tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02*
(actor/actor_hl1/Tensordot/ReadVariableOp
actor/actor_hl1/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
actor/actor_hl1/Tensordot/axes
actor/actor_hl1/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
actor/actor_hl1/Tensordot/free
actor/actor_hl1/Tensordot/ShapeShape"actor/actor_hl0/Relu:activations:0*
T0*
_output_shapes
:2!
actor/actor_hl1/Tensordot/Shape
'actor/actor_hl1/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl1/Tensordot/GatherV2/axisЁ
"actor/actor_hl1/Tensordot/GatherV2GatherV2(actor/actor_hl1/Tensordot/Shape:output:0'actor/actor_hl1/Tensordot/free:output:00actor/actor_hl1/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_hl1/Tensordot/GatherV2
)actor/actor_hl1/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)actor/actor_hl1/Tensordot/GatherV2_1/axisЇ
$actor/actor_hl1/Tensordot/GatherV2_1GatherV2(actor/actor_hl1/Tensordot/Shape:output:0'actor/actor_hl1/Tensordot/axes:output:02actor/actor_hl1/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$actor/actor_hl1/Tensordot/GatherV2_1
actor/actor_hl1/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_hl1/Tensordot/ConstР
actor/actor_hl1/Tensordot/ProdProd+actor/actor_hl1/Tensordot/GatherV2:output:0(actor/actor_hl1/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
actor/actor_hl1/Tensordot/Prod
!actor/actor_hl1/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!actor/actor_hl1/Tensordot/Const_1Ш
 actor/actor_hl1/Tensordot/Prod_1Prod-actor/actor_hl1/Tensordot/GatherV2_1:output:0*actor/actor_hl1/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 actor/actor_hl1/Tensordot/Prod_1
%actor/actor_hl1/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_hl1/Tensordot/concat/axis
 actor/actor_hl1/Tensordot/concatConcatV2'actor/actor_hl1/Tensordot/free:output:0'actor/actor_hl1/Tensordot/axes:output:0.actor/actor_hl1/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_hl1/Tensordot/concatЬ
actor/actor_hl1/Tensordot/stackPack'actor/actor_hl1/Tensordot/Prod:output:0)actor/actor_hl1/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
actor/actor_hl1/Tensordot/stackм
#actor/actor_hl1/Tensordot/transpose	Transpose"actor/actor_hl0/Relu:activations:0)actor/actor_hl1/Tensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2%
#actor/actor_hl1/Tensordot/transposeп
!actor/actor_hl1/Tensordot/ReshapeReshape'actor/actor_hl1/Tensordot/transpose:y:0(actor/actor_hl1/Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2#
!actor/actor_hl1/Tensordot/Reshapeо
 actor/actor_hl1/Tensordot/MatMulMatMul*actor/actor_hl1/Tensordot/Reshape:output:00actor/actor_hl1/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ@2"
 actor/actor_hl1/Tensordot/MatMul
!actor/actor_hl1/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!actor/actor_hl1/Tensordot/Const_2
'actor/actor_hl1/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl1/Tensordot/concat_1/axis
"actor/actor_hl1/Tensordot/concat_1ConcatV2+actor/actor_hl1/Tensordot/GatherV2:output:0*actor/actor_hl1/Tensordot/Const_2:output:00actor/actor_hl1/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"actor/actor_hl1/Tensordot/concat_1а
actor/actor_hl1/TensordotReshape*actor/actor_hl1/Tensordot/MatMul:product:0+actor/actor_hl1/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
actor/actor_hl1/TensordotМ
&actor/actor_hl1/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl1/BiasAdd/ReadVariableOpЧ
actor/actor_hl1/BiasAddBiasAdd"actor/actor_hl1/Tensordot:output:0.actor/actor_hl1/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
actor/actor_hl1/BiasAdd
actor/actor_hl1/ReluRelu actor/actor_hl1/BiasAdd:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
actor/actor_hl1/ReluР
&actor/actor_o/Tensordot/ReadVariableOpReadVariableOp/actor_actor_o_tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02(
&actor/actor_o/Tensordot/ReadVariableOp
actor/actor_o/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
actor/actor_o/Tensordot/axes
actor/actor_o/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
actor/actor_o/Tensordot/free
actor/actor_o/Tensordot/ShapeShape"actor/actor_hl1/Relu:activations:0*
T0*
_output_shapes
:2
actor/actor_o/Tensordot/Shape
%actor/actor_o/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_o/Tensordot/GatherV2/axis
 actor/actor_o/Tensordot/GatherV2GatherV2&actor/actor_o/Tensordot/Shape:output:0%actor/actor_o/Tensordot/free:output:0.actor/actor_o/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2"
 actor/actor_o/Tensordot/GatherV2
'actor/actor_o/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_o/Tensordot/GatherV2_1/axis
"actor/actor_o/Tensordot/GatherV2_1GatherV2&actor/actor_o/Tensordot/Shape:output:0%actor/actor_o/Tensordot/axes:output:00actor/actor_o/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_o/Tensordot/GatherV2_1
actor/actor_o/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
actor/actor_o/Tensordot/ConstИ
actor/actor_o/Tensordot/ProdProd)actor/actor_o/Tensordot/GatherV2:output:0&actor/actor_o/Tensordot/Const:output:0*
T0*
_output_shapes
: 2
actor/actor_o/Tensordot/Prod
actor/actor_o/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_o/Tensordot/Const_1Р
actor/actor_o/Tensordot/Prod_1Prod+actor/actor_o/Tensordot/GatherV2_1:output:0(actor/actor_o/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2 
actor/actor_o/Tensordot/Prod_1
#actor/actor_o/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2%
#actor/actor_o/Tensordot/concat/axisі
actor/actor_o/Tensordot/concatConcatV2%actor/actor_o/Tensordot/free:output:0%actor/actor_o/Tensordot/axes:output:0,actor/actor_o/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2 
actor/actor_o/Tensordot/concatФ
actor/actor_o/Tensordot/stackPack%actor/actor_o/Tensordot/Prod:output:0'actor/actor_o/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
actor/actor_o/Tensordot/stackж
!actor/actor_o/Tensordot/transpose	Transpose"actor/actor_hl1/Relu:activations:0'actor/actor_o/Tensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2#
!actor/actor_o/Tensordot/transposeз
actor/actor_o/Tensordot/ReshapeReshape%actor/actor_o/Tensordot/transpose:y:0&actor/actor_o/Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2!
actor/actor_o/Tensordot/Reshapeж
actor/actor_o/Tensordot/MatMulMatMul(actor/actor_o/Tensordot/Reshape:output:0.actor/actor_o/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2 
actor/actor_o/Tensordot/MatMul
actor/actor_o/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2!
actor/actor_o/Tensordot/Const_2
%actor/actor_o/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_o/Tensordot/concat_1/axis
 actor/actor_o/Tensordot/concat_1ConcatV2)actor/actor_o/Tensordot/GatherV2:output:0(actor/actor_o/Tensordot/Const_2:output:0.actor/actor_o/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_o/Tensordot/concat_1Ш
actor/actor_o/TensordotReshape(actor/actor_o/Tensordot/MatMul:product:0)actor/actor_o/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
actor/actor_o/TensordotЖ
$actor/actor_o/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_o_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02&
$actor/actor_o/BiasAdd/ReadVariableOpП
actor/actor_o/BiasAddBiasAdd actor/actor_o/Tensordot:output:0,actor/actor_o/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ2
actor/actor_o/BiasAdd
#actor/actor_o/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2%
#actor/actor_o/Max/reduction_indicesТ
actor/actor_o/MaxMaxactor/actor_o/BiasAdd:output:0,actor/actor_o/Max/reduction_indices:output:0*
T0*+
_output_shapes
:џџџџџџџџџ*
	keep_dims(2
actor/actor_o/Max
actor/actor_o/subSubactor/actor_o/BiasAdd:output:0actor/actor_o/Max:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
actor/actor_o/subz
actor/actor_o/ExpExpactor/actor_o/sub:z:0*
T0*+
_output_shapes
:џџџџџџџџџ2
actor/actor_o/Exp
#actor/actor_o/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2%
#actor/actor_o/Sum/reduction_indicesЙ
actor/actor_o/SumSumactor/actor_o/Exp:y:0,actor/actor_o/Sum/reduction_indices:output:0*
T0*+
_output_shapes
:џџџџџџџџџ*
	keep_dims(2
actor/actor_o/SumЂ
actor/actor_o/truedivRealDivactor/actor_o/Exp:y:0actor/actor_o/Sum:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
actor/actor_o/truedivЙ
IdentityIdentityactor/actor_o/truediv:z:0'^actor/actor_hl0/BiasAdd/ReadVariableOp)^actor/actor_hl0/Tensordot/ReadVariableOp'^actor/actor_hl1/BiasAdd/ReadVariableOp)^actor/actor_hl1/Tensordot/ReadVariableOp%^actor/actor_i/BiasAdd/ReadVariableOp'^actor/actor_i/Tensordot/ReadVariableOp%^actor/actor_o/BiasAdd/ReadVariableOp'^actor/actor_o/Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:џџџџџџџџџ0::::::::2P
&actor/actor_hl0/BiasAdd/ReadVariableOp&actor/actor_hl0/BiasAdd/ReadVariableOp2T
(actor/actor_hl0/Tensordot/ReadVariableOp(actor/actor_hl0/Tensordot/ReadVariableOp2P
&actor/actor_hl1/BiasAdd/ReadVariableOp&actor/actor_hl1/BiasAdd/ReadVariableOp2T
(actor/actor_hl1/Tensordot/ReadVariableOp(actor/actor_hl1/Tensordot/ReadVariableOp2L
$actor/actor_i/BiasAdd/ReadVariableOp$actor/actor_i/BiasAdd/ReadVariableOp2P
&actor/actor_i/Tensordot/ReadVariableOp&actor/actor_i/Tensordot/ReadVariableOp2L
$actor/actor_o/BiasAdd/ReadVariableOp$actor/actor_o/BiasAdd/ReadVariableOp2P
&actor/actor_o/Tensordot/ReadVariableOp&actor/actor_o/Tensordot/ReadVariableOp:T P
+
_output_shapes
:џџџџџџџџџ0
!
_user_specified_name	input_1
ю

*__inference_actor_hl1_layer_call_fn_547257

inputs
unknown
	unknown_0
identityЂStatefulPartitionedCallљ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_actor_hl1_layer_call_and_return_conditional_losses_5469772
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџ@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:џџџџџџџџџ@
 
_user_specified_nameinputs
Н%
д
"__inference__traced_restore_547338
file_prefix)
%assignvariableop_actor_actor_i_kernel)
%assignvariableop_1_actor_actor_i_bias+
'assignvariableop_2_actor_actor_o_kernel)
%assignvariableop_3_actor_actor_o_bias-
)assignvariableop_4_actor_actor_hl0_kernel+
'assignvariableop_5_actor_actor_hl0_bias-
)assignvariableop_6_actor_actor_hl1_kernel+
'assignvariableop_7_actor_actor_hl1_bias

identity_9ЂAssignVariableOpЂAssignVariableOp_1ЂAssignVariableOp_2ЂAssignVariableOp_3ЂAssignVariableOp_4ЂAssignVariableOp_5ЂAssignVariableOp_6ЂAssignVariableOp_7з
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*у
valueйBж	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB&variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names 
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
RestoreV2/shape_and_slicesи
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*8
_output_shapes&
$:::::::::*
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

IdentityЄ
AssignVariableOpAssignVariableOp%assignvariableop_actor_actor_i_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1Њ
AssignVariableOp_1AssignVariableOp%assignvariableop_1_actor_actor_i_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2Ќ
AssignVariableOp_2AssignVariableOp'assignvariableop_2_actor_actor_o_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3Њ
AssignVariableOp_3AssignVariableOp%assignvariableop_3_actor_actor_o_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4Ў
AssignVariableOp_4AssignVariableOp)assignvariableop_4_actor_actor_hl0_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5Ќ
AssignVariableOp_5AssignVariableOp'assignvariableop_5_actor_actor_hl0_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6Ў
AssignVariableOp_6AssignVariableOp)assignvariableop_6_actor_actor_hl1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7Ќ
AssignVariableOp_7AssignVariableOp'assignvariableop_7_actor_actor_hl1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_79
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp

Identity_8Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_8

Identity_9IdentityIdentity_8:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7*
T0*
_output_shapes
: 2

Identity_9"!

identity_9Identity_9:output:0*5
_input_shapes$
": ::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_7:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
Б

__inference__traced_save_547304
file_prefix3
/savev2_actor_actor_i_kernel_read_readvariableop1
-savev2_actor_actor_i_bias_read_readvariableop3
/savev2_actor_actor_o_kernel_read_readvariableop1
-savev2_actor_actor_o_bias_read_readvariableop5
1savev2_actor_actor_hl0_kernel_read_readvariableop3
/savev2_actor_actor_hl0_bias_read_readvariableop5
1savev2_actor_actor_hl1_kernel_read_readvariableop3
/savev2_actor_actor_hl1_bias_read_readvariableop
savev2_const

identity_1ЂMergeV2Checkpoints
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardІ
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameб
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*у
valueйBж	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB&variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
SaveV2/shape_and_slicesЪ
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0/savev2_actor_actor_i_kernel_read_readvariableop-savev2_actor_actor_i_bias_read_readvariableop/savev2_actor_actor_o_kernel_read_readvariableop-savev2_actor_actor_o_bias_read_readvariableop1savev2_actor_actor_hl0_kernel_read_readvariableop/savev2_actor_actor_hl0_bias_read_readvariableop1savev2_actor_actor_hl1_kernel_read_readvariableop/savev2_actor_actor_hl1_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
2	2
SaveV2К
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesЁ
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*W
_input_shapesF
D: :0d:d:@::d@:@:@@:@: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:0d: 

_output_shapes
:d:$ 

_output_shapes

:@: 

_output_shapes
::$ 

_output_shapes

:d@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:	

_output_shapes
: 
з

A__inference_actor_layer_call_and_return_conditional_losses_547047
input_1
actor_i_546894
actor_i_546896
actor_hl0_546941
actor_hl0_546943
actor_hl1_546988
actor_hl1_546990
actor_o_547041
actor_o_547043
identityЂ!actor_hl0/StatefulPartitionedCallЂ!actor_hl1/StatefulPartitionedCallЂactor_i/StatefulPartitionedCallЂactor_o/StatefulPartitionedCall
actor_i/StatefulPartitionedCallStatefulPartitionedCallinput_1actor_i_546894actor_i_546896*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџd*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *L
fGRE
C__inference_actor_i_layer_call_and_return_conditional_losses_5468832!
actor_i/StatefulPartitionedCallП
!actor_hl0/StatefulPartitionedCallStatefulPartitionedCall(actor_i/StatefulPartitionedCall:output:0actor_hl0_546941actor_hl0_546943*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_actor_hl0_layer_call_and_return_conditional_losses_5469302#
!actor_hl0/StatefulPartitionedCallС
!actor_hl1/StatefulPartitionedCallStatefulPartitionedCall*actor_hl0/StatefulPartitionedCall:output:0actor_hl1_546988actor_hl1_546990*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *N
fIRG
E__inference_actor_hl1_layer_call_and_return_conditional_losses_5469772#
!actor_hl1/StatefulPartitionedCallЗ
actor_o/StatefulPartitionedCallStatefulPartitionedCall*actor_hl1/StatefulPartitionedCall:output:0actor_o_547041actor_o_547043*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *L
fGRE
C__inference_actor_o_layer_call_and_return_conditional_losses_5470302!
actor_o/StatefulPartitionedCall
IdentityIdentity(actor_o/StatefulPartitionedCall:output:0"^actor_hl0/StatefulPartitionedCall"^actor_hl1/StatefulPartitionedCall ^actor_i/StatefulPartitionedCall ^actor_o/StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:џџџџџџџџџ0::::::::2F
!actor_hl0/StatefulPartitionedCall!actor_hl0/StatefulPartitionedCall2F
!actor_hl1/StatefulPartitionedCall!actor_hl1/StatefulPartitionedCall2B
actor_i/StatefulPartitionedCallactor_i/StatefulPartitionedCall2B
actor_o/StatefulPartitionedCallactor_o/StatefulPartitionedCall:T P
+
_output_shapes
:џџџџџџџџџ0
!
_user_specified_name	input_1
В 
ф
E__inference_actor_hl0_layer_call_and_return_conditional_losses_547208

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџd2
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Relu
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџd::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
В 
ф
E__inference_actor_hl0_layer_call_and_return_conditional_losses_546930

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџd2
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Relu
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџd::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџd
 
_user_specified_nameinputs
В 
ф
E__inference_actor_hl1_layer_call_and_return_conditional_losses_547248

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Relu
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџ@
 
_user_specified_nameinputs
ъ
}
(__inference_actor_o_layer_call_fn_547177

inputs
unknown
	unknown_0
identityЂStatefulPartitionedCallї
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *L
fGRE
C__inference_actor_o_layer_call_and_return_conditional_losses_5470302
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:џџџџџџџџџ@
 
_user_specified_nameinputs
ъ
}
(__inference_actor_i_layer_call_fn_547131

inputs
unknown
	unknown_0
identityЂStatefulPartitionedCallї
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџd*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8 *L
fGRE
C__inference_actor_i_layer_call_and_return_conditional_losses_5468832
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ0::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:џџџџџџџџџ0
 
_user_specified_nameinputs
В 
ф
E__inference_actor_hl1_layer_call_and_return_conditional_losses_546977

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Relu
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџ@
 
_user_specified_nameinputs
љ%
т
C__inference_actor_o_layer_call_and_return_conditional_losses_547168

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ2	
BiasAddy
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
Max/reduction_indices
MaxMaxBiasAdd:output:0Max/reduction_indices:output:0*
T0*+
_output_shapes
:џџџџџџџџџ*
	keep_dims(2
Maxg
subSubBiasAdd:output:0Max:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
subP
ExpExpsub:z:0*
T0*+
_output_shapes
:џџџџџџџџџ2
Expy
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
Sum/reduction_indices
SumSumExp:y:0Sum/reduction_indices:output:0*
T0*+
_output_shapes
:џџџџџџџџџ*
	keep_dims(2
Sumj
truedivRealDivExp:y:0Sum:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2	
truediv
IdentityIdentitytruediv:z:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџ@
 
_user_specified_nameinputs
а
т
C__inference_actor_i_layer_call_and_return_conditional_losses_547122

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:0d*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ02
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџd2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџd2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ0::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџ0
 
_user_specified_nameinputs
а
т
C__inference_actor_i_layer_call_and_return_conditional_losses_546883

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:0d*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ02
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџd2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџd2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџd2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџd2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ0::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџ0
 
_user_specified_nameinputs
љ%
т
C__inference_actor_o_layer_call_and_return_conditional_losses_547030

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂTensordot/ReadVariableOp
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axisб
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axisз
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axisА
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stack
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:џџџџџџџџџ@2
Tensordot/transpose
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:џџџџџџџџџџџџџџџџџџ2
Tensordot/Reshape
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisН
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
	Tensordot
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:џџџџџџџџџ2	
BiasAddy
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
Max/reduction_indices
MaxMaxBiasAdd:output:0Max/reduction_indices:output:0*
T0*+
_output_shapes
:џџџџџџџџџ*
	keep_dims(2
Maxg
subSubBiasAdd:output:0Max:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2
subP
ExpExpsub:z:0*
T0*+
_output_shapes
:џџџџџџџџџ2
Expy
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
џџџџџџџџџ2
Sum/reduction_indices
SumSumExp:y:0Sum/reduction_indices:output:0*
T0*+
_output_shapes
:џџџџџџџџџ*
	keep_dims(2
Sumj
truedivRealDivExp:y:0Sum:output:0*
T0*+
_output_shapes
:џџџџџџџџџ2	
truediv
IdentityIdentitytruediv:z:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*2
_input_shapes!
:џџџџџџџџџ@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:џџџџџџџџџ@
 
_user_specified_nameinputs
Њ
ж
&__inference_actor_layer_call_fn_547069
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identityЂStatefulPartitionedCallФ
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:џџџџџџџџџ**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8 *J
fERC
A__inference_actor_layer_call_and_return_conditional_losses_5470472
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:џџџџџџџџџ0::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:џџџџџџџџџ0
!
_user_specified_name	input_1"БL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*Г
serving_default
?
input_14
serving_default_input_1:0џџџџџџџџџ0@
output_14
StatefulPartitionedCall:0џџџџџџџџџtensorflow/serving/predict:уd
Ю
i
hidden_layers
o
	variables
regularization_losses
trainable_variables
	keras_api

signatures
<_default_save_signature
*=&call_and_return_all_conditional_losses
>__call__"ё
_tf_keras_modelз{"class_name": "Actor", "name": "actor", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"layer was saved without config": true}, "is_graph_network": false, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Actor"}}
ѓ

	kernel

bias
	variables
regularization_losses
trainable_variables
	keras_api
*?&call_and_return_all_conditional_losses
@__call__"Ю
_tf_keras_layerД{"class_name": "Dense", "name": "actor_i", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_i", "trainable": true, "dtype": "float32", "units": 100, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 48]}}
.
0
1"
trackable_list_wrapper
ђ

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
*A&call_and_return_all_conditional_losses
B__call__"Э
_tf_keras_layerГ{"class_name": "Dense", "name": "actor_o", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_o", "trainable": true, "dtype": "float32", "units": 8, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
 "
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
Ъ

layers
non_trainable_variables
	variables
layer_metrics
metrics
regularization_losses
trainable_variables
layer_regularization_losses
>__call__
<_default_save_signature
*=&call_and_return_all_conditional_losses
&="call_and_return_conditional_losses"
_generic_user_object
,
Cserving_default"
signature_map
&:$0d2actor/actor_i/kernel
 :d2actor/actor_i/bias
.
	0

1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
­

 layers
!non_trainable_variables
	variables
"layer_metrics
#metrics
regularization_losses
trainable_variables
$layer_regularization_losses
@__call__
*?&call_and_return_all_conditional_losses
&?"call_and_return_conditional_losses"
_generic_user_object
і

kernel
bias
%	variables
&regularization_losses
'trainable_variables
(	keras_api
*D&call_and_return_all_conditional_losses
E__call__"б
_tf_keras_layerЗ{"class_name": "Dense", "name": "actor_hl0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl0", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 100}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 100]}}
є

kernel
bias
)	variables
*regularization_losses
+trainable_variables
,	keras_api
*F&call_and_return_all_conditional_losses
G__call__"Я
_tf_keras_layerЕ{"class_name": "Dense", "name": "actor_hl1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl1", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
&:$@2actor/actor_o/kernel
 :2actor/actor_o/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
­

-layers
.non_trainable_variables
	variables
/layer_metrics
0metrics
regularization_losses
trainable_variables
1layer_regularization_losses
B__call__
*A&call_and_return_all_conditional_losses
&A"call_and_return_conditional_losses"
_generic_user_object
(:&d@2actor/actor_hl0/kernel
": @2actor/actor_hl0/bias
(:&@@2actor/actor_hl1/kernel
": @2actor/actor_hl1/bias
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
­

2layers
3non_trainable_variables
%	variables
4layer_metrics
5metrics
&regularization_losses
'trainable_variables
6layer_regularization_losses
E__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
­

7layers
8non_trainable_variables
)	variables
9layer_metrics
:metrics
*regularization_losses
+trainable_variables
;layer_regularization_losses
G__call__
*F&call_and_return_all_conditional_losses
&F"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
у2р
!__inference__wrapped_model_546849К
В
FullArgSpec
args 
varargsjargs
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ **Ђ'
%"
input_1џџџџџџџџџ0
2
A__inference_actor_layer_call_and_return_conditional_losses_547047Ю
В
FullArgSpec!
args
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ **Ђ'
%"
input_1џџџџџџџџџ0
ќ2љ
&__inference_actor_layer_call_fn_547069Ю
В
FullArgSpec!
args
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ **Ђ'
%"
input_1џџџџџџџџџ0
э2ъ
C__inference_actor_i_layer_call_and_return_conditional_losses_547122Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
в2Я
(__inference_actor_i_layer_call_fn_547131Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
э2ъ
C__inference_actor_o_layer_call_and_return_conditional_losses_547168Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
в2Я
(__inference_actor_o_layer_call_fn_547177Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
ЫBШ
$__inference_signature_wrapper_547092input_1"
В
FullArgSpec
args 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
я2ь
E__inference_actor_hl0_layer_call_and_return_conditional_losses_547208Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
д2б
*__inference_actor_hl0_layer_call_fn_547217Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
я2ь
E__inference_actor_hl1_layer_call_and_return_conditional_losses_547248Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
д2б
*__inference_actor_hl1_layer_call_fn_547257Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
!__inference__wrapped_model_546849y	
4Ђ1
*Ђ'
%"
input_1џџџџџџџџџ0
Њ "7Њ4
2
output_1&#
output_1џџџџџџџџџ­
E__inference_actor_hl0_layer_call_and_return_conditional_losses_547208d3Ђ0
)Ђ&
$!
inputsџџџџџџџџџd
Њ ")Ђ&

0џџџџџџџџџ@
 
*__inference_actor_hl0_layer_call_fn_547217W3Ђ0
)Ђ&
$!
inputsџџџџџџџџџd
Њ "џџџџџџџџџ@­
E__inference_actor_hl1_layer_call_and_return_conditional_losses_547248d3Ђ0
)Ђ&
$!
inputsџџџџџџџџџ@
Њ ")Ђ&

0џџџџџџџџџ@
 
*__inference_actor_hl1_layer_call_fn_547257W3Ђ0
)Ђ&
$!
inputsџџџџџџџџџ@
Њ "џџџџџџџџџ@Ћ
C__inference_actor_i_layer_call_and_return_conditional_losses_547122d	
3Ђ0
)Ђ&
$!
inputsџџџџџџџџџ0
Њ ")Ђ&

0џџџџџџџџџd
 
(__inference_actor_i_layer_call_fn_547131W	
3Ђ0
)Ђ&
$!
inputsџџџџџџџџџ0
Њ "џџџџџџџџџdА
A__inference_actor_layer_call_and_return_conditional_losses_547047k	
4Ђ1
*Ђ'
%"
input_1џџџџџџџџџ0
Њ ")Ђ&

0џџџџџџџџџ
 
&__inference_actor_layer_call_fn_547069^	
4Ђ1
*Ђ'
%"
input_1џџџџџџџџџ0
Њ "џџџџџџџџџЋ
C__inference_actor_o_layer_call_and_return_conditional_losses_547168d3Ђ0
)Ђ&
$!
inputsџџџџџџџџџ@
Њ ")Ђ&

0џџџџџџџџџ
 
(__inference_actor_o_layer_call_fn_547177W3Ђ0
)Ђ&
$!
inputsџџџџџџџџџ@
Њ "џџџџџџџџџ­
$__inference_signature_wrapper_547092	
?Ђ<
Ђ 
5Њ2
0
input_1%"
input_1џџџџџџџџџ0"7Њ4
2
output_1&#
output_1џџџџџџџџџ