”п
Ґч
B
AssignVariableOp
resource
value"dtype"
dtypetypeИ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
,
Exp
x"T
y"T"
Ttype:

2
≠
GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"

batch_dimsint "
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
М
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(И

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
Н
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
Њ
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
;
Sub
x"T
y"T
z"T"
Ttype:
2	
М
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	
Ц
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 И"serve*2.4.12v2.4.0-49-g85c8b2a817f8яс
Д
actor/actor_i/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0d*%
shared_nameactor/actor_i/kernel
}
(actor/actor_i/kernel/Read/ReadVariableOpReadVariableOpactor/actor_i/kernel*
_output_shapes

:0d*
dtype0
|
actor/actor_i/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*#
shared_nameactor/actor_i/bias
u
&actor/actor_i/bias/Read/ReadVariableOpReadVariableOpactor/actor_i/bias*
_output_shapes
:d*
dtype0
Д
actor/actor_o/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*%
shared_nameactor/actor_o/kernel
}
(actor/actor_o/kernel/Read/ReadVariableOpReadVariableOpactor/actor_o/kernel*
_output_shapes

:@*
dtype0
|
actor/actor_o/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameactor/actor_o/bias
u
&actor/actor_o/bias/Read/ReadVariableOpReadVariableOpactor/actor_o/bias*
_output_shapes
:*
dtype0
И
actor/actor_hl0/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d@*'
shared_nameactor/actor_hl0/kernel
Б
*actor/actor_hl0/kernel/Read/ReadVariableOpReadVariableOpactor/actor_hl0/kernel*
_output_shapes

:d@*
dtype0
А
actor/actor_hl0/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameactor/actor_hl0/bias
y
(actor/actor_hl0/bias/Read/ReadVariableOpReadVariableOpactor/actor_hl0/bias*
_output_shapes
:@*
dtype0
И
actor/actor_hl1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*'
shared_nameactor/actor_hl1/kernel
Б
*actor/actor_hl1/kernel/Read/ReadVariableOpReadVariableOpactor/actor_hl1/kernel*
_output_shapes

:@@*
dtype0
А
actor/actor_hl1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameactor/actor_hl1/bias
y
(actor/actor_hl1/bias/Read/ReadVariableOpReadVariableOpactor/actor_hl1/bias*
_output_shapes
:@*
dtype0

NoOpNoOp
э
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*Є
valueЃBЂ B§
Г
i
hidden_layers
o
regularization_losses
trainable_variables
	variables
	keras_api

signatures
h

	kernel

bias
regularization_losses
trainable_variables
	variables
	keras_api

0
1
h

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
 
8
	0

1
2
3
4
5
6
7
8
	0

1
2
3
4
5
6
7
≠
regularization_losses
layer_regularization_losses
metrics
non_trainable_variables
layer_metrics
trainable_variables

layers
	variables
 
MK
VARIABLE_VALUEactor/actor_i/kernel#i/kernel/.ATTRIBUTES/VARIABLE_VALUE
IG
VARIABLE_VALUEactor/actor_i/bias!i/bias/.ATTRIBUTES/VARIABLE_VALUE
 

	0

1

	0

1
≠
regularization_losses
 layer_regularization_losses
!metrics
"non_trainable_variables
#layer_metrics
trainable_variables

$layers
	variables
h

kernel
bias
%regularization_losses
&trainable_variables
'	variables
(	keras_api
h

kernel
bias
)regularization_losses
*trainable_variables
+	variables
,	keras_api
MK
VARIABLE_VALUEactor/actor_o/kernel#o/kernel/.ATTRIBUTES/VARIABLE_VALUE
IG
VARIABLE_VALUEactor/actor_o/bias!o/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
≠
regularization_losses
-layer_regularization_losses
.metrics
/non_trainable_variables
0layer_metrics
trainable_variables

1layers
	variables
\Z
VARIABLE_VALUEactor/actor_hl0/kernel0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEactor/actor_hl0/bias0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEactor/actor_hl1/kernel0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEactor/actor_hl1/bias0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUE
 
 
 
 

0
1
2
3
 
 
 
 
 
 

0
1

0
1
≠
%regularization_losses
2layer_regularization_losses
3metrics
4non_trainable_variables
5layer_metrics
&trainable_variables

6layers
'	variables
 

0
1

0
1
≠
)regularization_losses
7layer_regularization_losses
8metrics
9non_trainable_variables
:layer_metrics
*trainable_variables

;layers
+	variables
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
В
serving_default_input_1Placeholder*+
_output_shapes
:€€€€€€€€€0*
dtype0* 
shape:€€€€€€€€€0
ц
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1actor/actor_i/kernelactor/actor_i/biasactor/actor_hl0/kernelactor/actor_hl0/biasactor/actor_hl1/kernelactor/actor_hl1/biasactor/actor_o/kernelactor/actor_o/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8В *-
f(R&
$__inference_signature_wrapper_739005
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
у
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename(actor/actor_i/kernel/Read/ReadVariableOp&actor/actor_i/bias/Read/ReadVariableOp(actor/actor_o/kernel/Read/ReadVariableOp&actor/actor_o/bias/Read/ReadVariableOp*actor/actor_hl0/kernel/Read/ReadVariableOp(actor/actor_hl0/bias/Read/ReadVariableOp*actor/actor_hl1/kernel/Read/ReadVariableOp(actor/actor_hl1/bias/Read/ReadVariableOpConst*
Tin
2
*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *(
f#R!
__inference__traced_save_739217
ќ
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameactor/actor_i/kernelactor/actor_i/biasactor/actor_o/kernelactor/actor_o/biasactor/actor_hl0/kernelactor/actor_hl0/biasactor/actor_hl1/kernelactor/actor_hl1/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *+
f&R$
"__inference__traced_restore_739251†њ
„
Х
A__inference_actor_layer_call_and_return_conditional_losses_738960
input_1
actor_i_738807
actor_i_738809
actor_hl0_738854
actor_hl0_738856
actor_hl1_738901
actor_hl1_738903
actor_o_738954
actor_o_738956
identityИҐ!actor_hl0/StatefulPartitionedCallҐ!actor_hl1/StatefulPartitionedCallҐactor_i/StatefulPartitionedCallҐactor_o/StatefulPartitionedCallФ
actor_i/StatefulPartitionedCallStatefulPartitionedCallinput_1actor_i_738807actor_i_738809*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€d*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *L
fGRE
C__inference_actor_i_layer_call_and_return_conditional_losses_7387962!
actor_i/StatefulPartitionedCallњ
!actor_hl0/StatefulPartitionedCallStatefulPartitionedCall(actor_i/StatefulPartitionedCall:output:0actor_hl0_738854actor_hl0_738856*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_actor_hl0_layer_call_and_return_conditional_losses_7388432#
!actor_hl0/StatefulPartitionedCallЅ
!actor_hl1/StatefulPartitionedCallStatefulPartitionedCall*actor_hl0/StatefulPartitionedCall:output:0actor_hl1_738901actor_hl1_738903*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_actor_hl1_layer_call_and_return_conditional_losses_7388902#
!actor_hl1/StatefulPartitionedCallЈ
actor_o/StatefulPartitionedCallStatefulPartitionedCall*actor_hl1/StatefulPartitionedCall:output:0actor_o_738954actor_o_738956*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *L
fGRE
C__inference_actor_o_layer_call_and_return_conditional_losses_7389432!
actor_o/StatefulPartitionedCallМ
IdentityIdentity(actor_o/StatefulPartitionedCall:output:0"^actor_hl0/StatefulPartitionedCall"^actor_hl1/StatefulPartitionedCall ^actor_i/StatefulPartitionedCall ^actor_o/StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:€€€€€€€€€0::::::::2F
!actor_hl0/StatefulPartitionedCall!actor_hl0/StatefulPartitionedCall2F
!actor_hl1/StatefulPartitionedCall!actor_hl1/StatefulPartitionedCall2B
actor_i/StatefulPartitionedCallactor_i/StatefulPartitionedCall2B
actor_o/StatefulPartitionedCallactor_o/StatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€0
!
_user_specified_name	input_1
–
в
C__inference_actor_i_layer_call_and_return_conditional_losses_738796

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:0d*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€02
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€d2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€d2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€d2	
BiasAddЬ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€d2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€0::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€0
 
_user_specified_nameinputs
ШЂ
±
!__inference__wrapped_model_738762
input_13
/actor_actor_i_tensordot_readvariableop_resource1
-actor_actor_i_biasadd_readvariableop_resource5
1actor_actor_hl0_tensordot_readvariableop_resource3
/actor_actor_hl0_biasadd_readvariableop_resource5
1actor_actor_hl1_tensordot_readvariableop_resource3
/actor_actor_hl1_biasadd_readvariableop_resource3
/actor_actor_o_tensordot_readvariableop_resource1
-actor_actor_o_biasadd_readvariableop_resource
identityИҐ&actor/actor_hl0/BiasAdd/ReadVariableOpҐ(actor/actor_hl0/Tensordot/ReadVariableOpҐ&actor/actor_hl1/BiasAdd/ReadVariableOpҐ(actor/actor_hl1/Tensordot/ReadVariableOpҐ$actor/actor_i/BiasAdd/ReadVariableOpҐ&actor/actor_i/Tensordot/ReadVariableOpҐ$actor/actor_o/BiasAdd/ReadVariableOpҐ&actor/actor_o/Tensordot/ReadVariableOpј
&actor/actor_i/Tensordot/ReadVariableOpReadVariableOp/actor_actor_i_tensordot_readvariableop_resource*
_output_shapes

:0d*
dtype02(
&actor/actor_i/Tensordot/ReadVariableOpЖ
actor/actor_i/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
actor/actor_i/Tensordot/axesН
actor/actor_i/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
actor/actor_i/Tensordot/freeu
actor/actor_i/Tensordot/ShapeShapeinput_1*
T0*
_output_shapes
:2
actor/actor_i/Tensordot/ShapeР
%actor/actor_i/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_i/Tensordot/GatherV2/axisЧ
 actor/actor_i/Tensordot/GatherV2GatherV2&actor/actor_i/Tensordot/Shape:output:0%actor/actor_i/Tensordot/free:output:0.actor/actor_i/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2"
 actor/actor_i/Tensordot/GatherV2Ф
'actor/actor_i/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_i/Tensordot/GatherV2_1/axisЭ
"actor/actor_i/Tensordot/GatherV2_1GatherV2&actor/actor_i/Tensordot/Shape:output:0%actor/actor_i/Tensordot/axes:output:00actor/actor_i/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_i/Tensordot/GatherV2_1И
actor/actor_i/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
actor/actor_i/Tensordot/ConstЄ
actor/actor_i/Tensordot/ProdProd)actor/actor_i/Tensordot/GatherV2:output:0&actor/actor_i/Tensordot/Const:output:0*
T0*
_output_shapes
: 2
actor/actor_i/Tensordot/ProdМ
actor/actor_i/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_i/Tensordot/Const_1ј
actor/actor_i/Tensordot/Prod_1Prod+actor/actor_i/Tensordot/GatherV2_1:output:0(actor/actor_i/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2 
actor/actor_i/Tensordot/Prod_1М
#actor/actor_i/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2%
#actor/actor_i/Tensordot/concat/axisц
actor/actor_i/Tensordot/concatConcatV2%actor/actor_i/Tensordot/free:output:0%actor/actor_i/Tensordot/axes:output:0,actor/actor_i/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2 
actor/actor_i/Tensordot/concatƒ
actor/actor_i/Tensordot/stackPack%actor/actor_i/Tensordot/Prod:output:0'actor/actor_i/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
actor/actor_i/Tensordot/stackї
!actor/actor_i/Tensordot/transpose	Transposeinput_1'actor/actor_i/Tensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€02#
!actor/actor_i/Tensordot/transpose„
actor/actor_i/Tensordot/ReshapeReshape%actor/actor_i/Tensordot/transpose:y:0&actor/actor_i/Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2!
actor/actor_i/Tensordot/Reshape÷
actor/actor_i/Tensordot/MatMulMatMul(actor/actor_i/Tensordot/Reshape:output:0.actor/actor_i/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€d2 
actor/actor_i/Tensordot/MatMulМ
actor/actor_i/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2!
actor/actor_i/Tensordot/Const_2Р
%actor/actor_i/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_i/Tensordot/concat_1/axisГ
 actor/actor_i/Tensordot/concat_1ConcatV2)actor/actor_i/Tensordot/GatherV2:output:0(actor/actor_i/Tensordot/Const_2:output:0.actor/actor_i/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_i/Tensordot/concat_1»
actor/actor_i/TensordotReshape(actor/actor_i/Tensordot/MatMul:product:0)actor/actor_i/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€d2
actor/actor_i/Tensordotґ
$actor/actor_i/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_i_biasadd_readvariableop_resource*
_output_shapes
:d*
dtype02&
$actor/actor_i/BiasAdd/ReadVariableOpњ
actor/actor_i/BiasAddBiasAdd actor/actor_i/Tensordot:output:0,actor/actor_i/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€d2
actor/actor_i/BiasAdd∆
(actor/actor_hl0/Tensordot/ReadVariableOpReadVariableOp1actor_actor_hl0_tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02*
(actor/actor_hl0/Tensordot/ReadVariableOpК
actor/actor_hl0/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
actor/actor_hl0/Tensordot/axesС
actor/actor_hl0/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
actor/actor_hl0/Tensordot/freeР
actor/actor_hl0/Tensordot/ShapeShapeactor/actor_i/BiasAdd:output:0*
T0*
_output_shapes
:2!
actor/actor_hl0/Tensordot/ShapeФ
'actor/actor_hl0/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl0/Tensordot/GatherV2/axis°
"actor/actor_hl0/Tensordot/GatherV2GatherV2(actor/actor_hl0/Tensordot/Shape:output:0'actor/actor_hl0/Tensordot/free:output:00actor/actor_hl0/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_hl0/Tensordot/GatherV2Ш
)actor/actor_hl0/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)actor/actor_hl0/Tensordot/GatherV2_1/axisІ
$actor/actor_hl0/Tensordot/GatherV2_1GatherV2(actor/actor_hl0/Tensordot/Shape:output:0'actor/actor_hl0/Tensordot/axes:output:02actor/actor_hl0/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$actor/actor_hl0/Tensordot/GatherV2_1М
actor/actor_hl0/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_hl0/Tensordot/Constј
actor/actor_hl0/Tensordot/ProdProd+actor/actor_hl0/Tensordot/GatherV2:output:0(actor/actor_hl0/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
actor/actor_hl0/Tensordot/ProdР
!actor/actor_hl0/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!actor/actor_hl0/Tensordot/Const_1»
 actor/actor_hl0/Tensordot/Prod_1Prod-actor/actor_hl0/Tensordot/GatherV2_1:output:0*actor/actor_hl0/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 actor/actor_hl0/Tensordot/Prod_1Р
%actor/actor_hl0/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_hl0/Tensordot/concat/axisА
 actor/actor_hl0/Tensordot/concatConcatV2'actor/actor_hl0/Tensordot/free:output:0'actor/actor_hl0/Tensordot/axes:output:0.actor/actor_hl0/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_hl0/Tensordot/concatћ
actor/actor_hl0/Tensordot/stackPack'actor/actor_hl0/Tensordot/Prod:output:0)actor/actor_hl0/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
actor/actor_hl0/Tensordot/stackЎ
#actor/actor_hl0/Tensordot/transpose	Transposeactor/actor_i/BiasAdd:output:0)actor/actor_hl0/Tensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€d2%
#actor/actor_hl0/Tensordot/transposeя
!actor/actor_hl0/Tensordot/ReshapeReshape'actor/actor_hl0/Tensordot/transpose:y:0(actor/actor_hl0/Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2#
!actor/actor_hl0/Tensordot/Reshapeё
 actor/actor_hl0/Tensordot/MatMulMatMul*actor/actor_hl0/Tensordot/Reshape:output:00actor/actor_hl0/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€@2"
 actor/actor_hl0/Tensordot/MatMulР
!actor/actor_hl0/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!actor/actor_hl0/Tensordot/Const_2Ф
'actor/actor_hl0/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl0/Tensordot/concat_1/axisН
"actor/actor_hl0/Tensordot/concat_1ConcatV2+actor/actor_hl0/Tensordot/GatherV2:output:0*actor/actor_hl0/Tensordot/Const_2:output:00actor/actor_hl0/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"actor/actor_hl0/Tensordot/concat_1–
actor/actor_hl0/TensordotReshape*actor/actor_hl0/Tensordot/MatMul:product:0+actor/actor_hl0/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
actor/actor_hl0/TensordotЉ
&actor/actor_hl0/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl0_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl0/BiasAdd/ReadVariableOp«
actor/actor_hl0/BiasAddBiasAdd"actor/actor_hl0/Tensordot:output:0.actor/actor_hl0/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€@2
actor/actor_hl0/BiasAddМ
actor/actor_hl0/ReluRelu actor/actor_hl0/BiasAdd:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
actor/actor_hl0/Relu∆
(actor/actor_hl1/Tensordot/ReadVariableOpReadVariableOp1actor_actor_hl1_tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02*
(actor/actor_hl1/Tensordot/ReadVariableOpК
actor/actor_hl1/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2 
actor/actor_hl1/Tensordot/axesС
actor/actor_hl1/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2 
actor/actor_hl1/Tensordot/freeФ
actor/actor_hl1/Tensordot/ShapeShape"actor/actor_hl0/Relu:activations:0*
T0*
_output_shapes
:2!
actor/actor_hl1/Tensordot/ShapeФ
'actor/actor_hl1/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl1/Tensordot/GatherV2/axis°
"actor/actor_hl1/Tensordot/GatherV2GatherV2(actor/actor_hl1/Tensordot/Shape:output:0'actor/actor_hl1/Tensordot/free:output:00actor/actor_hl1/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_hl1/Tensordot/GatherV2Ш
)actor/actor_hl1/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2+
)actor/actor_hl1/Tensordot/GatherV2_1/axisІ
$actor/actor_hl1/Tensordot/GatherV2_1GatherV2(actor/actor_hl1/Tensordot/Shape:output:0'actor/actor_hl1/Tensordot/axes:output:02actor/actor_hl1/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2&
$actor/actor_hl1/Tensordot/GatherV2_1М
actor/actor_hl1/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_hl1/Tensordot/Constј
actor/actor_hl1/Tensordot/ProdProd+actor/actor_hl1/Tensordot/GatherV2:output:0(actor/actor_hl1/Tensordot/Const:output:0*
T0*
_output_shapes
: 2 
actor/actor_hl1/Tensordot/ProdР
!actor/actor_hl1/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2#
!actor/actor_hl1/Tensordot/Const_1»
 actor/actor_hl1/Tensordot/Prod_1Prod-actor/actor_hl1/Tensordot/GatherV2_1:output:0*actor/actor_hl1/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2"
 actor/actor_hl1/Tensordot/Prod_1Р
%actor/actor_hl1/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_hl1/Tensordot/concat/axisА
 actor/actor_hl1/Tensordot/concatConcatV2'actor/actor_hl1/Tensordot/free:output:0'actor/actor_hl1/Tensordot/axes:output:0.actor/actor_hl1/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_hl1/Tensordot/concatћ
actor/actor_hl1/Tensordot/stackPack'actor/actor_hl1/Tensordot/Prod:output:0)actor/actor_hl1/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2!
actor/actor_hl1/Tensordot/stack№
#actor/actor_hl1/Tensordot/transpose	Transpose"actor/actor_hl0/Relu:activations:0)actor/actor_hl1/Tensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2%
#actor/actor_hl1/Tensordot/transposeя
!actor/actor_hl1/Tensordot/ReshapeReshape'actor/actor_hl1/Tensordot/transpose:y:0(actor/actor_hl1/Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2#
!actor/actor_hl1/Tensordot/Reshapeё
 actor/actor_hl1/Tensordot/MatMulMatMul*actor/actor_hl1/Tensordot/Reshape:output:00actor/actor_hl1/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€@2"
 actor/actor_hl1/Tensordot/MatMulР
!actor/actor_hl1/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2#
!actor/actor_hl1/Tensordot/Const_2Ф
'actor/actor_hl1/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_hl1/Tensordot/concat_1/axisН
"actor/actor_hl1/Tensordot/concat_1ConcatV2+actor/actor_hl1/Tensordot/GatherV2:output:0*actor/actor_hl1/Tensordot/Const_2:output:00actor/actor_hl1/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2$
"actor/actor_hl1/Tensordot/concat_1–
actor/actor_hl1/TensordotReshape*actor/actor_hl1/Tensordot/MatMul:product:0+actor/actor_hl1/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
actor/actor_hl1/TensordotЉ
&actor/actor_hl1/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl1/BiasAdd/ReadVariableOp«
actor/actor_hl1/BiasAddBiasAdd"actor/actor_hl1/Tensordot:output:0.actor/actor_hl1/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€@2
actor/actor_hl1/BiasAddМ
actor/actor_hl1/ReluRelu actor/actor_hl1/BiasAdd:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
actor/actor_hl1/Reluј
&actor/actor_o/Tensordot/ReadVariableOpReadVariableOp/actor_actor_o_tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02(
&actor/actor_o/Tensordot/ReadVariableOpЖ
actor/actor_o/Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
actor/actor_o/Tensordot/axesН
actor/actor_o/Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
actor/actor_o/Tensordot/freeР
actor/actor_o/Tensordot/ShapeShape"actor/actor_hl1/Relu:activations:0*
T0*
_output_shapes
:2
actor/actor_o/Tensordot/ShapeР
%actor/actor_o/Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_o/Tensordot/GatherV2/axisЧ
 actor/actor_o/Tensordot/GatherV2GatherV2&actor/actor_o/Tensordot/Shape:output:0%actor/actor_o/Tensordot/free:output:0.actor/actor_o/Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2"
 actor/actor_o/Tensordot/GatherV2Ф
'actor/actor_o/Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2)
'actor/actor_o/Tensordot/GatherV2_1/axisЭ
"actor/actor_o/Tensordot/GatherV2_1GatherV2&actor/actor_o/Tensordot/Shape:output:0%actor/actor_o/Tensordot/axes:output:00actor/actor_o/Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2$
"actor/actor_o/Tensordot/GatherV2_1И
actor/actor_o/Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
actor/actor_o/Tensordot/ConstЄ
actor/actor_o/Tensordot/ProdProd)actor/actor_o/Tensordot/GatherV2:output:0&actor/actor_o/Tensordot/Const:output:0*
T0*
_output_shapes
: 2
actor/actor_o/Tensordot/ProdМ
actor/actor_o/Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2!
actor/actor_o/Tensordot/Const_1ј
actor/actor_o/Tensordot/Prod_1Prod+actor/actor_o/Tensordot/GatherV2_1:output:0(actor/actor_o/Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2 
actor/actor_o/Tensordot/Prod_1М
#actor/actor_o/Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2%
#actor/actor_o/Tensordot/concat/axisц
actor/actor_o/Tensordot/concatConcatV2%actor/actor_o/Tensordot/free:output:0%actor/actor_o/Tensordot/axes:output:0,actor/actor_o/Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2 
actor/actor_o/Tensordot/concatƒ
actor/actor_o/Tensordot/stackPack%actor/actor_o/Tensordot/Prod:output:0'actor/actor_o/Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
actor/actor_o/Tensordot/stack÷
!actor/actor_o/Tensordot/transpose	Transpose"actor/actor_hl1/Relu:activations:0'actor/actor_o/Tensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2#
!actor/actor_o/Tensordot/transpose„
actor/actor_o/Tensordot/ReshapeReshape%actor/actor_o/Tensordot/transpose:y:0&actor/actor_o/Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2!
actor/actor_o/Tensordot/Reshape÷
actor/actor_o/Tensordot/MatMulMatMul(actor/actor_o/Tensordot/Reshape:output:0.actor/actor_o/Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2 
actor/actor_o/Tensordot/MatMulМ
actor/actor_o/Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2!
actor/actor_o/Tensordot/Const_2Р
%actor/actor_o/Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2'
%actor/actor_o/Tensordot/concat_1/axisГ
 actor/actor_o/Tensordot/concat_1ConcatV2)actor/actor_o/Tensordot/GatherV2:output:0(actor/actor_o/Tensordot/Const_2:output:0.actor/actor_o/Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2"
 actor/actor_o/Tensordot/concat_1»
actor/actor_o/TensordotReshape(actor/actor_o/Tensordot/MatMul:product:0)actor/actor_o/Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
actor/actor_o/Tensordotґ
$actor/actor_o/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_o_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02&
$actor/actor_o/BiasAdd/ReadVariableOpњ
actor/actor_o/BiasAddBiasAdd actor/actor_o/Tensordot:output:0,actor/actor_o/BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€2
actor/actor_o/BiasAddХ
#actor/actor_o/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
€€€€€€€€€2%
#actor/actor_o/Max/reduction_indices¬
actor/actor_o/MaxMaxactor/actor_o/BiasAdd:output:0,actor/actor_o/Max/reduction_indices:output:0*
T0*+
_output_shapes
:€€€€€€€€€*
	keep_dims(2
actor/actor_o/MaxЯ
actor/actor_o/subSubactor/actor_o/BiasAdd:output:0actor/actor_o/Max:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
actor/actor_o/subz
actor/actor_o/ExpExpactor/actor_o/sub:z:0*
T0*+
_output_shapes
:€€€€€€€€€2
actor/actor_o/ExpХ
#actor/actor_o/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
€€€€€€€€€2%
#actor/actor_o/Sum/reduction_indicesє
actor/actor_o/SumSumactor/actor_o/Exp:y:0,actor/actor_o/Sum/reduction_indices:output:0*
T0*+
_output_shapes
:€€€€€€€€€*
	keep_dims(2
actor/actor_o/SumҐ
actor/actor_o/truedivRealDivactor/actor_o/Exp:y:0actor/actor_o/Sum:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
actor/actor_o/truedivє
IdentityIdentityactor/actor_o/truediv:z:0'^actor/actor_hl0/BiasAdd/ReadVariableOp)^actor/actor_hl0/Tensordot/ReadVariableOp'^actor/actor_hl1/BiasAdd/ReadVariableOp)^actor/actor_hl1/Tensordot/ReadVariableOp%^actor/actor_i/BiasAdd/ReadVariableOp'^actor/actor_i/Tensordot/ReadVariableOp%^actor/actor_o/BiasAdd/ReadVariableOp'^actor/actor_o/Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:€€€€€€€€€0::::::::2P
&actor/actor_hl0/BiasAdd/ReadVariableOp&actor/actor_hl0/BiasAdd/ReadVariableOp2T
(actor/actor_hl0/Tensordot/ReadVariableOp(actor/actor_hl0/Tensordot/ReadVariableOp2P
&actor/actor_hl1/BiasAdd/ReadVariableOp&actor/actor_hl1/BiasAdd/ReadVariableOp2T
(actor/actor_hl1/Tensordot/ReadVariableOp(actor/actor_hl1/Tensordot/ReadVariableOp2L
$actor/actor_i/BiasAdd/ReadVariableOp$actor/actor_i/BiasAdd/ReadVariableOp2P
&actor/actor_i/Tensordot/ReadVariableOp&actor/actor_i/Tensordot/ReadVariableOp2L
$actor/actor_o/BiasAdd/ReadVariableOp$actor/actor_o/BiasAdd/ReadVariableOp2P
&actor/actor_o/Tensordot/ReadVariableOp&actor/actor_o/Tensordot/ReadVariableOp:T P
+
_output_shapes
:€€€€€€€€€0
!
_user_specified_name	input_1
щ%
в
C__inference_actor_o_layer_call_and_return_conditional_losses_739081

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€2	
BiasAddy
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
€€€€€€€€€2
Max/reduction_indicesК
MaxMaxBiasAdd:output:0Max/reduction_indices:output:0*
T0*+
_output_shapes
:€€€€€€€€€*
	keep_dims(2
Maxg
subSubBiasAdd:output:0Max:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
subP
ExpExpsub:z:0*
T0*+
_output_shapes
:€€€€€€€€€2
Expy
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
€€€€€€€€€2
Sum/reduction_indicesБ
SumSumExp:y:0Sum/reduction_indices:output:0*
T0*+
_output_shapes
:€€€€€€€€€*
	keep_dims(2
Sumj
truedivRealDivExp:y:0Sum:output:0*
T0*+
_output_shapes
:€€€€€€€€€2	
truedivЧ
IdentityIdentitytruediv:z:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€@
 
_user_specified_nameinputs
™
÷
&__inference_actor_layer_call_fn_738982
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identityИҐStatefulPartitionedCallƒ
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8В *J
fERC
A__inference_actor_layer_call_and_return_conditional_losses_7389602
StatefulPartitionedCallТ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:€€€€€€€€€0::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€0
!
_user_specified_name	input_1
о

*__inference_actor_hl1_layer_call_fn_739170

inputs
unknown
	unknown_0
identityИҐStatefulPartitionedCallщ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_actor_hl1_layer_call_and_return_conditional_losses_7388902
StatefulPartitionedCallТ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€@
 
_user_specified_nameinputs
к
}
(__inference_actor_o_layer_call_fn_739090

inputs
unknown
	unknown_0
identityИҐStatefulPartitionedCallч
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *L
fGRE
C__inference_actor_o_layer_call_and_return_conditional_losses_7389432
StatefulPartitionedCallТ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€@::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€@
 
_user_specified_nameinputs
щ%
в
C__inference_actor_o_layer_call_and_return_conditional_losses_738943

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€2	
BiasAddy
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
€€€€€€€€€2
Max/reduction_indicesК
MaxMaxBiasAdd:output:0Max/reduction_indices:output:0*
T0*+
_output_shapes
:€€€€€€€€€*
	keep_dims(2
Maxg
subSubBiasAdd:output:0Max:output:0*
T0*+
_output_shapes
:€€€€€€€€€2
subP
ExpExpsub:z:0*
T0*+
_output_shapes
:€€€€€€€€€2
Expy
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
€€€€€€€€€2
Sum/reduction_indicesБ
SumSumExp:y:0Sum/reduction_indices:output:0*
T0*+
_output_shapes
:€€€€€€€€€*
	keep_dims(2
Sumj
truedivRealDivExp:y:0Sum:output:0*
T0*+
_output_shapes
:€€€€€€€€€2	
truedivЧ
IdentityIdentitytruediv:z:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€@
 
_user_specified_nameinputs
о

*__inference_actor_hl0_layer_call_fn_739130

inputs
unknown
	unknown_0
identityИҐStatefulPartitionedCallщ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€@*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_actor_hl0_layer_call_and_return_conditional_losses_7388432
StatefulPartitionedCallТ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€d::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€d
 
_user_specified_nameinputs
≤ 
д
E__inference_actor_hl0_layer_call_and_return_conditional_losses_738843

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€d2
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
ReluЮ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€d::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€d
 
_user_specified_nameinputs
≤ 
д
E__inference_actor_hl1_layer_call_and_return_conditional_losses_739161

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
ReluЮ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€@
 
_user_specified_nameinputs
И
‘
$__inference_signature_wrapper_739005
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identityИҐStatefulPartitionedCall§
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8В **
f%R#
!__inference__wrapped_model_7387622
StatefulPartitionedCallТ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*J
_input_shapes9
7:€€€€€€€€€0::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€0
!
_user_specified_name	input_1
–
в
C__inference_actor_i_layer_call_and_return_conditional_losses_739035

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:0d*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€02
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€d2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:d2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€d2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€d2	
BiasAddЬ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€d2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€0::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€0
 
_user_specified_nameinputs
е%
‘
"__inference__traced_restore_739251
file_prefix)
%assignvariableop_actor_actor_i_kernel)
%assignvariableop_1_actor_actor_i_bias+
'assignvariableop_2_actor_actor_o_kernel)
%assignvariableop_3_actor_actor_o_bias-
)assignvariableop_4_actor_actor_hl0_kernel+
'assignvariableop_5_actor_actor_hl0_bias-
)assignvariableop_6_actor_actor_hl1_kernel+
'assignvariableop_7_actor_actor_hl1_bias

identity_9ИҐAssignVariableOpҐAssignVariableOp_1ҐAssignVariableOp_2ҐAssignVariableOp_3ҐAssignVariableOp_4ҐAssignVariableOp_5ҐAssignVariableOp_6ҐAssignVariableOp_7€
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*Л
valueБBю	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names†
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
RestoreV2/shape_and_slicesЎ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*8
_output_shapes&
$:::::::::*
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity§
AssignVariableOpAssignVariableOp%assignvariableop_actor_actor_i_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1™
AssignVariableOp_1AssignVariableOp%assignvariableop_1_actor_actor_i_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2ђ
AssignVariableOp_2AssignVariableOp'assignvariableop_2_actor_actor_o_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3™
AssignVariableOp_3AssignVariableOp%assignvariableop_3_actor_actor_o_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4Ѓ
AssignVariableOp_4AssignVariableOp)assignvariableop_4_actor_actor_hl0_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5ђ
AssignVariableOp_5AssignVariableOp'assignvariableop_5_actor_actor_hl0_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6Ѓ
AssignVariableOp_6AssignVariableOp)assignvariableop_6_actor_actor_hl1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7ђ
AssignVariableOp_7AssignVariableOp'assignvariableop_7_actor_actor_hl1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_79
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpО

Identity_8Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_8А

Identity_9IdentityIdentity_8:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7*
T0*
_output_shapes
: 2

Identity_9"!

identity_9Identity_9:output:0*5
_input_shapes$
": ::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_7:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
к
}
(__inference_actor_i_layer_call_fn_739044

inputs
unknown
	unknown_0
identityИҐStatefulPartitionedCallч
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *+
_output_shapes
:€€€€€€€€€d*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *L
fGRE
C__inference_actor_i_layer_call_and_return_conditional_losses_7387962
StatefulPartitionedCallТ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*+
_output_shapes
:€€€€€€€€€d2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€0::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€0
 
_user_specified_nameinputs
≤ 
д
E__inference_actor_hl0_layer_call_and_return_conditional_losses_739121

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:d@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€d2
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
ReluЮ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€d::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€d
 
_user_specified_nameinputs
≤ 
д
E__inference_actor_hl1_layer_call_and_return_conditional_losses_738890

inputs%
!tensordot_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐTensordot/ReadVariableOpЦ
Tensordot/ReadVariableOpReadVariableOp!tensordot_readvariableop_resource*
_output_shapes

:@@*
dtype02
Tensordot/ReadVariableOpj
Tensordot/axesConst*
_output_shapes
:*
dtype0*
valueB:2
Tensordot/axesq
Tensordot/freeConst*
_output_shapes
:*
dtype0*
valueB"       2
Tensordot/freeX
Tensordot/ShapeShapeinputs*
T0*
_output_shapes
:2
Tensordot/Shapet
Tensordot/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2/axis—
Tensordot/GatherV2GatherV2Tensordot/Shape:output:0Tensordot/free:output:0 Tensordot/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2x
Tensordot/GatherV2_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/GatherV2_1/axis„
Tensordot/GatherV2_1GatherV2Tensordot/Shape:output:0Tensordot/axes:output:0"Tensordot/GatherV2_1/axis:output:0*
Taxis0*
Tindices0*
Tparams0*
_output_shapes
:2
Tensordot/GatherV2_1l
Tensordot/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/ConstА
Tensordot/ProdProdTensordot/GatherV2:output:0Tensordot/Const:output:0*
T0*
_output_shapes
: 2
Tensordot/Prodp
Tensordot/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2
Tensordot/Const_1И
Tensordot/Prod_1ProdTensordot/GatherV2_1:output:0Tensordot/Const_1:output:0*
T0*
_output_shapes
: 2
Tensordot/Prod_1p
Tensordot/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat/axis∞
Tensordot/concatConcatV2Tensordot/free:output:0Tensordot/axes:output:0Tensordot/concat/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concatМ
Tensordot/stackPackTensordot/Prod:output:0Tensordot/Prod_1:output:0*
N*
T0*
_output_shapes
:2
Tensordot/stackР
Tensordot/transpose	TransposeinputsTensordot/concat:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
Tensordot/transposeЯ
Tensordot/ReshapeReshapeTensordot/transpose:y:0Tensordot/stack:output:0*
T0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€2
Tensordot/ReshapeЮ
Tensordot/MatMulMatMulTensordot/Reshape:output:0 Tensordot/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€@2
Tensordot/MatMulp
Tensordot/Const_2Const*
_output_shapes
:*
dtype0*
valueB:@2
Tensordot/Const_2t
Tensordot/concat_1/axisConst*
_output_shapes
: *
dtype0*
value	B : 2
Tensordot/concat_1/axisљ
Tensordot/concat_1ConcatV2Tensordot/GatherV2:output:0Tensordot/Const_2:output:0 Tensordot/concat_1/axis:output:0*
N*
T0*
_output_shapes
:2
Tensordot/concat_1Р
	TensordotReshapeTensordot/MatMul:product:0Tensordot/concat_1:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
	TensordotМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpЗ
BiasAddBiasAddTensordot:output:0BiasAdd/ReadVariableOp:value:0*
T0*+
_output_shapes
:€€€€€€€€€@2	
BiasAdd\
ReluReluBiasAdd:output:0*
T0*+
_output_shapes
:€€€€€€€€€@2
ReluЮ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Tensordot/ReadVariableOp*
T0*+
_output_shapes
:€€€€€€€€€@2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€@::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp24
Tensordot/ReadVariableOpTensordot/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€@
 
_user_specified_nameinputs
ў
Ф
__inference__traced_save_739217
file_prefix3
/savev2_actor_actor_i_kernel_read_readvariableop1
-savev2_actor_actor_i_bias_read_readvariableop3
/savev2_actor_actor_o_kernel_read_readvariableop1
-savev2_actor_actor_o_bias_read_readvariableop5
1savev2_actor_actor_hl0_kernel_read_readvariableop3
/savev2_actor_actor_hl0_bias_read_readvariableop5
1savev2_actor_actor_hl1_kernel_read_readvariableop3
/savev2_actor_actor_hl1_bias_read_readvariableop
savev2_const

identity_1ИҐMergeV2CheckpointsП
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1Л
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard¶
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameщ
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*Л
valueБBю	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesЪ
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
SaveV2/shape_and_slices 
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0/savev2_actor_actor_i_kernel_read_readvariableop-savev2_actor_actor_i_bias_read_readvariableop/savev2_actor_actor_o_kernel_read_readvariableop-savev2_actor_actor_o_bias_read_readvariableop1savev2_actor_actor_hl0_kernel_read_readvariableop/savev2_actor_actor_hl0_bias_read_readvariableop1savev2_actor_actor_hl1_kernel_read_readvariableop/savev2_actor_actor_hl1_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
2	2
SaveV2Ї
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes°
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*W
_input_shapesF
D: :0d:d:@::d@:@:@@:@: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:0d: 

_output_shapes
:d:$ 

_output_shapes

:@: 

_output_shapes
::$ 

_output_shapes

:d@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:	

_output_shapes
: "±L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*≥
serving_defaultЯ
?
input_14
serving_default_input_1:0€€€€€€€€€0@
output_14
StatefulPartitionedCall:0€€€€€€€€€tensorflow/serving/predict:гd
ќ
i
hidden_layers
o
regularization_losses
trainable_variables
	variables
	keras_api

signatures
*<&call_and_return_all_conditional_losses
=__call__
>_default_save_signature"с
_tf_keras_model„{"class_name": "Actor", "name": "actor", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"layer was saved without config": true}, "is_graph_network": false, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Actor"}}
у

	kernel

bias
regularization_losses
trainable_variables
	variables
	keras_api
*?&call_and_return_all_conditional_losses
@__call__"ќ
_tf_keras_layerі{"class_name": "Dense", "name": "actor_i", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_i", "trainable": true, "dtype": "float32", "units": 100, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 48]}}
.
0
1"
trackable_list_wrapper
т

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
*A&call_and_return_all_conditional_losses
B__call__"Ќ
_tf_keras_layer≥{"class_name": "Dense", "name": "actor_o", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_o", "trainable": true, "dtype": "float32", "units": 8, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
 "
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
 
regularization_losses
layer_regularization_losses
metrics
non_trainable_variables
layer_metrics
trainable_variables

layers
	variables
=__call__
>_default_save_signature
*<&call_and_return_all_conditional_losses
&<"call_and_return_conditional_losses"
_generic_user_object
,
Cserving_default"
signature_map
&:$0d2actor/actor_i/kernel
 :d2actor/actor_i/bias
 "
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
≠
regularization_losses
 layer_regularization_losses
!metrics
"non_trainable_variables
#layer_metrics
trainable_variables

$layers
	variables
@__call__
*?&call_and_return_all_conditional_losses
&?"call_and_return_conditional_losses"
_generic_user_object
ц

kernel
bias
%regularization_losses
&trainable_variables
'	variables
(	keras_api
*D&call_and_return_all_conditional_losses
E__call__"—
_tf_keras_layerЈ{"class_name": "Dense", "name": "actor_hl0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl0", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 100}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 100]}}
ф

kernel
bias
)regularization_losses
*trainable_variables
+	variables
,	keras_api
*F&call_and_return_all_conditional_losses
G__call__"ѕ
_tf_keras_layerµ{"class_name": "Dense", "name": "actor_hl1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl1", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [2, 1, 64]}}
&:$@2actor/actor_o/kernel
 :2actor/actor_o/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
≠
regularization_losses
-layer_regularization_losses
.metrics
/non_trainable_variables
0layer_metrics
trainable_variables

1layers
	variables
B__call__
*A&call_and_return_all_conditional_losses
&A"call_and_return_conditional_losses"
_generic_user_object
(:&d@2actor/actor_hl0/kernel
": @2actor/actor_hl0/bias
(:&@@2actor/actor_hl1/kernel
": @2actor/actor_hl1/bias
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
≠
%regularization_losses
2layer_regularization_losses
3metrics
4non_trainable_variables
5layer_metrics
&trainable_variables

6layers
'	variables
E__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
≠
)regularization_losses
7layer_regularization_losses
8metrics
9non_trainable_variables
:layer_metrics
*trainable_variables

;layers
+	variables
G__call__
*F&call_and_return_all_conditional_losses
&F"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
Ч2Ф
A__inference_actor_layer_call_and_return_conditional_losses_738960ќ
Э≤Щ
FullArgSpec!
argsЪ
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ **Ґ'
%К"
input_1€€€€€€€€€0
ь2щ
&__inference_actor_layer_call_fn_738982ќ
Э≤Щ
FullArgSpec!
argsЪ
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ **Ґ'
%К"
input_1€€€€€€€€€0
г2а
!__inference__wrapped_model_738762Ї
Л≤З
FullArgSpec
argsЪ 
varargsjargs
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ **Ґ'
%К"
input_1€€€€€€€€€0
н2к
C__inference_actor_i_layer_call_and_return_conditional_losses_739035Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
“2ѕ
(__inference_actor_i_layer_call_fn_739044Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
н2к
C__inference_actor_o_layer_call_and_return_conditional_losses_739081Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
“2ѕ
(__inference_actor_o_layer_call_fn_739090Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
ЋB»
$__inference_signature_wrapper_739005input_1"Ф
Н≤Й
FullArgSpec
argsЪ 
varargs
 
varkwjkwargs
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
п2м
E__inference_actor_hl0_layer_call_and_return_conditional_losses_739121Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
‘2—
*__inference_actor_hl0_layer_call_fn_739130Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
п2м
E__inference_actor_hl1_layer_call_and_return_conditional_losses_739161Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
‘2—
*__inference_actor_hl1_layer_call_fn_739170Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 Ю
!__inference__wrapped_model_738762y	
4Ґ1
*Ґ'
%К"
input_1€€€€€€€€€0
™ "7™4
2
output_1&К#
output_1€€€€€€€€€≠
E__inference_actor_hl0_layer_call_and_return_conditional_losses_739121d3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€d
™ ")Ґ&
К
0€€€€€€€€€@
Ъ Е
*__inference_actor_hl0_layer_call_fn_739130W3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€d
™ "К€€€€€€€€€@≠
E__inference_actor_hl1_layer_call_and_return_conditional_losses_739161d3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€@
™ ")Ґ&
К
0€€€€€€€€€@
Ъ Е
*__inference_actor_hl1_layer_call_fn_739170W3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€@
™ "К€€€€€€€€€@Ђ
C__inference_actor_i_layer_call_and_return_conditional_losses_739035d	
3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€0
™ ")Ґ&
К
0€€€€€€€€€d
Ъ Г
(__inference_actor_i_layer_call_fn_739044W	
3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€0
™ "К€€€€€€€€€d∞
A__inference_actor_layer_call_and_return_conditional_losses_738960k	
4Ґ1
*Ґ'
%К"
input_1€€€€€€€€€0
™ ")Ґ&
К
0€€€€€€€€€
Ъ И
&__inference_actor_layer_call_fn_738982^	
4Ґ1
*Ґ'
%К"
input_1€€€€€€€€€0
™ "К€€€€€€€€€Ђ
C__inference_actor_o_layer_call_and_return_conditional_losses_739081d3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€@
™ ")Ґ&
К
0€€€€€€€€€
Ъ Г
(__inference_actor_o_layer_call_fn_739090W3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€@
™ "К€€€€€€€€€≠
$__inference_signature_wrapper_739005Д	
?Ґ<
Ґ 
5™2
0
input_1%К"
input_1€€€€€€€€€0"7™4
2
output_1&К#
output_1€€€€€€€€€