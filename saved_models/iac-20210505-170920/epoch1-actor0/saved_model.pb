НЯ
щ╛
B
AssignVariableOp
resource
value"dtype"
dtypetypeИ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(И

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
9
Softmax
logits"T
softmax"T"
Ttype:
2
╛
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
Ц
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 И"serve*2.4.12v2.4.0-49-g85c8b2a817f8ал
x
actor_i/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:/d*
shared_nameactor_i/kernel
q
"actor_i/kernel/Read/ReadVariableOpReadVariableOpactor_i/kernel*
_output_shapes

:/d*
dtype0
p
actor_i/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:d*
shared_nameactor_i/bias
i
 actor_i/bias/Read/ReadVariableOpReadVariableOpactor_i/bias*
_output_shapes
:d*
dtype0
x
actor_o/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*
shared_nameactor_o/kernel
q
"actor_o/kernel/Read/ReadVariableOpReadVariableOpactor_o/kernel*
_output_shapes

:@*
dtype0
p
actor_o/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameactor_o/bias
i
 actor_o/bias/Read/ReadVariableOpReadVariableOpactor_o/bias*
_output_shapes
:*
dtype0
|
actor_hl0/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:d@*!
shared_nameactor_hl0/kernel
u
$actor_hl0/kernel/Read/ReadVariableOpReadVariableOpactor_hl0/kernel*
_output_shapes

:d@*
dtype0
t
actor_hl0/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_nameactor_hl0/bias
m
"actor_hl0/bias/Read/ReadVariableOpReadVariableOpactor_hl0/bias*
_output_shapes
:@*
dtype0
|
actor_hl1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@@*!
shared_nameactor_hl1/kernel
u
$actor_hl1/kernel/Read/ReadVariableOpReadVariableOpactor_hl1/kernel*
_output_shapes

:@@*
dtype0
t
actor_hl1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_nameactor_hl1/bias
m
"actor_hl1/bias/Read/ReadVariableOpReadVariableOpactor_hl1/bias*
_output_shapes
:@*
dtype0

NoOpNoOp
е
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*р
value╓B╙ B╠
Г
i
hidden_layers
o
	variables
regularization_losses
trainable_variables
	keras_api

signatures
h

	kernel

bias
	variables
regularization_losses
trainable_variables
	keras_api

0
1
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
8
	0

1
2
3
4
5
6
7
 
8
	0

1
2
3
4
5
6
7
н
layer_regularization_losses
metrics
	variables
regularization_losses
layer_metrics
non_trainable_variables
trainable_variables

layers
 
GE
VARIABLE_VALUEactor_i/kernel#i/kernel/.ATTRIBUTES/VARIABLE_VALUE
CA
VARIABLE_VALUEactor_i/bias!i/bias/.ATTRIBUTES/VARIABLE_VALUE

	0

1
 

	0

1
н
 layer_regularization_losses
!metrics
	variables
regularization_losses
"layer_metrics
#non_trainable_variables
trainable_variables

$layers
h

kernel
bias
%	variables
&regularization_losses
'trainable_variables
(	keras_api
h

kernel
bias
)	variables
*regularization_losses
+trainable_variables
,	keras_api
GE
VARIABLE_VALUEactor_o/kernel#o/kernel/.ATTRIBUTES/VARIABLE_VALUE
CA
VARIABLE_VALUEactor_o/bias!o/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
н
-layer_regularization_losses
.metrics
	variables
regularization_losses
/layer_metrics
0non_trainable_variables
trainable_variables

1layers
LJ
VARIABLE_VALUEactor_hl0/kernel&variables/2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUEactor_hl0/bias&variables/3/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEactor_hl1/kernel&variables/4/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUEactor_hl1/bias&variables/5/.ATTRIBUTES/VARIABLE_VALUE
 
 
 
 

0
1
2
3
 
 
 
 
 

0
1
 

0
1
н
2layer_regularization_losses
3metrics
%	variables
&regularization_losses
4layer_metrics
5non_trainable_variables
'trainable_variables

6layers

0
1
 

0
1
н
7layer_regularization_losses
8metrics
)	variables
*regularization_losses
9layer_metrics
:non_trainable_variables
+trainable_variables

;layers
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
z
serving_default_input_1Placeholder*'
_output_shapes
:         /*
dtype0*
shape:         /
┴
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1actor_i/kernelactor_i/biasactor_hl0/kernelactor_hl0/biasactor_hl1/kernelactor_hl1/biasactor_o/kernelactor_o/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         **
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8В *,
f'R%
#__inference_signature_wrapper_66790
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
┬
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename"actor_i/kernel/Read/ReadVariableOp actor_i/bias/Read/ReadVariableOp"actor_o/kernel/Read/ReadVariableOp actor_o/bias/Read/ReadVariableOp$actor_hl0/kernel/Read/ReadVariableOp"actor_hl0/bias/Read/ReadVariableOp$actor_hl1/kernel/Read/ReadVariableOp"actor_hl1/bias/Read/ReadVariableOpConst*
Tin
2
*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *'
f"R 
__inference__traced_save_66916
Э
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameactor_i/kernelactor_i/biasactor_o/kernelactor_o/biasactor_hl0/kernelactor_hl0/biasactor_hl1/kernelactor_hl1/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В **
f%R#
!__inference__traced_restore_66950б№
ю	
▌
D__inference_actor_hl0_layer_call_and_return_conditional_losses_66840

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:d@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
ReluЧ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         d::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         d
 
_user_specified_nameinputs
Ў
╙
#__inference_signature_wrapper_66790
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identityИвStatefulPartitionedCallЯ
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         **
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8В *)
f$R"
 __inference__wrapped_model_666332
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:         /::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         /
!
_user_specified_name	input_1
╪
|
'__inference_actor_i_layer_call_fn_66809

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallЄ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         d*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_actor_i_layer_call_and_return_conditional_losses_666472
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         d2

Identity"
identityIdentity:output:0*.
_input_shapes
:         /::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         /
 
_user_specified_nameinputs
ю	
▌
D__inference_actor_hl0_layer_call_and_return_conditional_losses_66674

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:d@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
ReluЧ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         d::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         d
 
_user_specified_nameinputs
Ш
╒
%__inference_actor_layer_call_fn_66767
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
identityИвStatefulPartitionedCall┐
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         **
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8В *I
fDRB
@__inference_actor_layer_call_and_return_conditional_losses_667452
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:         /::::::::22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         /
!
_user_specified_name	input_1
Р	
█
B__inference_actor_i_layer_call_and_return_conditional_losses_66647

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:/d*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         d2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         d2	
BiasAddХ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         d2

Identity"
identityIdentity:output:0*.
_input_shapes
:         /::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         /
 
_user_specified_nameinputs
ю	
▌
D__inference_actor_hl1_layer_call_and_return_conditional_losses_66860

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
ReluЧ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
▄$
г
!__inference__traced_restore_66950
file_prefix#
assignvariableop_actor_i_kernel#
assignvariableop_1_actor_i_bias%
!assignvariableop_2_actor_o_kernel#
assignvariableop_3_actor_o_bias'
#assignvariableop_4_actor_hl0_kernel%
!assignvariableop_5_actor_hl0_bias'
#assignvariableop_6_actor_hl1_kernel%
!assignvariableop_7_actor_hl1_bias

identity_9ИвAssignVariableOpвAssignVariableOp_1вAssignVariableOp_2вAssignVariableOp_3вAssignVariableOp_4вAssignVariableOp_5вAssignVariableOp_6вAssignVariableOp_7╫
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*у
value┘B╓	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB&variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesа
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
RestoreV2/shape_and_slices╪
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*8
_output_shapes&
$:::::::::*
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

IdentityЮ
AssignVariableOpAssignVariableOpassignvariableop_actor_i_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1д
AssignVariableOp_1AssignVariableOpassignvariableop_1_actor_i_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2ж
AssignVariableOp_2AssignVariableOp!assignvariableop_2_actor_o_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3д
AssignVariableOp_3AssignVariableOpassignvariableop_3_actor_o_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4и
AssignVariableOp_4AssignVariableOp#assignvariableop_4_actor_hl0_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5ж
AssignVariableOp_5AssignVariableOp!assignvariableop_5_actor_hl0_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6и
AssignVariableOp_6AssignVariableOp#assignvariableop_6_actor_hl1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7ж
AssignVariableOp_7AssignVariableOp!assignvariableop_7_actor_hl1_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_79
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpО

Identity_8Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_8А

Identity_9IdentityIdentity_8:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7*
T0*
_output_shapes
: 2

Identity_9"!

identity_9Identity_9:output:0*5
_input_shapes$
": ::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_7:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
у*
Ш
 __inference__wrapped_model_66633
input_10
,actor_actor_i_matmul_readvariableop_resource1
-actor_actor_i_biasadd_readvariableop_resource2
.actor_actor_hl0_matmul_readvariableop_resource3
/actor_actor_hl0_biasadd_readvariableop_resource2
.actor_actor_hl1_matmul_readvariableop_resource3
/actor_actor_hl1_biasadd_readvariableop_resource0
,actor_actor_o_matmul_readvariableop_resource1
-actor_actor_o_biasadd_readvariableop_resource
identityИв&actor/actor_hl0/BiasAdd/ReadVariableOpв%actor/actor_hl0/MatMul/ReadVariableOpв&actor/actor_hl1/BiasAdd/ReadVariableOpв%actor/actor_hl1/MatMul/ReadVariableOpв$actor/actor_i/BiasAdd/ReadVariableOpв#actor/actor_i/MatMul/ReadVariableOpв$actor/actor_o/BiasAdd/ReadVariableOpв#actor/actor_o/MatMul/ReadVariableOp╖
#actor/actor_i/MatMul/ReadVariableOpReadVariableOp,actor_actor_i_matmul_readvariableop_resource*
_output_shapes

:/d*
dtype02%
#actor/actor_i/MatMul/ReadVariableOpЮ
actor/actor_i/MatMulMatMulinput_1+actor/actor_i/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         d2
actor/actor_i/MatMul╢
$actor/actor_i/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_i_biasadd_readvariableop_resource*
_output_shapes
:d*
dtype02&
$actor/actor_i/BiasAdd/ReadVariableOp╣
actor/actor_i/BiasAddBiasAddactor/actor_i/MatMul:product:0,actor/actor_i/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         d2
actor/actor_i/BiasAdd╜
%actor/actor_hl0/MatMul/ReadVariableOpReadVariableOp.actor_actor_hl0_matmul_readvariableop_resource*
_output_shapes

:d@*
dtype02'
%actor/actor_hl0/MatMul/ReadVariableOp╗
actor/actor_hl0/MatMulMatMulactor/actor_i/BiasAdd:output:0-actor/actor_hl0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
actor/actor_hl0/MatMul╝
&actor/actor_hl0/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl0_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl0/BiasAdd/ReadVariableOp┴
actor/actor_hl0/BiasAddBiasAdd actor/actor_hl0/MatMul:product:0.actor/actor_hl0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
actor/actor_hl0/BiasAddИ
actor/actor_hl0/ReluRelu actor/actor_hl0/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
actor/actor_hl0/Relu╜
%actor/actor_hl1/MatMul/ReadVariableOpReadVariableOp.actor_actor_hl1_matmul_readvariableop_resource*
_output_shapes

:@@*
dtype02'
%actor/actor_hl1/MatMul/ReadVariableOp┐
actor/actor_hl1/MatMulMatMul"actor/actor_hl0/Relu:activations:0-actor/actor_hl1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
actor/actor_hl1/MatMul╝
&actor/actor_hl1/BiasAdd/ReadVariableOpReadVariableOp/actor_actor_hl1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02(
&actor/actor_hl1/BiasAdd/ReadVariableOp┴
actor/actor_hl1/BiasAddBiasAdd actor/actor_hl1/MatMul:product:0.actor/actor_hl1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
actor/actor_hl1/BiasAddИ
actor/actor_hl1/ReluRelu actor/actor_hl1/BiasAdd:output:0*
T0*'
_output_shapes
:         @2
actor/actor_hl1/Relu╖
#actor/actor_o/MatMul/ReadVariableOpReadVariableOp,actor_actor_o_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02%
#actor/actor_o/MatMul/ReadVariableOp╣
actor/actor_o/MatMulMatMul"actor/actor_hl1/Relu:activations:0+actor/actor_o/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
actor/actor_o/MatMul╢
$actor/actor_o/BiasAdd/ReadVariableOpReadVariableOp-actor_actor_o_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02&
$actor/actor_o/BiasAdd/ReadVariableOp╣
actor/actor_o/BiasAddBiasAddactor/actor_o/MatMul:product:0,actor/actor_o/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
actor/actor_o/BiasAddЛ
actor/actor_o/SoftmaxSoftmaxactor/actor_o/BiasAdd:output:0*
T0*'
_output_shapes
:         2
actor/actor_o/Softmaxп
IdentityIdentityactor/actor_o/Softmax:softmax:0'^actor/actor_hl0/BiasAdd/ReadVariableOp&^actor/actor_hl0/MatMul/ReadVariableOp'^actor/actor_hl1/BiasAdd/ReadVariableOp&^actor/actor_hl1/MatMul/ReadVariableOp%^actor/actor_i/BiasAdd/ReadVariableOp$^actor/actor_i/MatMul/ReadVariableOp%^actor/actor_o/BiasAdd/ReadVariableOp$^actor/actor_o/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:         /::::::::2P
&actor/actor_hl0/BiasAdd/ReadVariableOp&actor/actor_hl0/BiasAdd/ReadVariableOp2N
%actor/actor_hl0/MatMul/ReadVariableOp%actor/actor_hl0/MatMul/ReadVariableOp2P
&actor/actor_hl1/BiasAdd/ReadVariableOp&actor/actor_hl1/BiasAdd/ReadVariableOp2N
%actor/actor_hl1/MatMul/ReadVariableOp%actor/actor_hl1/MatMul/ReadVariableOp2L
$actor/actor_i/BiasAdd/ReadVariableOp$actor/actor_i/BiasAdd/ReadVariableOp2J
#actor/actor_i/MatMul/ReadVariableOp#actor/actor_i/MatMul/ReadVariableOp2L
$actor/actor_o/BiasAdd/ReadVariableOp$actor/actor_o/BiasAdd/ReadVariableOp2J
#actor/actor_o/MatMul/ReadVariableOp#actor/actor_o/MatMul/ReadVariableOp:P L
'
_output_shapes
:         /
!
_user_specified_name	input_1
▄
~
)__inference_actor_hl0_layer_call_fn_66849

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallЇ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *M
fHRF
D__inference_actor_hl0_layer_call_and_return_conditional_losses_666742
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         d::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         d
 
_user_specified_nameinputs
╨
у
__inference__traced_save_66916
file_prefix-
)savev2_actor_i_kernel_read_readvariableop+
'savev2_actor_i_bias_read_readvariableop-
)savev2_actor_o_kernel_read_readvariableop+
'savev2_actor_o_bias_read_readvariableop/
+savev2_actor_hl0_kernel_read_readvariableop-
)savev2_actor_hl0_bias_read_readvariableop/
+savev2_actor_hl1_kernel_read_readvariableop-
)savev2_actor_hl1_bias_read_readvariableop
savev2_const

identity_1ИвMergeV2CheckpointsП
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1Л
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardж
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename╤
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*у
value┘B╓	B#i/kernel/.ATTRIBUTES/VARIABLE_VALUEB!i/bias/.ATTRIBUTES/VARIABLE_VALUEB#o/kernel/.ATTRIBUTES/VARIABLE_VALUEB!o/bias/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB&variables/5/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesЪ
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:	*
dtype0*%
valueB	B B B B B B B B B 2
SaveV2/shape_and_slicesЪ
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0)savev2_actor_i_kernel_read_readvariableop'savev2_actor_i_bias_read_readvariableop)savev2_actor_o_kernel_read_readvariableop'savev2_actor_o_bias_read_readvariableop+savev2_actor_hl0_kernel_read_readvariableop)savev2_actor_hl0_bias_read_readvariableop+savev2_actor_hl1_kernel_read_readvariableop)savev2_actor_hl1_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
2	2
SaveV2║
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesб
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*W
_input_shapesF
D: :/d:d:@::d@:@:@@:@: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:/d: 

_output_shapes
:d:$ 

_output_shapes

:@: 

_output_shapes
::$ 

_output_shapes

:d@: 

_output_shapes
:@:$ 

_output_shapes

:@@: 

_output_shapes
:@:	

_output_shapes
: 
Ї	
█
B__inference_actor_o_layer_call_and_return_conditional_losses_66728

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAdda
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:         2	
SoftmaxЦ
IdentityIdentitySoftmax:softmax:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Ї	
█
B__inference_actor_o_layer_call_and_return_conditional_losses_66820

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAdda
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:         2	
SoftmaxЦ
IdentityIdentitySoftmax:softmax:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ж
М
@__inference_actor_layer_call_and_return_conditional_losses_66745
input_1
actor_i_66658
actor_i_66660
actor_hl0_66685
actor_hl0_66687
actor_hl1_66712
actor_hl1_66714
actor_o_66739
actor_o_66741
identityИв!actor_hl0/StatefulPartitionedCallв!actor_hl1/StatefulPartitionedCallвactor_i/StatefulPartitionedCallвactor_o/StatefulPartitionedCallН
actor_i/StatefulPartitionedCallStatefulPartitionedCallinput_1actor_i_66658actor_i_66660*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         d*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_actor_i_layer_call_and_return_conditional_losses_666472!
actor_i/StatefulPartitionedCall╕
!actor_hl0/StatefulPartitionedCallStatefulPartitionedCall(actor_i/StatefulPartitionedCall:output:0actor_hl0_66685actor_hl0_66687*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *M
fHRF
D__inference_actor_hl0_layer_call_and_return_conditional_losses_666742#
!actor_hl0/StatefulPartitionedCall║
!actor_hl1/StatefulPartitionedCallStatefulPartitionedCall*actor_hl0/StatefulPartitionedCall:output:0actor_hl1_66712actor_hl1_66714*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *M
fHRF
D__inference_actor_hl1_layer_call_and_return_conditional_losses_667012#
!actor_hl1/StatefulPartitionedCall░
actor_o/StatefulPartitionedCallStatefulPartitionedCall*actor_hl1/StatefulPartitionedCall:output:0actor_o_66739actor_o_66741*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_actor_o_layer_call_and_return_conditional_losses_667282!
actor_o/StatefulPartitionedCallИ
IdentityIdentity(actor_o/StatefulPartitionedCall:output:0"^actor_hl0/StatefulPartitionedCall"^actor_hl1/StatefulPartitionedCall ^actor_i/StatefulPartitionedCall ^actor_o/StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:         /::::::::2F
!actor_hl0/StatefulPartitionedCall!actor_hl0/StatefulPartitionedCall2F
!actor_hl1/StatefulPartitionedCall!actor_hl1/StatefulPartitionedCall2B
actor_i/StatefulPartitionedCallactor_i/StatefulPartitionedCall2B
actor_o/StatefulPartitionedCallactor_o/StatefulPartitionedCall:P L
'
_output_shapes
:         /
!
_user_specified_name	input_1
╪
|
'__inference_actor_o_layer_call_fn_66829

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallЄ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *K
fFRD
B__inference_actor_o_layer_call_and_return_conditional_losses_667282
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
ю	
▌
D__inference_actor_hl1_layer_call_and_return_conditional_losses_66701

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         @2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:         @2
ReluЧ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
Р	
█
B__inference_actor_i_layer_call_and_return_conditional_losses_66800

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИвBiasAdd/ReadVariableOpвMatMul/ReadVariableOpН
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:/d*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         d2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:d*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         d2	
BiasAddХ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         d2

Identity"
identityIdentity:output:0*.
_input_shapes
:         /::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         /
 
_user_specified_nameinputs
▄
~
)__inference_actor_hl1_layer_call_fn_66869

inputs
unknown
	unknown_0
identityИвStatefulPartitionedCallЇ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *M
fHRF
D__inference_actor_hl1_layer_call_and_return_conditional_losses_667012
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         @2

Identity"
identityIdentity:output:0*.
_input_shapes
:         @::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs"▒L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*л
serving_defaultЧ
;
input_10
serving_default_input_1:0         /<
output_10
StatefulPartitionedCall:0         tensorflow/serving/predict:Эc
╬
i
hidden_layers
o
	variables
regularization_losses
trainable_variables
	keras_api

signatures
<_default_save_signature
=__call__
*>&call_and_return_all_conditional_losses"ё
_tf_keras_model╫{"class_name": "Actor", "name": "actor", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"layer was saved without config": true}, "is_graph_network": false, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Actor"}}
Ё

	kernel

bias
	variables
regularization_losses
trainable_variables
	keras_api
?__call__
*@&call_and_return_all_conditional_losses"╦
_tf_keras_layer▒{"class_name": "Dense", "name": "actor_i", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_i", "trainable": true, "dtype": "float32", "units": 100, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 47}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 47]}}
.
0
1"
trackable_list_wrapper
я

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
A__call__
*B&call_and_return_all_conditional_losses"╩
_tf_keras_layer░{"class_name": "Dense", "name": "actor_o", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_o", "trainable": true, "dtype": "float32", "units": 8, "activation": "softmax", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 64]}}
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
 "
trackable_list_wrapper
X
	0

1
2
3
4
5
6
7"
trackable_list_wrapper
╩
layer_regularization_losses
metrics
	variables
regularization_losses
layer_metrics
non_trainable_variables
trainable_variables

layers
=__call__
<_default_save_signature
*>&call_and_return_all_conditional_losses
&>"call_and_return_conditional_losses"
_generic_user_object
,
Cserving_default"
signature_map
 :/d2actor_i/kernel
:d2actor_i/bias
.
	0

1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
	0

1"
trackable_list_wrapper
н
 layer_regularization_losses
!metrics
	variables
regularization_losses
"layer_metrics
#non_trainable_variables
trainable_variables

$layers
?__call__
*@&call_and_return_all_conditional_losses
&@"call_and_return_conditional_losses"
_generic_user_object
є

kernel
bias
%	variables
&regularization_losses
'trainable_variables
(	keras_api
D__call__
*E&call_and_return_all_conditional_losses"╬
_tf_keras_layer┤{"class_name": "Dense", "name": "actor_hl0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl0", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 100}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 100]}}
ё

kernel
bias
)	variables
*regularization_losses
+trainable_variables
,	keras_api
F__call__
*G&call_and_return_all_conditional_losses"╠
_tf_keras_layer▓{"class_name": "Dense", "name": "actor_hl1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "actor_hl1", "trainable": true, "dtype": "float32", "units": 64, "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [1, 64]}}
 :@2actor_o/kernel
:2actor_o/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
н
-layer_regularization_losses
.metrics
	variables
regularization_losses
/layer_metrics
0non_trainable_variables
trainable_variables

1layers
A__call__
*B&call_and_return_all_conditional_losses
&B"call_and_return_conditional_losses"
_generic_user_object
": d@2actor_hl0/kernel
:@2actor_hl0/bias
": @@2actor_hl1/kernel
:@2actor_hl1/bias
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
н
2layer_regularization_losses
3metrics
%	variables
&regularization_losses
4layer_metrics
5non_trainable_variables
'trainable_variables

6layers
D__call__
*E&call_and_return_all_conditional_losses
&E"call_and_return_conditional_losses"
_generic_user_object
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
н
7layer_regularization_losses
8metrics
)	variables
*regularization_losses
9layer_metrics
:non_trainable_variables
+trainable_variables

;layers
F__call__
*G&call_and_return_all_conditional_losses
&G"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
▐2█
 __inference__wrapped_model_66633╢
Л▓З
FullArgSpec
argsЪ 
varargsjargs
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *&в#
!К
input_1         /
ў2Ї
%__inference_actor_layer_call_fn_66767╩
Э▓Щ
FullArgSpec!
argsЪ
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *&в#
!К
input_1         /
Т2П
@__inference_actor_layer_call_and_return_conditional_losses_66745╩
Э▓Щ
FullArgSpec!
argsЪ
jself
j
input_data
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *&в#
!К
input_1         /
╤2╬
'__inference_actor_i_layer_call_fn_66809в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ь2щ
B__inference_actor_i_layer_call_and_return_conditional_losses_66800в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╤2╬
'__inference_actor_o_layer_call_fn_66829в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ь2щ
B__inference_actor_o_layer_call_and_return_conditional_losses_66820в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╩B╟
#__inference_signature_wrapper_66790input_1"Ф
Н▓Й
FullArgSpec
argsЪ 
varargs
 
varkwjkwargs
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╙2╨
)__inference_actor_hl0_layer_call_fn_66849в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ю2ы
D__inference_actor_hl0_layer_call_and_return_conditional_losses_66840в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╙2╨
)__inference_actor_hl1_layer_call_fn_66869в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ю2ы
D__inference_actor_hl1_layer_call_and_return_conditional_losses_66860в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 Х
 __inference__wrapped_model_66633q	
0в-
&в#
!К
input_1         /
к "3к0
.
output_1"К
output_1         д
D__inference_actor_hl0_layer_call_and_return_conditional_losses_66840\/в,
%в"
 К
inputs         d
к "%в"
К
0         @
Ъ |
)__inference_actor_hl0_layer_call_fn_66849O/в,
%в"
 К
inputs         d
к "К         @д
D__inference_actor_hl1_layer_call_and_return_conditional_losses_66860\/в,
%в"
 К
inputs         @
к "%в"
К
0         @
Ъ |
)__inference_actor_hl1_layer_call_fn_66869O/в,
%в"
 К
inputs         @
к "К         @в
B__inference_actor_i_layer_call_and_return_conditional_losses_66800\	
/в,
%в"
 К
inputs         /
к "%в"
К
0         d
Ъ z
'__inference_actor_i_layer_call_fn_66809O	
/в,
%в"
 К
inputs         /
к "К         dз
@__inference_actor_layer_call_and_return_conditional_losses_66745c	
0в-
&в#
!К
input_1         /
к "%в"
К
0         
Ъ 
%__inference_actor_layer_call_fn_66767V	
0в-
&в#
!К
input_1         /
к "К         в
B__inference_actor_o_layer_call_and_return_conditional_losses_66820\/в,
%в"
 К
inputs         @
к "%в"
К
0         
Ъ z
'__inference_actor_o_layer_call_fn_66829O/в,
%в"
 К
inputs         @
к "К         г
#__inference_signature_wrapper_66790|	
;в8
в 
1к.
,
input_1!К
input_1         /"3к0
.
output_1"К
output_1         