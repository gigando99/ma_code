#pattern1 = r"([p,P]ick[u,U]p_[d,D]ate[t,T]ime)"
#pattern2 = r"([d,D]rop[o,O]ff_[d,D]ate[t,T]ime)"
#pattern3 = r"(PU[L,l]ocationID)"
#pattern4 = r"(DO[L,l]ocationID)"


#i entspricht 0 oder 1
'''
datetime_lists = list()
for i in range(len(self.change_to_dt)):
    datetime_lists.append(list())

    #j entspricht den einzelen datums-werten
    for j in elem[self.change_to_dt[i]]:
        j = datetime.datetime.strptime(j, "%Y-%m-%d %H:%M:%S")
        datetime_lists[i].append(j)
    elem = elem.drop([self.change_to_dt[i]], axis=1)
    elem[self.new_col_names[i]] = datetime_lists[i]
new_dfs.append(elem)

for i in self.change_to_dt:
    #print(elem[i])
    elem.loc[:,i] = pd.to_datetime(elem.loc[:,i], format="%Y-%m-%d %H:%M:%S")
'''
#new_dfs.append(elem)


#print(self.preprocessed_dfs)

#return new_dfs

def test_taxi_dataload(self, data_dfs): #for testing purposes only
    test_head = data_dfs[0].head()
    if self.verbose:
        print("Before transformation: \n", test_head.columns, "\n", test_head.iloc[0,2], type(test_head.iloc[0,2]))
    tester = self.preprocess_taxi_data([test_head])[0]
    if self.verbose:
        print("After transformation: \n", tester.columns, "\n", tester.iloc[0,1], type(tester.iloc[0,1]))
    return 0

    '''
    Required Arguments:
    paths (list of strings): contains the paths to all csvs to be imported as dfs
    ---
    Returns:
    csvs (list of dfs): contains all imported dfs

    csvs = list()
    n = len(list_of_paths)
    if not list_of_converters:
        list_of_converters = n*[None]

    for i in range(n):
        df = pd.read_csv(list_of_paths[i], converters = list_of_converters[i], parse_dates = parse_dates)
        csvs.append(df)

    return csvs
    '''

    #self.list_of_paths_to_data = list_of_paths_to_data
    #self.remaining_col = ["tpep_pickup_datetime", "tpep_dropoff_datetime", "PULocationID", "DOLocationID"]
    #self.change_to_dt = ["tpep_pickup_datetime", "tpep_dropoff_datetime"]
    #self.new_col_names = ["PU_datetime", "DO_datetime"]
    #self.parser = lambda date: datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
    #self.list_of_dfs = self._get_CSVs(list_of_paths = , list_of_converters = [{}, {}, {"route": eval}])
    #self.nBoroughs = len(self.boroughs)

    #input_test.test_taxi_dataload(input_test.data_dfs) #test function but dont save stuff
    #list_of_paths_to_data = ['data/taxi_data/raw/yellow_tripdata_2018-01.csv','data/taxi_data/raw/yellow_tripdata_2018-02.csv']
    #input_test.data_dfs = input_test._get_CSVs(list_of_paths_to_data, parse_dates = input_test.change_to_dt) #date_parser = self.parser
    #input_test.preprocessed_dfs = input_test.preprocess_taxi_data(input_test.data_dfs)

    #input_test.data_dfs = list()
    #list_of_paths_to_data = ['data/taxi_data/raw/yellow_tripdata_2018-01.csv','data/taxi_data/raw/yellow_tripdata_2018-02.csv']
    #df = input_test.read_data_dfs(list_of_paths_to_data[0])
    #input_test.data_dfs.append(df)

    #input_test.preprocessed_dfs = input_test.preprocess_taxi_data(input_test.data_dfs)

    print(input_test.preprocessed_dfs[0].iloc[0][0])
    print(input_test.preprocessed_dfs[0].iloc[0][0].year,
          input_test.list_of_dfs[0]["center"][0],
          input_test.list_of_dfs[2]["route"][0][0])
    print(input_test.boroughs)
    print(input_test.travel_stats[0][1])

    '''# new zone is not reached
    if self.data.travel_stats[old_loc[0], old_loc[1]] > old_loc[2] + self.step_interval:
        old_loc[2] += self.step_interval
    else:
        old_loc[0] = old_loc[1]
        new_goal = current_route[current_route.index(old_loc[0] + 1)]
        old_loc[1] = new_goal'''

#%% neglogs
import tensorflow as tf

y_hat = tf.constant(np.array([[0.5, 1.5, 0.1],[2.2, 1.3, 1.7]]))
y_true = tf.constant(np.array([[0.0, 1.0, 0.0],[0.0, 0.0, 1.0]]))
y_hat_softmax = tf.nn.softmax(y_hat)
print("--- yhat ---\n", y_hat)
print("--- ytrue ---\n", y_true)
print("--- yhat_softmax ---\n", y_hat_softmax)
loss_per_instance_1 = -tf.reduce_sum(y_true * tf.math.log(y_hat_softmax), [1])
#loss_per_instance_1 = -tf.reduce_sum(y_hat_softmax * tf.math.log(y_hat_softmax), [1])
total_loss_1 = tf.reduce_mean(loss_per_instance_1)
print("\n--- by hand ---")
print("y_hat_softmax = tf.nn.softmax(y_hat)")
print("tf.reduce_mean(-tf.reduce_sum(y_true * tf.math.log(y_hat_softmax)))")
print(total_loss_1)
loss_per_instance_2 = tf.nn.softmax_cross_entropy_with_logits(logits = y_hat, labels = y_true)
#loss_per_instance_2 = tf.nn.softmax_cross_entropy_with_logits(logits = y_hat, labels = y_hat_softmax)
total_loss_2 = tf.reduce_mean(loss_per_instance_2)
print("\n--- tf function ---")
print("tf.nn.softmax_cross_entropy_with_logits(logits = y_hat, labels = y_true)")
print(total_loss_2)

logs = tf.math.log(y_hat_softmax)
total_loss_3 = -tf.math.reduce_sum(logs * y_true, [1])
print("\n--- 3 ---\n", logs, total_loss_3)

loss_per_instance_4 = tf.nn.sparse_softmax_cross_entropy_with_logits(logits = y_hat, labels = [1,2])
total_loss_4 = tf.reduce_mean(loss_per_instance_4)
print("--- 4 ---\n", loss_per_instance_4, total_loss_4)

#prob = tf.math.log([[0.6, 0.4]])
#depth = prob.get_shape().as_list()[-1]
#action = tf.one_hot(1, depth)
#print(prob, action, tf.nn.softmax_cross_entropy_with_logits(logits = prob, labels = action))

###################################################################
###### tensorflow implementation ##################################
###################################################################
#
#  action_log_probs = tf.math.log(action_probs)
#  actor_loss = -tf.math.reduce_sum(action_log_probs * advantage)
#
###################################################################
###### tensorflow implementation ##################################
###################################################################


###################################################################
###### baseline implementation ####################################
###################################################################
#
#  x = tf.one_hot(x, self.logits.get_shape().as_list()[-1])
#  tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, labels=x)
#
#  neglogpac = pd.neglogp(actions)
#  pg_loss = tf.reduce_mean(advs * neglogpac)
#
###################################################################
###### baseline implementation ####################################
###################################################################

def actor_loss(self, logits, action, all_qs):

        if self.all_actions:
            #probs = tf.nn.softmax(tf.math.log(logits))
            log_probs = tf.math.log(logits + self.epsilon)
            #log_probs = tf.math.log(logits)
            if np.max(logits) == 1:
                print("problem", time.time())
            #print("probs", logits)

            loss = - tf.reduce_sum(log_probs * all_qs, [1])
            #loss = tf.reduce_sum(logits * log_probs * all_qs, [1])

        else:
            depth = logits.get_shape().as_list()[-1]
            action_onehot = tf.one_hot(action, depth)
            neglog = tf.nn.softmax_cross_entropy_with_logits(logits = logits, labels = action_onehot)
            q = all_qs * action_onehot
            loss = tf.reduce_mean(q * neglog)
            #print("softmax result: {}, aloss: {}".format(neglog,loss))

        #print("aloss: ", loss)
        return loss

#%% neglogs
import tensorflow as tf

y_hat = tf.constant(np.array([[0.5, 1.5, 0.1],[2.2, 1.3, 1.7]]))
y_true = tf.constant(np.array([[0.0, 1.0, 0.0],[0.0, 0.0, 1.0]]))
y_hat_softmax = tf.nn.softmax(y_hat)
print("--- yhat ---\n", y_hat)
print("\n--- ytrue ---\n", y_true)
print("\n--- yhat_softmax ---\n", y_hat_softmax)

ads = tf.constant(np.array([[2.0, 5.0, 4.0],[0.0, 0.0, 3.0]]))
single_ads = tf.reduce_sum(ads * y_true, axis = -1)

print(ads)
print("\n",single_ads)

#  action_log_probs = tf.math.log(action_probs)
#  actor_loss = -tf.math.reduce_sum(action_log_probs * advantage)

#-tf.reduce_sum(y_true * tf.math.log(y_hat_softmax))
print(-tf.reduce_sum(ads * y_true * tf.math.log(y_hat_softmax)))
print(-tf.reduce_sum(ads * tf.math.log(y_hat_softmax)))
print(-tf.reduce_sum(ads * y_hat_softmax * tf.math.log(y_hat_softmax)))

# one hot labels
loss_per_instance_2 = tf.nn.softmax_cross_entropy_with_logits(logits = y_hat, labels = y_true)
print(loss_per_instance_2)
total_loss_2 = tf.reduce_sum(single_ads * loss_per_instance_2)
print(total_loss_2)

# prob labels
loss_per_instance_2 = tf.nn.softmax_cross_entropy_with_logits(logits = y_hat, labels = y_hat_softmax)
print(loss_per_instance_2)
total_loss_2 = tf.reduce_sum(single_ads * loss_per_instance_2)
print(total_loss_2)

# %%

df = pd.DataFrame({
'salesman_id': [5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5011,5012],
'sale_jan':[150.5, 270.65, 65.26, 110.5, 948.5, 2400.6, 1760, 2983.43, 480.4,  1250.45, 75.29,1045.6]})
print("Original Orders DataFrame:")
print(df)
#result = df.groupby("salesman_id")
#print(result.groups)

bins = [5000, 5001, 5005, 5015]
labels = [0,1,2]
result = df.groupby(pd.cut(df.salesman_id,bins, right = False, labels = labels))
#print(result.get_group(0))
fdsa = dict(list(result))
print(fdsa[1])

#%% testing gradienttape

class testmodel(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(2)
        self.l2 = tf.keras.layers.Dense(1)

    def call(self, x):
        y = self.l1(x)
        y = self.l2(y)
        return y


'''
model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(2))
model.add(tf.keras.layers.Dense(1))
#model.compile(optimizer='sgd', loss='mse')

model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(2))
model2.add(tf.keras.layers.Dense(1))
#model2.compile(optimizer='sgd', loss='mse')
'''

def modelloss(x,y):
    print("x: ", x,"\ny: ", y)
    return (x-y)**2

def model2loss(x,y):
    return (x - y)**2

def test(g, model, loss):
    #print(g)
    return g.gradient(loss, model.trainable_variables)

def test2(model,x):
    return model(x)

x = tf.expand_dims([1.], 0)
x = tf.stack([x,x,x])
#print(x)
y = [0,0,0]
model = testmodel()
model2 = testmodel()
#y = np.random.randint(0, 2, (2, 2))
#print(x,y)
#model.fit(x, y)
y2 = model2(x)
with tf.GradientTape(persistent = True) as gg:
    #y = model(x)
    y = test2(model, x)
    #y2 = model2(x)

#with tf.GradientTape(persistent = True) as g: #, tf.GradientTape() as g2

    #print(y,y2)
    loss = modelloss(x,y)
    loss2 = model2loss(x,y2)
#for elem in model.trainable_variables:
    #print(elem)
#grads = g.gradient(loss, model.trainable_variables)
#grads2 = g.gradient(loss2, model2.trainable_variables)

grads = test(gg, model, loss)
grads2 = test(gg, model2, loss2)

print("grads: ", grads)
print("grads2: ", grads2)

del gg




#%%


def test():
    q = tf.Variable(4.0)
    with tf.GradientTape() as gg:
        #gg.watch(q)
        w = 5 * q
    dq_dw = gg.gradient(w,q)
    print(dq_dw)


x = tf.Variable(3.0)
with tf.GradientTape() as g:
    #g.watch(x)
    y = x * x
    test()
dy_dx = g.gradient(y, x)  # (4*x^3 at x = 3)
print(dy_dx)

import tensorflow as tf

class testmodel(tf.keras.Model):
    def __init__(self, activation):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(2)
        self.l2 = tf.keras.layers.Dense(2, activation = activation)

    def call(self, x):
        y = self.l1(x)
        y = self.l2(y)
        return y

def actorloss(log_logits, action):

    depth = log_logits.get_shape().as_list()[-1]
    action_onehot = tf.one_hot(action, depth)
    loss = - tf.reduce_sum(action_onehot * log_logits)

    return loss


list_of_x = [tf.expand_dims([1.,2.], 0),tf.expand_dims([2.,3.], 0),tf.expand_dims([3.,2.], 0),tf.expand_dims([1.,3.], 0)]

y_true = 0
actor = testmodel("softmax")
critic = testmodel(None)

# call model early
logits = actor(list_of_x[0])
log_logits = tf.math.log(logits)
action = tf.squeeze(tf.random.categorical(log_logits,1)).numpy()
#print("BEFORE: ", "\nLogits: ", logits, "\nLog Logits: ", log_logits, "\nAction: ", action)
print("BEFORE: ", "\nInput: ",list_of_x[0], "\nLogits: ", logits)

for episode in range(3):
    logits1 = actor(list_of_x[episode+1])
    log_logits1 = tf.math.log(logits1)
    action1 = tf.squeeze(tf.random.categorical(log_logits1,1)).numpy()
    #print("\nEpisode: ", episode, "\nLogits1: ", logits, "\nLog Logits1: ", log_logits, "\nAction1: ", action)
    print("\nEpisode: ", episode, "\nNext Action: ","\nInput: ",list_of_x[episode+1], "\nNext Logits: ", logits)

    with tf.GradientTape(persistent = True) as g:
        logits = actor(list_of_x[episode])
        print("GradientTape: ", "\nInput: ",list_of_x[episode], "\nLogits: ", logits)
        log_logits = tf.math.log(logits)

        ################################################################################
        # cannot use old logits here, need to be fresh from call within gradientTape   #
        # problem: not the same es before                                              #
        ################################################################################

        loss = actorloss(log_logits,action)

    grads = g.gradient(loss, actor.trainable_variables)
    #print(grads)
    tf.keras.optimizers.Adam(learning_rate = 0.1).apply_gradients(zip(grads, actor.trainable_variables))

    action = action1

'''
model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(2))
model.add(tf.keras.layers.Dense(1))
#model.compile(optimizer='sgd', loss='mse')

model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(2))
model2.add(tf.keras.layers.Dense(1))
#model2.compile(optimizer='sgd', loss='mse')
'''

'''
def model2loss(x,y):
    print("model 2\n","x: ", x,"\ny: ", y)
    return (x - y)**2

def modelloss(x,y):
    print("\nmodel 1","\nx: ", x,"\ny: ", y)
    return (x - y)**2

def test(g, model, loss):
    #print(g)
    return g.gradient(loss, model.trainable_variables)

def test2(model,x):
    return model(x)

with tf.GradientTape(persistent = True) as g:
    y_hat2 = model2(x2)

with tf.GradientTape(persistent = True) as gg:
    y_hat = model(x)
    #y_hat = test2(model, x)
    y_hat2_latent = model2(x)
    #y2 = model2(x)

#with tf.GradientTape(persistent = True) as g: #, tf.GradientTape() as g2

    #print(y,y2)
    loss = modelloss(y_true,y_hat)
    print("loss: ", loss)
    loss2 = model2loss(y_true,y_hat2)
    print("loss2: ", loss2)
#for elem in model.trainable_variables:
    #print(elem)

grads = gg.gradient(loss, model.trainable_variables)
grads2 = gg.gradient(loss2, model2.trainable_variables)

#grads = test(gg, model, loss)
#grads2 = test(gg, model2, loss2)
#print(model.summary())
print("grads: ", grads)
print("grads2: ", grads2)

del gg
'''

#%%
test_dict = dict()
names = ["time", "reward"]

for elem in names:
    test_dict[elem] = tf.keras.metrics.Sum(elem, dtype=tf.float32)


test_dict["reward"](5)
test_dict["reward"](6)

result_dict = dict()
for elem in names:
    result_dict[elem] = test_dict[elem].result()

print(result_dict)

# %%

stats_dict = {"total_length": [[],[],[],[]], "only_manhattan": [[],[],[],[]], "wo_in-zone": [[],[],[],[]], "cleaned": [[],[],[],[]]}

stats_dict["total_length"][0]

# %%
a = tf.constant(np.array([[0.5, 1.5, 0.1]]))
m = tf.keras.metrics.Mean()
m(tf.reduce_mean(a))
m(17)
print(m, m.result())

# %%
import matplotlib.pyplot as plt
asdf = tf.constant(np.array([[0.5, 1.5, 0.1],[2.2, 1.3, 1.7],[0.1,0.1,0.8],[1,1,8]]))
asdf = tf.random.categorical(tf.math.log(asdf), 1000)
probs1 = asdf[2].numpy()
probs2 = asdf[3].numpy()
plt.hist(probs1)
plt.show()
plt.hist(probs2)
plt.show()

#%%

plt.plot(range(int(nSteps/10)), aloss, "g")
plt.plot(range(int(nSteps/10)), closs, "b")
plt.plot(range(int(nSteps/10)), rewards, "y")
plt.show()

'''
def run(self, nEpochs = 1, nEpisodes = 1,  nSteps = 86400, mode = "train", **kwargs):

    #total_starttime = time.time()

    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    log_dir = 'logs/tb/' + current_time
    summary_writer = tf.summary.create_file_writer(log_dir)

    epoch = 0
    while epoch < nEpochs:
        print("-------- starting epoch {} --------".format(epoch))
        epoch_starttime = time.time()

        episode = 0
        while (episode < nEpisodes) and (episode < len(self.env.dict_of_list_of_data_days[mode])):
            print("-------- starting episode {} --------".format(episode))
            episode_starttime = time.time()

            # get day
            data_day = self.env.dict_of_list_of_data_days[mode][episode]
            print("chosen day: ", data_day.strftime("%Y-%m-%d"))

            # load day into env
            picklestarttime = time.time()
            self.env.pick_data_by_day(data_day)
            pickle_time = round(time.time() - picklestarttime,4)

            # initialize state and get first actions
            resetstarttime = time.time()
            if self.setting == "nh":
                state = self.env.reset(transformed=False)
            else:
                state = self.env.reset(transformed=True)
            reset_time = round(time.time() - resetstarttime,4)

            list_of_actions = self.get_list_of_actions(state)

            done = False
            while state[0] < nSteps and not done:

                stepstarttime = time.time()

                next_state, list_of_rewards, done = self.timed_env_step(list_of_actions)
                list_of_next_actions = self.get_list_of_actions(next_state)


                if mode == "train":
                    self.learn(self._prep_input(state),
                               self._prep_input(next_state),
                               list_of_rewards,
                               list_of_actions,
                               list_of_next_actions)

                state = next_state
                list_of_actions = list_of_next_actions

                self.dict_of_metrics["reward"](sum(list_of_rewards))

                step_time = round(time.time() - stepstarttime,4)
                self.dict_of_metrics["step_time"](step_time)

            #print("total reward for day {} of epoch {} is {}".format(day, epoch, total_reward))
            #print(all_actions)

            with summary_writer.as_default():
                tf.summary.scalar('reward', self.dict_of_metrics["reward"].result(), step=episode)

                if mode == "train":
                    tf.summary.scalar('aloss', self.dict_of_metrics["aloss"].result(), step=episode)
                    tf.summary.scalar('closs', self.dict_of_metrics["closs"].result(), step=episode)


            # print some output at the end of every episode
            if mode == "train":
                template = 'Epoch {} -- Day {} -- Setting {}\naLoss: {}\ncLoss: {}\nReward: {}\n'
                print(template.format(epoch, episode, self.setting,
                                      self.dict_of_metrics["aloss"].result(),
                                      self.dict_of_metrics["closs"].result(),
                                      self.dict_of_metrics["reward"].result()))

                self.dict_of_metrics["aloss"].reset_states()
                self.dict_of_metrics["closs"].reset_states()


            else:
                template = 'Epoch {} -- Day {} -- Setting {}\nReward: {}\n'
                print(template.format(epoch, episode, self.setting,
                                      self.dict_of_metrics["reward"].result()))

            # print elapsed time
            episode_time = round(time.time() - episode_starttime,4)
            #self.dict_of_metrics["episode_time"](episode_time)

            print("Total Elapsed Time: ", episode_time)
            print("-- of which pickle : ", pickle_time)
            #print("-- of which reset : ", reset_time)
            print("Step time : ", np.round(self.dict_of_metrics["step_time"].result().numpy(),4))
            print("-- of which env.step : ", np.round(self.dict_of_metrics["env_time"].result().numpy(),4))
            print("-- of which fwd : ", np.round(self.dict_of_metrics["fwd_time"].result().numpy(),4))
            print("-- of which learning : ", np.round(self.dict_of_metrics["learning_time"].result().numpy(),4))
            print("\n")
            print(self.env.stats_dict)
            print("\n")

            # Reset metrics every episode
            self.dict_of_metrics["reward"].reset_states()
            self.dict_of_metrics["learning_time"].reset_states()
            self.dict_of_metrics["step_time"].reset_states()
            self.dict_of_metrics["env_time"].reset_states()
            self.dict_of_metrics["fwd_time"].reset_states()

            episode += 1

            #print(self.list_of_actors[0].summary(), self.list_of_critics[0].summary())
            #episode_counter += 1
            #if episode_counter >= nEpisodes:
            #    break

        epoch_time = round(time.time() - epoch_starttime,4)
        print("Finished epoch after {}s".format(epoch_time))
        epoch += 1

    #return all_aloss, all_closs, all_rewards
'''

'''
# dont think this works, insertt after env.step in loop:
# while state[0] < nSteps and not done:
if mode == "test":
    list_of_next_actions = self.get_list_of_actions(next_state)

else:
    # mode == "train"
    with tf.GradientTape() as t:
        list_of_next_actions = self.get_list_of_actions(next_state)
        aloss, closs = self.compute_losses(self.prep_input(state),
                   self.prep_input(next_state),
                   list_of_rewards,
                   list_of_actions,
                   list_of_next_actions)

    self.apply_gradients(t, aloss, closs)
'''
'''
# then not necessary anymore
def apply_gradients(self, tape, list_of_alosses, list_of_closses):

    if self.setting == "iac":
        for agent in self.list_of_agents:
            grads1 = tape.gradient(list_of_closses[agent], self.list_of_critics[agent].trainable_variables)
            grads2 = tape.gradient(list_of_alosses[agent], self.list_of_actors[agent].trainable_variables)
            self.c_opt.apply_gradients(zip(grads1, self.list_of_critics[agent].trainable_variables))
            self.a_opt.apply_gradients(zip(grads2, self.list_of_actors[agent].trainable_variables))

    pass

def copmute_losses(self, preprocessed_state, preprocessed_next_state, list_of_actions, list_of_next_actions, list_of_rewards):

    list_of_alosses = list()
    list_of_closses = list()

    if self.setting == "iac":
        # each agent updates parameters with own experience
        for agent in self.list_of_agents:
            closs, all_qs = self._critic_learn(agent,
                                           preprocessed_state,
                                           preprocessed_next_state,
                                           list_of_actions[agent],
                                           list_of_next_actions[agent],
                                           list_of_rewards[agent])

            aloss = self._actor_learn(agent,
                                       preprocessed_state,
                                       q,
                                       list_of_actions[agent])

            list_of_alosses.append(aloss)
            list_of_closses.append(closs)

            self.dict_of_metrics["aloss"](aloss)
            self.dict_of_metrics["closs"](closs)



    pass
'''

'''
def run_epoch(self, nSteps, nEpisodes, epoch, mode, summary_writer, data_set):
    episode = 0
    while (episode < nEpisodes):

        # get day
        data_day = data_set[episode]
        #print("chosen day: ", data_day.strftime("%Y-%m-%d"))

        print("-------- starting episode {}, day: {}, mode: {} --------".format(episode, data_day.strftime("%Y-%m-%d"), mode))
        episode_starttime = time.time()

        # load day into env
        #picklestarttime = time.time()
        self.env.pick_data_by_day(data_day)
        #pickle_time = round(time.time() - picklestarttime,4)

        # initialize state and get first actions
        #resetstarttime = time.time()
        if self.setting == "nh":
            state = self.env.reset(transformed=False)
        else:
            state = self.env.reset(transformed=True)
        #reset_time = round(time.time() - resetstarttime,4)

        list_of_actions = self.get_list_of_actions(state)

        #print("chose actions: ", list_of_actions)

        done = False
        while state[0] < nSteps and not done:

            #stepstarttime = time.time()
            #print("before env: ", list_of_actions)
            if self.setting in ["nh", "random"]:
                next_state, list_of_rewards, done = self.env.step(list_of_actions, transformed = False)
            else:
                next_state, list_of_rewards, done = self.env.step(list_of_actions, transformed = True)

            #next_state, list_of_rewards, done = self.timed_env_step(list_of_actions)
            #print("after env: ", list_of_actions)
            list_of_next_actions = self.get_list_of_actions(next_state)
            #print("next actions: ", list_of_next_actions)


            #if list_of_actions != list_of_next_actions:
            #    print(list_of_actions, list_of_next_actions)


            if mode == "train":
                self.learn(self._prep_input(state),
                           self._prep_input(next_state),
                           list_of_rewards,
                           list_of_actions,
                           list_of_next_actions)

            state = next_state
            #print("now", list_of_actions)
            #print("check: ", list_of_next_actions == list_of_actions)
            #print("current: {}\nnext: {}".format(list_of_actions, list_of_next_actions))
            #if list_of_next_actions != list_of_actions:
            #    print("within if: ", state[0], list_of_actions, list_of_next_actions)
            #print(list_of_next_actions, list_of_actions)
            list_of_actions = list_of_next_actions
            #print("overwriting loa: ", list_of_actions)
            #print("actions: ", list_of_actions)


            self.dict_of_metrics["reward"](sum(list_of_rewards))

            #step_time = round(time.time() - stepstarttime,4)
            #self.dict_of_metrics["step_time"](step_time)

            self.dict_of_metrics["missed_jobs"](self.env.stats_dict["missed"])
            self.dict_of_metrics["served_jobs"](self.env.stats_dict["served"])
            sum_of_rebalancing_jobs = sum([self.env.stats_dict[x][0] for x in range(self.env.nAgents)])
            self.dict_of_metrics["rebalancing_jobs"](sum_of_rebalancing_jobs)

        #print("total reward for day {} of epoch {} is {}".format(day, epoch, total_reward))
        #print(all_actions)
        with summary_writer.as_default():
            hp.hparams(self.hparams)
            tf.summary.scalar("reward", self.dict_of_metrics["reward"].result(), step=self.global_episode_counter)
            tf.summary.scalar('missed_jobs', self.dict_of_metrics["missed_jobs"].result(), step=self.global_episode_counter)
            tf.summary.scalar('rebalancing_jobs', self.dict_of_metrics["rebalancing_jobs"].result(), step=self.global_episode_counter)
            tf.summary.scalar('served_jobs', self.dict_of_metrics["served_jobs"].result(), step=self.global_episode_counter)
            #tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)
            if mode == "train":
                tf.summary.scalar('aloss', self.dict_of_metrics["aloss"].result(), step=self.global_episode_counter)
                tf.summary.scalar('closs', self.dict_of_metrics["closs"].result(), step=self.global_episode_counter)

        # print some output at the end of every episode
        if mode == "train":
            template = 'Epoch {} -- Day {} -- Setting {}\naLoss: {}\ncLoss: {}\nReward: {}\n'
            print(template.format(epoch, episode, self.setting,
                                  self.dict_of_metrics["aloss"].result(),
                                  self.dict_of_metrics["closs"].result(),
                                  self.dict_of_metrics["reward"].result()))

            self.dict_of_metrics["aloss"].reset_states()
            self.dict_of_metrics["closs"].reset_states()


        else:
            template = 'Epoch {} -- Day {} -- Setting {}\nReward: {}\n'
            print(template.format(epoch, episode, self.setting,
                                  self.dict_of_metrics["reward"].result()))

        # print elapsed time
        episode_time = round(time.time() - episode_starttime,4)
        self.dict_of_metrics["episode_time"](episode_time)
        #self.dict_of_metrics["episode_time"](episode_time)

        print("Total Elapsed Time: ", episode_time)
        print("-- of which pickle : ", pickle_time)
        #print("-- of which reset : ", reset_time)
        print("Step time : ", np.round(self.dict_of_metrics["step_time"].result().numpy(),4))
        print("-- of which env.step : ", np.round(self.dict_of_metrics["env_time"].result().numpy(),4))
        print("-- of which fwd : ", np.round(self.dict_of_metrics["fwd_time"].result().numpy(),4))
        print("-- of which learning : ", np.round(self.dict_of_metrics["learning_time"].result().numpy(),4))
        print("\n")
        print("Environment Stats:\n",self.env.stats_dict)
        print("\n")

        # Reset metrics every episode
        self.dict_of_metrics["reward"].reset_states()
        self.dict_of_metrics["learning_time"].reset_states()
        self.dict_of_metrics["step_time"].reset_states()
        self.dict_of_metrics["env_time"].reset_states()
        self.dict_of_metrics["fwd_time"].reset_states()
        self.dict_of_metrics["missed_jobs"].reset_states()
        self.dict_of_metrics["served_jobs"].reset_states()
        self.dict_of_metrics["rebalancing_jobs"].reset_states()



        episode += 1
        self.global_episode_counter += 1
'''

'''
def _init_dict_of_metrics(self):

    metric_dict = dict()
    metrics = ["aloss", "closs", "reward",
               "env_time", "fwd_time", "step_time", "learning_time", "episode_time", "epoch_time",
               "served_jobs", "missed_jobs", "rebalancing_jobs"]
    # show tensors as well? https://stackoverflow.com/questions/48851439/how-to-display-a-list-of-all-gradients-in-tensorboard

    for metric_name in metrics:
        if metric_name == "reward":
            metric_dict[metric_name] = tf.keras.metrics.Sum(metric_name, dtype=tf.float32)
        else:
            metric_dict[metric_name] = tf.keras.metrics.Mean(metric_name, dtype=tf.float32)

    return metric_dict
'''

### stackoverflow:
'''
class testmodel(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(20)
        self.l2 = tf.keras.layers.Dense(20)
        self.l3 = tf.keras.layers.Dense(2, activation = "softmax")

    def call(self, x):
        y = self.l1(x)
        y = self.l2(y)
        y = self.l3(y)
        return y

class MARL():
    def __init__(self, nAgents, input_shape):
        self.nAgents = nAgents
        self.list_of_actors = list()
        self.list_of_optimizers = list()
        for agent in range(nAgents):
            self.list_of_actors.append(testmodel())
            self.list_of_optimizers.append(tf.keras.optimizers.Adam(learning_rate = 0.001))
            self.list_of_actors[agent].build(input_shape = input_shape)

    @tf.function
    def learn_for_loop(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        for agent in range(self.nAgents):
            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[agent](x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
            self.list_of_optimizers[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))


    @tf.function
    def learn_for_loop_exp(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        for agent in range(self.nAgents):
            self.learn(x, agent)

    @tf.function
    def learn_tf_loop(self):

        def body(i,x):
            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[i](x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, self.list_of_actors[i].trainable_variables)
            self.list_of_optimizers[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))

    @tf.function
    def learn_tf_loop_exp(self):

        def body(i,x):
            self.learn(x,i)

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))

    @tf.function
    def learn(self, x, agent):

        with tf.GradientTape() as g:

            y_hat = self.list_of_actors[agent](x)
            loss = y_hat - tf.constant([0.,0])

        grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
        self.list_of_optimizers[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

test_instance = MARL(10, (1,20))

tic = time.time()
for _ in range(100):
    test_instance.learn_for_loop()
print(time.time() - tic)
# without @tf.function: ~ 7s
# with @tf.function: ~ 3.5s

tic = time.time()
for _ in range(100):
    test_instance.learn_for_loop_exp()
print(time.time() - tic)

tic = time.time()
for _ in range(100):
    test_instance.learn_tf_loop_exp()
print(time.time() - tic)

tic = time.time()
for _ in range(100):
    test_instance.learn_tf_loop()
print(time.time() - tic)
# without @tf.function: ~ 7s
# with @tf.function: super problematic
'''

'''def actor_norm(self, state):
    normed_inputs = list()
    for agent in self.list_of_agents:
        extended_state = tf.concat([state, [[agent]]],1)
        #normed_inputs.append(tf.keras.utils.normalize(extended_state))
        #normed_inputs.append(tf.math.l2_normalize(extended_state))
        normed_inputs.append(extended_state / np.max(self.env.data.travel_stats))
    stacked_normed_inputs = tf.stack(normed_inputs)
    #print(stacked_normed_inputs)
    return stacked_normed_inputs

def critic_norm(self, state, list_of_actions):
    normed_inputs = list()
    for agent in self.list_of_agents:
        input = self.get_action_expanded_state(state, list_of_actions, tf.constant(agent), add_agent = tf.constant(True))
        #normed_inputs.append(tf.keras.utils.normalize(input))
        normed_inputs.append(input / np.max(self.env.data.travel_stats))
    stacked_normed_inputs = tf.stack(normed_inputs)
    return stacked_normed_inputs'''


#### old learn function
@tf.function
def learn(self, actor_inputs, critic_inputs, critic_next_inputs, list_of_rewards, list_of_actions, list_of_next_actions, step, episode):
    #self, preprocessed_state, preprocessed_next_state, list_of_rewards, list_of_actions, list_of_next_actions, step, episode, **kwargs

    # each agent updates parameters with own experience
    # note: actor_inputs = critic_inputs
    if self.setting == "iac":

        for agent in self.list_of_agents:

            c_loss, q = getattr(self, "clearn" + str(agent))(self, self.list_of_critics[agent], self.list_of_c_opts[agent],
                                                            critic_inputs,
                                                            critic_next_inputs,
                                                            list_of_actions[agent],
                                                            list_of_next_actions[agent],
                                                            list_of_rewards[agent])

            a_loss = getattr(self, "alearn" + str(agent))(self, self.list_of_actors[agent], self.list_of_a_opts[agent],
                                                           critic_inputs,
                                                           q,
                                                           list_of_actions[agent],
                                                           step,
                                                           episode)


            self.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)
            self.dict_of_temp_metrics["closs-critic"+str(agent)](c_loss)


        '''def condition(agent, actor_inputs, critic_inputs, critic_next_inputs,
                      list_of_actions, list_of_next_actions, list_of_rewards,
                      step, episode):
            return tf.less(agent,self.env.nAgents)

        def body(agent, actor_inputs, critic_inputs, critic_next_inputs,
                 list_of_actions, list_of_next_actions, list_of_rewards,
                 step, episode):

            str_agent = str(agent.numpy().tolist())

            c_loss, q = getattr(self, "clearn" + str_agent)(self, self.list_of_critics[agent], self.list_of_c_opts[agent],
                                                            critic_inputs,
                                                            critic_next_inputs,
                                                            list_of_actions[agent],
                                                            list_of_next_actions[agent],
                                                            list_of_rewards[agent])

            a_loss = getattr(self, "alearn" + str_agent)(self, self.list_of_actors[agent], self.list_of_a_opts[agent],
                                                           critic_inputs,
                                                           q,
                                                           list_of_actions[agent],
                                                           step,
                                                           episode)'''

            '''c_loss, q = self._critic_learn(agent,
                                           critic_inputs,
                                           critic_next_inputs,
                                           list_of_actions[agent],
                                           list_of_next_actions[agent],
                                           list_of_rewards[agent])

            a_loss = self._actor_learn(agent,
                                       actor_inputs,
                                       q,
                                       list_of_actions[agent],
                                       step = step,
                                       episode = episode)'''
            '''
            self.dict_of_temp_metrics["aloss-actor"+str_agent](a_loss)
            self.dict_of_temp_metrics["closs-critic"+str_agent](c_loss)

            return (tf.add(agent,1), actor_inputs, critic_inputs, critic_next_inputs,
                     list_of_actions, list_of_next_actions, list_of_rewards,
                     step, episode)

        agent = 0

        _, _, _, _, _, _, _, _ =  tf.while_loop(condition, body, (agent, actor_inputs, critic_inputs, critic_next_inputs,
                 list_of_actions, list_of_next_actions, list_of_rewards,
                 step, episode))
            '''


    # update parameters of all agents sharing actions in critic update
    elif self.setting == "shared_actions":

        for agent in self.list_of_agents:

            c_loss, q = getattr(self, "clearn" + str(agent))(self, self.list_of_critics[agent], self.list_of_c_opts[agent],
                                                            critic_inputs[agent],
                                                            critic_next_inputs[agent],
                                                            list_of_actions[agent],
                                                            list_of_next_actions[agent],
                                                            list_of_rewards[agent])

            a_loss = getattr(self, "alearn" + str(agent))(self, self.list_of_actors[agent], self.list_of_a_opts[agent],
                                                           actor_inputs,
                                                           q,
                                                           list_of_actions[agent],
                                                           step,
                                                           episode)


            self.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)
            self.dict_of_temp_metrics["closs-critic"+str(agent)](c_loss)

        '''for agent in self.list_of_agents:

            c_loss, q = self._critic_learn(agent,
                                           critic_inputs[agent],
                                           critic_next_inputs[agent],
                                           list_of_actions[agent],
                                           list_of_next_actions[agent],
                                           list_of_rewards[agent])

            a_loss = self._actor_learn(agent,
                                       actor_inputs,
                                       q,
                                       list_of_actions[agent],
                                       step = step,
                                       episode = episode)

            self.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)
            self.dict_of_temp_metrics["closs-critic"+str(agent)](c_loss)'''

        '''def condition(agent, preprocessed_state, preprocessed_next_state,
                      list_of_actions, list_of_next_actions, list_of_rewards):
            return tf.less(agent,self.env.nAgents)

        def body(agent, preprocessed_state, preprocessed_next_state,
                 list_of_actions, list_of_next_actions, list_of_rewards):

            agent_preprocessed_state = self.get_action_expanded_state(preprocessed_state,
                                                                      list_of_actions,
                                                                      agent)

            agent_preprocessed_next_state = self.get_action_expanded_state(preprocessed_next_state,
                                                                           list_of_next_actions,
                                                                           agent)

            c_loss, q = self._critic_learn(agent,
                                           agent_preprocessed_state,
                                           agent_preprocessed_next_state,
                                           list_of_actions[agent],
                                           list_of_next_actions[agent],
                                           list_of_rewards[agent])

            a_loss = self._actor_learn(agent,
                                       preprocessed_state,
                                       q,
                                       list_of_actions[agent])

            self.dict_of_temp_metrics["aloss-actor"+str(agent.numpy().tolist())](a_loss)
            self.dict_of_temp_metrics["closs-critic"+str(agent.numpy().tolist())](c_loss)

            return (tf.add(agent,1), preprocessed_state, preprocessed_next_state,
                     list_of_actions, list_of_next_actions, list_of_rewards)

        agent = tf.constant(0)

        _, _, _, _, _, _ =  tf.while_loop(condition, body, (agent, preprocessed_state, preprocessed_next_state,
                 list_of_actions, list_of_next_actions, list_of_rewards))'''


    elif self.setting == "centralized_critic":

        # train one centralized critic which learns from batched input of all agents
        default_agent = 0
        preprocessed_state = kwargs["actor_inputs"]
        stacked_c_inputs = kwargs["critic_inputs"]
        stacked_c_next_inputs = kwargs["critic_next_inputs"]

        '''list_of_c_inputs = list()
        list_of_c_next_inputs = list()

        for agent in self.list_of_agents:
        #for agent in tf.range(self.env.nAgents):

            agent_preprocessed_state = self.get_action_expanded_state(preprocessed_state,
                                                                      list_of_actions,
                                                                      agent,
                                                                      add_agent = True)

            agent_preprocessed_next_state = self.get_action_expanded_state(preprocessed_next_state,
                                                                           list_of_next_actions,
                                                                           agent,
                                                                           add_agent = True)
            list_of_c_inputs.append(agent_preprocessed_state)
            list_of_c_next_inputs.append(agent_preprocessed_next_state)

        #print(list_of_inputs)
        stacked_c_inputs = tf.stack(list_of_c_inputs)
        stacked_c_next_inputs = tf.stack(list_of_c_next_inputs)
        #print(stacked_inputs)'''

        c_loss, q = self._critic_learn(default_agent,
                                                stacked_c_inputs,
                                                stacked_c_next_inputs,
                                                list_of_actions,
                                                list_of_next_actions,
                                                list_of_rewards)

        self.dict_of_temp_metrics["closs-critic0"](c_loss)

        '''for agent in self.list_of_agents:
            a_loss = self._actor_learn(agent,
                                       preprocessed_state,
                                       q,
                                       list_of_actions[agent])

            self.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)'''

        def condition(agent, preprocessed_state, list_of_actions, step, episode):
            return tf.less(agent,self.env.nAgents)

        def body(agent, preprocessed_state, list_of_actions,step, episode):

            a_loss = self._actor_learn(agent,
                                       preprocessed_state,
                                       q,
                                       list_of_actions[agent],
                                       step = step,
                                       episode = episode)

            self.dict_of_temp_metrics["aloss-actor"+str(agent.numpy().tolist())](a_loss)

            return (tf.add(agent,1), preprocessed_state, list_of_actions, step, episode)

        agent = tf.constant(0)

        _, _, _, _, _ =  tf.while_loop(condition, body, (agent, preprocessed_state, list_of_actions, step, episode))

    # synchronously update the two networks with all experiences
    elif self.setting == "exemplary":

        default_agent = 0

        c_loss, all_qs = self._critic_learn(default_agent,
                                            critic_inputs,
                                            critic_next_inputs,
                                            list_of_actions,
                                            list_of_next_actions,
                                            list_of_rewards)

        a_loss = self._actor_learn(default_agent,
                                   actor_inputs,
                                   all_qs,
                                   list_of_actions,
                                   step,
                                   episode)

        self.dict_of_temp_metrics["aloss-actor0"](a_loss)
        self.dict_of_temp_metrics["closs-critic0"](c_loss)

    # update parameters of single agent based on combined experience
    # note: actor inputs and critic inputs are the same
    elif self.setting == "single_agent":

        default_agent = 0
        single_a = self.transform_to_single_action(list_of_actions, self.env.nActions)
        single_an = self.transform_to_single_action(list_of_next_actions, self.env.nActions)
        sum_of_rewards = sum(list_of_rewards)

        c_loss, q = self._critic_learn(default_agent,
                                       critic_inputs,
                                       critic_next_inputs,
                                       single_a,
                                       single_an,
                                       sum_of_rewards)

        a_loss = self._actor_learn(default_agent,
                                   critic_inputs,
                                   q,
                                   single_a,
                                   step,
                                   episode)

        self.dict_of_temp_metrics["aloss-actor0"](a_loss)
        self.dict_of_temp_metrics["closs-critic0"](c_loss)

    return 0

## jupyter notebook cleaning
'''
class testmodel(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(20)
        self.l2 = tf.keras.layers.Dense(20)
        self.l3 = tf.keras.layers.Dense(2, activation = "softmax")

    def call(self, x):
        y = self.l1(x)
        y = self.l2(y)
        y = self.l3(y)
        return y

class MARL():
    def __init__(self, nAgents, input_shape):
        self.nAgents = nAgents
        self.list_of_actors = list()
        self.list_of_critics = list()
        self.list_of_a_opts = list()
        self.list_of_c_opts = list()
        for agent in range(nAgents):
            self.list_of_actors.append(testmodel())
            self.list_of_critics.append(testmodel())
            self.list_of_a_opts.append(tf.keras.optimizers.Adam(learning_rate = 0.001))
            self.list_of_c_opts.append(tf.keras.optimizers.Adam(learning_rate = 0.001))
            self.list_of_actors[agent].build(input_shape = input_shape)
            self.list_of_critics[agent].build(input_shape = input_shape)
        self.alearn0 = self.get_apply_grad_fn()
        self.alearn1 = self.get_apply_grad_fn()
        self.alearn2 = self.get_apply_grad_fn()
        self.alearn3 = self.get_apply_grad_fn()
        self.alearn4 = self.get_apply_grad_fn()
        self.alearn5 = self.get_apply_grad_fn()
        self.alearn6 = self.get_apply_grad_fn()
        self.alearn7 = self.get_apply_grad_fn()
        self.alearn8 = self.get_apply_grad_fn()
        self.alearn9 = self.get_apply_grad_fn()
        self.clearn0 = self.get_apply_grad_fn()
        self.clearn1 = self.get_apply_grad_fn()
        self.clearn2 = self.get_apply_grad_fn()
        self.clearn3 = self.get_apply_grad_fn()
        self.clearn4 = self.get_apply_grad_fn()
        self.clearn5 = self.get_apply_grad_fn()
        self.clearn6 = self.get_apply_grad_fn()
        self.clearn7 = self.get_apply_grad_fn()
        self.clearn8 = self.get_apply_grad_fn()
        self.clearn9 = self.get_apply_grad_fn()

    @tf.function
    def learn_for_loop(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        for agent in range(self.nAgents):
            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[agent](x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
            self.list_of_a_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

    @tf.function
    def learn_for_loop_exp(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)
        #tf.print(x)

        for agent in range(self.nAgents):
            #self.a_learn(x, agent)
            #self.c_learn(x, agent)
            getattr(self,"alearn"+str(i))(x, self.list_of_actors[i], self.list_of_a_opts[i])
            getattr(self,"clearn"+str(i))(x, self.list_of_critics[i], self.list_of_c_opts[i])

    @tf.function
    def learn_for_loop_exp_man(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)
        #tf.print(x)

        self.alearn0(x, self.list_of_actors[0], self.list_of_a_opts[0])
        self.alearn1(x, self.list_of_actors[1], self.list_of_a_opts[1])
        self.alearn2(x, self.list_of_actors[2], self.list_of_a_opts[2])
        self.alearn3(x, self.list_of_actors[3], self.list_of_a_opts[3])
        self.alearn4(x, self.list_of_actors[4], self.list_of_a_opts[4])
        self.alearn5(x, self.list_of_actors[5], self.list_of_a_opts[5])
        self.alearn6(x, self.list_of_actors[6], self.list_of_a_opts[6])
        self.alearn7(x, self.list_of_actors[7], self.list_of_a_opts[7])
        self.alearn8(x, self.list_of_actors[8], self.list_of_a_opts[8])
        self.alearn9(x, self.list_of_actors[9], self.list_of_a_opts[9])
        self.clearn0(x, self.list_of_critics[0], self.list_of_c_opts[0])
        self.clearn1(x, self.list_of_critics[1], self.list_of_c_opts[1])
        self.clearn2(x, self.list_of_critics[2], self.list_of_c_opts[2])
        self.clearn3(x, self.list_of_critics[3], self.list_of_c_opts[3])
        self.clearn4(x, self.list_of_critics[4], self.list_of_c_opts[4])
        self.clearn5(x, self.list_of_critics[5], self.list_of_c_opts[5])
        self.clearn6(x, self.list_of_critics[6], self.list_of_c_opts[6])
        self.clearn7(x, self.list_of_critics[7], self.list_of_c_opts[7])
        self.clearn8(x, self.list_of_critics[8], self.list_of_c_opts[8])
        self.clearn9(x, self.list_of_critics[9], self.list_of_c_opts[9])

    @tf.function
    def learn_tf_loop(self):

        def body(i,x):
            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[i](x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, self.list_of_actors[i].trainable_variables)
            self.list_of_optimizers[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))

    @tf.function
    def learn_tf_loop_exp(self):

        def body(i,x):
            self.a_learn(x,i)
            self.c_learn(x,i)

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))

    @tf.function
    def learn_tf_loop_exp_man(self):

        def body(i,x):
            self.a_learn(x,i)

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))

    @tf.function
    def a_learn(self, x, agent):

        with tf.GradientTape() as g:

            y_hat = self.list_of_actors[agent](x)
            loss = y_hat - tf.constant([0.,0])
            #print("actor {}: {}".format(agent, y_hat))
            #print("loss: ", loss)

        grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
        self.list_of_a_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

    @tf.function
    def c_learn(self, x, agent):

        with tf.GradientTape() as g:

            y_hat = self.list_of_critics[agent](x)
            loss = y_hat - tf.constant([0.,0])
            #print("critic {}: {}".format(agent, y_hat))
            #print("loss: ", loss)

        grads = g.gradient(loss, self.list_of_critics[agent].trainable_variables)
        self.list_of_c_opts[agent].apply_gradients(zip(grads, self.list_of_critics[agent].trainable_variables))

    def get_apply_grad_fn(self):

        @tf.function
        def apply_grad(X, model, optimizer):

            with tf.GradientTape() as t:

                output = model(X)

                loss = tf.constant([0.,0]) - output

            grads = t.gradient(loss, model.trainable_variables)

            optimizer.apply_gradients(zip(grads, model.trainable_variables))

            return loss
        return apply_grad

'''

nAgents = 10
nSteps = 1000

### keras
keras_m = list()
for agent in range(nAgents):
    keras_m.append(tf.keras.metrics.Sum("test"+str(agent), dtype=tf.float32))

### numpy
np_m = np.zeros((nSteps, nAgents))

### tracking
time_tracker = np.zeros((2,2))
results = np.zeros((2,nAgents))

for i in range(nSteps):
    list_of_rewards = np.random.random(nAgents).tolist()

    tic = time.time()
    for agent in range(nAgents):
        keras_m[agent](list_of_rewards[agent])
    time_tracker[0][0] += time.time()-tic

    tic = time.time()
    np_m[i] = list_of_rewards
    time_tracker[1][0] += time.time()-tic

tic = time.time()
for agent in range(nAgents):
    results[0][agent] = keras_m[agent].result()
time_tracker[0][1] += time.time()-tic

tic = time.time()
results[1] = np.sum(np_m,axis = 0)
time_tracker[1][1] += time.time()-tic

np.set_printoptions(suppress=True)
print(time_tracker)
print(results[1]-results[0])

#####

@tf.function
def choose_action(i):
    eq_logits = tf.ones((1,20))
    action = tf.random.categorical(eq_logits, 1)
    tf.print(action)
    return action

@tf.function
def call_choose():
    for i in range(10):
        choose_action(i)

call_choose() # 10 times: 11
call_choose() # 10 times: 3

####

class testerclass():
    def __init__(self):
        self.int = 5

    def test1(self, a):
        print("test1", a)

    def test2(self, b):
        print("test2", b)

    def evaltest(self):
        for i in range(1,3):
            eval("self.test" + str(i) + "(self.int)")

    def localstest(self):
        for i in range(1,3):
            locals()["test"+str(i)](self.int)

    def getattrtest(self):
        for i in range(1,3):
            getattr(self, "test" + str(i))(self.int)

    def sctest(self):
        tf.switch_case(tf.constant(1),[self.test1(4), self.test2(5)])

asdf = testerclass()
asdf.evaltest()
asdf.getattrtest()
#asdf.sctest()

class testmodel(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(20)
        self.l2 = tf.keras.layers.Dense(20)
        self.l3 = tf.keras.layers.Dense(2, activation = "softmax")

    def call(self, x):
        y = self.l1(x)
        y = self.l2(y)
        y = self.l3(y)
        return y

class MARL():
    def __init__(self, nAgents, input_shape):
        self.nAgents = nAgents
        self.list_of_actors = list()
        self.list_of_critics = list()
        self.list_of_a_opts = list()
        self.list_of_c_opts = list()
        for agent in range(nAgents):
            self.list_of_actors.append(testmodel())
            self.list_of_critics.append(testmodel())
            self.list_of_a_opts.append(tf.keras.optimizers.Adam(learning_rate = 0.001))
            self.list_of_c_opts.append(tf.keras.optimizers.Adam(learning_rate = 0.001))
            self.list_of_actors[agent].build(input_shape = input_shape)
            self.list_of_critics[agent].build(input_shape = input_shape)


        self.alearn0 = self.a_learn_wrapper()
        self.alearn1 = self.a_learn_wrapper()
        self.alearn2 = self.a_learn_wrapper()
        self.alearn3 = self.a_learn_wrapper()
        self.alearn4 = self.a_learn_wrapper()
        self.alearn5 = self.a_learn_wrapper()
        self.alearn6 = self.a_learn_wrapper()
        self.alearn7 = self.a_learn_wrapper()
        self.alearn8 = self.a_learn_wrapper()
        self.alearn9 = self.a_learn_wrapper()
        self.clearn0 = self.c_learn_wrapper()
        self.clearn1 = self.c_learn_wrapper()
        self.clearn2 = self.c_learn_wrapper()
        self.clearn3 = self.c_learn_wrapper()
        self.clearn4 = self.c_learn_wrapper()
        self.clearn5 = self.c_learn_wrapper()
        self.clearn6 = self.c_learn_wrapper()
        self.clearn7 = self.c_learn_wrapper()
        self.clearn8 = self.c_learn_wrapper()
        self.clearn9 = self.c_learn_wrapper()

        '''self.alearn = dict()
        self.clearn = dict()

        for i in range(nAgents):
            self.alearn[i] = self.a_learn_wrapper()
            self.clearn[i] = self.c_learn_wrapper()'''

    @tf.function
    def learn_for_loop(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        for agent in range(self.nAgents):
            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[agent](x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
            self.list_of_a_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

    @tf.function
    def learn_for_loop_exp(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)
        #tf.print(x)

        for agent in range(self.nAgents):
            #getattr(self,"alearn"+str(i))(x, self.list_of_actors[i], self.list_of_a_opts[i])
            #getattr(self,"clearn"+str(i))(x, self.list_of_critics[i], self.list_of_c_opts[i])
            getattr(self,"alearn"+str(i))(self, self.list_of_actors[i], self.list_of_a_opts[i], x)
            getattr(self,"clearn"+str(i))(self, self.list_of_critics[i], self.list_of_c_opts[i], x)
            #getattr(self,"alearn")[i](self, self.list_of_actors[i], self.list_of_a_opts[i], x)
            #getattr(self,"clearn")[i](self, self.list_of_critics[i], self.list_of_c_opts[i],x)

    @tf.function
    def learn_tf_loop(self):

        def body(i,x):
            self.alearn[i.ref()](self.list_of_actors[i], self.list_of_a_opts[i], x)

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))

    @tf.function
    def a_learn(self, x, agent):

        with tf.GradientTape() as g:

            y_hat = self.list_of_actors[agent](x)
            loss = y_hat - tf.constant([0.,0])
            #print("actor {}: {}".format(agent, y_hat))
            #print("loss: ", loss)

        grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
        self.list_of_a_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

    @tf.function
    def c_learn(self, x, agent):

        with tf.GradientTape() as g:

            y_hat = self.list_of_critics[agent](x)
            loss = y_hat - tf.constant([0.,0])
            #print("critic {}: {}".format(agent, y_hat))
            #print("loss: ", loss)

        grads = g.gradient(loss, self.list_of_critics[agent].trainable_variables)
        self.list_of_c_opts[agent].apply_gradients(zip(grads, self.list_of_critics[agent].trainable_variables))


    def a_learn_wrapper(self):
        @tf.function
        def a_learn_wrapped(self, actor, opt, x):

            with tf.GradientTape() as g:

                y_hat = actor(x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, actor.trainable_variables)
            opt.apply_gradients(zip(grads, actor.trainable_variables))
        return a_learn_wrapped

    def c_learn_wrapper(self):
        @tf.function
        def c_learn_wrapped(self, critic, opt, x):

            with tf.GradientTape() as g:

                y_hat = critic(x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, critic.trainable_variables)
            opt.apply_gradients(zip(grads, critic.trainable_variables))

        return c_learn_wrapped

    def get_apply_grad_fn(self):

        @tf.function
        def apply_grad(X, model, optimizer):

            with tf.GradientTape() as t:

                output = model(X)

                loss = tf.constant([0.,0]) - output

            grads = t.gradient(loss, model.trainable_variables)

            optimizer.apply_gradients(zip(grads, model.trainable_variables))

            return loss
        return apply_grad

########

test_instance = MARL(5, (1,20))

tic = time.time()
for i in range(1):
    test_instance.learn_for_loop_exp()
    if i % 10 == 0:
        print(time.time() - tic)

# 100 agents
# 7.579921722412109 mit schleife
# 7.8973069190979 manuell

# 500 agents
#

######

@tf.function
def costly_expression_easyToFindLabel(x,y):
    z = tf.matmul(x,y)

@tf.function
def for_loop(z, lista, dicta):
    for i in tf.range(z):
        x = tf.random.uniform((2000,2000))
        y = tf.random.uniform((2000,2000))
        costly_expression_easyToFindLabel(x,y)
        qqq = lista[i]
        tf.print(qqq)
        tf.print(dicta[qqq])

z = 10
lista = tf.constant([str(x) for x in range(z)])
dicta = dict()
#for x in range(z):
#    dicta[str(x)] = str(x)+"q"
tic = time.time()
for_loop(z, lista, dicta)
print(time.time() - tic)

######

test_instance = MARL(100, (1,20))

tic = time.time()
for i in range(100):
    test_instance.learn_tf_loop()
    if i % 10 == 0:
        print(time.time() - tic)

test_instance = MARL(2, (1,20))

tic = time.time()
for _ in range(100):
    test_instance.learn_for_loop()
print(time.time() - tic)
# without @tf.function: ~ 7s
# with @tf.function: ~ 3.5s

tic = time.time()
for _ in range(100):
    test_instance.learn_for_loop_exp()
print(time.time() - tic)

tic = time.time()
for _ in range(100):
    test_instance.learn_tf_loop_exp()
print(time.time() - tic)

tic = time.time()
for _ in range(100):
    test_instance.learn_tf_loop()
print(time.time() - tic)
# without @tf.function: ~ 7s
# with @tf.function: super problematic


### stackoverflow ###

@tf.function
def learn_for_loop(self):
    x = np.random.random_sample((20,)).tolist()
    x = tf.expand_dims(x, 0)

    for agent in range(wrapper.nAgents):
        with tf.GradientTape() as g:

            y_hat = self.list_of_actors[agent](x)
            loss = y_hat - tf.constant([0.,0])

        grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
        self.list_of_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

@tf.function
def learn_tf_loop(self):

    def body(i,x):
        with tf.GradientTape() as g:

            y_hat = self.list_of_actors[i](x)
            loss = y_hat - tf.constant([0.,0])

        grads = g.gradient(loss, self.list_of_actors[i].trainable_variables)
        self.list_of_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

        return (tf.add(i,1),x)

    def condition(i,x):
        return tf.less(i,self.nAgents)

    i = tf.constant(0)
    x = np.random.random_sample((20,)).tolist()
    x = tf.expand_dims(x, 0)

    r = tf.while_loop(condition, body, (i,x))

######

import time

#@tf.function
def learn_tf_loop(wrapper):


    def body(i,x):
        #actor = list_of_actors[i]

        # EACH ACTOR CAN WORK WITH A DIFFERENT SET OF VARIABLES
        '''x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)
        listx = tf.squeeze(x).numpy().tolist()
        del listx[0]
        listx.append(0.05)
        x = tf.expand_dims(listx, 0)'''

        # WE CAN CHANGE X
        #asdf = np.random.random_sample((10,)).tolist()
        #x = tf.expand_dims(asdf, 0)

        with tf.GradientTape() as g:

            y_hat = wrapper.list_of_actors[i](x)
            loss = y_hat - tf.constant([0.,0])
            #print(loss)

        grads = g.gradient(loss, wrapper.list_of_actors[i].trainable_variables)
        #tf.keras.optimizers.Adam(learning_rate = 0.1).apply_gradients(zip(grads, wrapper.list_of_actors[i].trainable_variables))
        wrapper.list_of_opts[agent].apply_gradients(zip(grads, wrapper.list_of_actors[agent].trainable_variables))

        return (tf.add(i,1),x)

    def condition(i,x):
        return tf.less(i,wrapper.nAgents)

    i = tf.constant(0)
    x = np.random.random_sample((20,)).tolist()
    x = tf.expand_dims(x, 0)


    r = tf.while_loop(condition, body, (i,x))



tic = time.time()
learn_tf_loop(wrapper)
print(time.time() - tic)

#######

x = tf.constant(0)
refs = {x.ref()}
dicta = {x.ref(): 45}
y = tf.constant(0)
dicta[y.ref()]

#@tf.function
def call_dict(aDict):
    y = tf.constant(0)
    print(aDict[y.ref()])

#call_dict(aDict)

import tensorflow as tf

class testmodel(tf.keras.Model):
    def __init__(self, activation):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(20)
        self.l2 = tf.keras.layers.Dense(20)
        self.l3 = tf.keras.layers.Dense(2, activation = activation)

    #@tf.function
    def call(self, x):
        #print(tf.shape(x))
        y = self.l1(x)
        #print(tf.shape(y))
        y = self.l2(y)
        #print(tf.shape(y))
        y = self.l3(y)
        return y


#list_of_x = [tf.expand_dims([1.,2.], 0),tf.expand_dims([2.,3.], 0),tf.expand_dims([3.,2.], 0),tf.expand_dims([1.,3.], 0)]
class A():
    def __init__(self):
        self.nAgents = 100
        self.list_of_actors = list()
        self.list_of_opts = list()

        for agent in range(self.nAgents):
            self.list_of_actors.append(testmodel("softmax"))
            self.list_of_opts.append(tf.keras.optimizers.Adam(learning_rate = 0.1))
            self.list_of_actors[agent].build(input_shape = (1,20))


    @tf.function
    def learn_for_loop(self):
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        #tic = time.time()
        for agent in range(wrapper.nAgents):
            #actor = list_of_actors[agent]
            #x = tf.expand_dims(asdf, 0)

            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[agent](x)
                #print(y_hat)
                loss = y_hat - tf.constant([0.,0])
                #print(loss)

            #tf.print(agent)
            grads = g.gradient(loss, self.list_of_actors[agent].trainable_variables)
            #tf.keras.optimizers.Adam(learning_rate = 0.1).apply_gradients(zip(grads, wrapper.list_of_actors[agent].trainable_variables))
            self.list_of_opts[agent].apply_gradients(zip(grads, self.list_of_actors[agent].trainable_variables))

        #tf.print(time.time() - tic)

    @tf.function
    def learn_tf_loop(self):

        def body(i,x):
            with tf.GradientTape() as g:

                y_hat = self.list_of_actors[i](x)
                loss = y_hat - tf.constant([0.,0])

            grads = g.gradient(loss, self.list_of_actors[i].trainable_variables)
            self.list_of_opts[i].apply_gradients(zip(grads, self.list_of_actors[i].trainable_variables))

            return (tf.add(i,1),x)

        def condition(i,x):
            return tf.less(i,self.nAgents)

        i = tf.constant(0)
        x = np.random.random_sample((20,)).tolist()
        x = tf.expand_dims(x, 0)

        r = tf.while_loop(condition, body, (i,x))


#y_true = 0
wrapper = A()

########

import time

toc = time.time()
for _ in range(10):
    wrapper.learn_for_loop()
print(time.time() - toc)

toc = time.time()
for _ in range(10):
    wrapper.learn_tf_loop()
print(time.time() - toc)

######

import matplotlib.pyplot as plt

len_of_inputs = list()
acc = list()

env.pick_data_randomly()
for i in range(8640):
    env.current_time = i * 10
    #print(env.current_time)
    len_of_inputs.append(len(env._incoming_requests()))
    acc.append(sum(len_of_inputs))

plt.figure(figsize=(40,20))
plt.plot(range(8640),acc)

######
class A:
    def __init__(self, a):
        self.a = a
    def __call__(self, c):
        time.sleep(1)
        print("calling", time.time(),self.a, c)
        return 0

class B:
    def __init__(self):
        self.loi = list_of_instances = [A(i) for i in range(3)]
        print("created loi", time.time(),self.loi)

    def make_use_item_function(self, i):
        obj = self.loi[i]
        def use_item():
            y = obj("test") # do_calculations with obj
            return y
        return use_item
        #return obj("test")

wrapper = B()


@tf.function
def extract():
    list_of_callables = [wrapper.make_use_item_function(i) for i in range(3)]
    print("created loc", time.time(),list_of_callables)
    time.sleep(1)
    idx = tf.constant(2)
    return tf.switch_case(idx, list_of_callables)

extract()
print(time.time())

######

#env.reward_dict = {"selection": 1/3, "pickup": 1/3, "dropoff": 1/3}
class temp():
    def __init__(self, a):
        self.a = a
    def __call__(self):
        print("called ", self.a)

@tf.function
def look_up(lista):
    input = "blub"
    #lista = {0: temp(1).call_object(), 1: temp(2).call_object(), 2: temp(3).call_object()}
    #print(lista)
    a = tf.constant(1)
    return tf.switch_case(a,lista)
    #return tf.gather(lista,a)
    #for b in tf.range(a):
    #    print(b, tf.gather(lista,b))

lista = [temp(1), temp(2), temp(3)]
x = look_up(lista)

'''class an_object():
    def __init__(self, a):
        self.a = a
    def __call__(self):
        print("called ", self.a)

@tf.function
def a_function(a_list):
    #idx = tf.constant(1)
    idx = 1
    return a_list[idx]
    #return tf.switch_case(idx,a_list)

a_list = [an_object(1), an_object(2), an_object(3)]
x = a_function(a_list)'''

######

def actor_loss(action_probs, action, all_qs):
        '''
        depth = action_probs.get_shape().as_list()[-1]
        action_onehot = tf.one_hot(tf.cast(action, tf.int32), depth)
        loss = tf.nn.softmax_cross_entropy_with_logits(labels = action_onehot, logits = action_probs)
        '''
        log_probs = tf.math.log(action_probs)
        depth = action_probs.get_shape().as_list()[-1]
        action_onehot = tf.one_hot(tf.cast(action, tf.int32), depth)
        print("action_probs: ", action_probs)
        print("log_probs:    ", log_probs)

        entropy = - tf.reduce_sum(log_probs * action_probs)
        print("entropy:      ", entropy)

        loss1 = - tf.reduce_sum(action_probs * log_probs * all_qs)
        print("all_actions=1, entropy=0: ", loss1)

        loss2 = - tf.reduce_sum(action_onehot * log_probs * all_qs)
        print("all_actions=0, entropy=0: ", loss2)

        print("all_actions=0, entropy=1: ", loss1 - entropy)
        print("all_actions=1, entropy=1: ", loss2 - entropy)

def postprocess(epsilon, output):
    #print(output, tf.shape(output)[0])
    logits = (tf.constant(1.) - epsilon) * output + epsilon / tf.cast(tf.shape(output)[0], dtype = tf.float32)
    return logits

all_actions = 1
action_probs = tf.constant([0.,1.,0.,0.])
action_probs = postprocess(tf.constant(0.5),action_probs)
action = 1
all_qs = tf.constant([0.,0.2,1.5,0.])

actor_loss(action_probs, action, all_qs)

#####


path = path = "../../LRZ Sync+Share/Mobile Uploads/Uni/WiSe2021/Masterarbeit/Abgabe/Results/what_didnt_work/results/100/"
lof = os.listdir(path)
print(lof)
result = analyzer.get_test_avg_arr("test-action_distribution", path,lof[0])
result = result/1000/8640

def get_color_list():
        n1 = range(19)
        n2 = range(100)
        n3 = range(1)
        c = list()
        for i in n1:
            c.append("red")
        for i in n2:
            c.append("green")
        c.append("blue")

        return c

def plot_ad(result_merged):
        fig,axs = plt.subplots(figsize = (5,3))
        columns = ["SPAC"]
        rows = ["r0.5"]
        font = {'fontname':'Arial'}
        for i in range(len(rows)):
            for j in range(len(columns)):
                if i == 0:
                    #axs[i,j].set_title(columns[j], **font,pad=20)
                    axs.set_title(columns[j], **font,pad=20)
                if j == 0:
                    #axs[i,j].set_ylabel(rows[i], rotation=0, size='large', **font, labelpad=60)
                    axs.set_ylabel(rows[i], rotation=0, size='large', **font, labelpad=20)

                c = get_color_list()
                #axs[i,j].bar(range(len(result_merged.iloc[i,j])),result_merged.iloc[i,j], color = c)
                axs.bar(range(len(result_merged)),result_merged, color = c)


        fig.tight_layout()
        now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        plt.savefig("figures/" + now + "_ad_pretty.jpg")
        plt.show()
plot_ad(result)
