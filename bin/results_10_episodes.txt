-------- starting epoch 0 --------
-------- starting episode 0 --------
chosen day:  2019-10-01
borough filter:  0.06804537773132324
Epoch 0 -- Day 0 -- Setting centralized_critic
aLoss: -11153602.0
cLoss: 10450.1376953125
Reward: -4.966975688934326

Total Elapsed Time:  674.6168
-- of which pickle :  2.6177
Step time :  0.0761
-- of which env.step :  0.0054
-- of which fwd :  0.004
-- of which learning :  0.0609


{'missed': 14350, 0: [9, 0, 0], 1: [4, 0, 0]}


-------- starting episode 1 --------
chosen day:  2019-12-03
borough filter:  0.026154518127441406
Epoch 0 -- Day 1 -- Setting centralized_critic
aLoss: -68845640.0
cLoss: 1700.048583984375
Reward: -4.789989471435547

Total Elapsed Time:  642.6682
-- of which pickle :  2.7408
Step time :  0.0726
-- of which env.step :  0.0036
-- of which fwd :  0.0039
-- of which learning :  0.061


{'missed': 14346, 0: [3, 0, 0], 1: [8, 0, 0]}


-------- starting episode 2 --------
chosen day:  2018-03-27
borough filter:  0.0804295539855957
Epoch 0 -- Day 2 -- Setting centralized_critic
aLoss: -88393208.0
cLoss: 829.16015625
Reward: -5.321004390716553

Total Elapsed Time:  685.2747
-- of which pickle :  3.1497
Step time :  0.0775
-- of which env.step :  0.0045
-- of which fwd :  0.0038
-- of which learning :  0.0612


{'missed': 14350, 0: [5, 0, 0], 1: [8, 0, 0]}


-------- starting episode 3 --------
chosen day:  2019-07-10
borough filter:  0.07932615280151367
Epoch 0 -- Day 3 -- Setting centralized_critic
aLoss: 10646047.0
cLoss: 646.3350219726562
Reward: -3.003002882003784

Total Elapsed Time:  679.5312
-- of which pickle :  3.0368
Step time :  0.0769
-- of which env.step :  0.0049
-- of which fwd :  0.0038
-- of which learning :  0.061


{'missed': 14350, 0: [0, 0, 0], 1: [7, 0, 0]}


-------- starting episode 4 --------
chosen day:  2018-04-24
borough filter:  0.07925152778625488
Epoch 0 -- Day 4 -- Setting centralized_critic
aLoss: 12454049.0
cLoss: 383.2064208984375
Reward: -3.7220029830932617

Total Elapsed Time:  683.893
-- of which pickle :  2.9125
Step time :  0.0774
-- of which env.step :  0.0053
-- of which fwd :  0.0038
-- of which learning :  0.061


{'missed': 14350, 0: [1, 0, 0], 1: [9, 0, 0]}


-------- starting episode 5 --------
chosen day:  2019-05-01
borough filter:  0.025488853454589844
Epoch 0 -- Day 5 -- Setting centralized_critic
aLoss: 1675483.0
cLoss: 263.7538757324219
Reward: -2.43300199508667

Total Elapsed Time:  643.3094
-- of which pickle :  2.3527
Step time :  0.0727
-- of which env.step :  0.0047
-- of which fwd :  0.0038
-- of which learning :  0.0611


{'missed': 14350, 0: [5, 0, 0], 1: [2, 0, 0]}


-------- starting episode 6 --------
chosen day:  2018-12-28
borough filter:  0.07190871238708496
Epoch 0 -- Day 6 -- Setting centralized_critic
aLoss: -611296.0625
cLoss: 224.64170837402344
Reward: -3.4710030555725098

Total Elapsed Time:  682.9438
-- of which pickle :  2.3724
Step time :  0.0773
-- of which env.step :  0.0048
-- of which fwd :  0.0038
-- of which learning :  0.0616


{'missed': 14350, 0: [5, 0, 0], 1: [4, 0, 0]}


-------- starting episode 7 --------
chosen day:  2018-10-26
borough filter:  0.11467480659484863
Epoch 0 -- Day 7 -- Setting centralized_critic
aLoss: -1969690.75
cLoss: 188.1167755126953
Reward: -1.8839975595474243

Total Elapsed Time:  701.607
-- of which pickle :  2.7913
Step time :  0.0794
-- of which env.step :  0.0052
-- of which fwd :  0.0038
-- of which learning :  0.0618


{'missed': 14350, 0: [2, 0, 0], 1: [3, 0, 0]}


-------- starting episode 8 --------
chosen day:  2018-05-25
borough filter:  0.07657790184020996
Epoch 0 -- Day 8 -- Setting centralized_critic
aLoss: -10155921.0
cLoss: 131.7831268310547
Reward: -4.718995094299316

Total Elapsed Time:  686.8723
-- of which pickle :  2.8249
Step time :  0.0777
-- of which env.step :  0.0053
-- of which fwd :  0.0038
-- of which learning :  0.0616


{'missed': 14350, 0: [7, 0, 0], 1: [5, 0, 0]}


-------- starting episode 9 --------
chosen day:  2019-05-17
borough filter:  0.02529764175415039
Epoch 0 -- Day 9 -- Setting centralized_critic
aLoss: -17110442.0
cLoss: 108.93189239501953
Reward: -5.6320061683654785

Total Elapsed Time:  650.3958
-- of which pickle :  2.4153
Step time :  0.0735
-- of which env.step :  0.005
-- of which fwd :  0.0038
-- of which learning :  0.0617


{'missed': 14350, 0: [4, 0, 0], 1: [10, 0, 0]}


Finished epoch after 6731.2128s
-------- starting epoch 0 --------
-------- starting episode 0 --------
chosen day:  2019-09-09
borough filter:  0.06824874877929688
Epoch 0 -- Day 0 -- Setting nh
Reward: 1221.5499267578125

Total Elapsed Time:  111.259
-- of which pickle :  2.3015
Step time :  0.0112
-- of which env.step :  0.0071
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14310, 0: [0, 15, 57], 1: [0, 8, 45]}


-------- starting episode 1 --------
chosen day:  2019-11-25
borough filter:  0.019918203353881836
Epoch 0 -- Day 1 -- Setting nh
Reward: 1262.007568359375

Total Elapsed Time:  77.6856
-- of which pickle :  2.6239
Step time :  0.0072
-- of which env.step :  0.0051
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14296, 0: [0, 15, 69], 1: [0, 8, 37]}


-------- starting episode 2 --------
chosen day:  2019-03-01
borough filter:  0.02873063087463379
Epoch 0 -- Day 2 -- Setting nh
Reward: 1234.889892578125

Total Elapsed Time:  79.7389
-- of which pickle :  2.7555
Step time :  0.0075
-- of which env.step :  0.0045
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14319, 0: [0, 14, 73], 1: [0, 3, 28]}


-------- starting episode 3 --------
chosen day:  2019-08-15
borough filter:  0.07060360908508301
Epoch 0 -- Day 3 -- Setting nh
Reward: 1289.26025390625

Total Elapsed Time:  114.0253
-- of which pickle :  2.3101
Step time :  0.0115
-- of which env.step :  0.0053
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14314, 0: [0, 16, 66], 1: [0, 9, 32]}


-------- starting episode 4 --------
chosen day:  2019-09-17
borough filter:  0.07837414741516113
Epoch 0 -- Day 4 -- Setting nh
Reward: 1276.3023681640625

Total Elapsed Time:  116.3003
-- of which pickle :  2.9306
Step time :  0.0117
-- of which env.step :  0.0057
-- of which fwd :  1e-04
-- of which learning :  0.0


{'missed': 14311, 0: [0, 14, 69], 1: [0, 7, 34]}


-------- starting episode 5 --------
chosen day:  2018-11-20
borough filter:  0.07959556579589844
Epoch 0 -- Day 5 -- Setting nh
Reward: 1179.556640625

Total Elapsed Time:  118.3046
-- of which pickle :  3.183
Step time :  0.0119
-- of which env.step :  0.0061
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14313, 0: [0, 19, 68], 1: [0, 5, 32]}


-------- starting episode 6 --------
chosen day:  2018-04-02
borough filter:  0.07144808769226074
Epoch 0 -- Day 6 -- Setting nh
Reward: 1264.46435546875

Total Elapsed Time:  113.7779
-- of which pickle :  2.712
Step time :  0.0114
-- of which env.step :  0.0062
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14308, 0: [0, 15, 68], 1: [0, 6, 40]}


-------- starting episode 7 --------
chosen day:  2019-08-28
borough filter:  0.06596565246582031
Epoch 0 -- Day 7 -- Setting nh
Reward: 1342.130859375

Total Elapsed Time:  111.4027
-- of which pickle :  2.4661
Step time :  0.0112
-- of which env.step :  0.0063
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14297, 0: [0, 21, 77], 1: [0, 5, 37]}


-------- starting episode 8 --------
chosen day:  2018-09-27
borough filter:  0.07801532745361328
Epoch 0 -- Day 8 -- Setting nh
Reward: 1231.9752197265625

Total Elapsed Time:  117.6781
-- of which pickle :  2.581
Step time :  0.0119
-- of which env.step :  0.0065
-- of which fwd :  0.0002
-- of which learning :  0.0


{'missed': 14317, 0: [0, 16, 59], 1: [0, 7, 32]}


-------- starting episode 9 --------
chosen day:  2018-01-09
borough filter:  0.07374072074890137
Epoch 0 -- Day 9 -- Setting nh
Reward: 1230.78173828125

Total Elapsed Time:  115.7478
-- of which pickle :  2.6326
Step time :  0.0116
-- of which env.step :  0.0066
-- of which fwd :  1e-04
-- of which learning :  0.0


{'missed': 14316, 0: [0, 19, 63], 1: [0, 9, 36]}


Finished epoch after 1076.0265s