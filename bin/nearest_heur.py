#import numpy as np
#import environment.env as ec
#import utils.data_preprocessing as dp


class nearestHeur():

    # assigns each request to the nearest agent
    # will not rebalance

    def __init__(self, env):
        self.env = env
        self.data = env.data

    def choose_actions(self, state):
        time = state[0]
        rq = state[1]
        loc = state[2]
        aq = state[3]
        actions = [self.env.no_op for i in range(self.env.nAgents)]

        # check if elements in rq
        if rq:

            dict_ttj = dict()

            # for each request
            for request in rq:

                # skip this request if time to origin has already been looked at
                if request[0] in dict_ttj.keys():
                    continue
                else:
                    dict_ttj[request[0]] = list()

                    # calculate agent who is closest
                    for agent in range(self.env.nAgents):

                        time_to_job = 0

                        # add time already in queue
                        for action in range(len(aq[agent])):
                            time_to_job += aq[agent][action][2]

                        # add time from last destination to new origin
                        if len(aq[agent]) != 0:
                            last_d = aq[agent][-1][1]
                        else:
                            last_d = loc[agent][1]
                        new_o = request[0]
                        #o = self.data.boroughs.index(last_d)
                        #d = self.data.boroughs.index(new_o)
                        o = last_d
                        d = new_o
                        time_to_job += self.data.travel_stats[o, d, 0]

                        # save ttj per agent
                        dict_ttj[request[0]].append(time_to_job)

            # choose agent to assign request to
            av_agents = list(range(self.env.nAgents))
            unav_agents = list()

            counter = 0
            for request in rq:
                if av_agents:
                    all_agents = dict_ttj[request[0]] # list of ttj per agent

                    cc = 0
                    new_all = list()
                    for temp in all_agents:
                        if cc in av_agents: # only appen agents' time if they are still available
                            new_all.append(temp)
                        cc += 1
                    #all = new_all


                    min_ttj = min(new_all)

                    if min_ttj <= request[4]: # only necessary if job can be reached
                        index = all_agents.index(min_ttj)
                        init_index = index
                        
                        counter3 = 0
                        while index in unav_agents:   
                        #if index in unav_agents:
                            index2 = all_agents[index+1:].index(min_ttj)
                            index += index2 + 1
                            counter3 += 1
                        #if index != init_index:   
                            #print("truly all: ", av_agents, all_agents)
                            #print("after removing unav: ", unav_agents, new_all)
                            #print("found free agent after %s iterations. Initial agent: %s, now: %s. " %(counter3, index, index2))

                        # each agent can only take one job
                        '''addition = 0
                        for n in unav_agents:
                            if n <= index:
                                addition += 1
                        print(index, av_agents, unav_agents)
                        index += addition
                        print(index)'''
                        #print(index)
                        av_agents.remove(index)
                        unav_agents.append(index)
                        actions[index] = self.data.nBoroughs + counter
                        counter += 1

                    else: # no agent can reach the request before a timeout occurs
                        counter += 1
                        #self.env.request_queue.remove(request)
                        continue
                else: # all agents have already selected a job
                    continue

        return actions






def run_nh(env, nSteps):
    #seed = env.seed
    #np.random.seed(seed)
    
    nh = nearestHeur(env)

    s = env.reset()
    
    track_r = []

    while s[0] < nSteps: #done::
        
        a = nh.choose_actions(s)

        s_, r, done = env.step(a)

        track_r.append(r[0])

        s = s_


    print("sum of rewards: ", sum(track_r))
            
