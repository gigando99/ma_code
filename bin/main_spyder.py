# this will replace my MASTER NOTEBOOK for the tf environment

# %% IMPORTING

import common.data_downloading as dd
import common.data_preprocessing as dp
#import common.env as envi
import common.utils as utils
#import nearest_heur as nh
import a2c_rework as a2c

import tensorflow as tf
import numpy as np
import sys
import time
import matplotlib.pyplot as plt
import pandas as pd
import datetime as datetime



# Step by step:
# %% Create downloading object and check if csvs are downloaded

#downloader = dd.dataDownloader()
#downloader.download_taxi_data()

# %% Create data object and check if pickles files exist

#data = dp.inputData(downloader, verbose = False)
#data.check_or_create_pickles()

#%%

#data.stats_dict = {'total_length': [187203269, 14851353, 302850955, 234629119],
# 'only_manhattan': [157382220, 4358698, 81652032, 74024891],
# 'wo_in-zone': [147915831, 3758167, 79286024, 71588309],
# 'cleaned': [147504131, 3750119, 79138401, 71475248]}

# %% Create environment object and check for boroughs

#env = envi.AmodEnv(data, nAgents = 2, nBoroughs = 2)
#print(env.data.boroughs)


# all in one function:
# %% getting ready

def hyperparameter_settings():
        # potential hyperparameters:
        # init of env:
        ### nAgents, nBoroughs, stepInterval, rq_limit, patience
        ### reward setting (+ split ratio)
        ### not: batch size, lambda in td(lambda)
        # init of trainer:
        ### MA setting
        ### lr_a, lr_c # prio
        ### nNodes (64,128), nLayers (2,3,4), activation (each for actor and critic respectively)
        pass

env = utils.prep_pipeline(nBoroughs = 63, nAgents = 2, rq_limit = 50)
#env.pick_data()



# %%
    
def hyperparameter_tuning():
    
    env_kwargs = {
            "nAgents": 6,
            "nBoroughs": 63,
            "step_interval": 10,
            "rq_limit": 50,
            "reward_param": 0,
            "patience": 300,
            "gas_cost": - 0.0003
            }
    
    trainer_kwargs = {
            "lr_a": 0.001,
            "lr_c": 0.0005,
            "nNodes": 64,
            "nLayers": 2,
            "activation": "relu",
            "setting": "iac",
            "all_actions": False,
            }
    
    general_kwargs = {
            "seed": 4,
            "verbosity": 0,
            }
    
    running_kwargs = {
            "nSteps": 86400,
            "nEpisodes": 1,
            "nEpochs": 1,
            "mode": "train",
            }  

    
    trainer = utils.prep_pipeline_final(**env_kwargs, **trainer_kwargs, **general_kwargs)
    trainer.run(**running_kwargs)
    
  

def test_prep_pipeline(nAgents = 2, nBoroughs = 63, step_interval = 10, rq_limit = 50, reward_param = 0, patience = 180,
                  lr_a = 0.0001, lr_c = 0.0005, nNodes = 64, nLayers = 2, activation = "relu",
                  seed = 1, verbosity = 1, **kwargs):
    print(nAgents, lr_a, seed)
    #print(gen_kwargs)
    
    

def test_kwargs(lr_a = 0.001, lr_c = None, **kwargs):
    print(lr_a)
    print(lr_c)
    print(kwargs)
    test_kwargs2(lr_c)
    
def test_kwargs2(lr_c = 0.01):
    print(lr_c)

#test_kwargs(**env_kwargs, **trainer_kwargs)

#test_prep_pipeline(**env_kwargs, **trainer_kwargs, **general_kwargs)
hyperparameter_tuning()




# %% unified



nSteps = 1000
nEpochs = 1
nEpisodes = 1
all_actions = False
env.verbose = 0
#mode, setting = "test", "nh"

mode = "train"
setting = "single_agent"
#for setting in ["iac","shared_actions","centralized_critic","exemplary","single_agent"]:
trainer = a2c.unifiedTrainer(env, setting, all_actions = all_actions)
trainer.run(nEpochs, nEpisodes, nSteps, mode = mode)




# %%

env.stats_dict


#%% testing gradienttape

class testmodel(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.l1 = tf.keras.layers.Dense(2)
        self.l2 = tf.keras.layers.Dense(1)
        
    def call(self, x):
        y = self.l1(x)
        y = self.l2(y)
        return y
        
        
'''
model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(2))
model.add(tf.keras.layers.Dense(1))
#model.compile(optimizer='sgd', loss='mse')

model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(2))
model2.add(tf.keras.layers.Dense(1))
#model2.compile(optimizer='sgd', loss='mse')
'''

def modelloss(x,y):
    print("x: ", x,"\ny: ", y)
    return (x-y)**2

def model2loss(x,y):
    return (x - y)**2

def test(g, model, loss):
    #print(g)
    return g.gradient(loss, model.trainable_variables)

def test2(model,x):
    return model(x)

x = tf.expand_dims([1.], 0)
x = tf.stack([x,x,x])
#print(x)
y = [0,0,0]
model = testmodel()
model2 = testmodel()
#y = np.random.randint(0, 2, (2, 2))
#print(x,y)
#model.fit(x, y)
with tf.GradientTape(persistent = True) as g:
    y2 = model2(x)
with tf.GradientTape(persistent = True) as gg:
    #y = model(x)
    y = test2(model, x)
    #y2 = model2(x)
    
#with tf.GradientTape(persistent = True) as g: #, tf.GradientTape() as g2
    
    #print(y,y2)
    loss = modelloss(x,y)
    loss2 = model2loss(x,y2)
#for elem in model.trainable_variables:
    #print(elem)
#grads = g.gradient(loss, model.trainable_variables)
#grads2 = g.gradient(loss2, model2.trainable_variables)

grads = test(gg, model, loss)
grads2 = test(gg, model2, loss2)

print("grads: ", grads)
print("grads2: ", grads2)

del gg
#%%
a = tf.constant([[1.,2.,3.],[4.,5.,6.]])
b = tf.constant([1,2])
depth = a.get_shape().as_list()[-1]
print(a)
print(b)
print(depth)
#c = a * tf.one_hot(b, depth)
b_one_hot = tf.one_hot(b, depth, axis = 0)
#b_one_hot = tf.constant([[], [0.,0.,1.]])
print(b_one_hot)

tf.matmul(a,b_one_hot)

# %%
a = tf.constant([[1, 2, 3, 4, 5, 6],[1, 2, 3, 4, 5, 6]])
b = tf.constant([[1, 0, 0, 1, 0, 0],[0, 0, 1, 0, 0,1]])
#tf.matmul(a,b)
print(a[0],b[0])
#tf.tensordot(a,b,axes = (1,1))[0]
tf.reduce_sum(a * b,-1)



# %%
lista = [1,2,3,4,5,6,7,4]
listb = lista[:]
del listb[3]
print(lista,listb)

# %%
logits = tf.constant([100., 300.])
labels = 1
tf.nn.sparse_softmax_cross_entropy_with_logits(logits = tf.math.log(logits), labels = labels)
#%% 

x = tf.constant([100., 300.])
y = tf.constant([1., 5.])
probs1 = tf.nn.softmax(tf.math.log(x))
probs2 = tf.nn.softmax(x)
probs3 = probs2 + tf.constant([0.000000001])
print(tf.math.log(probs2), tf.math.log(probs3))


#%%
length = len(env.tracking_lists[0])

plt.plot(range(length), env.tracking_lists[0], "g")
plt.plot(range(length), env.tracking_lists[1], "y")
plt.plot(range(length), env.tracking_lists[2], "b")
plt.plot(range(length), env.tracking_lists[3], "r")

plt.show()


# %%
# maybe use this for incoming requests to reduce looping through lists?
lista = [1,2,3,4]
listb = [2,4]
listd = [None] * 5
listc = list(set(lista)-set(listb))
listd[3] = 1

print(listd)

# %%
x = tf.constant([1., 3])
y = tf.constant([2., 5])
z = tf.constant([3., 6, 6])
a = tf.constant([5., 6, 8])
qwer = [x,z]
asdf = [y,a]
temp = [t[0] for t in [qwer,asdf]]

#xyz = tf.stack([x, y, z])
res = list()
for i in range(2):
    x = tf.stack([qwer[i],asdf[i]])
    res.append(tf.reduce_mean(x,0))
res
#tf.reduce_mean(xyz,0)

#%% maybe use zips to speed up exemplary
x = tf.constant([1., 3])
y = tf.constant([2., 5])
z = tf.constant([3., 6, 6])
a = tf.constant([5., 6, 8])
qwer = [x,z]
asdf = [y,a]
temp = [qwer,asdf]

listc = list(zip(*temp))
print(listc, len(listc[0]))
tf.reduce_mean(listc[0],0)



#%%



# %% nearest-heuristic:

def main_nh():

    nSteps = 1000
    
    #old_stdout = utils.log_wrapper_before("nearest_heur", nSteps, env)
    
    nh.run_nh(env, nSteps)
    
    #utils.log_wrapper_after(old_stdout)

main_nh()

# %% iac

nSteps = 1000

#old_stdout = utils.log_wrapper_before("iac", nSteps, env)

aloss, closs, rewards = a2c.run_iac(env, nSteps)

#utils.log_wrapper_after(old_stdout)

# %% single agent

nSteps = 1000

#old_stdout = utils.log_wrapper_before("single_agent", nSteps, env)

a2c.run_single_agent(env, nSteps)

#utils.log_wrapper_after(old_stdout)

# %% 

#sys.stdout = old_stdout

# %%
print(env.no_op, env.nActions)

# %%
'''
from functools import wraps
def timing(f):
        @wraps(f)
        def wrap(*args, **kw):
            ts = time.time()
            result = f(*args, **kw)
            te = time.time()
            print('func:%r took: %2.4f sec' %(f.__name__, te-ts))
            return result
        return wrap
'''
# %%

def test_args_kwargs(arg1, arg2, arg3, arg4=None):
    print("arg1:", arg1)
    print("arg2:", arg2)
    print("arg3:", arg3)
    print("arg4:", arg4)

print("--- first args ---")    
# first with *args
args = ("two", 3, 5,8)
test_args_kwargs(*args)
print("--- now kwargs ---")
# now with **kwargs:
kwargs = {"arg3": 3, "arg2": "two", "arg1": 5, "arg5": 7}
test_args_kwargs(**kwargs)


# %% 
###################################################################
################### STATS #########################################
# object creation

import common.stats as stats

downloader = dd.dataDownloader()
#downloader.download_taxi_data()
preprocessor = dp.inputData(downloader)
preprocessor.check_or_create_pickles()
analyzer = stats.DataAnalyzer(downloader, preprocessor)
preprocessor.stats_dict

# %%


trips_total = analyzer.calc_trips_per_car_in_original_data()
analyzer.calc_adapted_fleet_size(trips_total)




#%%
analyzer.calc_average_trip_duration()

#%%
        
def idx(zone):
    return preprocessor.boroughs.index(zone)

def zone(idx):
    return preprocessor.boroughs[idx]

# %% Analyses

# runs approx. 15 min. shows nice graph
# estimate rq limit for a step interval of 10
#analyzer.estimate_rq_limit(10, ["DO_id != PU_id"])

# trips per day
#analyzer.get_trips_per_day()

#trips per smaller cluster, runs approx 2min
#temp = analyzer.get_trips_per_cluster()
#analyzer.analyze_trips_per_cluster(temp)


# runs fast
analyzer.plot_data_reduction()

# runs approx. 5 min and takes looooooads of memory
#analyzer.get_hourly_cutoff()
