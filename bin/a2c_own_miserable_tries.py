# %% Imports

import utils.data_preprocessing as dp
import matplotlib.pyplot as plt
import utils.data_downloading as dd
import tensorflow as tf
import numpy as np
import environment.env as envi
import nearest_heur as nh

# %% Create downloading object and check if csvs are downloaded

downloader = dd.dataDownloader()
downloader.download_taxi_data()

# %% Create data object and check if pickles files exist

data = dp.inputData(downloader, verbose = False)
data.check_or_create_pickles()

# %% Create environment object and check for boroughs

env = envi.AmodEnv(data, nAgents = 2, nBoroughs = 2)
print(env.data.boroughs)    
    
# %% 
        
def ffnn(input_shape, type, nLayers = 2, nHidden = 64, activation = "relu"):
    
    """
    takes:
        - input_shape (int)
        - type (str): actor or critic
        - nLayers (int)
        - nHidden (int or list)
        - activation (tf)
    """
    
    print("input shape is ", input_shape)
    x_input = tf.keras.Input(shape=input_shape, name=type+"_input")
    h = x_input
    
    if isinstance(nHidden, int):
        nHidden = [nHidden for i in range(nLayers)]
        
    for i in range(nLayers):
        h = tf.keras.layers.Dense(units = nHidden[i], name=type+"_hlayer_"+str(i), activation = activation)(h)
        
    network = tf.keras.Model(inputs = [x_input], outputs = [h])
    
    return network


        
def fc(input_shape, scope, nOutputs, activation = None):
    with tf.name_scope(scope):
        layer = tf.keras.layers.Dense(units = nOutputs, activation = activation)
        layer.build(input_shape)
    return layer


class Actor():
    def __init__(self, input_shape, nLayers, nHidden, nActions, nAgents, lr_a):
        self.network = ffnn(input_shape, "actor", nLayers, nHidden)
        self.opt = tf.keras.optimizers.Adam(learning_rate = lr_a)
        self.act_prob_l = fc(self.network.output_shape, "act_prob_layer", nActions, activation = "softmax")
        self.nAgents = nAgents
        
    def choose_action(self, state):
        
        temp = self.network(state)
        action_probabilities = self.act_prob_l(temp)
        action = tf.random.categorical(action_probabilities, self.nAgents)
        return action_probabilities, action
    
    def loss(self, advantage, action_probabilities):
        action_log_probs = tf.math.log(action_probabilities)
        return -tf.math.reduce_sum(action_log_probs * advantage) #zhou takes reduce_mean
        
    def learn(self, probs, advantage):
        
        with tf.GradientTape() as tape_a:
            al = self.actor.loss(probs, advantage)
        a_grads = tape_a.gradient(al, self.actor.network.trainable_variables)
        self.actor.opt.apply_gradients(zip(a_grads, self.actor.network.trainable_variables))
        
        
class Critic():
    def __init__(self, input_shape, nLayers, nHidden, nActions, lr_c):
        self.network = ffnn(input_shape, "critic", nLayers, nHidden)
        self.opt = tf.keras.optimizers.Adam(learning_rate = lr_c)
        self.vf = fc(self.network.output_shape, "vf_layer", 1)
            
    def get_value(self, state):
        temp = self.network(state)
        value = self.vf(temp)
        return value
    
    def loss(self, advantage):
        return tf.math.square(advantage)
        
    def learn(self, advantage):
        
        with tf.GradientTape() as tape_c:
            cl = self.critic.loss(advantage)
        c_grads = tape_c.gradient(cl, self.critic.network.trainable_variables)
        self.critic.opt.apply_gradients(zip(c_grads, self.critic.network.trainable_variables))
    
    
class A2C():
    def __init__(self, env, lr_a, lr_c, gamma):
        
        self.env = env
        self.actor = Actor((env.state_len, ), 1, 2, self.env.no_op, self.env.nAgents, lr_a)
        self.critic = Critic((env.state_len, ), 1, 2, self.env.no_op, lr_c)
        
        #self.actor.network.compile(optimizer = tf.keras.optimizers.Adam(learning_rate = lr_a)) # define loss as well?
        #self.critic.network.compile(optimizer = tf.keras.optimizers.Adam(learning_rate = lr_c)) # define loss as well?
        
        self.gamma = gamma
        
    def test_a(self, a):
        print(a, type(a))
        print(a.numpy())
        print(np.squeeze(a.numpy()))
        
        return a.numpy()
        
    #@tf.function
    def train(self, s, nSteps):
        print("entered training function")
        
        probs, a = self.actor.choose_action(s)
        print(" #################### ", a, type(a))
        a = self.test_a(a)
        s_, r, done = self.env.step(a)
        print(s_)
        s_ = self.env.transform_state_to_input()
        print(s_)
        advantage = r + self.gamma * self.critic.get_value(s_) - self.critic.get_value(s)
        
        self.actor.learn(probs, advantage)
        self.critic.learn(advantage)
        
        s = s_
            
            # advantage = r + gamma * v(s_) - v(s)
            # critic.learn from this
            # actor.learn from this
            
    def run(self, nSteps):
        #seed = env.seed
        #np.random.seed(seed)
    
        s = self.env.reset()
        
        track_r = []
    
        while True:
            
            a = self.actor.choose_action(s)[1]
    
            s_, r, done = self.env.step(a)
    
            track_r.append(r[0])
    
            s = s_
    
            if s[0] > nSteps: #done:
                print("sum of rewards: ", sum(track_r))
                break

# %% testing
                
class Tester():
    def __init__(self, input_shape, nLayers, nHidden, nActions, lr_a):
        self.network = ffnn(input_shape, "tester", nLayers, nHidden)
        self.opt = tf.keras.optimizers.Adam(learning_rate = lr_a)
        self.act_prob_l = fc(self.network.output_shape, "act_prob_layer", nActions, activation = "softmax")
        
    def choose_action(self, state):
        
        temp = self.network(state)
        action_probabilities = self.act_prob_l(temp)
        action = tf.random.categorical(action_probabilities,1)
        return action_probabilities, action
    
    def loss(self, advantage, action_probabilities):
        action_log_probs = tf.math.log(action_probabilities)
        print(action_log_probs)
        return -tf.math.reduce_sum(action_log_probs * advantage) #zhou takes reduce_mean
    
    @tf.function
    def train(self, s):
        
        with tf.GradientTape() as tape:
            
            probs, a = self.choose_action(s)
            al = self.loss(probs, 4.0)
            print("loss: ", probs, al)
        
        x = tape.watched_variables()
        print("x: ", x)
        grads = tape.gradient(al, x)
        print("grads_tape: ", grads)
    
        self.opt.apply_gradients(zip(grads, self.network.trainable_variables))
    
tobject = Tester(4,2,10,5,0.01)

#%% 
tstate = tf.expand_dims(np.array([1,2,3.0,4]),0)

tobject.train(tstate)
#print(tobject.network.trainable_variables)



# %% init
                
seed = env.seed
tf.random.set_seed(seed)
np.random.seed(seed)
env.pick_data()

test = A2C(env, 0.01, 0.1, 0.99)

# %%
init_state = tf.expand_dims(env.transform_state_to_input(),0)
weird_thingy = test.train(init_state, 50)




# %%
samples = tf.random.categorical(tf.math.log([[0.5, 0.5]]), 5)
samples.numpy()


# %%
weird_thingy














# %% eval
actor.network.compile(loss="mse")
actor.network.trainable_variables
actor.network(np.array([[1,1],[1,1]]))
#actor.compile(loss="mse")
#actor.trainable_variables

# %% more analysis
model = tf.keras.Sequential(
    [
        tf.keras.layers.InputLayer(input_shape=(2,)),
        tf.keras.layers.Dense(2),
    ]
)
model.compile(loss='mse')

#print(model.trainable_variables, model.summary())

W = model.trainable_variables[0]
W.assign(np.array([[1.0, 0.0], [0.0, 0.0]]).T)

#input = np.array([[0,1],[2,3]])
input = np.array([[2,1]])

print(model(input))

# %%
    
x = tf.constant([-1,1,2.0])
tf.math.log(x) 

# %%
a = tf.random.categorical([[0.5,0.5]], 1)
print("a: ", a)
b = np.squeeze(a)
#b = a.numpy()
print("b: ", b)

# %%

# %% testing

import tensorflow as tf
from tensorflow import keras
import tensorflow.keras.layers as kl
import numpy as np

class Tester(keras.Model):
    def __init__(self):
        super(Tester, self).__init__()
        self.l1 = kl.Dense(10, activation=tf.nn.relu, name="input")
        self.l2 = kl.Dense(10, activation=tf.nn.softmax, name="output")
        
    def call(self, input):
        x = self.l1(input)
        return self.l2(x)
    
x_train = [0,2,4,6]
y_train = [0,4,8,9]
x_test = [1,2,5,7]
y_test = [2,4,9,9]

tester = Tester()
tester.compile(optimizer = "adam", loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
tester.fit(x_train, y_train, epochs = 500)

test_loss, test_acc = tester.evaluate(x_test, y_test, verbose=2)
predictions = tester.predict(x_test)
print('\nTest accuracy:', test_acc)

for i in range(len(x_test)):
    print(np.argmax(predictions[i]), y_test[i])
    
# %%

x = tf.constant(3.0)
with tf.GradientTape(persistent=True) as g:
  #g.watch(x)
  y = x * x
  z = y * y
print(g.watched_variables())
dz_dx = g.gradient(z, x)  # (4*x^3 at x = 3)
print(dz_dx)
dy_dx = g.gradient(y, x)
print(dy_dx)

# %% importing stuff

import tensorflow as tf
import pandas as pd
import numpy as np



# %% running stuff

print(tf.__version__, pd.__version__, np.__version__)

# %% actual code

import tensorflow as tf
from tensorflow import keras

(x_train, y_train), (x_val, y_val) = keras.datasets.fashion_mnist.load_data()

# %%

def preprocess(x, y):
  x = tf.cast(x, tf.float32) / 255.0
  y = tf.cast(y, tf.int64)

  return x, y

def create_dataset(xs, ys, n_classes=10):
  ys = tf.one_hot(ys, depth=n_classes)
  return tf.data.Dataset.from_tensor_slices((xs, ys)) \
    .map(preprocess) \
    .shuffle(len(ys)) \
    .batch(128)
    
# %% test
    
train_dataset = create_dataset(x_train, y_train)
val_dataset = create_dataset(x_val, y_val)

# %%

model = keras.Sequential([
    keras.layers.Reshape(target_shape=(28 * 28,), input_shape=(28, 28)),
    keras.layers.Dense(units=256, activation='relu'),
    keras.layers.Dense(units=192, activation='relu'),
    keras.layers.Dense(units=128, activation='relu'),
    keras.layers.Dense(units=10, activation='softmax')
])
    
# %% 
    
model.compile(optimizer='adam', 
              loss=tf.losses.CategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

history = model.fit(
    train_dataset.repeat(), 
    epochs=10, 
    steps_per_epoch=500,
    validation_data=val_dataset.repeat(), 
    validation_steps=2
) 