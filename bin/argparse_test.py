import argparse

if __name__ == '__main__':
   parser = argparse.ArgumentParser()
   parser.add_argument('--a', type = int)
   parser.add_argument('--b', dest = "nAgents")
   parser.add_argument('--c', default = 4)
   args = parser.parse_args()
   print("hello", args)

   print(args.a)
   print(args.nAgents)
   print(args.c)

   my_dict = {'a': args.a, 'b': args.nAgents, 'c': args.c}
   print(my_dict)

'''import argparse

parser = argparse.ArgumentParser(description='Test command line argparsing')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                    help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const',
                    const=sum, default=max,
                    help='sum the integers (default: find the max)')

args = parser.parse_args()
print(args.accumulate(args.integers))'''
