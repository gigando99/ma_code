# using code from https://github.com/abhisheksuran/Reinforcement_Learning/blob/master/ActorCritic.ipynb
# finally exactly what I have been looking for

# %% imports

import common.utils as utils
import tensorflow as tf
import numpy as np
import datetime
import time

# %% implementation

class Critic(tf.keras.Model):
    '''
    Inputs:
        - input_shape (tuple): shape of input (typically state shape)
    Functionality:
        - tf model (mlp) containing 2 hidden layers
    On call:
        - takes state as input
        - gives value of the given state as output
    '''
    
    def __init__(self, input_shape, output_shape, nLayers = 2, nodes_per_layer = 64, activation_fct = "relu"):
        super().__init__()
        self.nLayers = nLayers
        self.nodes_per_layer = nodes_per_layer
        self.activation_fct = activation_fct
        self.i = tf.keras.layers.Dense(units=self.nodes_per_layer, name = "critic_i") #, input_shape = input_shape
        #self.d1 = tf.keras.layers.Dense(100,activation='relu', name = "critic_hl1")
        #self.d2 = tf.keras.layers.Dense(100,activation='relu', name = "critic_hl2")
        self.hidden_layers = self.create_hidden_layers()
        self.o = tf.keras.layers.Dense(output_shape, activation = None, name = "critic_o")
        

    def call(self, input_data):
        #print("Critic: ", input_data.dtype, input_data)
        #print("critic: ", input_data.dtype)
        x = self.i(input_data)
        #print("critic2: ",x.dtype)
        for hl in self.hidden_layers:
            x = hl(x)
        #x = self.d1(x)
        #x = self.d2(x)
        o = self.o(x)
        #print(self.summary())
        return o
    
    def create_hidden_layers(self):
        
        list_of_hls = list()
        if isinstance(self.nodes_per_layer, int):
            self.nodes_per_layer = [self.nodes_per_layer for i in range(self.nLayers)]
            
        if isinstance(self.activation_fct, str):
            self.activation_fct = [self.activation_fct for i in range(self.nLayers)]
            
        for i in range(self.nLayers):
            list_of_hls.append(tf.keras.layers.Dense(self.nodes_per_layer[i],activation=self.activation_fct[i], name = "critic_hl"+str(i)))
        return list_of_hls

class Actor(tf.keras.Model):
    '''
    Inputs:
        - input_shape (tuple): shape of input (typically state shape)
        - nActions (int): number of possible actions which determines the number of nodes in the output layer
    Functionality:
        - tf model (mlp) containing 2 hidden layers
    On call:
        - takes state as input
        - gives probabilities of each action as output
    '''
    
    def __init__(self, input_shape, output_shape, all_actions, nLayers = 2, nodes_per_layer = 64, activation_fct = "relu"):
        super().__init__()
        self.nLayers = nLayers
        self.nodes_per_layer = nodes_per_layer
        self.activation_fct = activation_fct
        self.i = tf.keras.layers.Dense(units=100, name = "actor_i") #, input_shape = input_shape
        #self.d1 = tf.keras.layers.Dense(100, activation='relu', name = "actor_hl1")
        #self.d2 = tf.keras.layers.Dense(100, activation='relu', name = "actor_hl2")
        self.hidden_layers = self.create_hidden_layers()
        if all_actions:
            self.o = tf.keras.layers.Dense(output_shape, activation = "softmax", name = "actor_o")
        else:
            self.o = tf.keras.layers.Dense(output_shape, activation = None, name = "actor_o")
    
    def call(self, input_data):
        #print("Actor: ", input_data.dtype, input_data)
        #print("actor1: ", input_data.dtype)
        x = self.i(input_data)
        #print("actor2: ",x.dtype)
        #x = self.d1(x)
        #x = self.d2(x)
        for hl in self.hidden_layers:
            x = hl(x)
            #print("actor3: ",x.dtype)
        #print("before ol: ",x)
        o = self.o(x)
        #print("after ol: ",o)
        return o
    
    def create_hidden_layers(self):
        
        list_of_hls = list()
        if isinstance(self.nodes_per_layer, int):
            self.nodes_per_layer = [self.nodes_per_layer for i in range(self.nLayers)]
            
        if isinstance(self.activation_fct, str):
            self.activation_fct = [self.activation_fct for i in range(self.nLayers)]
            
        for i in range(self.nLayers):
            list_of_hls.append(tf.keras.layers.Dense(self.nodes_per_layer[i],activation=self.activation_fct[i], name = "actor_hl"+str(i)))
        return list_of_hls

# %% general setting to not always have to adapt code

class unifiedTrainer():
    '''
    Main training and running functionality
    
    Takes:
        - env (AmodEnv object)
        - setting (str): "iac", "shared_actions", "centralized_critic", "exemplary", "single_agent"
        - gamma (float): discount factor
        - lr_a (float): actor learning rate
        - lr_c (float): critic learning rate (should be large than lr_a)
        
    Relevant Functions:
        - run(): runs the program and calls all other functions
    '''
    
    def __init__(self, env, setting, gamma = 0.99, lr_a = 0.001, lr_c = 0.005, all_actions = False):
        self.env = env
        self.setting = setting
        #self.nSteps = nSteps
        #self.nEpochs = nEpochs
        self.gamma = gamma
        self.lr_a = lr_a
        self.lr_c = lr_c
        self.all_actions = all_actions
        self.a_opt = tf.keras.optimizers.Adam(learning_rate = self.lr_a)
        self.c_opt = tf.keras.optimizers.Adam(learning_rate = self.lr_c)
        self.list_of_agents = range(env.nAgents)
        self.list_of_actors = list()
        self.list_of_critics = list()
        #self.actor_shapes = actor_shapes # tuple: (input_shape_a, output_shape_a)
        #self.critic_shapes = critic_shapes # tuple: (input_shape_c, output_shape_c)
    
    def _create_agents(self):
        '''
        Creates necessary networks for the immanent setting:
            - single_agent: 1xC, 1xA
            - iac: 1xC, 1xA per agent
            - shared_actions: 1xC, 1xA per agent
            - exemplary: 1xC, 1xA
            - centralized_critic: 1xA per agent, 1xC
        '''
        
        state_length = self.env.state_len
        print(state_length)
            
        if self.setting == "iac":
            
            # input: state, output: one node per possible action
            actor_shapes = ((state_length,), self.env.nActions)
            
            # input: state, output: one node per possible action
            critic_shapes = ((state_length,), self.env.nActions)
            
            for _ in self.list_of_agents:
                self.list_of_actors.append(Actor(actor_shapes[0], 
                                                 actor_shapes[1], 
                                                 all_actions = self.all_actions))
                
                self.list_of_critics.append(Critic(critic_shapes[0], 
                                                   critic_shapes[1]))
                
                
        elif self.setting == "shared_actions":
            
            # input: state, output: one node per possible action
            actor_shapes = ((state_length,), self.env.nActions)
            
            # input: state + action of every other agent, output: one node per possible action
            critic_shapes = ((state_length + self.env.nAgents - 1,), self.env.nActions)
            
            for _ in self.list_of_agents:
                self.list_of_actors.append(Actor(actor_shapes[0], 
                                                 actor_shapes[1], 
                                                 all_actions = self.all_actions))
                
                self.list_of_critics.append(Critic(critic_shapes[0], 
                                                   critic_shapes[1]))
    
        elif self.setting == "centralized_critic":
            
            # input: state, output: one node per possible action
            actor_shapes = ((state_length,), self.env.nActions)
            
            # input: (batch_size = nAgents, state + action of every other agent + agent id), output: one node per possible action
            critic_shapes = ((self.env.nAgents, state_length + self.env.nAgents - 1 + 1,), self.env.nActions)
            
            for _ in self.list_of_agents:
                self.list_of_actors.append(Actor(actor_shapes[0], 
                                                 actor_shapes[1], 
                                                 all_actions = self.all_actions))
                
            self.list_of_critics.append(Critic(critic_shapes[0], 
                                               critic_shapes[1]))
            
        elif self.setting == "exemplary":
            
            # input: (batch_size = nAgents, state + agent id), output: one node per possible action
            actor_shapes = ((self.env.nAgents, state_length + 1,), self.env.nActions)
            
            # input: (batch_size = nAgents, state + action of every other agent + agent id), output: one node per possible action
            critic_shapes = ((self.env.nAgents, state_length + self.env.nAgents - 1 + 1,), self.env.nActions)
            
            self.list_of_actors.append(Actor(actor_shapes[0], 
                                             actor_shapes[1], 
                                             all_actions = self.all_actions))
            
            self.list_of_critics.append(Critic(critic_shapes[0], 
                                               critic_shapes[1]))
            
        elif self.setting == "single_agent":
            
            # input: state, output: one node per possible action combination
            actor_shapes = ((state_length,), self.env.nActions ** self.env.nAgents)
            
            # input: state, output: one node per possible action combination
            critic_shapes = ((state_length,), self.env.nActions ** self.env.nAgents)
            
            self.list_of_actors.append(Actor(actor_shapes[0], 
                                             actor_shapes[1], 
                                             all_actions = self.all_actions))
            
            self.list_of_critics.append(Critic(critic_shapes[0], 
                                               critic_shapes[1]))
            
            
    
    
    def choose_action(self, actor, preprocessed_state):
        '''
        Given actor network performs fwd pass with given state
        
        Returns:
            - action (int)
        '''

        logits = actor(preprocessed_state)
        #print(logits)
        action = tf.random.categorical(tf.math.log(logits), 1) 
        
        return int(np.squeeze(action))

    
    def actor_loss(self, logits, action, all_qs): 
        
        if self.all_actions:
            epsilon = tf.constant([0.000000001])
            #probs = tf.nn.softmax(tf.math.log(logits))
            log_probs = tf.math.log(logits + epsilon)
            #log_probs = tf.math.log(logits)
            if np.max(logits) == 1:
                print("problem", time.time())
            #print("probs", logits)
            loss = - tf.reduce_sum(logits * log_probs * all_qs, [1])
            
        else:
            depth = logits.get_shape().as_list()[-1]
            action_onehot = tf.one_hot(action, depth)
            neglog = - tf.nn.softmax_cross_entropy_with_logits(logits = logits, labels = action_onehot)
            q = all_qs * action_onehot
            loss = tf.reduce_mean(q * neglog)    
            #print("softmax result: {}, aloss: {}".format(neglog,loss))

        #print("aloss: ", loss)
        return loss
    
    
    def critic_loss(self, td):
        # mse or huberloss
        return tf.reduce_mean(td ** 2)
    
    #@tf.function    
    def _critic_learn(self, index, preprocessed_state, preprocessed_next_state, action, next_action, reward):
        
        with tf.GradientTape() as tape2: 
            all_qs =  self.list_of_critics[index](preprocessed_state, training=True)
            all_qns = self.list_of_critics[index](preprocessed_next_state, training=True)
            depth = all_qs.get_shape().as_list()[-1]
            q = all_qs * tf.one_hot(action, depth)
            qn = all_qns * tf.one_hot(next_action, depth)
            td = reward + self.gamma * qn - q
            c_loss = self.critic_loss(td)
            
        grads2 = tape2.gradient(c_loss, self.list_of_critics[index].trainable_variables)
            
        if self.setting == "exemplary" or self.setting == "centralized_critic":
            return grads2, c_loss, all_qs
        
        else:   
            self.c_opt.apply_gradients(zip(grads2, self.list_of_critics[index].trainable_variables))
            return c_loss, all_qs
    
    #@tf.function
    def _actor_learn(self, index, preprocessed_state, all_qs, action):

        with tf.GradientTape() as tape1:

            logits = self.list_of_actors[index](preprocessed_state, training=True)
            #logits = tf.math.log(logits)
            a_loss = self.actor_loss(logits, action, all_qs)
                  
        grads1 = tape1.gradient(a_loss, self.list_of_actors[index].trainable_variables)
        
        if self.all_actions:
            #print("test: ", grads1.shape())
            grads1 = [tf.clip_by_value(grad, -200., 200.) for grad in grads1]
        
        if self.setting == "exemplary":
            return grads1, a_loss
        
        else:   
            self.a_opt.apply_gradients(zip(grads1, self.list_of_actors[index].trainable_variables))
              
            return a_loss
    
    def learn(self, preprocessed_state, preprocessed_next_state, list_of_rewards, list_of_actions, list_of_next_actions):
        
        aloss_metric = tf.keras.metrics.Mean()
        closs_metric = tf.keras.metrics.Mean()
        #print(list_of_actions)
        
        if self.setting == "iac":
            # each agent updates parameters with own experience
            for agent in self.list_of_agents:
                c_loss, q = self._critic_learn(agent, 
                                               preprocessed_state, 
                                               preprocessed_next_state, 
                                               list_of_actions[agent], 
                                               list_of_next_actions[agent], 
                                               list_of_rewards[agent])
                
                a_loss = self._actor_learn(agent, 
                                           preprocessed_state, 
                                           q, 
                                           list_of_actions[agent])
                
                aloss_metric(a_loss)
                closs_metric(c_loss)
            
        elif self.setting == "shared_actions":
            # update parameters of all agents sharing actions in critic update
            # does not this also mean that the actor learns with all actions (partly)?
            #single_a = self.transform_to_single_action(list_of_actions, self.env.nActions)
            #single_an = self.transform_to_single_action(list_of_next_actions, self.env.nActions)
            #r = sum(list_of_rewards)
            for agent in self.list_of_agents:
                
                agent_preprocessed_state = self.get_action_expanded_state(preprocessed_state, 
                                                                          list_of_actions, 
                                                                          agent)
                
                agent_preprocessed_next_state = self.get_action_expanded_state(preprocessed_next_state, 
                                                                               list_of_next_actions, 
                                                                               agent)
                
                c_loss, q = self._critic_learn(agent, 
                                               agent_preprocessed_state, 
                                               agent_preprocessed_next_state,
                                               list_of_actions[agent], 
                                               list_of_next_actions[agent], 
                                               list_of_rewards[agent])
                
                a_loss = self._actor_learn(agent, 
                                           preprocessed_state, 
                                           q, 
                                           list_of_actions[agent])
                aloss_metric(a_loss)
                closs_metric(c_loss)
                
        
        elif self.setting == "centralized_critic":
            
            #single_a = self.transform_to_single_action(list_of_actions, self.env.nActions)
            #single_an = self.transform_to_single_action(list_of_next_actions, self.env.nActions)
            
            default_agent = 0
            list_of_c_grads = list()
            
            for agent in self.list_of_agents:
                
                agent_preprocessed_state = self.get_action_expanded_state(preprocessed_state, 
                                                                          list_of_actions, 
                                                                          agent,
                                                                          add_agent = True)
                
                agent_preprocessed_next_state = self.get_action_expanded_state(preprocessed_next_state, 
                                                                               list_of_next_actions, 
                                                                               agent,
                                                                               add_agent = True)
                
                c_grads, c_loss, all_qs = self._critic_learn(default_agent, 
                                                        agent_preprocessed_state, 
                                                        agent_preprocessed_next_state, 
                                                        list_of_actions[agent], 
                                                        list_of_next_actions[agent], 
                                                        list_of_rewards[agent])
                list_of_c_grads.append(c_grads)
                closs_metric(c_loss)
                
                a_loss = self._actor_learn(agent, 
                                           preprocessed_state, 
                                           all_qs, 
                                           list_of_actions[agent])
                aloss_metric(a_loss)
                
            mean_c_grads = self.accumulate_gradients(list_of_c_grads)
            self.c_opt.apply_gradients(zip(mean_c_grads, self.list_of_critics[default_agent].trainable_variables))
                
        
        elif self.setting == "exemplary":
            # synchronously update the two networks with all experiences
            # pretty sure i should not work with single actions here...
            
            #single_a = self.transform_to_single_action(list_of_actions, self.env.nActions)
            #single_an = self.transform_to_single_action(list_of_next_actions, self.env.nActions)
            #r = sum(list_of_rewards)
            #c_loss, q = critic.learn(state, next_state, single_action, next_single_action)
            
            default_agent = 0
            list_of_a_grads = list()
            list_of_c_grads = list()
            
            for agent in self.list_of_agents:
                
                actor_preprocessed_state = tf.concat([preprocessed_state, [[agent]]],1)
                
                critic_preprocessed_state = self.get_action_expanded_state(preprocessed_state, 
                                                                           list_of_actions, 
                                                                           agent,
                                                                           add_agent = True)
                critic_preprocessed_next_state = self.get_action_expanded_state(preprocessed_next_state, 
                                                                                list_of_actions, 
                                                                                agent,
                                                                                add_agent = True)
                
                c_grads, c_loss, all_qs = self._critic_learn(default_agent, 
                                                        critic_preprocessed_state, 
                                                        critic_preprocessed_next_state, 
                                                        list_of_actions[agent], 
                                                        list_of_next_actions[agent], 
                                                        list_of_rewards[agent])
                
                a_grads, a_loss = self._actor_learn(default_agent, 
                                                    actor_preprocessed_state, 
                                                    all_qs, 
                                                    list_of_actions[agent])
                
                #print("new agent",len(grads),agent, grads)
                list_of_c_grads.append(c_grads)
                list_of_a_grads.append(a_grads)
                aloss_metric(a_loss)
                closs_metric(c_loss)
                
            # accumulate gradients            
            mean_c_grads = self.accumulate_gradients(list_of_c_grads)
            mean_a_grads = self.accumulate_gradients(list_of_a_grads)
            
            self.c_opt.apply_gradients(zip(mean_c_grads, self.list_of_critics[default_agent].trainable_variables))
            self.a_opt.apply_gradients(zip(mean_a_grads, self.list_of_actors[default_agent].trainable_variables))
            
            
            
                
        elif self.setting == "single_agent":
            # update parameters of single agent based on combined experience
            single_a = self.transform_to_single_action(list_of_actions, self.env.nActions)
            single_an = self.transform_to_single_action(list_of_next_actions, self.env.nActions)
            sum_of_rewards = sum(list_of_rewards)
            
            default_agent = 0
            c_loss, q = self._critic_learn(default_agent, 
                                           preprocessed_state, 
                                           preprocessed_next_state, 
                                           single_a, 
                                           single_an, 
                                           sum_of_rewards)
            
            a_loss = self._actor_learn(default_agent, 
                                       preprocessed_state, 
                                       q, 
                                       single_a)
            
            aloss_metric(a_loss)
            closs_metric(c_loss)
        
        
        return aloss_metric.result(), closs_metric.result()
    
    
    def accumulate_gradients(self, list_of_grads):
        mean_grads = list()
        for i in range(len(list_of_grads[0])):
                y = tf.stack([x[i] for x in list_of_grads])
                mean_grads.append(tf.reduce_mean(y,0))
        return mean_grads
        
    
    
    def get_list_of_actions(self, preprocessed_state):
        
        list_of_actions = list()
            
        if self.setting == "single_agent":
            single_action = self.choose_action(self.list_of_actors[0], preprocessed_state)
            list_of_actions = self.transform_to_separate_actions(single_action, self.env.nActions, self.env.nAgents)
        
        elif self.setting == "iac" or self.setting == "shared_actions" or self.setting == "centralized_critic":
            for agent_id in self.list_of_agents:
                list_of_actions.append(self.choose_action(self.list_of_actors[agent_id], preprocessed_state))
        
        elif self.setting == "exemplary":
            for agent_id in self.list_of_agents:
                list_of_actions.append(self.choose_action(self.list_of_actors[0], tf.concat([preprocessed_state, [[agent_id]]],1)))                  
            
        return list_of_actions
    
    
    def run(self, nEpochs, nEpisodes,  nSteps = 86400, mode = "train", **kwargs):
        
        self._create_agents()
        
        previous_time = time.time()
        time_list = [previous_time]
        epoch = 0
        early_stopping = False
        
        current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        #a_log_dir = 'logs/tb/' + current_time + 'actor'
        #a_summary_writer = tf.summary.create_file_writer(a_log_dir)
        #c_log_dir = 'logs/tb/' + current_time + 'critic'
        #c_summary_writer = tf.summary.create_file_writer(c_log_dir)
        log_dir = 'logs/tb/' + current_time
        summary_writer = tf.summary.create_file_writer(log_dir)
        
        while epoch < nEpochs and not early_stopping:
            print("-------- starting epoch {} --------".format(epoch))
            
            episode = 0
            while (episode < nEpisodes) and (episode < len(self.env.dict_of_list_of_data_days[mode])):
                print("-------- starting episode {} --------".format(episode))
                
                # define metrics and logs
                if mode == "train":
                    #all_aloss = list()
                    #all_closs = list()
                    #all_rewards = list()
                    #total_reward = 0
                    #all_actions = list()
                
                    # tensorboard
                    tb_aloss = tf.keras.metrics.Mean('actor_loss', dtype=tf.float32)
                    tb_closs = tf.keras.metrics.Mean('critic_loss', dtype=tf.float32)
                    
                    # show tensors as well? https://stackoverflow.com/questions/48851439/how-to-display-a-list-of-all-gradients-in-tensorboard
                    
                tb_reward = tf.keras.metrics.Sum('reward', dtype=tf.float32)
                tb_envTime = tf.keras.metrics.Mean('env time', dtype=tf.float32)
                tb_fwdTime = tf.keras.metrics.Mean('env time', dtype=tf.float32)
                tb_stepTime = tf.keras.metrics.Mean('step time', dtype=tf.float32)
                tb_learningTime = tf.keras.metrics.Mean('learning time', dtype=tf.float32)
                
                #current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
                #a_log_dir = 'logs/tb/' + current_time + 'actor'
                #a_summary_writer = tf.summary.create_file_writer(a_log_dir)
                #c_log_dir = 'logs/tb/' + current_time + 'critic'
                #c_summary_writer = tf.summary.create_file_writer(c_log_dir)
                #log_dir = 'logs/tb/' + current_time
                #summary_writer = tf.summary.create_file_writer(log_dir)
                
                # load day into env
                data_day = self.env.dict_of_list_of_data_days[mode][episode]
                print("chosen day: ", data_day.strftime("%Y-%m-%d"))
                picklestarttime = time.time()
                self.env.pick_data_by_day(data_day)
                pickle_time = round(time.time() - picklestarttime,4)
                #print("time to load pickle: ", pickle_time)
                
                if self.setting == "nh":
                    state = self.env.reset(transformed=False)
                    fwdstarttime = time.time()
                    list_of_actions = self.nh_choose_actions(state)
                    fwd_time = round(time.time() - fwdstarttime,4)
                    tb_fwdTime(fwd_time)
                else:
                    # choose actions
                    state = self.env.reset(transformed=True)
                    preprocessed_state = self.prep_input(state)
                    #preprocessed_state = tf.dtypes.cast(preprocessed_state, tf.float32)
                    #print("run: ", preprocessed_state.dtype)
                    fwdstarttime = time.time()
                    list_of_actions = self.get_list_of_actions(preprocessed_state)
                    #print("test1")
                    fwd_time = round(time.time() - fwdstarttime,4)
                    tb_fwdTime(fwd_time)
                
                done = False
                
                while state[0] < nSteps and not done:
                    
                    stepstarttime = time.time()
                    
                    if self.setting == "nh":
                        envstarttime = time.time()
                        next_state, list_of_rewards, done = self.env.step(list_of_actions, transformed = False)
                        env_time = round(time.time() - envstarttime,4)
                        tb_envTime(env_time)
                        fwdstarttime = time.time()
                        list_of_next_actions = self.nh_choose_actions(next_state)
                        fwd_time = round(time.time() - fwdstarttime,4)
                        tb_fwdTime(fwd_time)
                    else:
                        #print("run {}, step {}: chosen actions: {}".format(self.setting, state[0], list_of_actions))
                        envstarttime = time.time()
                        next_state, list_of_rewards, done = self.env.step(list_of_actions, transformed = True)
                        env_time = round(time.time() - envstarttime,4)
                        tb_envTime(env_time)
                        # choose next actions
                        preprocessed_next_state = self.prep_input(next_state)
                        #preprocessed_next_state = tf.dtypes.cast(preprocessed_state, tf.float32)
                        fwdstarttime = time.time()
                        #print("test2")
                        list_of_next_actions = self.get_list_of_actions(preprocessed_next_state)
                        #print("test3")
                        fwd_time = round(time.time() - fwdstarttime,4)
                        tb_fwdTime(fwd_time)
                    
                    
                    if mode == "train":
                        # adapt parameters
                        start_learning_time = time.time()
                        a_loss, c_loss = self.learn(self.prep_input(state), 
                                                    self.prep_input(next_state),
                                                    list_of_rewards,
                                                    list_of_actions, 
                                                    list_of_next_actions)
                        learning_time = round(time.time() - start_learning_time,4)
                        #print("time to adapt params: ", learning_time)
                        tb_learningTime(learning_time)
                        tb_aloss(a_loss)
                        tb_closs(c_loss)
                        
                        #all_actions.append(list_of_actions)
                        #all_aloss.append(a_loss)
                        #all_closs.append(c_loss)
                        #all_rewards.append(reward)
                        
                    state = next_state
                    list_of_actions = list_of_next_actions
                    #total_reward += sum(reward)
                    tb_reward(sum(list_of_rewards))
                    step_time = round(time.time() - stepstarttime,4)
                    tb_stepTime(step_time)
                    
                #print("total reward for day {} of epoch {} is {}".format(day, epoch, total_reward))
                #print(all_actions)
                
                #with a_summary_writer.as_default():
                #    tf.summary.scalar('loss', tb_aloss.result(), step=0)
                #with c_summary_writer.as_default():
                #    tf.summary.scalar('loss', tb_closs.result(), step=0)
                #asdf = self.list_of_actors[0](self.prep_input(state))
                
                with summary_writer.as_default():
                    tf.summary.scalar('reward', tb_reward.result(), step=epoch)
                    if mode == "train":
                        tf.summary.scalar('aloss', tb_aloss.result(), step=epoch)
                        tf.summary.scalar('closs', tb_closs.result(), step=epoch)
                        
                current_time = time.time()
                diff_time = current_time - previous_time
                time_list.append(diff_time)
                previous_time = sum(time_list)
                    
                if mode == "train":
                    # print output
                    template = 'Epoch {} -- Day {} -- Setting {}\naLoss: {}\ncLoss: {}\nReward: {}\n'
                    print(template.format(episode, epoch, self.setting,
                                          tb_aloss.result(), 
                                          tb_closs.result(),
                                          tb_reward.result()))
                
                    tb_aloss.reset_states()
                    tb_closs.reset_states()                    
                    
                
                else:   
                    template = 'Epoch {} -- Day {} -- Setting {}\nReward: {}\n'
                    print(template.format(episode, epoch, self.setting,
                                          tb_reward.result()))
                    tb_reward.reset_states()
                    
                print("Total Elapsed Time: ", round(diff_time,4))
                print("-- of which pickle : ", pickle_time)
                print("Step time : ", np.round(tb_stepTime.result().numpy(),4))
                print("-- of which env.step : ", np.round(tb_envTime.result().numpy(),4))
                print("-- of which fwd : ", np.round(tb_fwdTime.result().numpy(),4))
                print("-- of which learning : ", np.round(tb_learningTime.result().numpy(),4))
                print("\n")
                
                # Reset metrics every epoch (episode so far)
                tb_reward.reset_states()
                tb_learningTime.reset_states()
                tb_stepTime.reset_states()
                
                episode += 1
            epoch += 1
        
        #return all_aloss, all_closs, all_rewards
    
    def transform_help(self, single_action, counter, nActions, nAgents):
    
        a = int(single_action / (nActions ** (nAgents - counter)))
        #print(counter, a, single_action, (nActions ** (nAgents - counter)))
        single_action -= a * (nActions ** (nAgents - counter))
        
        return single_action, a
        
    def transform_to_separate_actions(self, single_action, nActions, nAgents):
        list_of_actions = list()
        for counter in range(1, nAgents + 1):
            single_action, a = self.transform_help(single_action, counter, nActions, nAgents)
            list_of_actions.append(a)
            
        return list_of_actions
    
    def transform_to_single_action(self, actions, nActions):
        single_action = 0
        counter = 0
        for action in reversed(actions):
            single_action += action * (nActions ** counter)
            counter += 1
            
        return single_action
    
    def prep_input(self, input):
        input = tf.expand_dims(input, 0)
        input = tf.cast(input, tf.float32)
        #input = input[:,tf.newaxis].astype("float32")
        #print(input.dtype())
        return input
    
    def get_action_expanded_state(self, state, list_of_actions, agent, add_agent = False):
        copy_of_loa = list_of_actions[:]
        del copy_of_loa[agent]
        
        if add_agent:
            return tf.concat([state, [copy_of_loa], [[agent]]],1)
        else:
            return tf.concat([state, [copy_of_loa]],1)

    def nh_choose_actions(self, state):
        
        #time = state[0]
        rq = state[1]
        loc = state[2]
        aq = state[3]
        actions = [self.env.no_op for i in range(self.env.nAgents)]

        # check if elements in rq
        if rq:

            dict_ttj = dict()

            # for each request
            for request in rq:

                # skip this request if time to origin has already been looked at
                if request[0] in dict_ttj.keys():
                    continue
                else:
                    dict_ttj[request[0]] = list()

                    # calculate agent who is closest
                    for agent in range(self.env.nAgents):
                        
                        time_to_job = 0
                        job_counter = 0
                        # add time already in queue
                        # check for too many jobs in parallel
                        for action in range(len(aq[agent])):
                            if aq[agent][action][4] == 2:
                                job_counter += 1
                            time_to_job += aq[agent][action][2]
                        
                        if job_counter >= 1: 
                            continue

                        # add time from last destination to new origin
                        if len(aq[agent]) != 0:
                            last_d = aq[agent][-1][1]
                        else:
                            last_d = loc[agent][1]
                        new_o = request[0]
                        #o = self.data.boroughs.index(last_d)
                        #d = self.data.boroughs.index(new_o)
                        o = last_d
                        d = new_o
                        time_to_job += self.env.data.travel_stats[o, d, 0]

                        # save ttj per agent
                        dict_ttj[request[0]].append(time_to_job)

            # choose agent to assign request to
            av_agents = list(range(self.env.nAgents))
            unav_agents = list()

            counter = 0
            for request in rq:
                if av_agents:
                    all_agents = dict_ttj[request[0]] # list of ttj per agent

                    cc = 0
                    new_all = list()
                    for temp in all_agents:
                        if cc in av_agents: # only appen agents' time if they are still available
                            new_all.append(temp)
                        cc += 1
                    #all = new_all

                    #print(state[0], new_all)
                    min_ttj = min(new_all)

                    if min_ttj <= request[4]: # only necessary if job can be reached
                        index = all_agents.index(min_ttj)
                        #init_index = index
                        
                        counter3 = 0
                        while index in unav_agents:   
                        #if index in unav_agents:
                            index2 = all_agents[index+1:].index(min_ttj)
                            index += index2 + 1
                            counter3 += 1
                        #if index != init_index:   
                            #print("truly all: ", av_agents, all_agents)
                            #print("after removing unav: ", unav_agents, new_all)
                            #print("found free agent after %s iterations. Initial agent: %s, now: %s. " %(counter3, index, index2))

                        # each agent can only take one job
                        '''addition = 0
                        for n in unav_agents:
                            if n <= index:
                                addition += 1
                        print(index, av_agents, unav_agents)
                        index += addition
                        print(index)'''
                        #print(index)
                        av_agents.remove(index)
                        unav_agents.append(index)
                        actions[index] = self.env.data.nBoroughs + counter
                        counter += 1

                    else: # no agent can reach the request before a timeout occurs
                        counter += 1
                        #self.env.request_queue.remove(request)
                        continue
                else: # all agents have already selected a job
                    continue

        return actions

    
    '''
    def choose_action(self, preprocessed_state):

        logits = self.actor(preprocessed_state)
        # probs should be unnormalized acc. to tf, therefore no activation in last actor layer
        #print(logits)
        action_tensor = tf.random.categorical(tf.math.log(logits), 1) 
        
        return int(np.squeeze(action_tensor))

    def get_value(self, preprocessed_state, action):
        
        all_qs = self.critic(preprocessed_state)
        depth = all_qs.get_shape().as_list()[-1]
        q = all_qs * tf.one_hot(action, depth)

        return q
    '''
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

# %%
class a2cAgent():
    def __init__(self, input_shape_a, input_shape_c, output_shape_a, output_shape_c, gamma = 0.99, lr_a = 0.001, lr_c = 0.005):
        self.gamma = gamma
        self.a_opt = tf.keras.optimizers.Adam(learning_rate = lr_a)
        self.c_opt = tf.keras.optimizers.Adam(learning_rate = lr_c)
        self.actor = Actor(input_shape_a, output_shape_a)
        self.critic = Critic(input_shape_c, output_shape_c)
        #self.log_prob = None
    
    def choose_action(self, state):
        state = self.prep_input(state)
        logits = self.actor(state)
        # probs should be unnormalized acc. to tf, therefore no activation in last actor layer
        #print(logits)
        action = tf.random.categorical(tf.math.log(logits), 1) 
        
        return int(np.squeeze(action))

    def actor_loss(self, logits, action, q):
        depth = logits.get_shape().as_list()[-1]
        #print("calculating actor loss: ", depth, action)
        action_onehot = tf.one_hot(action, depth)
        #print("calculating actor loss: ", action_onehot)
        neglog = tf.nn.softmax_cross_entropy_with_logits(logits = logits, labels = action_onehot)
        # produces the same neglog: 
        # neglog = tf.nn.sparse_softmax_cross_entropy_with_logits(logits = logits, labels = [action])
        # do i need a minus here to perform gradient ascent? i dont think so, but check with tobias
        loss = - tf.reduce_mean(q * neglog)
        
        # first tries
        #log_prob = tf.math.log(prob)
        #loss = - log_prob * td
        #loss = -tf.math.reduce_mean(log_prob * td)
        
        # CS285 for policy gradient:
        # neglog = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=action)
        # loss = tf.math.reduce_mean(neglog)
        
        # baselineAI:
        # ad = rewards - values
        # neglog = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=action)
        # entropy = (some calculations on the logits)
        # critic_loss = tf.reduce_mean(tf.square(value - rewards))
        # actor_loss = tf.reduce_mean(ad * neglog)
        # loss = actor_loss - entropy * self.ent_coef + critic_loss * self.vf_coef

        return loss
    
    def critic_loss(self, td):
        # mse or huberloss
        return tf.reduce_mean(td ** 2)
    
    #@tf.function
    def learn(self, state, next_state, reward, setting = "iac", action = None, next_action = None, single_action = None, next_single_action=None):
        state = self.prep_input(state)
        next_state = self.prep_input(next_state)
        
        with tf.GradientTape() as tape1, tf.GradientTape() as tape2:
            logits = self.actor(state, training=True) 
            v =  self.critic(state, training=True)
            vn = self.critic(next_state, training=True)
            depth = v.get_shape().as_list()[-1]
            
            if setting == "iac":
                
                q = v * tf.one_hot(action, depth)
                qn = vn * tf.one_hot(next_action, depth)
                td = reward + self.gamma * qn - q
                #td = reward + self.gamma * vn - v
                a_loss = self.actor_loss(logits, action, q)
                c_loss = self.critic_loss(td)
                
            elif setting == "single_agent":
                q = v * tf.one_hot(single_action, depth)
                qn = vn * tf.one_hot(next_single_action, depth)
                td = reward + self.gamma * qn - q
                #td = reward + self.gamma * vn - v
                a_loss = self.actor_loss(logits, single_action, q)
                c_loss = self.critic_loss(td)
                
            elif setting == "shared_actions":
                q = v * tf.one_hot(single_action, depth)
                qn = vn * tf.one_hot(next_single_action, depth)
                td = reward + self.gamma * qn - q
                #td = reward + self.gamma * vn - v
                a_loss = self.actor_loss(logits, action, q)
                c_loss = self.critic_loss(td)
            
        #print("watched variables 1: ", tape1.watched_variables())
        #print("watched variables 2: ", tape2.watched_variables())
        #print(a_loss, c_loss)
        grads1 = tape1.gradient(a_loss, self.actor.trainable_variables)
        grads2 = tape2.gradient(c_loss, self.critic.trainable_variables)
        self.a_opt.apply_gradients(zip(grads1, self.actor.trainable_variables))
        self.c_opt.apply_gradients(zip(grads2, self.critic.trainable_variables))
        
        return a_loss, c_loss
    
    def prep_input(self, input):
        input = tf.expand_dims(input, 0)
        #tf.cast(input, tf.float32)
        return input


# %% creating own objects

#env = utils.prep_pipeline()
#env.pick_data()

# %%

def run_iac(env, nSteps = 1000):
    
    list_of_agents = list()
    for _ in range(env.nAgents):
        list_of_agents.append(a2cAgent(input_shape_a = (279,), input_shape_c = (279,), nActions = env.nActions))

    #agent1 = agent(input_shape_a = (279,), input_shape_c = (279,), nActions = env.no_op)
    #agent2 = agent(input_shape_a = (279,), input_shape_c = (279,), nActions = env.no_op)
    #steps = 100
    #for s in range(steps):
    all_aloss = []
    all_closs = []
    all_rewards = []
      
    done = False
    state = env.reset(transformed=True)
    total_reward = 0
    
    # choose actions
    list_of_actions = list()
    for agent in list_of_agents:
        list_of_actions.append(agent.choose_action(state))
    
    while state[0] < nSteps: #not done:
            
        print("run_iac(): chosen actions: ", list_of_actions)
        next_state, reward, done = env.step(list_of_actions, transformed = True) 
        
        # choose next actions
        list_of_next_actions = list()
        for agent in list_of_agents:
            list_of_next_actions.append(agent.choose_action(next_state))
        
        index = 0
        # adapt parameters
        for agent in list_of_agents:
            a_loss, c_loss = agent.learn(state, next_state, list_of_actions[index], list_of_next_actions[index], reward[index])
            index += 1
        
        all_aloss.append(a_loss)
        all_closs.append(c_loss)
        all_rewards.append(reward)
        print("    - Current time: {}, {}% finished".format(state[0],state[0]/nSteps*100))
        state = next_state
        list_of_actions = list_of_next_actions
        total_reward += sum(reward)
        #print("aloss: {}, closs: {}, reward: {}".format(a_loss, c_loss, sum(reward)))
        
    print("total reward is {}".format(total_reward))
    
    return all_aloss, all_closs, all_rewards
          
#run_iac(env)

# %% TENSORBOARD

# run this in command line: 
# (ma) C:\Users\lgdem\gitws\ma_code>tensorboard --logdir logs/tb
'''   
def tensorboard_fct():
    
    # Define metrics
    #train_loss = tf.keras.metrics.Mean('train_loss', dtype=tf.float32)
    #train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('train_accuracy')
    #test_loss = tf.keras.metrics.Mean('test_loss', dtype=tf.float32)
    #test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('test_accuracy')
    
    #create file writer
    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    #train_log_dir = 'logs/tb/' + current_time + '/train'
    train_log_dir = 'logs/tb/' + current_time
    #test_log_dir = 'logs/gradient_tape/' + current_time + '/test'
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    #test_summary_writer = tf.summary.create_file_writer(test_log_dir)
    
    # run model and save metrics
    # should be called within step
    #train_loss(loss)
    #train_accuracy(y_train, predictions)
    #test_loss(loss)
    #test_accuracy(y_test, predictions)
    
    with train_summary_writer.as_default():
        tf.summary.scalar('loss', train_loss.result(), step=epoch)
        tf.summary.scalar('accuracy', train_accuracy.result(), step=epoch)
    with test_summary_writer.as_default():
        tf.summary.scalar('loss', test_loss.result(), step=epoch)
        tf.summary.scalar('accuracy', test_accuracy.result(), step=epoch)
    
    # print output
    template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
    print (template.format(epoch+1,
                     train_loss.result(), 
                     train_accuracy.result()*100,
                     test_loss.result(), 
                     test_accuracy.result()*100))
    
    # Reset metrics every epoch
    train_loss.reset_states()
    test_loss.reset_states()
    train_accuracy.reset_states()
    test_accuracy.reset_states()
'''    
# %% tests
    
#env = utils.prep_pipeline()

# %% single agent

def transform_help(single_action, counter, nActions, nAgents):
    
    a = int(single_action / (nActions ** (nAgents - counter)))
    #print(counter, a, single_action, (nActions ** (nAgents - counter)))
    single_action -= a * (nActions ** (nAgents - counter))
    
    return single_action, a
    
def transform_to_separate_actions(single_action, nActions, nAgents):
    list_of_actions = list()
    for counter in range(1, nAgents + 1):
        single_action, a = transform_help(single_action, counter, nActions, nAgents)
        list_of_actions.append(a)
        
    return list_of_actions

def transform_to_single_action(actions, nActions):
    single_action = 0
    counter = 0
    for action in reversed(actions):
        single_action += action * (nActions ** counter)
        counter += 1
        
    return single_action

def run_single_agent(env, nSteps = 1000):
    
    single_agent = a2cAgent(input_shape_a = (279,), input_shape_c = (279,), output_shape_a = env.nActions ** env.nAgents, output_shape_c = env.nActions ** env.nAgents)
    done = False
    state = env.reset(transformed=True)
    total_reward = 0
    
    # choose actions
    a = single_agent.choose_action(state)
    list_of_actions = transform_to_separate_actions(a, env.nActions, env.nAgents)
      
    while state[0] < nSteps: #not done:
        
        print("run_single_agent(): chosen actions: ", list_of_actions)
        next_state, reward, done = env.step(list_of_actions, transformed = True)
        
        # choose next actions
        next_a = single_agent.choose_action(next_state)
        list_of_next_actions = transform_to_separate_actions(next_a, env.nActions, env.nAgents)
        
        # adapt parameters
        reward = sum(reward)
        a_loss, c_loss = single_agent.learn(state, next_state, a, next_a, reward)
        
        #all_aloss.append(a_loss)
        #all_closs.append(c_loss)
        state = next_state
        list_of_actions = list_of_next_actions
        total_reward += reward
        
    print("total reward is {}".format(total_reward))
    

#x = transform_to_separate_actions(56,53,2)
#print(transform_to_single_action(x,53),x)