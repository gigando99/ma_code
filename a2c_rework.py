# much is copied from a2c.py

# %% imports

import common.utils as utils
import tensorflow as tf
import numpy as np
import datetime
import time
import os
import pickle

from tensorboard.plugins.hparams import api as hp
import matplotlib.pyplot as plt

# %% Models

def create_hidden_layers(nodes_per_layer, nLayers, activation_fct, name):

        list_of_hls = list()

        if isinstance(nodes_per_layer, int):
            nodes_per_layer = [nodes_per_layer for i in range(nLayers)]

        if isinstance(activation_fct, str):
            activation_fct = [activation_fct for i in range(nLayers)]

        for i in range(nLayers):
            list_of_hls.append(tf.keras.layers.Dense(nodes_per_layer[i],activation=activation_fct[i], name = name + "_hl"+str(i)))

        return list_of_hls

class Critic(tf.keras.Model):
    '''
    Inputs:
        - input_shape (tuple): shape of input (typically state shape)
    Functionality:
        - tf model (mlp) containing 2 hidden layers
    On call:
        - takes state as input
        - gives value of the given state as output
    '''

    def __init__(self, input_shape, output_shape, nLayers, nodes_per_layer, activation_fct):
        super().__init__()
        self.nLayers = nLayers
        self.nodes_per_layer = nodes_per_layer
        self.activation_fct = activation_fct
        self.i = tf.keras.layers.Dense(units=self.nodes_per_layer, input_shape = input_shape, name = "critic_i") #, input_shape = input_shape
        self.hidden_layers = create_hidden_layers(self.nodes_per_layer, self.nLayers, self.activation_fct, "critic")
        self.o = tf.keras.layers.Dense(output_shape, activation = None, name = "critic_o")

    #@tf.function # seems to slow down performance
    def call(self, input_data):
        x = self.i(input_data)
        for hl in self.hidden_layers:
            x = hl(x)
        o = self.o(x)

        return o

class Actor(tf.keras.Model):
    '''
    Inputs:
        - input_shape (tuple): shape of input (typically state shape)
        - nActions (int): number of possible actions which determines the number of nodes in the output layer
    Functionality:
        - tf model (mlp) containing 2 hidden layers
    On call:
        - takes state as input
        - gives probabilities of each action as output
    '''

    def __init__(self, input_shape, output_shape, all_actions, nLayers, nodes_per_layer, activation_fct):
        super().__init__()
        self.nLayers = nLayers
        self.nodes_per_layer = nodes_per_layer
        self.activation_fct = activation_fct
        self.i = tf.keras.layers.Dense(units=100, name = "actor_i") #, input_shape = input_shape
        self.hidden_layers = self.hidden_layers = create_hidden_layers(self.nodes_per_layer, self.nLayers, self.activation_fct, "actor")
        self.o = tf.keras.layers.Dense(output_shape, activation = "softmax", name = "actor_o")

    #@tf.function # seems to slow down performance
    def call(self, input_data):
        x = self.i(input_data)
        for hl in self.hidden_layers:
            x = hl(x)
        o = self.o(x)

        return o

# %% a2c instance

class unifiedTrainer():
    '''
    Main training and running functionality

    Takes:
        - env (AmodEnv object)
        - setting (str): "iac", "shared_actions", "centralized_critic", "exemplary", "single_agent"
        - lr_a (float): actor learning rate
        - all_actions (bool): indicates if all actions are used for learning or just the chosen one
        - n_random_start_episodes (int): number of initial episodes with completely randomly chosen actions
        - entropy (float): entropy coefficient

    Relevant Functions:
        - run_final(): runs the program and calls all other functions
    '''

    def __init__(self, env, setting, lr_a = 0.001, all_actions = False, n_random_start_episodes = 3, entropy = 1):

        self.env = env
        self.setting = setting

        # regarding the models
        self.lr_a = lr_a
        self.lr_c_factor = 5 # critic learns faster than actor
        self.lr_c = self.lr_a * self.lr_c_factor
        self.all_actions = all_actions
        self.a_opt = tf.keras.optimizers.Adam(learning_rate = self.lr_a)
        self.c_opt = tf.keras.optimizers.Adam(learning_rate = self.lr_c)
        self.list_of_a_opts = list()
        self.list_of_c_opts = list()
        self.nLayers = 2
        self.nodes_per_layer = 64
        self.activation_fct = "relu"

        self.gamma = tf.constant(0.999)

        self.epsilon = self.epsilon_scheduler

        self.list_of_agents = range(env.nAgents)
        self.list_of_actors = list()
        self.list_of_critics = list()

        if self.setting in ["iac", "shared_actions", "centralized_critic"]:

            self.alearn = dict()
            for i in range(env.nAgents):
                self.alearn[i] = self.alearn_wrapper()

            if self.setting in ["iac", "shared_actions"]:
                self.clearn = dict()
                for i in range(env.nAgents):
                    self.clearn[i] = self.clearn_wrapper()

        if setting not in ["nh", "random"]:
            self._create_agents()

        self.hparams = None
        self.global_train_episode_counter = tf.constant(0, dtype = tf.int32)
        self.n_random_start_episodes = n_random_start_episodes
        self.env.logger.activate_tf_stats(len(self.list_of_actors),len(self.list_of_critics))
        self.writer = None

        self.entropy_coef = entropy #0.05

    def __repr__(self):
        return "A2C object: unifiedTrainer(lr_a = {}, lr_c = {}, setting = {}), nActors: {}, nCritics: {}".format(self.lr_a, self.lr_c, self.setting, len(self.list_of_actors), len(self.list_of_critics))

    def _create_agents(self):
        '''
        Creates necessary networks for the immanent setting:
            - single_agent: 1xC, 1xA
            - iac: 1xC, 1xA per agent
            - shared_actions: 1xC, 1xA per agent
            - exemplary: 1xC, 1xA
            - centralized_critic: 1xA per agent, 1xC
        '''

        state_length = self.env.state_len

        if self.setting == "iac":

            # input: state, output: one node per possible action
            actor_shapes = ((1,state_length,), self.env.nActions)
            nActors = self.env.nAgents

            # input: state, output: one node per possible action
            critic_shapes = ((1,state_length,), self.env.nActions)
            nCritics = self.env.nAgents

        if self.setting == "shared_actions":

            # input: state, output: one node per possible action
            actor_shapes = ((1,state_length,), self.env.nActions)
            nActors = self.env.nAgents

            # input: state + action of every other agent, output: one node per possible action
            critic_shapes = ((1, state_length + self.env.nAgents - 1), self.env.nActions)
            nCritics = self.env.nAgents

        if self.setting == "centralized_critic":

            # input: state, output: one node per possible action
            actor_shapes = ((1,state_length,), self.env.nActions)
            nActors = self.env.nAgents

            # input: (batch_size = nAgents, state + action of every other agent + agent id), output: one node per possible action
            critic_shapes = ((1,self.env.nAgents, state_length + self.env.nAgents - 1 + 1,), self.env.nActions)
            nCritics = 1

        if self.setting == "exemplary":

            # input: (batch_size = nAgents, state + agent id), output: one node per possible action
            actor_shapes = ((self.env.nAgents, state_length + 1,), self.env.nActions)
            nActors = 1

            # input: (batch_size = nAgents, state + action of every other agent + agent id), output: one node per possible action
            critic_shapes = ((self.env.nAgents, state_length + self.env.nAgents - 1 + 1,), self.env.nActions)
            nCritics = 1

        if self.setting == "single_agent":

            # input: state, output: one node per possible action combination
            actor_shapes = ((1,state_length,), self.env.nActions ** self.env.nAgents)
            nActors = 1

            # input: state, output: one node per possible action combination
            critic_shapes = ((1,state_length,), self.env.nActions ** self.env.nAgents)
            nCritics = 1

        for i in range(nActors):

            self.list_of_actors.append(Actor(actor_shapes[0],
                                             actor_shapes[1],
                                             all_actions = self.all_actions,
                                             nLayers = self.nLayers,
                                             nodes_per_layer = self.nodes_per_layer,
                                             activation_fct = self.activation_fct))

            self.list_of_a_opts.append(tf.keras.optimizers.Adam(learning_rate = self.lr_a))
            self.list_of_actors[i].build(input_shape = actor_shapes[0])

        for i in range(nCritics):

            self.list_of_critics.append(Critic(critic_shapes[0],
                                               critic_shapes[1],
                                               nLayers = self.nLayers,
                                               nodes_per_layer = self.nodes_per_layer,
                                               activation_fct = self.activation_fct))

            self.list_of_c_opts.append(tf.keras.optimizers.Adam(learning_rate = self.lr_c))
            self.list_of_critics[i].build(input_shape = critic_shapes[0])


    @tf.function
    def choose_action(self, actor, preprocessed_state, ep_counter, postprocess, step, episode, **kwargs):
        '''
        Given actor network performs fwd pass with given state

        Returns:
            - action (int)
        '''

        output = actor(preprocessed_state)

        # will be True in train mode
        if postprocess:
            output = self.postprocess_actor_output(output, step, episode)

        # random.categorical expects log input
        logits = tf.math.log(output)

        if self.setting == "exemplary":
            logits = tf.squeeze(logits)

        # choose random actions if within prespecified random start episodes (train mode only)
        if ep_counter < self.n_random_start_episodes and postprocess:
            eq_logits = tf.zeros(tf.shape(logits))
            if "seed" in kwargs.keys():
                action = tf.random.categorical(eq_logits, 1, seed=kwargs["seed"])
            else:
                action = tf.random.categorical(eq_logits, 1)

        else:
            if "seed" in kwargs.keys():
                action = tf.random.categorical(logits, 1, seed=kwargs["seed"])
            else:
                action = tf.random.categorical(logits, 1)

        return action

    @tf.function
    def postprocess_actor_output(self, output, step, episode):
        """
            applies Foerster2018's epsilon decay
        """
        epsilon = self.epsilon(tf.cast(step, tf.int32), tf.cast(episode,tf.int32)) #somehow need this casting even though both are already tf.int32 because otherwise dtype error in multiplication
        logits = (tf.constant(1.) - epsilon) * output + epsilon / tf.constant(self.env.nActions, dtype = tf.float32)
        return logits

    @tf.function
    def epsilon_scheduler(self, step, episode):
        '''
            implements Foerster2018's epsilon decay
        '''
        nSteps = tf.constant(86400)
        decline = 15 * nSteps
        ub_minus_lb = tf.constant(0.5 - 0.02)

        a = tf.math.add(tf.math.multiply(episode, nSteps), step)
        c = tf.cast(tf.math.divide(a, decline), tf.float32)

        y = tf.constant(0.5) - tf.math.multiply(c, ub_minus_lb)
        x = tf.math.maximum(y, tf.constant(0.02))

        return x

    @tf.function
    def actor_loss(self, action_probs, action, all_qs):
        '''
        depth = action_probs.get_shape().as_list()[-1]
        action_onehot = tf.one_hot(tf.cast(action, tf.int32), depth)
        loss = tf.nn.softmax_cross_entropy_with_logits(labels = action_onehot, logits = action_probs)
        '''
        log_probs = tf.math.log(action_probs)

        if self.all_actions:
            loss = - tf.reduce_sum(action_probs * log_probs * all_qs)

        else:
            depth = action_probs.get_shape().as_list()[-1]
            action_onehot = tf.one_hot(tf.cast(action, tf.int32), depth)
            loss = - tf.reduce_sum(action_onehot * log_probs * all_qs)

        if self.entropy_coef:
            entropy = - tf.reduce_sum(log_probs * action_probs)
            return loss - self.entropy_coef * entropy

        else:
            return loss

    @tf.function
    def critic_loss(self, td):
        # mse or huberloss
        return tf.reduce_mean(td ** 2)

    @tf.function
    def get_list_of_actions(self, state, postprocess, step, episode):
        '''
            - Calls choose_action
            - Returns list_of_actions
        '''

        if self.setting == "nh":
            list_of_actions = tf.constant(self.nh_choose_actions(state))
        elif self.setting == "random":
            list_of_actions = tf.constant(self.random_choose_actions())

        else:
            # tensorflow models, need preprocessed input
            list_of_actions = list()
            preprocessed_state = self._prep_input_norm(state, a_or_c = "actor")

            if self.setting == "single_agent":
                single_action = self.choose_action(self.list_of_actors[0],
                                                   preprocessed_state,
                                                   ep_counter = self.global_train_episode_counter,
                                                   postprocess = postprocess,
                                                   step = step,
                                                   episode = episode)

                list_of_actions = tf.squeeze(tf.convert_to_tensor(self.transform_to_separate_actions(int(single_action),
                                                                     self.env.nActions,
                                                                     self.env.nAgents)))
                '''list_of_actions = self.transform_to_separate_actions(int(single_action),
                                                                     self.env.nActions,
                                                                     self.env.nAgents)'''

            elif self.setting == "iac" or self.setting == "shared_actions" or self.setting == "centralized_critic":

                list_of_actions = tf.TensorArray(dtype=tf.int64, size=0, dynamic_size=True)
                for agent_id in self.list_of_agents:
                    list_of_actions = list_of_actions.write(agent_id, self.choose_action(self.list_of_actors[agent_id],
                                                              preprocessed_state,
                                                              ep_counter = self.global_train_episode_counter,
                                                              postprocess = postprocess,
                                                              step = step,
                                                              episode = episode,
                                                              seed = agent_id))

                list_of_actions = tf.squeeze(list_of_actions.stack())

            elif self.setting == "exemplary":
                default_agent = 0

                list_of_actions = tf.squeeze(self.choose_action(self.list_of_actors[default_agent],
                                                                preprocessed_state,
                                                                ep_counter = self.global_train_episode_counter,
                                                                postprocess = postprocess,
                                                                step = step,
                                                                episode = episode))

        return list_of_actions

    def get_data_set_fraction(self, mode, sample_size, epoch):
        if mode == "train":
            len_of_dataset = len(self.env.dict_of_list_of_data_days["train"])
            nEpochs = int(len_of_dataset / sample_size)
            return np.array_split(self.env.dict_of_list_of_data_days["train"],nEpochs)[epoch]

        elif mode == "test":
            return self.env.dict_of_list_of_data_days["test"][0:sample_size]

        else:
            return np.random.choice(self.env.dict_of_list_of_data_days[mode], size = sample_size, replace = False)

    def run_final(self, nEpisodes_per_mode, nEpochs = 1, nSteps = 86400, save_models = True, **kwargs):

        # initialize list of modes
        #modes = ["train", "validate", "test"]
        modes = [key for key,val in nEpisodes_per_mode.items() if val > 0]

        runstarttime = time.time()
        current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

        HP_LRA = hp.HParam('lr_a', hp.Discrete([0.005, 0.001, 0.0005, 0.0001, 0.00005, 0.00001, 0.000005]))
        HP_NZONES = hp.HParam('nZones', hp.Discrete([5, 8, 13, 19, 26, 38, 63]))
        HP_NAGENTS = hp.HParam('nAgents', hp.Discrete([5, 10, 20]))
        HP_SETTING = hp.HParam('setting', hp.Discrete(["iac", "shared_actions", "centralized_critic", "exemplary", "single_agent"]))
        HP_REWARD_SETTING = hp.HParam("reward_setting", hp.Discrete([0.0, 0.2, 0.4, 0.6, 0.8, 1.0]))
        HP_ALL_ACTIONS = hp.HParam("all_actions", hp.Discrete([True, False]))

        self.hparams = {HP_LRA: self.lr_a, HP_NZONES: self.env.nBoroughs, HP_NAGENTS: self.env.nAgents, HP_SETTING: self.setting, HP_REWARD_SETTING: self.env.reward_param, HP_ALL_ACTIONS: self.all_actions}

        id = "{}-lr{}-r{}-{}".format(self.setting[0:3],self.lr_a,self.env.reward_param,current_time)
        stats_path = "results/" + id

        print("\n\nStarted running: " + self.setting + "-" + current_time)
        print("HYPERPARAMETERS:\n",{h.name: self.hparams[h] for h in self.hparams},"\n")

        log_dir = 'results/tb/' + id

        self.writer = tf.summary.create_file_writer(log_dir)
        with self.writer.as_default():
            hp.hparams_config(
                hparams=list(self.hparams.keys()),
                metrics=[hp.Metric("reward", display_name='reward')])

        epoch = 0
        while epoch < nEpochs:
            print("-------- starting epoch {} --------".format(epoch))
            epoch_starttime = time.time()

            for mode in modes:
                if mode != "test":

                    data_set = self.get_data_set_fraction(mode = mode, sample_size = nEpisodes_per_mode[mode], epoch = epoch)
                    self.run_epoch(nSteps = nSteps, nEpisodes = nEpisodes_per_mode[mode], epoch = epoch, mode = mode, data_set = data_set) #summary_writer = dict_of_writers[mode]

            epoch_time = round(time.time() - epoch_starttime,4)
            avg_reward = self.env.logger.postprocess_epoch(epoch_time)

            with self.writer.as_default():
                hp.hparams(self.hparams)
                tf.summary.scalar("reward", avg_reward, step=epoch)

            #if self.check_stopping_criterion():
            #    break

            epoch += 1

        print("Finished training after {}s. ".format(round(time.time() - runstarttime,4)))
        print("Average Epoch time: ", np.round(self.env.logger.dict_of_temp_metrics["epoch_time"].result().numpy(),4))

        if save_models == True:
            self.save_models(current_time, epoch, id)

        if "test" in modes:
            teststarttime = time.time()
            mode = "test"
            data_set = self.get_data_set_fraction(mode = mode, sample_size = nEpisodes_per_mode[mode], epoch = epoch)
            self.run_epoch(nSteps = nSteps, nEpisodes = nEpisodes_per_mode[mode], epoch = epoch, mode = mode, data_set = data_set)
            print("Finished final evaluation after {}s. ".format(round(time.time() - teststarttime,4)))

        with open(stats_path + ".pkl", 'wb') as f:
            pickle.dump(self.env.logger.dict_of_final_metrics, f)

        print("Finished execution after {}s. ".format(round(time.time() - runstarttime,4)))

    def check_stopping_criterion(self):
        """
            Implements stopping criterion
        """
        cutoff = 15
        counter = -1
        previous = self.env.logger.dict_of_final_metrics["validate-action_distribution"][-1]

        for elem in reversed(self.env.logger.dict_of_final_metrics["validate-action_distribution"]):
            if np.array_equal(elem,previous) and counter < 15:
                counter += 1
                previous = elem
            else:
                break

        if counter == 15:
            print("Action distribution of last 15 validation runs was identical. Stopping early".format(counter))
            return True
        else:
            print("Action distribution of last {} validation runs was identical. Continuing learning.".format(counter))
            return False


    def save_models(self, current_time, epoch, id):
        savingtime = time.time()
        model_dir = "saved_models/" + id
        os.mkdir(model_dir)
        # save model every epoch just in case
        if self.setting not in ["nh", "random"]:
            for n_c, critic in enumerate(self.list_of_critics):
                critic.save(model_dir + "/epoch{}-critic{}".format(epoch, n_c))
            for n_a, actor in enumerate(self.list_of_actors):
                actor.save(model_dir + "/epoch{}-actor{}".format(epoch, n_a))
            print("Time to save models: {}s".format(round(time.time() - savingtime, 4)))

    def run_epoch(self, nSteps, nEpisodes, epoch, mode, data_set):

        if mode == "train":
            postprocess = tf.constant(True)
        else:
            postprocess = tf.constant(False)

        episode = 0
        while (episode < nEpisodes):

            episode_starttime = time.time()

            # get day
            data_day = data_set[episode]
            print("\n-------- starting episode {} (day: {}) of epoch {}, mode: {} --------".format(episode, data_day.strftime("%Y-%m-%d"), epoch, mode))

            # load day into env
            self.env.pick_data_by_day(data_day)

            # initialize state and get first actions
            if self.setting in ["nh", "random"]:
                state = self.env.reset(transformed=False)
            else:
                state = self.env.reset(transformed=True)

            list_of_actions = self.get_list_of_actions(state, postprocess,
                                                        step = tf.constant(round(state[0] * 86400), dtype = tf.int32),
                                                        episode = self.global_train_episode_counter)

            done = False
            counter = 0
            while state[0] < nSteps / self.env.terminal_time and not done:

                env_list_of_actions = list_of_actions.numpy().tolist()

                if self.setting in ["nh", "random"]:
                    next_state, list_of_rewards, done = self.env.step(env_list_of_actions, transformed = False)
                else:
                    next_state, list_of_rewards, done = self.env.step(env_list_of_actions, transformed = True)

                list_of_next_actions = self.get_list_of_actions(next_state, postprocess,
                                                                tf.constant(round(state[0] * 86400) + self.env.step_interval, dtype = tf.int32),
                                                                episode = self.global_train_episode_counter)

                if mode == "train" and self.setting not in ["nh", "random"]:

                    list_of_rewards = tf.constant(list_of_rewards, dtype = tf.float32)
                    actor_inputs = self._prep_input_norm(state, a_or_c = "actor")
                    critic_inputs = self._prep_input_norm(state, a_or_c = "critic", list_of_actions = list_of_actions)
                    critic_next_inputs = self._prep_input_norm(state, a_or_c = "critic", list_of_actions = list_of_next_actions)

                    self.learn(actor_inputs,
                               critic_inputs,
                               critic_next_inputs,
                               list_of_rewards,
                               list_of_actions,
                               list_of_next_actions,
                               tf.constant(round(state[0] * 86400), dtype = tf.int32),
                               self.global_train_episode_counter)

                for agent in self.list_of_agents:
                    self.env.logger.dict_of_temp_metrics["reward_per_agent"][counter] = list_of_rewards

                list_of_actions = list_of_next_actions
                state = next_state
                counter += 1

            # save episode stats:
            episode_time = round(time.time() - episode_starttime,4)
            episode_reward = self.env.logger.save_episode_stats(episode_time, mode, nSteps)
            self.env.logger.print_episode_stats(nSteps, episode_time, episode_reward, mode)

            episode += 1

            if mode == "train":
                self.global_train_episode_counter = tf.add(self.global_train_episode_counter,1)

    @tf.function
    def _critic_learn(self, index, preprocessed_state, preprocessed_next_state, action, next_action, reward):

        with tf.GradientTape() as tape2:
            all_qs =  tf.squeeze(self.list_of_critics[index](preprocessed_state, training=True))
            all_qns = tf.squeeze(self.list_of_critics[index](preprocessed_next_state, training=True))
            depth = all_qs.get_shape().as_list()[-1]

            q = tf.reduce_sum(all_qs * tf.one_hot(tf.cast(action, tf.int32), depth),axis = -1)
            qn = tf.reduce_sum(all_qns * tf.one_hot(tf.cast(next_action, tf.int32), depth),axis = -1)

            td = reward + self.gamma * qn - q
            c_loss = self.critic_loss(td)

        grads2 = tape2.gradient(c_loss, self.list_of_critics[index].trainable_variables)

        self.list_of_c_opts[index].apply_gradients(zip(grads2, self.list_of_critics[index].trainable_variables))

        return c_loss, all_qs

    @tf.function
    def _actor_learn(self, index, preprocessed_state, all_qs, action, step, episode):

        with tf.GradientTape() as tape1:
            logits = self.list_of_actors[index](preprocessed_state, training=True)
            logits = self.postprocess_actor_output(logits, step, episode)
            a_loss = self.actor_loss(logits, action, all_qs)

        grads1 = tape1.gradient(a_loss, self.list_of_actors[index].trainable_variables)

        self.list_of_a_opts[index].apply_gradients(zip(grads1, self.list_of_actors[index].trainable_variables))

        return a_loss

    def clearn_wrapper(self):
        @tf.function
        def critic_learn_wrapped(self, critic, copt, preprocessed_state, preprocessed_next_state, action, next_action, reward):
            with tf.GradientTape() as tape2:

                all_qs =  tf.squeeze(critic(preprocessed_state, training=True))
                all_qns = tf.squeeze(critic(preprocessed_next_state, training=True))
                depth = all_qs.get_shape().as_list()[-1]

                q = tf.reduce_sum(all_qs * tf.one_hot(tf.cast(action, tf.int32), depth),axis = -1)
                qn = tf.reduce_sum(all_qns * tf.one_hot(tf.cast(next_action, tf.int32), depth),axis = -1)
                td = reward + self.gamma * qn - q
                c_loss = self.critic_loss(td)

            grads2 = tape2.gradient(c_loss, critic.trainable_variables)
            copt.apply_gradients(zip(grads2, critic.trainable_variables))

            return c_loss, all_qs
        return critic_learn_wrapped

    def alearn_wrapper(self):
        @tf.function
        def actor_learn_wrapped(self, actor, aopt, preprocessed_state, all_qs, action, step, episode):

            with tf.GradientTape() as tape1:

                logits = actor(preprocessed_state, training=True)
                logits = self.postprocess_actor_output(logits, step, episode)
                a_loss = self.actor_loss(logits, action, all_qs)

            grads1 = tape1.gradient(a_loss, actor.trainable_variables)

            aopt.apply_gradients(zip(grads1, actor.trainable_variables))

            return a_loss
        return actor_learn_wrapped

    @tf.function
    def learn(self, actor_inputs, critic_inputs, critic_next_inputs, list_of_rewards, list_of_actions, list_of_next_actions, step, episode):

        # each agent updates parameters with own experience
        # note: actor_inputs = critic_inputs
        if self.setting == "iac":

            for agent in self.list_of_agents:
                c_loss, q = getattr(self,"clearn")[agent](self, self.list_of_critics[agent], self.list_of_c_opts[agent],
                                                                critic_inputs,
                                                                critic_next_inputs,
                                                                list_of_actions[agent],
                                                                list_of_next_actions[agent],
                                                                list_of_rewards[agent])

                a_loss = getattr(self, "alearn")[agent](self, self.list_of_actors[agent], self.list_of_a_opts[agent],
                                                               critic_inputs,
                                                               q,
                                                               list_of_actions[agent],
                                                               step,
                                                               episode)

                self.env.logger.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)
                self.env.logger.dict_of_temp_metrics["closs-critic"+str(agent)](c_loss)

        # update parameters of all agents sharing actions in critic update
        elif self.setting == "shared_actions":

            for agent in self.list_of_agents:

                c_loss, q = getattr(self,"clearn")[agent](self, self.list_of_critics[agent], self.list_of_c_opts[agent],
                                                                critic_inputs[agent],
                                                                critic_next_inputs[agent],
                                                                list_of_actions[agent],
                                                                list_of_next_actions[agent],
                                                                list_of_rewards[agent])

                a_loss = getattr(self, "alearn")[agent](self, self.list_of_actors[agent], self.list_of_a_opts[agent],
                                                               actor_inputs,
                                                               q,
                                                               list_of_actions[agent],
                                                               step,
                                                               episode)

                self.env.logger.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)
                self.env.logger.dict_of_temp_metrics["closs-critic"+str(agent)](c_loss)

        # train one centralized critic which learns from batched input of all agents
        elif self.setting == "centralized_critic":

            default_agent = 0

            c_loss, q = self._critic_learn(default_agent,
                                            critic_inputs,
                                            critic_next_inputs,
                                            list_of_actions,
                                            list_of_next_actions,
                                            list_of_rewards)

            self.env.logger.dict_of_temp_metrics["closs-critic0"](c_loss)

            for agent in self.list_of_agents:
                a_loss = getattr(self, "alearn")[agent](self, self.list_of_actors[agent], self.list_of_a_opts[agent],
                                                               actor_inputs,
                                                               q,
                                                               list_of_actions[agent],
                                                               step,
                                                               episode)

                self.env.logger.dict_of_temp_metrics["aloss-actor"+str(agent)](a_loss)

        # synchronously update the two networks with all experiences
        elif self.setting == "exemplary":

            default_agent = 0

            c_loss, all_qs = self._critic_learn(default_agent,
                                                critic_inputs,
                                                critic_next_inputs,
                                                list_of_actions,
                                                list_of_next_actions,
                                                list_of_rewards)

            a_loss = self._actor_learn(default_agent,
                                       actor_inputs,
                                       all_qs,
                                       list_of_actions,
                                       step,
                                       episode)

            self.env.logger.dict_of_temp_metrics["aloss-actor0"](a_loss)
            self.env.logger.dict_of_temp_metrics["closs-critic0"](c_loss)

        # update parameters of single agent based on combined experience
        # note: actor inputs and critic inputs are the same
        elif self.setting == "single_agent":

            default_agent = 0
            single_a = self.transform_to_single_action(list_of_actions, self.env.nActions)
            single_an = self.transform_to_single_action(list_of_next_actions, self.env.nActions)
            sum_of_rewards = tf.reduce_sum(list_of_rewards)

            c_loss, q = self._critic_learn(default_agent,
                                           critic_inputs,
                                           critic_next_inputs,
                                           single_a,
                                           single_an,
                                           sum_of_rewards)

            a_loss = self._actor_learn(default_agent,
                                       critic_inputs,
                                       q,
                                       single_a,
                                       step,
                                       episode)

            self.env.logger.dict_of_temp_metrics["aloss-actor0"](a_loss)
            self.env.logger.dict_of_temp_metrics["closs-critic0"](c_loss)

        return 0

    def first_step_norm(self, state):
        state = tf.expand_dims(state, 0)
        state = tf.cast(state, tf.float32)
        return state

    @tf.function
    def normalization_fct(self, state):
        #return tf.keras.utils.normalize(state)
        return state / np.max(self.env.data.travel_stats)

    @tf.function
    def _prep_input_norm(self, state, a_or_c, **kwargs):

        state = self.first_step_norm(state)

        if self.setting in ["iac", "single_agent"]:
            normed_state = self.normalization_fct(state)
            return normed_state

        elif self.setting == "shared_actions":
            if a_or_c == "actor":
                normed_state = self.normalization_fct(state)
                return normed_state

            elif a_or_c == "critic":
                add_agent = tf.constant(False)
                normed_inputs = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
                list_of_actions = kwargs["list_of_actions"]
                for agent in self.list_of_agents:
                    input = self.get_action_expanded_state(state, list_of_actions, agent, add_agent = add_agent)
                    normed_inputs = normed_inputs.write(agent, self.normalization_fct(input))
                stacked_normed_inputs = normed_inputs.stack()
                return stacked_normed_inputs

        elif self.setting == "centralized_critic":
            if a_or_c == "actor":
                normed_state = self.normalization_fct(state)
                return normed_state

            elif a_or_c == "critic":
                add_agent = tf.constant(True) # only difference to "shared_actions"
                normed_inputs = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
                list_of_actions = kwargs["list_of_actions"]
                for agent in self.list_of_agents:
                    input = self.get_action_expanded_state(state, list_of_actions, agent, add_agent = add_agent)
                    normed_inputs = normed_inputs.write(agent, self.normalization_fct(input))
                stacked_normed_inputs = normed_inputs.stack()
                return stacked_normed_inputs

        elif self.setting == "exemplary":
            normed_inputs = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
            if a_or_c == "actor":
                for agent in self.list_of_agents:
                    extended_state = tf.concat([state, [[agent]]],1)
                    normed_inputs = normed_inputs.write(agent, self.normalization_fct(extended_state))
                stacked_normed_inputs = normed_inputs.stack()
                return stacked_normed_inputs

            elif a_or_c == "critic":
                list_of_actions = kwargs["list_of_actions"]
                for agent in self.list_of_agents:
                    input = self.get_action_expanded_state(state, list_of_actions, tf.constant(agent), add_agent = tf.constant(True))
                    normed_inputs = normed_inputs.write(agent, self.normalization_fct(input))
                stacked_normed_inputs = normed_inputs.stack()
                return stacked_normed_inputs

    @tf.function
    def get_action_expanded_state(self, state, list_of_actions, agent, add_agent = False):

        copy_of_loa1 = list_of_actions[:agent]
        copy_of_loa2 = list_of_actions[agent+1:]
        copy_of_loa = tf.concat([copy_of_loa1,copy_of_loa2],0)

        if add_agent:
            return tf.concat([state, [copy_of_loa], [[agent]]],1)
        else:
            return tf.concat([state, [copy_of_loa]],1)

    def transform_help(self, single_action, counter, nActions, nAgents):

        a = int(single_action / (nActions ** (nAgents - counter)))
        single_action -= a * (nActions ** (nAgents - counter))

        return single_action, a

    def transform_to_separate_actions(self, single_action, nActions, nAgents):
        list_of_actions = list()
        for counter in range(1, nAgents + 1):
            single_action, a = self.transform_help(single_action, counter, nActions, nAgents)
            list_of_actions.append(a)

        return list_of_actions

    def transform_to_single_action(self, actions, nActions):
        single_action = 0
        counter = actions.shape[0]
        for x in tf.range(counter):
            action = actions[x]
            single_action += action * (nActions ** counter)
            counter -= 1

        return single_action

    #def _adjust_lrs_from_setting(self):
    #
    #    if self.setting == "centralized_critic":
    #        self.lr_c = self.lr_c / self.env.nAgents
    #
    #    elif self.setting == "exemplary":
    #        self.lr_c = self.lr_c / self.env.nAgents
    #        self.lr_a = self.lr_a / self.env.nAgents

    def random_choose_actions(self):
        actions = np.random.choice(range(self.env.no_op + 1), self.env.nAgents)
        return actions

    def nh_choose_actions(self, state):

        rq = state[1]
        loc = state[2]
        aq = state[3]
        actions = [self.env.no_op for i in self.list_of_agents]

        # check if elements in rq
        if rq:

            # saves time to job for all jobs and agents
            dict_ttj = self.nh_calculate_dict_ttj(rq, loc, aq)

            av_agents = list(range(self.env.nAgents))
            unav_agents = list()

            for counter, request in enumerate(rq):
                if av_agents:
                    all_agents = dict_ttj[request[0]] # list of ttj per agent

                    #new_all will contain correct ttj of all available agents, sufficiently large number otherwise
                    new_all = list()
                    for cc, temp in enumerate(all_agents):
                        if cc in av_agents: # correct time if they are still available
                            new_all.append(temp)
                        else: # large number otherwise
                            new_all.append(self.env.patience_timeout + 1)

                    min_ttj = min(new_all)

                    # job assignment only necessary if job can be reached
                    #if min_ttj <= request[4]: can do it anymore with reduced state. not necessary anyway
                    if min_ttj <= self.env.patience_timeout:
                        # first
                        #index = new_all.index(min_ttj)
                        # random
                        index = np.random.choice(np.flatnonzero(new_all == min_ttj))

                        av_agents.remove(index)
                        unav_agents.append(index)
                        actions[index] = self.env.data.nBoroughs + counter

                    else: # no agent can reach the request before a timeout occurs
                        continue

                else: # all agents have already selected a job
                    continue

        return actions

    def nh_calculate_dict_ttj(self, rq, loc, aq):

        dict_ttj = dict()

        # for each request
        for request in rq:

            # skip this request if time to origin has already been calculated
            if request[0] in dict_ttj.keys():
                continue

            else:
                dict_ttj[request[0]] = list()

                # calculate agent who is closest
                for agent in self.list_of_agents:

                    time_to_job = 0
                    job_counter = 0
                    # add time already in queue
                    # check for too many jobs in parallel
                    for action in range(len(aq[agent])):
                        if aq[agent][action][4] == 2:
                            job_counter += 1
                        time_to_job += aq[agent][action][2]

                    if job_counter >= 1:
                        time_to_job = self.env.patience_timeout + 1

                    # add time from last destination to new origin
                    if len(aq[agent]) != 0:
                        last_d = aq[agent][-1][1]
                    else:
                        last_d = loc[agent][1]

                    new_o = request[0]
                    o = last_d
                    d = new_o
                    time_to_job += self.env.data.travel_stats[o, d, 0]

                    # save ttj per agent
                    dict_ttj[request[0]].append(time_to_job)

        return dict_ttj
